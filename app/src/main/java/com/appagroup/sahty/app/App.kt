package com.appagroup.sahty.app

import android.app.Activity
import android.app.Application
import com.google.firebase.FirebaseApp
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()

        instance = this

        FirebaseApp.initializeApp(this)

//        registerActivityLifecycleCallbacks(ApplicationLifecycleHandler())

//        if (LeakCanary.isInAnalyzerProcess(this)) {
//             //This process is dedicated to LeakCanary for heap analysis.
//             //You should not init your app in this process.
//            return
//        }
//        LeakCanary.install(this)
    }

    @Inject
    override fun activityInjector(): AndroidInjector<Activity> = activityInjector

    companion object {
        @get:Synchronized
        var instance: App? = null
            private set
    }
}
