package com.appagroup.sahty.ui.post_story

import android.util.Log
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class PostStoryPresenter(view: PostStoryContract.View) : BasePresenter<PostStoryContract.View>(view), PostStoryContract.Presenter {

    private val TAG = "PostStoryPresenter"

    override fun postStory(enTitle: RequestBody, enContent: RequestBody, arTitle: RequestBody,
                           arContent: RequestBody, categoryId: RequestBody, image: MultipartBody.Part) {
        view.showProgressDialog()
        subscribe(dataManager.postStory(enTitle, enContent, arTitle, arContent, categoryId, image),
                object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "postStory -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                DisposableManager.addArticlesDisposable(d)
                Log.d(TAG, "postStory -> onSubscribe")
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "postStory -> onNext -> code = ${t.code()}")
                Log.d(TAG, "postStory -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "postStory -> onNext -> status = ${t.body()!!.status}")
                    Log.d(TAG, "postStory -> onNext -> message = ${t.body()!!.message}")
                    view.storyPostedSuccessfully()
                } else
                    view.showError(R.string.something_wrong)
                view.hideProgressDialog()
            }

            override fun onError(e: Throwable) {
                view.showError(R.string.something_wrong)
                view.hideProgressDialog()
            }
        })
    }

    override fun getUserName(): String =
            dataManager.getUserName()

    override fun getUserImage(): String =
            dataManager.getUserImage()

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeArticles()
    }
}