package com.appagroup.sahty.ui.messages.patient_groups.group_content

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.MedicalFile

interface GroupContentContract {

    interface View : IBaseView {

        fun setListeners()

        fun changeProgressBarVisibility(visibility: Int)

        fun updateFiles(files: MutableList<MedicalFile>)
    }

    interface Presenter : IBasePresenter<View> {

        fun getMedicalFiles(patientId: Long, groupId: Long)
    }
}