package com.appagroup.sahty.ui.medical_files

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.MedicalFileGroup
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.ItemLongClickListener
import com.appagroup.sahty.ui.listener.OnClick
import kotlinx.android.synthetic.main.item_medical_file_group.view.*

class MedicalFileGroupAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_ADD_GROUP = 0
    private val TYPE_FILE_GROUP = 1
    private var itemHeight: Int = 400
    private val groups: MutableList<MedicalFileGroup> = mutableListOf()
    private lateinit var addFileGroupClickListener: OnClick
    private lateinit var fileGroupClickListener: ItemClickListener<MedicalFileGroup>
    private lateinit var fileGroupLongClickListener: ItemLongClickListener<Long>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ADD_GROUP -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_add_medical_file_group, parent, false)
                AddMedicalFileGroupHolder(view)
            }
            TYPE_FILE_GROUP -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_medical_file_group, parent, false)
                MedicalFileGroupHolder(view)
            }
            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1)
            TYPE_ADD_GROUP
        else TYPE_FILE_GROUP
    }

    override fun getItemCount(): Int =
        if (groups.size == 0)
            0
        else
            groups.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val params = holder.itemView.layoutParams
        params.height = itemHeight
        holder.itemView.layoutParams = params

        when (holder) {
            is MedicalFileGroupHolder -> {
                holder.bind(groups[position])
                holder.itemView.setOnClickListener {
                    fileGroupClickListener.onClick(groups[position])
                }
                holder.itemView.setOnLongClickListener {
                    fileGroupLongClickListener.onLongClick(groups[position].id, holder.itemView.file_group_title)
                    true
                }
            }

            is AddMedicalFileGroupHolder ->
                holder.itemView.setOnClickListener {
                    addFileGroupClickListener.onClick()
                }
        }
    }

    fun updateItemHeight(itemHeight: Int) {
        this.itemHeight = itemHeight
    }

    fun updateMedicalFileGroups(groups: MutableList<MedicalFileGroup>) {
        this.groups.addAll(groups)
        notifyDataSetChanged()
    }

    fun addMedicalFileGroup(medicalFileGroup: MedicalFileGroup) {
        this.groups.add(medicalFileGroup)
        notifyItemInserted(groups.size-1)
    }

    fun removeSelectedMedicalFileGroups(groupId: Long) {
        for (position in 0 until groups.size) {
            if (groups[position].id == groupId) {
                this.groups.removeAt(position)
                notifyDataSetChanged()
                break
            }
        }
    }

    fun setOnAddFileGroupClickListener(addFileGroupClickListener: OnClick) {
        this.addFileGroupClickListener = addFileGroupClickListener
    }

    fun setOnFileGroupClickListener(fileGroupClickListener: ItemClickListener<MedicalFileGroup>) {
        this.fileGroupClickListener = fileGroupClickListener
    }

    fun setOnFileGroupLongClickListener(fileGroupLongClickListener: ItemLongClickListener<Long>) {
        this.fileGroupLongClickListener = fileGroupLongClickListener
    }

    inner class AddMedicalFileGroupHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class MedicalFileGroupHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(medicalFileGroup: MedicalFileGroup) {
            val padding: Int
            itemView.file_group_title.text = medicalFileGroup.title
            when (medicalFileGroup.filesCount) {
                0, 1 -> {
                    padding = 16
                    itemView.file_group_icon.setImageResource(R.drawable.ic_file_group_one)
                }
                2 -> {
                    padding = 40
                    itemView.file_group_icon.setImageResource(R.drawable.ic_file_group_two)
                }
                else -> {
                    padding = 68
                    itemView.file_group_icon.setImageResource(R.drawable.ic_file_group_three)
                }
            }

            itemView.file_group_icon.viewTreeObserver.addOnGlobalLayoutListener {
                val params = itemView.file_group_title.layoutParams
                params.height = itemView.file_group_icon.height
                params.width = itemView.file_group_icon.width
                itemView.file_group_title.layoutParams = params
                itemView.file_group_title.setPadding(16, 16, padding, padding)
            }
        }
    }
}