package com.appagroup.sahty.ui.post_article

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface PostArticleContract {

    interface View : IBaseView {

        fun setListeners()

        fun checkFieldsValidate(): Boolean

        fun articlePostedSuccessfully()

        fun showProgressDialog()

        fun hideProgressDialog()
    }

    interface Presenter : IBasePresenter<View> {

        fun postArticleWithVideo(
                enTitle: RequestBody,
                enContent: RequestBody,
                arTitle: RequestBody,
                arContent: RequestBody,
                categoryId: RequestBody,
                video: MultipartBody.Part)

        fun postArticleWithImages(
                enTitle: RequestBody,
                enContent: RequestBody,
                arTitle: RequestBody,
                arContent: RequestBody,
                categoryId: RequestBody,
                images: MutableList<MultipartBody.Part>)

        fun getUserName(): String

        fun getUserImage(): String
    }
}