package com.appagroup.sahty.ui.medical_files.group_content.import_files

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.MedicalFileGroup
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface ImportFilesContract {

    interface View : IBaseView {

        fun setListeners()

        fun showProgressDialog()

        fun hideProgressDialog()

        fun uploadCompleted(fileGroup: MedicalFileGroup?)
    }

    interface Presenter : IBasePresenter<View> {

        fun uploadFiles(groupId: RequestBody, groupName: RequestBody, files: MutableList<MultipartBody.Part>)

    }
}