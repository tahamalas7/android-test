package com.appagroup.sahty.ui.messages.select_doctor

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Doctor
import com.appagroup.sahty.entity.Paginate
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response


class SelectDoctorPresenter(view: SelectDoctorContract.View) : BasePresenter<SelectDoctorContract.View>(view), SelectDoctorContract.Presenter {

    val TAG = "SelectDoctorPresenter"
    var pageCounter = 1
    var loadMoreDoctors = true

    override fun searchForDoctors(keyWord: String, filter: Int) {
        Log.d(TAG, "searchForDoctors -> FILTER = $filter")
        if (loadMoreDoctors) {
            if(pageCounter == 1)
                view.changeProgressBarVisibility(View.VISIBLE)
            else view.changeLoaderVisibility(View.VISIBLE)
            DisposableManager.disposeSearch()
            subscribe(dataManager.searchForDoctors(keyWord, filter, pageCounter), object : Observer<Response<BaseResponse<Paginate<MutableList<Doctor>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "searchForDoctors -> onComplete")
                }

                override fun onSubscribe(d: Disposable) {
                    Log.d(TAG, "searchForDoctors -> onSubscribe")
                    DisposableManager.addSearchDisposable(d)
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Doctor>>>>) {
                    Log.d(TAG, "searchForDoctors -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "searchForDoctors -> onNext -> body = ${t.body()}")
                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        Log.d(TAG, "searchForDoctors -> onNext -> data = ${t.body()!!.data}")
                        Log.d(TAG, "searchForDoctors -> onNext -> size = ${t.body()!!.data!!.data!!.size}")
                        pageCounter++
                        view.updateDoctors(t.body()!!.data!!.data!!)
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMoreDoctors = false
                    }
                    view.changeProgressBarVisibility(View.GONE)
                    view.changeLoaderVisibility(View.GONE)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "searchForDoctors -> onError -> error = ${e.message}")
                    view.showError(R.string.something_wrong)
                    view.changeProgressBarVisibility(View.GONE)
                    view.changeLoaderVisibility(View.GONE)
                }
            })
        }
    }

    override fun resetPageCounter() {
        loadMoreDoctors = true
        pageCounter = 1
    }

    override fun getUserId(): Long =
            dataManager.getUserId()

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeSearch()
    }
}