package com.appagroup.sahty.ui.messages.patient_groups

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.MedicalFileGroup
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response
import com.appagroup.sahty.ui.messages.patient_groups.PatientFileGroupsContract as Contract

class PatientFileGroupsPresenter(view: Contract.View) : BasePresenter<Contract.View>(view), Contract.Presenter {

    private val TAG = "PatientGroupsPresenter"

    override fun getMedicalFileGroups(patientId: Long) {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.getPatientMedicalFileGroups(patientId), object : Observer<Response<BaseResponse<MutableList<MedicalFileGroup>>>> {
            override fun onComplete() {
                Log.d(TAG, "getMedicalFileGroups -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "getMedicalFileGroups -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<MutableList<MedicalFileGroup>>>) {
                Log.d(TAG, "getMedicalFileGroups -> onNext -> Code = ${t.code()}")
                Log.d(TAG, "getMedicalFileGroups -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status)
                    view.updateGroups(t.body()!!.data!!)
                else
                    view.showError(R.string.err_failed_medical_files)

                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                view.changeProgressBarVisibility(View.GONE)
                Log.d(TAG, "getMedicalFileGroups -> onError -> Error = ${e.message}")
            }
        })
    }
}