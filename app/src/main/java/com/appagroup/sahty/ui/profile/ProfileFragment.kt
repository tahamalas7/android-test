package com.appagroup.sahty.ui.profile

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Parcelable
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.entity.UserProfile
import com.appagroup.sahty.ui.FollowService
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.ui.profile.edit_profile.EditProfileActivity
import com.appagroup.sahty.utils.UserRole
import com.facebook.drawee.backends.pipeline.Fresco
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.fragment_profile.*

class ProfileFragment : BaseFragment<ProfileContract.Presenter>(), ProfileContract.View {

    private val followService = FollowService()
    private lateinit var accreditationsAdapter: AccreditationAdapter
    private lateinit var pagerAdapter: FollowersPagerAdapter
    private lateinit var followingAdapter: FollowingAdapter
    private lateinit var followersAdapter: FollowersAdapter
    private lateinit var lightTypeface: Typeface
    private lateinit var boldTypeface: Typeface

    override fun instantiatePresenter(): ProfileContract.Presenter = ProfilePresenter(this)

    override fun getLayout(): Int = R.layout.fragment_profile

    override fun initViews(view: View) {
        // hide search bar
        (activity as MainActivity).changeSearchBarVisibility(View.GONE)
        presenter.getUserProfile()

        // initialize Typefaces
        lightTypeface = Typeface.createFromAsset(activity!!.assets, "fonts/HelveticaNeueLight.ttf")
        boldTypeface = Typeface.createFromAsset(activity!!.assets, "fonts/HelveticaNeueBold.ttf")

        // initialize adapters
        followingAdapter = FollowingAdapter(lightTypeface)
        followersAdapter = FollowersAdapter(lightTypeface)
        accreditationsAdapter = AccreditationAdapter(lightTypeface)
        pagerAdapter = FollowersPagerAdapter(followingAdapter, followersAdapter)

        // set typeface to fragment textViews
        profile_username.typeface = boldTypeface
        profile_accreditations_lbl.typeface = boldTypeface

        // initialize viewPager
        profile_follow_viewPager.adapter = pagerAdapter
        profile_follow_viewPager.currentItem = 0
        selectFollowingTab()

        // initialize accreditation recyclerView
        profile_accreditations_recyclerView.layoutManager = LinearLayoutManager(activity)
        profile_accreditations_recyclerView.adapter = accreditationsAdapter
        profile_username.text = presenter.getUsername()

        if (presenter.getUserImage().isNotEmpty())
            Glide.with(this)
                    .load(BuildConfig.IMAGE + presenter.getUserImage())
                    .into(profile_user_image)

        // need to check if user is not a doctor to hide accreditations space
        if (presenter.getUserRole() == UserRole.ROLE_READER)
            profile_accreditations_bar.visibility = View.GONE

        setListeners()
    }

    override fun setListeners() {
        profile_back_arrow.setOnClickListener { activity!!.onBackPressed() }

        profile_edit_btn.setOnClickListener {
            val intent = Intent(activity, EditProfileActivity::class.java)
            if (presenter.getUserRole() != UserRole.ROLE_READER)
                intent.putParcelableArrayListExtra(
                        EditProfileActivity.ACCREDITATION_LIST,
                        ArrayList<Parcelable>(accreditationsAdapter.getAccreditations()))
            startActivityForResult(intent, EDIT_PROFILE_REQUEST_CODE)
        }

        profile_following_tab.setOnClickListener {
            if (profile_follow_viewPager.currentItem == 1){
                profile_follow_viewPager.currentItem = 0
                selectFollowingTab()
            }
        }

        profile_followers_tab.setOnClickListener {
            if (profile_follow_viewPager.currentItem == 0){
                profile_follow_viewPager.currentItem = 1
                selectFollowersTab()
            }
        }

        profile_follow_viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {}
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}
            override fun onPageSelected(p0: Int) {
                when (p0) {
                    0 -> selectFollowingTab()
                    1 -> selectFollowersTab()
                }
            }
        })

        profile_user_image.setOnClickListener {
            if (presenter.getUserImage().isNotEmpty()) {
                Fresco.initialize(activity)
                ImageViewer.Builder(activity, arrayListOf(BuildConfig.IMAGE + presenter.getUserImage()))
                        .setStartPosition(0)
                        .show()
            }
        }

        followingAdapter.setOnFollowButtonClickListener(object : FollowButtonClickListener {
            override fun followUser(follow: Follow) {}

            override fun unfollowUser(follow: Follow) {
                followService.unfollowUser(follow.id)
                followersAdapter.removeFollowingUser(follow.id)
                profile_following_count.text = followingAdapter.itemCount.toString()
            }
        })

        followersAdapter.setOnFollowButtonClickListener(object : FollowButtonClickListener {
            override fun followUser(follow: Follow) {
                followService.followUser(follow.id)
                followingAdapter.addFollowingUser(follow)
                profile_following_count.text = followingAdapter.itemCount.toString()
            }

            override fun unfollowUser(follow: Follow) {
                followService.unfollowUser(follow.id)
                followingAdapter.removeFollowingUser(follow)
                profile_following_count.text = followingAdapter.followingList.size.toString()
            }
        })
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        profile_follow_progressBar.visibility = visibility
    }

    override fun updateProfile(userProfile: UserProfile) {
        profile_following_count.text = userProfile.followingCount.toString()
        profile_followers_count.text = userProfile.followersCount.toString()

        followingAdapter.updateFollows(userProfile.following)
        followersAdapter.updateFollows(userProfile.following, userProfile.followers)

        if (userProfile.accreditations != null)
            accreditationsAdapter.updateAccreditations(userProfile.accreditations)
        else
            profile_accreditations_lbl.visibility = View.GONE
    }

    private fun selectFollowingTab() {
        profile_following_lbl.typeface = boldTypeface
        profile_following_count.typeface = boldTypeface

        profile_followers_lbl.typeface = lightTypeface
        profile_followers_count.typeface = lightTypeface
    }

    private fun selectFollowersTab() {
        profile_followers_lbl.typeface = boldTypeface
        profile_followers_count.typeface = boldTypeface

        profile_following_lbl.typeface = lightTypeface
        profile_following_count.typeface = lightTypeface
    }

    override fun onPause() {
        super.onPause()
        (activity as MainActivity).changeSearchBarVisibility(View.VISIBLE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == EDIT_PROFILE_REQUEST_CODE) {
            Glide.with(activity!!)
                    .load(BuildConfig.IMAGE + presenter.getUserImage())
                    .into(profile_user_image)
        }
    }

    companion object {
        val TAG = "ProfileFragment"
        val EDIT_PROFILE_REQUEST_CODE = 44
        fun getInstance(): ProfileFragment =
                ProfileFragment()
    }
}