package com.appagroup.sahty.ui.search

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.*
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class SearchPresenter(view: SearchContract.View) : BasePresenter<SearchContract.View>(view), SearchContract.Presenter {

    private val TAG = "SearchActivity"
    private var page = 1
    private var loadMore = true

    override fun makeGeneralSearch(searchKey: String) {
        view.changeProgressBarVisibility(View.VISIBLE)
        view.setSearchProgressStatus(true)
        subscribe(dataManager.makeGeneralSearch(searchKey), object : Observer<Response<BaseResponse<GeneralSearchResult>>> {
            override fun onComplete() {
                Log.d(TAG, "makeGeneralSearch -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "makeGeneralSearch -> onSubscribe")
                DisposableManager.addSearchDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<GeneralSearchResult>>) {
                Log.d(TAG, "makeGeneralSearch -> onNext -> code = ${t.code()}")
                Log.d(TAG, "makeGeneralSearch -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null) {
                    Log.d(TAG, "makeGeneralSearch -> onNext -> status = ${t.body()!!.status}")
                    view.setGeneralSearchResults(t.body()!!.data!!)
                } else
                    view.showError(R.string.something_wrong)
                view.changeProgressBarVisibility(View.GONE)
                view.setSearchProgressStatus(false)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "makeGeneralSearch -> onError -> error = ${e.message}")
                view.showError(R.string.something_wrong)
                view.changeProgressBarVisibility(View.GONE)
                view.setSearchProgressStatus(false)
            }
        })
    }

    override fun searchForFacilities(searchKey: String, filter: Int) {
        if (loadMore) {
            view.changeProgressBarVisibility(View.VISIBLE)
            view.setSearchProgressStatus(true)
            subscribe(dataManager.makeSearchForFacilities(searchKey, filter, page), object : Observer<Response<BaseResponse<Paginate<MutableList<Facility>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "searchForFacilities -> onComplete")
                }

                override fun onSubscribe(d: Disposable) {
                    Log.d(TAG, "searchForFacilities -> onSubscribe")
                    DisposableManager.addSearchDisposable(d)
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Facility>>>>) {
                    Log.d(TAG, "searchForFacilities -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "searchForFacilities -> onNext -> body = ${t.body()}")

                    if (t.code() == 200 && t.body() != null) {
                        Log.d(TAG, "searchForFacilities -> onNext -> status = ${t.body()!!.status}")
                        if (page == 1)
                            view.clearFacilityItems()
                        view.setFacilitiesSearchResults(t.body()!!.data!!.data)
                        page++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMore = false
                    } else {
                        view.showError(R.string.something_wrong)
                    }
                    view.changeProgressBarVisibility(View.GONE)
                    view.setSearchProgressStatus(false)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "searchForFacilities -> onError -> error = ${e.message}")
                    view.showError(R.string.something_wrong)
                    view.changeProgressBarVisibility(View.GONE)
                    view.setSearchProgressStatus(false)
                }
            })
        }
    }

    override fun searchForArticles(searchKey: String, filter: Int) {
        if (loadMore) {
            view.changeProgressBarVisibility(View.VISIBLE)
            view.setSearchProgressStatus(true)
            subscribe(dataManager.makeSearchForArticles(searchKey, filter, page), object : Observer<Response<BaseResponse<Paginate<MutableList<Article>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "searchForArticles -> onComplete")
                }

                override fun onSubscribe(d: Disposable) {
                    Log.d(TAG, "searchForArticles -> onSubscribe")
                    DisposableManager.addSearchDisposable(d)
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Article>>>>) {
                    Log.d(TAG, "searchForArticles -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "searchForArticles -> onNext -> body = ${t.body()}")

                    if (t.code() == 200 && t.body() != null) {
                        Log.d(TAG, "searchForArticles -> onNext -> status = ${t.body()!!.status}")
                        if (page == 1)
                            view.clearArticleItems()
                        view.setArticlesSearchResults(t.body()!!.data!!.data)
                        page++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMore = false
                    } else {
                        view.showError(R.string.something_wrong)
                    }
                    view.changeProgressBarVisibility(View.GONE)
                    view.setSearchProgressStatus(false)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "searchForArticles -> onError -> error = ${e.message}")
                    view.showError(R.string.something_wrong)
                    view.changeProgressBarVisibility(View.GONE)
                    view.setSearchProgressStatus(false)
                }
            })
        }
    }

    override fun searchForDoctors(searchKey: String, filter: Int) {
        if (loadMore) {
            view.changeProgressBarVisibility(View.VISIBLE)
            view.setSearchProgressStatus(true)
            subscribe(dataManager.makeSearchForDoctors(searchKey, filter, page), object : Observer<Response<BaseResponse<Paginate<MutableList<Doctor>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "searchForDoctors -> onComplete")
                }

                override fun onSubscribe(d: Disposable) {
                    Log.d(TAG, "searchForDoctors -> onSubscribe")
                    DisposableManager.addSearchDisposable(d)
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Doctor>>>>) {
                    Log.d(TAG, "searchForDoctors -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "searchForDoctors -> onNext -> body = ${t.body()}")

                    if (t.code() == 200 && t.body() != null) {
                        Log.d(TAG, "searchForDoctors -> onNext -> status = ${t.body()!!.status}")
                        if (page == 1)
                            view.clearDoctorItems()
                        view.setDoctorsSearchResults(t.body()!!.data!!.data)
                        page++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMore = false
                    } else {
                        view.showError(R.string.something_wrong)
                    }
                    view.changeProgressBarVisibility(View.GONE)
                    view.setSearchProgressStatus(false)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "searchForDoctors -> onError -> error = ${e.message}")
                    view.showError(R.string.something_wrong)
                    view.changeProgressBarVisibility(View.GONE)
                    view.setSearchProgressStatus(false)
                }
            })
        }
    }

    override fun resetPaginate() {
        page = 1
        loadMore = true
    }

    override fun disposeSearch() {
        DisposableManager.disposeSearch()
        view.setSearchProgressStatus(false)
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeSearch()
    }
}
