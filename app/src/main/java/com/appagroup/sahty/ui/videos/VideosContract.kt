package com.appagroup.sahty.ui.videos

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Video

interface VideosContract {

    interface View : IBaseView {

        fun setListeners()

        fun updateVideos(videos: MutableList<Video>)

        fun changeProgressBarVisibility(visibility: Int)

        fun changeLoaderVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun getVideos()
    }
}