package com.appagroup.sahty.ui.messages.patient_groups

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.MedicalFileGroup

interface PatientFileGroupsContract {

    interface View : IBaseView {

        fun setListeners()

        fun changeProgressBarVisibility(visibility: Int)

        fun updateGroups(groups: MutableList<MedicalFileGroup>)
    }

    interface Presenter : IBasePresenter<View> {

        fun getMedicalFileGroups(patientId: Long)
    }
}