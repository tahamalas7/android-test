package com.appagroup.sahty.ui.videos

import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.entity.Video
import com.appagroup.sahty.ui.ResumeFragment
import com.appagroup.sahty.ui.SaveService
import com.appagroup.sahty.ui.articles.article_content.SaveBottomSheetDialog
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.OnClick
import com.appagroup.sahty.utils.SaveTypes
import kotlinx.android.synthetic.main.fragment_videos.*

class VideosFragment : BaseFragment<VideosContract.Presenter>(), VideosContract.View, ResumeFragment {

    private val adapter = VideosAdapter()
    private val saveService = SaveService()
    private var fragmentLaunched = false

    override fun instantiatePresenter(): VideosContract.Presenter = VideosPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_videos

    override fun initViews(view: View) {
        videos_recyclerView.layoutManager = LinearLayoutManager(activity)
        videos_recyclerView.adapter = adapter

        setListeners()
    }

    override fun setListeners() {
        adapter.setOnPlayButtonClickListener(object : ItemClickListener<Video> {
            override fun onClick(item: Video) {
                val intent = Intent(activity, VideoPlayerActivity::class.java)
                intent.putExtra(VideoPlayerActivity.VIDEO_URL, item.url)
                intent.putExtra(VideoPlayerActivity.VIDEO_SOURCE, VideoPlayerActivity.TYPE_EXTERNAL)
                startActivity(intent)
            }
        })

        adapter.setOnVideosBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                presenter.getVideos()
            }
        })

        adapter.setOnOptionsButtonClickListener(object : ItemClickListener<Video> {
            override fun onClick(item: Video) {
                val dialog = SaveBottomSheetDialog.newInstance(item.isSaved, getString(R.string.video))
                dialog.show(activity!!.supportFragmentManager, "SaveBottomSheetDialog")

                dialog.setOnBottomSheetClickListener(object : OnClick {
                    override fun onClick() {
                        if (item.isSaved) {
                            saveService.unsave(item.id, SaveTypes.VIDEO)
                            item.isSaved = false
                        } else {
                            saveService.save(item.id, SaveTypes.VIDEO)
                            item.isSaved = true
                        }
                        dialog.dismiss()
                    }
                })
            }
        })
    }

    override fun updateVideos(videos: MutableList<Video>) {
        adapter.updateVideos(videos)
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        video_progressBar.visibility = visibility
    }

    override fun changeLoaderVisibility(visibility: Int) {
        adapter.changeLoaderVisibility(visibility)
    }

    override fun onResumeFragment() {
        if (!fragmentLaunched) {
            presenter.getVideos()
            fragmentLaunched = true
        }
    }

    companion object {
        val TAG = "VideosFragment"
        fun getInstance(): VideosFragment =
                VideosFragment()
    }
}