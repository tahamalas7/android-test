package com.appagroup.sahty.ui.listener

interface BottomReachListener {

    fun onBottomReach()
}