package com.appagroup.sahty.ui.visit_profile

import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_follow.view.*

class FollowingAdapter(private val typeface: Typeface, private val userId: Long)
    : RecyclerView.Adapter<FollowingAdapter.FollowingHolder>() {

    private lateinit var followButtonClickListener: FollowButtonClickListener
    private lateinit var userClickListener: ItemClickListener<Follow>
    private val followings: MutableList<Follow> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): FollowingAdapter.FollowingHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_follow, parent, false)
        return FollowingHolder(view)
    }

    override fun getItemCount(): Int = followings.size

    override fun onBindViewHolder(holder: FollowingAdapter.FollowingHolder, position: Int) {
        val following = followings[position]
        holder.bind(following)

        holder.itemView.setOnClickListener {
            if (following.id != userId)
                userClickListener.onClick(following)
        }

        holder.itemView.follow_btn.setOnClickListener {
            if (following.isFollowed) {
                Log.d("Follow", "Followwwww trueeee, id = ${following.id}")
                followButtonClickListener.unfollowUser(following)
                following.isFollowed = false
            } else {
                Log.d("Follow", "Followwwww falseee, id = ${following.id}")
                followButtonClickListener.followUser(following)
                following.isFollowed = true
            }
            notifyItemChanged(position)
        }
    }

    fun updateFollows(followings: MutableList<Follow>) {
        this.followings.addAll(followings)
        notifyDataSetChanged()
    }

    fun setOnFollowButtonClickListener(followButtonClickListener: FollowButtonClickListener) {
        this.followButtonClickListener = followButtonClickListener
    }

    fun setOnUserClickListener(userClickListener: ItemClickListener<Follow>) {
        this.userClickListener = userClickListener
    }

    inner class FollowingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(following: Follow) {
            itemView.follow_username.text = following.name
            itemView.follow_username.typeface = typeface

            if (following.image != null)
                Glide.with(itemView.context)
                        .load(BuildConfig.IMAGE + following.image)
                        .into(itemView.follow_user_image)

            if (following.isFollowed)
                setAsFollowed(following)
            else setAsNotFollowed(following)

            if (following.id == userId)
                itemView.follow_btn.visibility = View.GONE
            else itemView.follow_btn.visibility = View.VISIBLE
        }

        fun setAsFollowed(following: Follow) {
            itemView.follow_btn.setBackgroundResource(R.drawable.background_follow_button_on)
            itemView.follow_btn.text = itemView.context.getString(R.string.unfollow)
            following.isFollowed = true
        }

        fun setAsNotFollowed(following: Follow) {
            itemView.follow_btn.setBackgroundResource(R.drawable.background_follow_button_off)
            itemView.follow_btn.text = itemView.context.getString(R.string.follow)
            following.isFollowed = false
        }
    }
}