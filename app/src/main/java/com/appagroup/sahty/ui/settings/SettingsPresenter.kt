package com.appagroup.sahty.ui.settings

import com.appagroup.sahty.base.BasePresenter

class SettingsPresenter(view: SettingsContract.View) : BasePresenter<SettingsContract.View>(view), SettingsContract.Presenter {

    override fun getApplicationLanguage(): String =
            dataManager.getApplicationLanguage()

    override fun setApplicationLanguage(language: String) =
        dataManager.setApplicationLanguage(language)

    override fun getMobileDataStatus(): Boolean =
            dataManager.getMobileDataStatus()

    override fun setMobileDataStatus(status: Boolean) =
            dataManager.setMobileDataStatus(status)

    override fun getNotificationsStatus(): Boolean =
            dataManager.getNotificationsStatus()

    override fun setNotificationsStatus(status: Boolean) =
            dataManager.setNotificationsStatus(status)
}