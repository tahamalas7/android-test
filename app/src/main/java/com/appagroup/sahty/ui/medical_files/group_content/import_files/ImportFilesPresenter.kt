package com.appagroup.sahty.ui.medical_files.group_content.import_files

import android.util.Log
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.MedicalFileGroup
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class ImportFilesPresenter(view: ImportFilesContract.View) : BasePresenter<ImportFilesContract.View>(view), ImportFilesContract.Presenter {
    private val TAG = "ImportFilesPresenter"

    override fun uploadFiles(groupId: RequestBody, groupName: RequestBody, files: MutableList<MultipartBody.Part>) {
        view.showProgressDialog()
        subscribe(dataManager.uploadFiles(groupId, groupName, files), object : Observer<Response<BaseResponse<MedicalFileGroup?>>> {
            override fun onComplete() {
                Log.d(TAG, "uploadFiles -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "uploadFiles -> onSubscribe")
                DisposableManager.addUploadDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<MedicalFileGroup?>>) {
                Log.d(TAG, "uploadFiles -> onNext -> code = ${t.code()}")
                Log.d(TAG, "uploadFiles -> onNext -> errorBody = ${t.errorBody()}")
                Log.d(TAG, "uploadFiles -> onNext -> body = ${t.body()}")
                Log.d(TAG, "uploadFiles -> onNext -> headers = ${t.headers()}")
                Log.d(TAG, "uploadFiles -> onNext -> message = ${t.message()}")
                Log.d(TAG, "uploadFiles -> onNext -> raw = ${t.raw()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "uploadFiles -> onNext -> SERVER status = ${t.body()!!.status}")
                    Log.d(TAG, "uploadFiles -> onNext -> SERVER message = ${t.body()!!.message}")
                    Log.d(TAG, "uploadFiles -> onNext -> Medical File Group is = ${t.body()!!.data}")
//                    Log.d(TAG, "uploadFiles -> onNext -> SERVER data = ${t.body()!!.data}")
                    view.uploadCompleted(t.body()!!.data)
                } else
                    view.showError(R.string.upload_failed)
                view.hideProgressDialog()
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "uploadFiles -> onError -> body = ${e.message}")
                view.showError(R.string.upload_failed)
                view.hideProgressDialog()
            }
        })
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeUpload()
    }
}