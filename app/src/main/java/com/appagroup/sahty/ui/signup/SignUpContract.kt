package com.appagroup.sahty.ui.signup

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Specialty
import com.appagroup.sahty.entity.User

interface SignUpContract {

    interface View : IBaseView {

        fun signUpSuccessfully(name: String)

        fun showProgressBar()

        fun hideProgressBar()

        fun showEnterAccreditationDialog()

        fun setListeners()

        fun checkData(): Boolean

        fun checkAccreditationData(view: android.view.View): Boolean

        fun changeSignUpProcessStatus(status: Boolean)

        fun pickImage()

        fun updateSpecialties(specialties: MutableList<Specialty>)

        fun showSpecialtySearchProgressBar()

        fun hideSpecialtySearchProgressBar()
    }

    interface Presenter : IBasePresenter<View> {

        fun commitSignUp(name: String, email: String, password: String, role: Int, fcm_token: String)

        fun commitSignUpWithAccreditation(name: String, email: String, password: String, role: Int, fcm_token: String, acc_type: String, acc_specialty: Long, acc_location: String, acc_year: String, acc_image: String)

        fun saveUserData(user: User)

        fun getSpecialties(name: String)
    }
}