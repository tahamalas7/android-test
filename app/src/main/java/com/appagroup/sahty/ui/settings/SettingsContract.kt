package com.appagroup.sahty.ui.settings

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView

interface SettingsContract {

    interface View : IBaseView {

        fun setListeners()

        fun showLanguagesDialog()
    }

    interface Presenter : IBasePresenter<View> {

        fun getApplicationLanguage(): String

        fun setApplicationLanguage(language: String)

        fun getMobileDataStatus(): Boolean

        fun setMobileDataStatus(status: Boolean)

        fun getNotificationsStatus(): Boolean

        fun setNotificationsStatus(status: Boolean)
    }
}