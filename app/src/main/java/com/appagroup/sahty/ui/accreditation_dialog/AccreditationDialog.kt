package com.appagroup.sahty.ui.accreditation_dialog

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseDialog
import com.appagroup.sahty.entity.Accreditation
import com.appagroup.sahty.entity.Specialty
import com.appagroup.sahty.ui.listener.DialogDismissListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.utils.UIUtils
import com.appagroup.sahty.utils.toRequestBody
import com.facebook.drawee.backends.pipeline.Fresco
import com.stfalcon.frescoimageviewer.ImageViewer
import gun0912.tedbottompicker.TedBottomPicker
import kotlinx.android.synthetic.main.dialog_accreditation.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class AccreditationDialog : BaseDialog<AccreditationContract.Presenter>(), AccreditationContract.View {

    private val TAG = "AccreditationDialog"
    private var dialogType: Int = -1
    private var imageChanged = false
    private var accreditation: Accreditation = Accreditation(specialityId = 0L)
    private lateinit var specialtiesAdapter: SpecialtiesAdapter
    private lateinit var dialogDismissListener: DialogDismissListener

    override fun instantiatePresenter(): AccreditationContract.Presenter = AccreditationPresenter(this)

    override fun getLayout(): Int = R.layout.dialog_accreditation

    override fun initViews(view: View) {
        Log.d(TAG, "initViews")
        val typeface = Typeface.createFromAsset(activity!!.assets, "fonts/GOTHIC.TTF")
        specialtiesAdapter = SpecialtiesAdapter(typeface)
        accreditation_specialties_recyclerView.layoutManager = LinearLayoutManager(activity)
        accreditation_specialties_recyclerView.adapter = specialtiesAdapter
        dialogType = arguments!!.getInt(DIALOG_TYPE, -1)

        when (dialogType) {
            TYPE_CREATE -> {
                delete_accreditation_button.visibility = View.GONE
                accreditation_description.visibility = View.GONE
                accreditation_dialog_title.text = getString(R.string.enter_accreditation)
                setListeners()
            }

            TYPE_EDIT -> {
                accreditation = arguments!!.getSerializable(ACCREDITATION) as Accreditation
                Log.d(TAG, "Accreditation = $accreditation")
                accreditation_type.setText(accreditation.type)
                accreditation_year.setText(accreditation.year)
                accreditation_location.setText(accreditation.location)
                accreditation_specialty.setText(accreditation.speciality)
                Glide.with(this)
                        .load(BuildConfig.IMAGE + accreditation.image)
                        .into(accreditation_image)

                delete_accreditation_button.visibility = View.VISIBLE
                accreditation_description.visibility = View.GONE
                accreditation_dialog_title.text = getString(R.string.edit_accreditation)
                setListeners()
            }

            TYPE_VIEW -> {
                accreditation = arguments!!.getSerializable(ACCREDITATION) as Accreditation
                Log.d(TAG, "Accreditation = $accreditation")
                accreditation_type.setText(accreditation.type)
                accreditation_year.setText(accreditation.year)
                accreditation_location.setText(accreditation.location)
                accreditation_specialty.setText(accreditation.speciality)
                Glide.with(this)
                        .load(BuildConfig.IMAGE + accreditation.image)
                        .into(accreditation_image)

                accreditation_type.inputType = EditorInfo.TYPE_NULL
                accreditation_year.inputType = EditorInfo.TYPE_NULL
                accreditation_location.inputType = EditorInfo.TYPE_NULL
                accreditation_specialty.inputType = EditorInfo.TYPE_NULL

                accreditation_description.visibility = View.GONE
                accreditation_dialog_title.visibility = View.GONE
                submit_accreditation_button.visibility = View.GONE
                delete_accreditation_button.visibility = View.GONE
                delete_accreditation_ok.visibility = View.VISIBLE

                delete_accreditation_ok.setOnClickListener { dismiss() }

                accreditation_image.setOnClickListener {
                    if (accreditation.image != null && accreditation.image!!.isNotEmpty()) {
                        Fresco.initialize(activity)
                        ImageViewer.Builder(activity, arrayListOf(BuildConfig.IMAGE + accreditation.image))
                                .setStartPosition(0)
                                .show()
                    }
                }
            }
        }
    }

    override fun setListeners() {
        Log.d(TAG, "setListeners")
        submit_accreditation_button.setOnClickListener {
            if (checkAccreditationData()) {
                accreditation.type = accreditation_type.text.toString()
                accreditation.year = accreditation_year.text.toString()
                accreditation.location = accreditation_location.text.toString()

                val imgFile = File(accreditation.image)
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile)

                when (dialogType) {
                    TYPE_CREATE -> {
                        presenter.createAccreditation(
                                accreditation.type!!.toRequestBody(),
                                accreditation.year!!.toRequestBody(),
                                accreditation.location!!.toRequestBody(),
                                accreditation.specializationId.toString().toRequestBody(),
                                MultipartBody.Part.createFormData("image", imgFile.name, requestFile)
                        )
                    }

                    TYPE_EDIT -> {
                        Log.d("oooooooo", "specializationId = ${accreditation.specializationId}")
                        Log.d("oooooooo", "type = ${accreditation.type}")
                        Log.d("oooooooo", "year = ${accreditation.year}")
                        Log.d("oooooooo", "location = ${accreditation.location}")
                        if (imageChanged)
                            presenter.editAccreditationWithImage(
                                    accreditation.id,
                                    accreditation.type!!.toRequestBody(),
                                    accreditation.year!!.toRequestBody(),
                                    accreditation.location!!.toRequestBody(),
                                    accreditation.specializationId.toString().toRequestBody(),
                                    MultipartBody.Part.createFormData("image", imgFile.name, requestFile)
                            )
                        else
                            presenter.editAccreditation(
                                    accreditation.id,
                                    accreditation.type!!.toRequestBody(),
                                    accreditation.year!!.toRequestBody(),
                                    accreditation.location!!.toRequestBody(),
                                    accreditation.specializationId.toString().toRequestBody()
                            )
                    }
                }

                Log.d(TAG, "accreditation is\ntype = ${accreditation.type}\nlocation = ${accreditation.location}\nyear = ${accreditation.year}\nspeciality = ${accreditation.speciality}\nimage = ${accreditation.image}\n")
            }
        }

        delete_accreditation_button.setOnClickListener {
            presenter.deleteAccreditation(accreditation.id)
        }

        accreditation_image.setOnClickListener {
            accreditation_image_error_border.visibility = View.GONE
            Log.d(TAG , "accreditation_image setOnClick")
            // request READ_EXTERNAL_STORAGE and READ_EXTERNAL_STORAGE permissions
            ActivityCompat.requestPermissions(activity!!, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_PERMISSIONS_CODE)
        }

        accreditation_dialog_title.setOnClickListener {
            accreditation_image_error_border.visibility = View.GONE
            Log.d(TAG , "accreditation_dialog_title setOnClick")
            // request READ_EXTERNAL_STORAGE and READ_EXTERNAL_STORAGE permissions
            ActivityCompat.requestPermissions(activity!!, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    REQUEST_PERMISSIONS_CODE)
        }

        // detect chosen specialty
        specialtiesAdapter.setSpecialtyClickListener(object : ItemClickListener<Specialty> {
            override fun onClick(item: Specialty) {
                accreditation_specialty.setText(item.name)
                accreditation.speciality = item.name
                accreditation.specializationId = item.id
                accreditation_specialties_recyclerView.visibility = View.GONE
            }
        })

        // set RecyclerView width the same of specialty field
        accreditation_specialty.viewTreeObserver.addOnGlobalLayoutListener {
            val params = accreditation_specialties_recyclerView.layoutParams
            params.width = accreditation_specialty.width
            accreditation_specialties_recyclerView.layoutParams = params
        }

        // detect text change of specialty
        accreditation_specialty.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Log.d(TAG, "SpecialtyTextWatcher -> onTextChanged -> text = ${s.toString()}")
                if (s!!.isEmpty())
                    accreditation_specialties_recyclerView.visibility = View.GONE
                else {
                    accreditation_specialties_recyclerView.visibility = View.VISIBLE
                    presenter.getSpecialties(s.toString())
                }
            }
        })
    }

    override fun pickImage() {
        try {
            Log.d(TAG, "pickImage")
            val tedBottomPicker = TedBottomPicker.Builder(activity!!)
                    .setOnImageSelectedListener { uri ->
                        var finalUri = uri.toString()
                        finalUri = finalUri.substring(7)
                        finalUri = finalUri.replace("%20".toRegex(), " ")

                        if (finalUri != "") {
                            finalUri = UIUtils.compressImage(activity!!, finalUri)
                            Glide.with(this).load(finalUri).into(accreditation_image)
                            accreditation.image = finalUri
                            imageChanged = true
                        }
                        Log.d(TAG, "Final Uri = $finalUri")
                    }
                    .setTitle(getString(R.string.pick_an_image))
                    .setCameraTileBackgroundResId(R.color.ironGrayColor)
                    .setGalleryTileBackgroundResId(R.color.hintColor)
                    .setPeekHeight(1600)
                    .setCompleteButtonText(R.string.done)
                    .setEmptySelectionText(getString(R.string.no_select))
                    .create()

            tedBottomPicker.show(activity!!.supportFragmentManager)
        } catch (e: NullPointerException) {
            Log.e(TAG, "NullPointerException: ${e.message}")
        }
    }

    override fun updateSpecialties(specialties: MutableList<Specialty>) {
        specialtiesAdapter.updateSpecialties(specialties)
    }

    override fun checkAccreditationData(): Boolean {
        var isValid = true

        // check accreditation type if empty
        if (accreditation_type.text.isEmpty()) {
            accreditation_type.error = getString(R.string.error_empty_field)
            isValid = false
        }

        // check accreditation year if valid year
        if (accreditation_year.text.length != 4) {
            accreditation_year.error = getString(R.string.error_invalid_year)
            isValid = false
        }

        // check accreditation location if empty
        if (accreditation_location.text.isEmpty()) {
            accreditation_location.error = getString(R.string.error_empty_field)
            isValid = false
        }

        // check accreditation speciality if empty
        if (accreditation.speciality == "") {
            accreditation_specialty.error = getString(R.string.error_empty_field)
            isValid = false
        }

        // check accreditation image if empty
        if (accreditation.image == "") {
            accreditation_image_error_border.visibility = View.VISIBLE
            isValid = false
        } else
            accreditation_image_error_border.visibility = View.GONE

        return isValid
    }

    override fun accreditationCreatedSuccessfully(accreditation: Accreditation) {
        dialogDismissListener.accreditationCreated(accreditation)
        dialog.dismiss()
    }

    override fun accreditationEditedSuccessfully(accreditation: Accreditation) {
        dialogDismissListener.accreditationEdited(accreditation)
        dialog.dismiss()
    }

    override fun accreditationDeletedSuccessfully() {
        dialogDismissListener.accreditationDeleted(accreditation.id)
        dialog.dismiss()
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        accreditation_progressBar.visibility = visibility
    }

    override fun changeSpecialtySearchProgressBarVisibility(visibility: Int) {
        enter_accreditation_specialty_progressBar.visibility = visibility
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        Log.d(TAG, "onRequestPermissionsResult -> requestCode = $requestCode")
        Log.d(TAG, "onRequestPermissionsResult -> grantResults = $grantResults")
        if (requestCode == REQUEST_PERMISSIONS_CODE) {
            // call pickImage method if permission granted
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage()
            }
        }
    }

    fun setOnDialogDismissed(dialogDismissListener: DialogDismissListener) {
        this.dialogDismissListener = dialogDismissListener
    }

    companion object {
        val REQUEST_PERMISSIONS_CODE = 46
        val ACCREDITATION = "ACCREDITATION"
        val DIALOG_TYPE = "DIALOG_TYPE"
        val TYPE_CREATE = 0
        val TYPE_EDIT = 1
        val TYPE_VIEW = 2

        fun getInstance(dialogType: Int, accreditation: Accreditation?): AccreditationDialog {
            val instance = AccreditationDialog()
            val bundle = Bundle()
            bundle.putInt(DIALOG_TYPE, dialogType)
            bundle.putSerializable(ACCREDITATION, accreditation)
            instance.arguments = bundle
            return instance
        }

    }
}