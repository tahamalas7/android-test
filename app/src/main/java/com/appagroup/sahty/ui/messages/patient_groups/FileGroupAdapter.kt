package com.appagroup.sahty.ui.messages.patient_groups

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.MedicalFileGroup
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_medical_file_group.view.*

class FileGroupAdapter : RecyclerView.Adapter<FileGroupAdapter.FileGroupHolder>() {

    private lateinit var groupClickListener: ItemClickListener<MedicalFileGroup>
    private val groups: MutableList<MedicalFileGroup> = mutableListOf()
    private var itemHeight = 400

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): FileGroupHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_medical_file_group, parent, false)
        return FileGroupHolder(view)
    }

    override fun getItemCount(): Int = groups.size

    override fun onBindViewHolder(holder: FileGroupHolder, position: Int) {
        val params = holder.itemView.layoutParams
        params.height = itemHeight
        holder.itemView.layoutParams = params

        holder.bind(groups[position])
        holder.itemView.setOnClickListener {
            groupClickListener.onClick(groups[position])
        }
    }

    fun updateGroups(groups: MutableList<MedicalFileGroup>) {
        this.groups.addAll(groups)
        notifyDataSetChanged()
    }

    fun updateItemHeight(itemHeight: Int) {
        this.itemHeight = itemHeight
    }

    fun setOnMedicalFileGroupClickListener(groupClickListener: ItemClickListener<MedicalFileGroup>) {
        this.groupClickListener = groupClickListener
    }

    inner class FileGroupHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(group: MedicalFileGroup) {
            val padding: Int
            itemView.file_group_title.text = group.title
            when (group.filesCount) {
                0, 1 -> {
                    padding = 16
                    itemView.file_group_icon.setImageResource(R.drawable.ic_file_group_one)
                }
                2 -> {
                    padding = 40
                    itemView.file_group_icon.setImageResource(R.drawable.ic_file_group_two)
                }
                else -> {
                    padding = 68
                    itemView.file_group_icon.setImageResource(R.drawable.ic_file_group_three)
                }
            }

            itemView.file_group_icon.viewTreeObserver.addOnGlobalLayoutListener {
                val params = itemView.file_group_title.layoutParams
                params.height = itemView.file_group_icon.height
                params.width = itemView.file_group_icon.width
                itemView.file_group_title.layoutParams = params
                itemView.file_group_title.setPadding(16, 16, padding, padding)
            }
        }
    }
}