package com.appagroup.sahty.ui.poll

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Poll

interface PollContract {

    interface View : IBaseView {

        fun setListeners()

        fun updatePolls(polls: MutableList<Poll>)

        fun changeLoaderVisibility(visibility: Int)

        fun changeProgressBarVisibility(visibility: Int)

        fun openSelectFilterDialog()

        fun notifyPollsDone()
    }

    interface Presenter : IBasePresenter<View> {

        fun loadPolls(filter: Int)

        fun submitVote(optionId: Long)

        fun resetPaginate()
    }
}