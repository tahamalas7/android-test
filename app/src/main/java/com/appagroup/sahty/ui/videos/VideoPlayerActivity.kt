package com.appagroup.sahty.ui.videos

import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.easyvideoplayer.EasyVideoCallback
import com.afollestad.easyvideoplayer.EasyVideoPlayer
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import kotlinx.android.synthetic.main.activity_video_player.*

class VideoPlayerActivity : AppCompatActivity(), EasyVideoCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_player)
        video_player_view.setCallback(this)
        video_player_view.setSource(Uri.parse(
                        if (intent.getIntExtra(VIDEO_SOURCE, TYPE_INTERNAL) == TYPE_EXTERNAL)
                            BuildConfig.IMAGE + intent.getStringExtra(VIDEO_URL)
                        else
                            intent.getStringExtra(VIDEO_URL)))
        video_player_view.setAutoPlay(true)

    }

    override fun onRetry(player: EasyVideoPlayer?, source: Uri?) {}

    override fun onPrepared(player: EasyVideoPlayer?) {}

    override fun onStarted(player: EasyVideoPlayer?) {}

    override fun onCompletion(player: EasyVideoPlayer?) {}

    override fun onSubmit(player: EasyVideoPlayer?, source: Uri?) {}

    override fun onBuffering(percent: Int) {}

    override fun onPreparing(player: EasyVideoPlayer?) {}

    override fun onError(player: EasyVideoPlayer?, e: Exception?) {}

    override fun onPaused(player: EasyVideoPlayer?) {}

    override fun onPause() {
        super.onPause()
        video_player_view.pause()
    }

    companion object {
        val TAG = "VideoPlayerActivity"
        val VIDEO_URL = "VIDEO_URL"
        val VIDEO_SOURCE = "VIDEO_SOURCE"
        val TYPE_INTERNAL = 0
        val TYPE_EXTERNAL = 1
    }
}