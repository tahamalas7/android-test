package com.appagroup.sahty.ui.messages

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Message
import com.appagroup.sahty.entity.Paginate
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class MessagesPresenter(view: MessagesContract.View) : BasePresenter<MessagesContract.View>(view), MessagesContract.Presenter {

    private val TAG = "MessagesPresenter"
    private var messagesPage = 1
    private var loadMoreMessages = true

    override fun loadMessages() {
        if (loadMoreMessages) {
            if (messagesPage != 1) view.changeLoaderVisibility(View.VISIBLE)
            subscribe(dataManager.getMessages(messagesPage), object : Observer<Response<BaseResponse<Paginate<MutableList<Message>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "loadMessages -> onComplete")
                }

                override fun onSubscribe(d: Disposable) {
                    Log.d(TAG, "loadMessages -> onSubscribe")
                    DisposableManager.add(d)
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Message>>>>) {
                    Log.d(TAG, "loadMessages -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "loadMessages -> onNext -> body = ${t.body()}")
                    Log.d(TAG, "loadMessages -> onNext -> status = ${t.body()!!.status}")

                    view.hideMessagesProgressBar()
                    view.changeLoaderVisibility(View.GONE)

                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        Log.d(TAG, "loadReplies -> onNext -> messages count = ${t.body()!!.data!!.data!!.size}")
                        view.updateMessages(t.body()!!.data!!.data!!)
                        messagesPage++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMoreMessages = false
                    } else
                        if (messagesPage == 1)
                            view.showError(R.string.err_failed_load_messages)
                        else view.showError(R.string.err_failed_load_more_messages)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "loadMessages -> onError -> error = ${e.message}")
                    view.hideMessagesProgressBar()
                    view.changeLoaderVisibility(View.GONE)

                    if (messagesPage == 1)
                        view.showError(R.string.err_failed_load_messages)
                    else view.showError(R.string.err_failed_load_more_messages)
                }
            })
        }
    }

    override fun resetPaginate() {
        messagesPage = 1
        loadMoreMessages = true
    }

    override fun getUserId(): Long =
            dataManager.getUserId()

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.dispose()
    }
}