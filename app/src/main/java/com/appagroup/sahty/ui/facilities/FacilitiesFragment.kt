package com.appagroup.sahty.ui.facilities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.appagroup.sahty.R
import com.appagroup.sahty.ui.ResumeFragment
import com.appagroup.sahty.ui.facilities.hospitals.HospitalsFragment
import com.appagroup.sahty.ui.facilities.pharmacies.PharmaciesFragment
import kotlinx.android.synthetic.main.fragment_facilities.*
import kotlinx.android.synthetic.main.item_tab.view.*

class FacilitiesFragment : Fragment(), ResumeFragment {

    private var fragmentLaunched = false

    companion object {
        val TAG = "FacilitiesFragment"
        fun getInstance(): FacilitiesFragment = FacilitiesFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_facilities, container, false)
    }

    override fun onResumeFragment() {
        if (!fragmentLaunched) {
            val adapter = FacilitiesAdapter(activity!!.supportFragmentManager)
            facilities_viewpager.adapter = adapter
            facilities_tabs.setupWithViewPager(facilities_viewpager)
            fragmentLaunched = true

            val tabHospitals = LayoutInflater.from(activity).inflate(R.layout.item_tab, null)
            tabHospitals.tab_text.text = getString(R.string.hospitals)
            facilities_tabs.getTabAt(0)!!.customView = tabHospitals

            val tabPharmacies = LayoutInflater.from(activity).inflate(R.layout.item_tab, null)
            tabPharmacies.tab_text.text = getString(R.string.pharmacies)
            facilities_tabs.getTabAt(1)!!.customView = tabPharmacies
        }
    }

    inner class FacilitiesAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        override fun getItem(position: Int): Fragment {
            return if (position == 0)
                HospitalsFragment.getInstance()
            else
                PharmaciesFragment.getInstance()
        }

        override fun getCount(): Int = 2
    }
}