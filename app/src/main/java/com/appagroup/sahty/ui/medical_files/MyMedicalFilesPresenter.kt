package com.appagroup.sahty.ui.medical_files

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.MedicalFileGroup
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class MyMedicalFilesPresenter(view: MyMedicalFilesContract.View) : BasePresenter<MyMedicalFilesContract.View>(view), MyMedicalFilesContract.Presenter {

    private val TAG = "MyMedicalFilesPresenter"

    override fun loadMedicalFileGroups() {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.getMedicalFileGroups(), object :Observer<Response<BaseResponse<MutableList<MedicalFileGroup>>>> {
            override fun onComplete() {
                Log.d(TAG, "loadMedicalFileGroups -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "loadMedicalFileGroups -> onSubscribe")
                DisposableManager.addMedicalFilesDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<MutableList<MedicalFileGroup>>>) {
                Log.d(TAG, "loadMedicalFileGroups -> onNext -> code = ${t.code()}")
                Log.d(TAG, "loadMedicalFileGroups -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "loadMedicalFileGroups -> onNext -> data size = ${t.body()!!.data!!.size}")
                    view.updateMedicalFileGroups(t.body()!!.data!!)
                } else
                    view.showError(R.string.something_wrong)
                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "loadMedicalFileGroups -> onError -> ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
            }
        })
    }

    override fun deleteMedicalFileGroup(groupId: Long) {
        view.showDeleteProgressDialog()
        subscribe(dataManager.deleteMedicalFileGroup(groupId), object :Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "deleteMedicalFileGroup -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "deleteMedicalFileGroup -> onSubscribe")
                DisposableManager.addMedicalFilesDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "deleteMedicalFileGroup -> onNext -> code = ${t.code()}")
                Log.d(TAG, "deleteMedicalFileGroup -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status)
                    view.removeDeletedGroup(groupId)
                else
                    view.showError(R.string.something_wrong)
                view.dismissDeleteProgressDialog()
                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "deleteMedicalFileGroup -> onError -> ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
                view.dismissDeleteProgressDialog()
            }
        })
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeMedicalFiles()
    }
}