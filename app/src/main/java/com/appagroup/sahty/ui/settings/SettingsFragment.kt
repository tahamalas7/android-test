package com.appagroup.sahty.ui.settings

import android.content.Intent
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.ui.splash.SplashScreenActivity
import com.appagroup.sahty.utils.Languages
import com.appagroup.sahty.utils.LocalHelper
import kotlinx.android.synthetic.main.dialog_language.view.*
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : BaseFragment<SettingsContract.Presenter>(), SettingsContract.View {

    override fun instantiatePresenter(): SettingsContract.Presenter = SettingsPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_settings

    override fun initViews(view: View) {
        // hide search bar
        (activity as MainActivity).changeSearchBarVisibility(View.GONE)

        if (presenter.getMobileDataStatus())
            settings_data_checkBox.isChecked = true

        if (presenter.getNotificationsStatus())
            settings_notifications_checkBox.isChecked = true
        setListeners()
    }

    override fun setListeners() {
        settings_back_arrow.setOnClickListener {
            activity!!.onBackPressed()
        }

        settings_notifications_checkBox.setOnCheckedChangeListener { _, isChecked ->
            presenter.setNotificationsStatus(isChecked)
        }

        settings_data_checkBox.setOnCheckedChangeListener { _, isChecked ->
            presenter.setMobileDataStatus(isChecked)
        }

        settings_change_language_layout.setOnClickListener {
            showLanguagesDialog()
        }
    }

    override fun showLanguagesDialog() {
        val builder = AlertDialog.Builder(activity!!)
        val view = activity!!.layoutInflater.inflate(R.layout.dialog_language, null)

        if (presenter.getApplicationLanguage() == Languages.ENGLISH_LANGUAGE)
            view.settings_english_radBtn.isChecked = true
        else view.settings_arabic_radBtn.isChecked = true

        view.settings_english_radBtn.setOnClickListener {
            if (presenter.getApplicationLanguage() != Languages.ENGLISH_LANGUAGE) {
                view.settings_english_radBtn.isChecked = true
                presenter.setApplicationLanguage(Languages.ENGLISH_LANGUAGE)
                LocalHelper.setLocale(activity!!, Languages.ENGLISH_LANGUAGE)
                startActivity(Intent(activity, SplashScreenActivity::class.java))
                activity!!.finish()
            }
        }

        view.settings_arabic_radBtn.setOnClickListener {
            if (presenter.getApplicationLanguage() != Languages.ARABIC_LANGUAGE) {
                view.settings_arabic_radBtn.isChecked = true
                presenter.setApplicationLanguage(Languages.ARABIC_LANGUAGE)
                LocalHelper.setLocale(activity!!, Languages.ARABIC_LANGUAGE)
                startActivity(Intent(activity, SplashScreenActivity::class.java))
                activity!!.finish()
            }
        }

        builder.setView(view)
        val dialog = builder.create()
        dialog.show()
    }

    override fun onPause() {
        super.onPause()
        (activity as MainActivity).changeSearchBarVisibility(View.VISIBLE)
    }

    companion object {
        val TAG = "SettingsFragment"
        fun getInstance(): SettingsFragment =
                SettingsFragment()
    }
}