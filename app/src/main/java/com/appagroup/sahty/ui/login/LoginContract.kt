package com.appagroup.sahty.ui.login

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.User

interface LoginContract {

    interface View : IBaseView {

        fun setListeners()

        fun checkData(): Boolean

        fun showProgressBar()

        fun hideProgressBar()

        fun changeLoginProcessStatus(status: Boolean)

        fun getString(string_id: Int): String

        fun loginSuccessfully(username: String)

        fun initializeFacebookAuth()

        fun initializeGmailAuth()

        fun initializeInstagramAuth()

        fun openSelectRoleDialog(username: String)
    }

    interface Presenter : IBasePresenter<View> {

        fun login(email: String, password: String, fcm_token: String)

        fun loginWithFacebook(fcm_token: String)

        fun socialLogin(name: String, email: String, fcm_token: String, social_token: String, provider: String)

        fun saveUserData(user: User)

        fun clearUserData()

        fun setUserRole(role: Int)

        fun requestInstagramAPI(authToken: String, fcm_token: String)
    }
}