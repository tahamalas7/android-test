package com.appagroup.sahty.ui.search

import android.content.Intent
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.Article
import com.appagroup.sahty.entity.Doctor
import com.appagroup.sahty.entity.Facility
import com.appagroup.sahty.entity.GeneralSearchResult
import com.appagroup.sahty.ui.articles.article_content.ArticleContentActivity
import com.appagroup.sahty.ui.facilities.hospitals.hospital_details.HospitalDetailsFragment
import com.appagroup.sahty.ui.facilities.pharmacies.pharmacy_details.PharmacyDetailsFragment
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.visit_profile.VisitProfileActivity
import com.appagroup.sahty.utils.GeneralSearch
import com.appagroup.sahty.utils.UIUtils
import kotlinx.android.synthetic.main.activity_search.*
import com.appagroup.sahty.utils.Facility as FacilityType

class SearchActivity : BaseActivity<SearchContract.Presenter>(), SearchContract.View {

    private val TAG = "SearchActivity"
    private val facilitiesAdapter = FacilityResultsAdapter()
    private val articlesAdapter = ArticleResultsAdapter()
    private val doctorsAdapter = DoctorResultsAdapter()
    private var searchFilter = GeneralSearch.NO_FILTER
    private var searchInProgress = false

    override fun instantiatePresenter(): SearchContract.Presenter = SearchPresenter(this)

    override fun getContentView(): Int = R.layout.activity_search

    override fun initViews() {
        search_facilities_recyclerView.layoutManager = LinearLayoutManager(this)
        search_facilities_recyclerView.adapter = facilitiesAdapter

        search_articles_recyclerView.layoutManager = LinearLayoutManager(this)
        search_articles_recyclerView.adapter = articlesAdapter

        search_doctors_recyclerView.layoutManager = LinearLayoutManager(this)
        search_doctors_recyclerView.adapter = doctorsAdapter

        setListeners()
    }

    override fun setListeners() {
        search_facilities_filter.setOnClickListener {
            selectFacilitiesFilter()
            makeSearch()
        }

        search_articles_filter.setOnClickListener {
            selectArticlesFilter()
            makeSearch()
        }

        search_doctors_filter.setOnClickListener {
            selectDoctorsFilter()
            makeSearch()
        }

        search_view_more_facilities.setOnClickListener {
            selectFacilitiesFilter()
            makeSearch()
        }

        search_view_more_articles.setOnClickListener {
            selectArticlesFilter()
            makeSearch()
        }

        search_view_more_doctors.setOnClickListener {
            selectDoctorsFilter()
            makeSearch()
        }

        search_go.setOnClickListener {
            makeSearch()
            UIUtils.hideSoftKeyboard(this, search_editText)
            Log.d(TAG, "searchFilter = $searchFilter")
        }

        // detect text change of search EditText
        search_editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                Log.d(TAG, "onTextChanged -> text = ${s.toString()}")
                presenter.disposeSearch()
                presenter.resetPaginate()
            }
        })

        facilitiesAdapter.setOnFacilityClickListener(object : ItemClickListener<Facility> {
            override fun onClick(item: Facility) {
                when (item.type) {
                    FacilityType.TYPE_HOSPITAL -> {
                        val hospitalDetailsFragment = HospitalDetailsFragment.newInstance(item.id)
                        hospitalDetailsFragment.show(supportFragmentManager, HospitalDetailsFragment.TAG)
                    }

                    FacilityType.TYPE_PHARMACY -> {
                        val pharmacyDetailsFragment = PharmacyDetailsFragment.newInstance(item.id)
                        pharmacyDetailsFragment.show(supportFragmentManager, PharmacyDetailsFragment.TAG)
                    }
                }
            }
        })

        facilitiesAdapter.setOnBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                if (searchFilter == GeneralSearch.FACILITIES_FILTER)
                    makeSearch()
            }
        })

        articlesAdapter.setOnArticleClickListener(object : ItemClickListener<Article> {
            override fun onClick(item: Article) {
                val intent = Intent(this@SearchActivity, ArticleContentActivity::class.java)
                intent.putExtra(ArticleContentActivity.ARTICLE_ID, item.id)
                startActivity(intent)
            }
        })

        articlesAdapter.setOnBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                if (searchFilter == GeneralSearch.ARTICLES_FILTER)
                    makeSearch()
            }
        })

        doctorsAdapter.setOnDoctorClickListener(object : ItemClickListener<Doctor> {
            override fun onClick(item: Doctor) {
                val intent = Intent(this@SearchActivity, VisitProfileActivity::class.java)
                intent.putExtra(VisitProfileActivity.USER_ID, item.id)
                intent.putExtra(VisitProfileActivity.USER_NAME, item.name)
                startActivity(intent)
            }
        })

        doctorsAdapter.setOnBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                if (searchFilter == GeneralSearch.DOCTORS_FILTER)
                    makeSearch()
            }
        })
    }

    override fun setGeneralSearchResults(generalSearchResult: GeneralSearchResult) {
        search_filter_bar.visibility = View.VISIBLE
        clearFacilityItems()
        clearArticleItems()
        clearDoctorItems()

        if (generalSearchResult.facilities == null || generalSearchResult.facilities.isEmpty())
            search_facilities_bar.visibility = View.GONE
        else {
            search_facilities_bar.visibility = View.VISIBLE
            search_view_more_facilities.visibility = View.VISIBLE
            facilitiesAdapter.updateFacilities(generalSearchResult.facilities)
        }

        if (generalSearchResult.articles == null || generalSearchResult.articles.isEmpty())
            search_articles_bar.visibility = View.GONE
        else {
            search_articles_bar.visibility = View.VISIBLE
            search_view_more_articles.visibility = View.VISIBLE
            articlesAdapter.updateArticles(generalSearchResult.articles)
        }

        if (generalSearchResult.doctors == null || generalSearchResult.doctors.isEmpty())
            search_doctors_bar.visibility = View.GONE
        else {
            search_doctors_bar.visibility = View.VISIBLE
            search_view_more_doctors.visibility = View.VISIBLE
            doctorsAdapter.updateDoctors(generalSearchResult.doctors)
        }
    }

    private fun makeSearch() {
        if (!searchInProgress && search_editText.text.toString().trim().isNotEmpty())
            when (searchFilter) {
                GeneralSearch.NO_FILTER -> presenter.makeGeneralSearch(search_editText.text.toString().trim())

                GeneralSearch.FACILITIES_FILTER -> presenter.searchForFacilities(search_editText.text.toString().trim(), searchFilter)

                GeneralSearch.ARTICLES_FILTER -> presenter.searchForArticles(search_editText.text.toString().trim(), searchFilter)

                GeneralSearch.DOCTORS_FILTER -> presenter.searchForDoctors(search_editText.text.toString().trim(), searchFilter)
            }
    }

    override fun setFacilitiesSearchResults(facilities: MutableList<Facility>?) {
        search_facilities_bar.visibility = View.VISIBLE
        search_articles_bar.visibility = View.GONE
        search_doctors_bar.visibility = View.GONE

        if (facilities == null || (facilities.isEmpty() && facilitiesAdapter.itemCount == 0)) {
            search_facilities_recyclerView.visibility = View.GONE
            search_facilities_no_results_message.visibility = View.VISIBLE
        } else {
            search_facilities_recyclerView.visibility = View.VISIBLE
            search_facilities_no_results_message.visibility = View.GONE
            facilitiesAdapter.updateFacilities(facilities)
        }

        search_view_more_facilities.visibility = View.GONE
    }

    override fun setArticlesSearchResults(articles: MutableList<Article>?) {
        search_facilities_bar.visibility = View.GONE
        search_articles_bar.visibility = View.VISIBLE
        search_doctors_bar.visibility = View.GONE

        if (articles == null || (articles.isEmpty() && articlesAdapter.itemCount == 0)) {
            search_articles_recyclerView.visibility = View.GONE
            search_articles_no_results_message.visibility = View.VISIBLE
        } else {
            search_articles_recyclerView.visibility = View.VISIBLE
            search_articles_no_results_message.visibility = View.GONE
            articlesAdapter.updateArticles(articles)
        }

        search_view_more_articles.visibility = View.GONE
    }

    override fun setDoctorsSearchResults(doctors: MutableList<Doctor>?) {
        search_facilities_bar.visibility = View.GONE
        search_articles_bar.visibility = View.GONE
        search_doctors_bar.visibility = View.VISIBLE

        if (doctors == null || (doctors.isEmpty() && doctorsAdapter.itemCount == 0)) {
            search_doctors_recyclerView.visibility = View.GONE
            search_doctors_no_results_message.visibility = View.VISIBLE
        } else {
            search_doctors_recyclerView.visibility = View.VISIBLE
            search_doctors_no_results_message.visibility = View.GONE
            doctorsAdapter.updateDoctors(doctors)
        }

        search_view_more_doctors.visibility = View.GONE
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        search_progressBar.visibility = visibility
    }

    override fun setSearchProgressStatus(status: Boolean) {
        searchInProgress = status
    }

    override fun clearFacilityItems() = facilitiesAdapter.clearItems()

    override fun clearArticleItems() = articlesAdapter.clearItems()

    override fun clearDoctorItems() = doctorsAdapter.clearItems()

    private fun selectFacilitiesFilter() {
        presenter.resetPaginate()
        presenter.disposeSearch()

        searchFilter = if (searchFilter != GeneralSearch.FACILITIES_FILTER) {
            search_facilities_filter.setBackgroundResource(R.drawable.back_button_enabled)
            search_articles_filter.setBackgroundResource(R.drawable.back_button_disabled)
            search_doctors_filter.setBackgroundResource(R.drawable.back_button_disabled)
            GeneralSearch.FACILITIES_FILTER
        } else {
            search_facilities_filter.setBackgroundResource(R.drawable.back_button_disabled)
            GeneralSearch.NO_FILTER
        }
    }

    private fun selectArticlesFilter() {
        presenter.resetPaginate()
        presenter.disposeSearch()

        searchFilter = if (searchFilter != GeneralSearch.ARTICLES_FILTER) {
            search_facilities_filter.setBackgroundResource(R.drawable.back_button_disabled)
            search_articles_filter.setBackgroundResource(R.drawable.back_button_enabled)
            search_doctors_filter.setBackgroundResource(R.drawable.back_button_disabled)
            GeneralSearch.ARTICLES_FILTER
        } else {
            search_articles_filter.setBackgroundResource(R.drawable.back_button_disabled)
            GeneralSearch.NO_FILTER
        }
    }

    private fun selectDoctorsFilter() {
        presenter.resetPaginate()
        presenter.disposeSearch()

        searchFilter = if (searchFilter != GeneralSearch.DOCTORS_FILTER) {
            search_facilities_filter.setBackgroundResource(R.drawable.back_button_disabled)
            search_articles_filter.setBackgroundResource(R.drawable.back_button_disabled)
            search_doctors_filter.setBackgroundResource(R.drawable.back_button_enabled)
            GeneralSearch.DOCTORS_FILTER
        } else {
            search_doctors_filter.setBackgroundResource(R.drawable.back_button_disabled)
            GeneralSearch.NO_FILTER
        }
    }
}