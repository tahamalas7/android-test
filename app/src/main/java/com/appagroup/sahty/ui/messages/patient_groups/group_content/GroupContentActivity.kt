package com.appagroup.sahty.ui.messages.patient_groups.group_content

import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.GridLayoutManager
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.MedicalFile
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.medical_files.group_content.MedicalFileGroupContentFragment
import com.appagroup.sahty.utils.FileHandler
import kotlinx.android.synthetic.main.fragment_medical_file_group_content.*
import com.appagroup.sahty.ui.messages.patient_groups.group_content.GroupContentContract as Contract

class GroupContentActivity : BaseActivity<Contract.Presenter>(), Contract.View {

    private val adapter = MedicalFileAdapter()
    private var groupName = ""
    private var patientId = 0L
    private var groupId = 0L

    override fun instantiatePresenter(): Contract.Presenter = GroupContentPresenter(this)

    override fun getContentView(): Int = R.layout.fragment_medical_file_group_content

    override fun initViews() {
        group_files_recyclerView.layoutManager = GridLayoutManager(this, 3)
        group_files_recyclerView.adapter = adapter
        group_files_recyclerView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                group_files_recyclerView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                adapter.updateItemHeight(group_files_recyclerView.height / 3)
            }
        })

        groupName = intent!!.getStringExtra(GROUP_NAME)
        groupId = intent!!.getLongExtra(GROUP_ID, 0L)
        patientId = intent!!.getLongExtra(PATIENT_ID, 0L)

        group_content_title.text = groupName
        group_content_title.visibility = View.VISIBLE

        presenter.getMedicalFiles(patientId, groupId)
        setListeners()

        Log.d(TAG, "PATIENT ID = $patientId")
        Log.d(TAG, "GROUP ID = $groupId")
        Log.d(TAG, "GROUP NAME = $groupName")
    }

    override fun setListeners() {
        group_content_back_arrow.setOnClickListener { onBackPressed() }

        adapter.setOnMedicalFileClickListener(object : ItemClickListener<MedicalFile> {
            override fun onClick(item: MedicalFile) {
                // open file
                val intent = FileHandler.getFileIntent(BuildConfig.IMAGE + item.url)
                Log.d("presenter", "URI = ${BuildConfig.IMAGE + item.url}")
                try {
                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.d(MedicalFileGroupContentFragment.TAG, "error open file = ${e.message}")
                }
            }
        })
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        group_content_progressBar.visibility = visibility
    }

    override fun updateFiles(files: MutableList<MedicalFile>) {
        adapter.updateFiles(files)
        Log.d(TAG, "updateFiles, size = ${files.size}")
    }

    companion object {
        val TAG = "GroupContentActivity"
        val GROUP_NAME = "GROUP_NAME"
        val PATIENT_ID = "PATIENT_ID"
        val GROUP_ID = "GROUP_ID"
    }
}