package com.appagroup.sahty.ui

import android.util.Log
import com.appagroup.sahty.base.DataManagerProvider
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.utils.DisposableManager
import com.appagroup.sahty.utils.subscribe
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class SaveService : DataManagerProvider() {

    private val TAG = "FollowService"

    fun save(id: Long, type: Int) {
        subscribe(dataManager.save(id, type), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "save -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "save -> onSubscribe")
                DisposableManager.addSaveDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "save -> onNext -> code = ${t.code()}")
                Log.d(TAG, "save -> onNext -> status = ${t.body()!!.status}")
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "save -> onError -> error = ${e.message}")
            }
        })
    }

    fun unsave(id: Long, type: Int) {
        subscribe(dataManager.unsave(id, type), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "unsave -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "unsave -> onSubscribe")
                DisposableManager.addSaveDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "unsave -> onNext -> code = ${t.code()}")
                Log.d(TAG, "unsave -> onNext -> status = ${t.body()!!.status}")
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "unsave -> onError -> error = ${e.message}")
            }
        })
    }

    fun dispose() {
        DisposableManager.disposeSave()
    }
}