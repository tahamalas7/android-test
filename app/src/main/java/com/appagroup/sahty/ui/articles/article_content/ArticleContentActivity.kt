package com.appagroup.sahty.ui.articles.article_content

import android.app.Activity
import android.content.Intent
import android.widget.ImageView
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.ArticleContent
import com.appagroup.sahty.entity.Media
import com.appagroup.sahty.ui.SaveService
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.OnClick
import com.appagroup.sahty.ui.videos.VideoPlayerActivity
import com.appagroup.sahty.ui.visit_profile.VisitProfileActivity
import com.appagroup.sahty.utils.SaveTypes
import com.facebook.drawee.backends.pipeline.Fresco
import com.like.LikeButton
import com.like.OnLikeListener
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_article_content.*

class ArticleContentActivity : BaseActivity<ArticleContentContract.Presenter>(), ArticleContentContract.View {

    private var VISIT_PROFILE_REQUEST_CODE = 78
    private var articleId = 0L
    private var isArticleUnsaved = false  // used when start this activity from SavedArticleActivity or SavedStoriesActivity
    private val mediaAdapter = MediaAdapter()
    private val saveService = SaveService()
    private lateinit var article: ArticleContent
    private lateinit var pagerDotsIndicator: MutableList<ImageView>

    override fun instantiatePresenter(): ArticleContentContract.Presenter = ArticleContentPresenter(this)

    override fun getContentView(): Int = R.layout.activity_article_content

    override fun initViews() {
        article_content_media_pager.adapter = mediaAdapter
        if (intent != null)
            articleId = intent.getLongExtra(ARTICLE_ID, 0)
        presenter.getArticleContent(articleId)
        pagerDotsIndicator = mutableListOf()

        setListeners()
    }

    override fun setListeners() {
        article_content_back_arrow.setOnClickListener {
            onBackPressed()
        }

        article_content_writer_name.setOnClickListener {
            if (::article.isInitialized && article.writer.id != presenter.getUserId()) {
                val intent = Intent(this, VisitProfileActivity::class.java)
                intent.putExtra(VisitProfileActivity.USER_ID, article.writer.id)
                intent.putExtra(VisitProfileActivity.USER_NAME, article.writer.name)
                startActivityForResult(intent, VISIT_PROFILE_REQUEST_CODE)
            }
        }

        article_content_like_btn.setOnLikeListener(object : OnLikeListener {
            override fun liked(p0: LikeButton?) {
                if (::article.isInitialized) {
                    presenter.likeArticle(articleId)
                    article.likeCount++
                    article_content_like_count.text = article.likeCount.toString()
                }
            }

            override fun unLiked(p0: LikeButton?) {
                if (::article.isInitialized) {
                    presenter.unlikeArticle(articleId)
                    article.likeCount--
                    article_content_like_count.text = article.likeCount.toString()
                }
            }
        })

        article_content_options.setOnClickListener {
            if (::article.isInitialized) {
                val dialog =
                        SaveBottomSheetDialog.newInstance(article.isSaved, getString(R.string.article))
                dialog.show(supportFragmentManager, "SaveBottomSheetDialog")

                dialog.setOnBottomSheetClickListener(object : OnClick {
                    override fun onClick() {
                        if (article.isSaved) {
                            saveService.unsave(articleId, SaveTypes.ARTICLE_OR_STORY)
                            article.isSaved = false
                            isArticleUnsaved = true
                        } else {
                            saveService.save(articleId, SaveTypes.ARTICLE_OR_STORY)
                            article.isSaved = true
                            isArticleUnsaved = false
                        }
                        dialog.dismiss()
                    }
                })
            }
        }

        mediaAdapter.setOnVideoPlayButtonClickListener(object : ItemClickListener<String> {
            override fun onClick(item: String) {
                val intent = Intent(this@ArticleContentActivity, VideoPlayerActivity::class.java)
                intent.putExtra(VideoPlayerActivity.VIDEO_URL, item)
                intent.putExtra(VideoPlayerActivity.VIDEO_SOURCE, VideoPlayerActivity.TYPE_EXTERNAL)
                startActivity(intent)
            }
        })

        mediaAdapter.setOnImageClickListener(object : ItemClickListener<Int> {
            override fun onClick(item: Int) {
                Fresco.initialize(this@ArticleContentActivity)
                ImageViewer.Builder(this@ArticleContentActivity, mediaAdapter.getAllMediaItems())
                        .setStartPosition(item)
                        .setFormatter(ImageViewer.Formatter<Media> { image -> BuildConfig.IMAGE + image.url })
                        .show()
            }
        })
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        article_content_progressBar.visibility = visibility
    }

    override fun changeContentVisibility(visibility: Int) {
        article_content_container.visibility = visibility
        article_content_options.visibility = visibility
    }

    override fun setArticleContent(articleContent: ArticleContent) {
        article_content_like_count.text = articleContent.likeCount.toString()
        article_content_text.text = articleContent.content
        article_content_title.text = articleContent.title
//        article_content_date.text = UIUtils.getTimeAgo(articleContent.date, this)
        article_content_writer_name.text = "${getString(R.string.by)} ${articleContent.writer.name}"

        if (articleContent.isLiked)
            article_content_like_btn.isLiked = true

        if (articleContent.media != null) {
            mediaAdapter.updateMediaItems(articleContent.media)
            if (articleContent.media.size > 1)
                article_content_media_pager_indicator.setupWithViewPager(article_content_media_pager)
        }
        article = articleContent
        articleId = articleContent.id
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == VISIT_PROFILE_REQUEST_CODE) {

        }
    }

    override fun onPause() {
        super.onPause()
        val intent = Intent()
        if (::article.isInitialized) {
            intent.putExtra(ARTICLE_ID, articleId)
            intent.putExtra(IS_ARTICLE_UNSAVED, isArticleUnsaved)
            intent.putExtra(ARTICLE_WRITER_FOLLOW_STATUS, article.writer.isFollowed)
            setResult(Activity.RESULT_OK, intent)
        } else setResult(Activity.RESULT_CANCELED, intent)
    }

    companion object {
        val TAG = "ArticleContentActivity"
        val ARTICLE_ID = "ARTICLE_ID"
        val IS_ARTICLE_UNSAVED = "IS_ARTICLE_UNSAVED"
        val ARTICLE_WRITER_FOLLOW_STATUS = "ARTICLE_WRITER_FOLLOW_STATUS"
    }
}