package com.appagroup.sahty.ui.medical_files.group_content.import_files.edit_file_name

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView

interface EditFileNameContract {

    interface View : IBaseView {

    }

    interface Presenter : IBasePresenter<View> {


    }
}