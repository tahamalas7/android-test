package com.appagroup.sahty.ui.facilities.pharmacies

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Pharmacy
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_pharmacy.view.*

class PharmacyAdapter : RecyclerView.Adapter<PharmacyAdapter.PharmacyHolder>() {

    private var itemHeight: Int = 400
    private val pharmacies: MutableList<Pharmacy> = mutableListOf()
    private lateinit var pharmacyClickListener: ItemClickListener<Pharmacy>
    private lateinit var bottomReachListener: BottomReachListener

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): PharmacyHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pharmacy, parent, false)
        return PharmacyHolder(view)
    }

    override fun getItemCount(): Int = pharmacies.size

    override fun onBindViewHolder(holder: PharmacyHolder, position: Int) {
        val params = holder.itemView.layoutParams
        params.height = itemHeight
        holder.itemView.layoutParams = params

        val pharmacy = pharmacies[position]
        holder.bind(pharmacy)

        holder.itemView.setOnClickListener {
            pharmacyClickListener.onClick(pharmacy)
        }

        if (position > itemCount - 2)
            bottomReachListener.onBottomReach()
    }

    fun updatePharmacies(pharmacies: MutableList<Pharmacy>) {
        this.pharmacies.addAll(pharmacies)
        notifyDataSetChanged()
    }

    fun updateItemHeight(itemHeight: Int) {
        this.itemHeight = itemHeight
    }

    fun setOnPharmaciesClickListener(pharmacyClickListener: ItemClickListener<Pharmacy>) {
        this.pharmacyClickListener = pharmacyClickListener
    }

    fun setOnBottomReachListener(bottomReachListener: BottomReachListener) {
        this.bottomReachListener = bottomReachListener
    }

    inner class PharmacyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(pharmacy: Pharmacy) {
            itemView.pharmacy_name.text = pharmacy.name
            itemView.pharmacy_location.text = pharmacy.location
            itemView.pharmacy_rate.text = if (pharmacy.rate != null)
                pharmacy.rate.toString()
            else
                0F.toString()

            if (pharmacy.image != null)
                Glide.with(itemView)
                        .load(BuildConfig.IMAGE + pharmacy.image)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?,
                                                      model: Any?,
                                                      target: Target<Drawable>?,
                                                      isFirstResource: Boolean): Boolean {
                                itemView.pharmacy_image_progressBar.visibility = View.GONE
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?,
                                                         model: Any?,
                                                         target: Target<Drawable>?,
                                                         dataSource: DataSource?,
                                                         isFirstResource: Boolean): Boolean {
                                itemView.pharmacy_image_progressBar.visibility = View.GONE
                                return false
                            }
                        })
                        .into(itemView.pharmacy_image)
            else
                itemView.pharmacy_image_progressBar.visibility = View.GONE
        }
    }
}