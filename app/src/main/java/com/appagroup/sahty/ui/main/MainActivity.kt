package com.appagroup.sahty.ui.main

import android.app.Activity
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.MenuItem
import com.appagroup.sahty.ui.facilities.FacilitiesFragment
import com.appagroup.sahty.ui.facilities.rating_dialog.RatingDialog
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.login.LoginActivity
import com.appagroup.sahty.ui.login.select_role.RoleSubmitListener
import com.appagroup.sahty.ui.login.select_role.SelectRoleDialog
import com.appagroup.sahty.ui.medical_files.MyMedicalFilesFragment
import com.appagroup.sahty.ui.medical_files.group_content.MedicalFileGroupContentFragment
import com.appagroup.sahty.ui.messages.MessagesFragment
import com.appagroup.sahty.ui.poll.PollsFragment
import com.appagroup.sahty.ui.post_article.PostArticleFragment
import com.appagroup.sahty.ui.post_story.PostStoryFragment
import com.appagroup.sahty.ui.profile.ProfileFragment
import com.appagroup.sahty.ui.saved.SavedFragment
import com.appagroup.sahty.ui.search.SearchActivity
import com.appagroup.sahty.ui.settings.SettingsFragment
import com.appagroup.sahty.ui.videos.VideosFragment
import com.appagroup.sahty.utils.*
import com.facebook.login.LoginManager
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.menu_header.*
import java.util.*


class MainActivity : BaseActivity<MainContract.Presenter>(), MainContract.View, LocationListener {

    val TAG = "MainActivity"
    private lateinit var postArticleFragment: PostArticleFragment
    private lateinit var postStoryFragment: PostStoryFragment
    private lateinit var profileFragment: ProfileFragment
    private lateinit var mainPagerAdapter: MainPagerAdapter

    override fun instantiatePresenter(): MainContract.Presenter = MainPresenter(this)

    override fun getContentView(): Int = R.layout.activity_main

    override fun initViews() {
        /** set user role as 'Confirmed Doctor' when launch this activity from notification */
        if (intent.getBooleanExtra(FROM_NOTIFICATION, false))
            presenter.setUserRole(UserRole.ROLE_CONFIRMED_DOCTOR)

        setListeners()
        initSideMenu()

        /** set application language & layout direction */
        val locale: Locale = if (presenter.getApplicationLanguage() == Languages.ARABIC_LANGUAGE)
            Locale("ar")
        else Locale("en")

        val configuration = resources.configuration
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)
        resources.updateConfiguration(configuration, resources.displayMetrics)

        mainPagerAdapter = MainPagerAdapter(supportFragmentManager)
        main_viewPager.adapter = mainPagerAdapter
        main_viewPager.offscreenPageLimit = 3

        // setup Bottom Navigation Bar and link it with main_viewPager
        bottom_navigation_bar.itemIconTintList = null
        bottom_navigation_bar.setOnNavigationItemSelectedListener(object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: android.view.MenuItem): Boolean {
                when (item.itemId) {
                    R.id.bottom_navigation_centers -> main_viewPager.currentItem = 0
                    R.id.bottom_navigation_videos -> main_viewPager.currentItem = 1
                    R.id.bottom_navigation_home -> main_viewPager.currentItem = 2
                    R.id.bottom_navigation_medical_files -> main_viewPager.currentItem = 3
                }
                if (sub_fragment_container.visibility == View.VISIBLE)
                    hideSubFragmentContainer()
                return true
            }
        })

        main_viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {}
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {}
            override fun onPageSelected(p0: Int) {
                when (p0) {
                    0 -> {
                        bottom_navigation_bar.selectedItemId = R.id.bottom_navigation_centers
                        (mainPagerAdapter.getItem(MainPagerAdapter.FACILITIES_FRAGMENT)
                                as FacilitiesFragment).onResumeFragment()
                    }

                    1 -> {
                        bottom_navigation_bar.selectedItemId = R.id.bottom_navigation_videos
                        (mainPagerAdapter.getItem(MainPagerAdapter.VIDEOS_FRAGMENT)
                                as VideosFragment).onResumeFragment()
                    }

                    2 -> bottom_navigation_bar.selectedItemId = R.id.bottom_navigation_home

                    3 -> {
                        bottom_navigation_bar.selectedItemId = R.id.bottom_navigation_medical_files
                        (mainPagerAdapter.getItem(MainPagerAdapter.MEDICAL_FILES_FRAGMENT)
                                as MyMedicalFilesFragment).onResumeFragment()
                    }
                }
            }
        })
        bottom_navigation_bar.selectedItemId = R.id.bottom_navigation_home

        if (presenter.getUserRole() == UserRole.ROLE_UNKNOWN_USER) {
            val dialog = SelectRoleDialog.getInstance()
            dialog.show(supportFragmentManager, SelectRoleDialog.TAG)

            dialog.setOnRoleSubmitListener(object : RoleSubmitListener {
                override fun roleSubmittedSuccessfully(role: Int) {
                    presenter.setUserRole(role)
                    dialog.dismiss()
                }

                override fun dialogDismissed() {
                    presenter.clearUserData()
                    startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                }
            })
        }
    }

    override fun setListeners() {
        menu_button.setOnClickListener {
            if (drawer_layout.isDrawerVisible(GravityCompat.START))
                drawer_layout.closeDrawer(GravityCompat.START)
            else {
                drawer_layout.openDrawer(GravityCompat.START)
                changeStatusBarColor(R.color.menuStartColor)
            }
        }

        drawer_layout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerStateChanged(p0: Int) {}

            override fun onDrawerSlide(p0: View, p1: Float) {}

            override fun onDrawerClosed(p0: View) {
                changeStatusBarColor(R.color.colorPrimaryDark)
            }

            override fun onDrawerOpened(p0: View) {}
        })

        search_bar.setOnClickListener {
            startActivity(Intent(this, SearchActivity::class.java))
        }
    }

    private fun changeStatusBarColor(color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = resources.getColor(color)
        }
    }

    private fun initSideMenu() {
        val adapter = MenuItemsAdapter(presenter.getUserRole())
        menu_recyclerView.layoutManager = LinearLayoutManager(this)
        menu_recyclerView.adapter = adapter

        presenter.getUserData()

        adapter.setMenuItemClickListener(object : ItemClickListener<MenuItem> {
            override fun onClick(item: MenuItem) {
                when (item.id) {
                    Menu.MENU_HOME -> {
                        hideMainFragmentContainer()
                        main_viewPager.currentItem = 2
                    }

                    Menu.MENU_MESSAGES -> {
                        showMainFragmentContainer()
                        startFragment(
                                MessagesFragment.getInstance(),
                                MessagesFragment.TAG,
                                MAIN_FRAGMENT_CONTAINER)
                    }

                    Menu.MENU_POLLS -> {
                        showMainFragmentContainer()
                        startFragment(
                                PollsFragment.getInstance(),
                                PollsFragment.TAG,
                                MAIN_FRAGMENT_CONTAINER)
                    }

                    Menu.MENU_PROFILE -> {
                        showMainFragmentContainer()
                        profileFragment = ProfileFragment.getInstance()
                        startFragment(
                                profileFragment,
                                ProfileFragment.TAG,
                                MAIN_FRAGMENT_CONTAINER)
                    }

                    Menu.MENU_SAVED -> {
                        showMainFragmentContainer()
                        startFragment(
                                SavedFragment.getInstance(),
                                SavedFragment.TAG,
                                MAIN_FRAGMENT_CONTAINER)
                    }

                    Menu.MENU_FEEDBACK -> {
                        val dialog = RatingDialog.getInstance(-1)
                        dialog.show(supportFragmentManager, FEEDBACK_TAG)
                    }

                    Menu.MENU_SETTINGS -> {
                        showMainFragmentContainer()
                        startFragment(
                                SettingsFragment.getInstance(),
                                SettingsFragment.TAG,
                                MAIN_FRAGMENT_CONTAINER)
                    }

                    Menu.MENU_POST_ARTICLE -> {
                        showMainFragmentContainer()
                        postArticleFragment = PostArticleFragment.getInstance()
                        startFragment(
                                postArticleFragment,
                                PostArticleFragment.TAG,
                                MAIN_FRAGMENT_CONTAINER)
                    }

                    Menu.MENU_POST_STORY -> {
                        showMainFragmentContainer()
                        postStoryFragment = PostStoryFragment.getInstance()
                        startFragment(
                                postStoryFragment,
                                PostStoryFragment.TAG,
                                MAIN_FRAGMENT_CONTAINER)
                    }

                    Menu.MENU_LOGOUT -> openLogoutDialog()
                }

                drawer_layout.closeDrawer(GravityCompat.START)
            }
        })
    }

    fun showSubFragmentContainer() {
        sub_fragment_container.visibility = View.VISIBLE // show sub fragment container
        main_viewPager.visibility = View.GONE // hide main viewPager
    }

    fun hideSubFragmentContainer() {
        sub_fragment_container.removeAllViews() // remove all sub fragment container views
        sub_fragment_container.visibility = View.GONE // hide sub fragment container
        main_viewPager.visibility = View.VISIBLE // show main viewPager
    }

    fun showMainFragmentContainer() {
        sub_fragment_container.removeAllViews() // remove all sub fragment container views in case its open
        sub_fragment_container.visibility = View.GONE // hide sub fragment container in case its open
        bottom_navigation_bar.visibility = View.GONE // hide bottom navigation view
        main_viewPager.visibility = View.GONE // hide main viewPager
        main_fragment_container.visibility = View.VISIBLE // show main fragment container
    }

    fun hideMainFragmentContainer() {
        main_viewPager.visibility = View.VISIBLE // show main viewPager
        bottom_navigation_bar.visibility = View.VISIBLE // show bottom navigation view
        main_fragment_container.removeAllViews() // remove all main fragment container
        main_fragment_container.visibility = View.GONE // hide main fragment container
    }

    fun startFragment(fragment: Fragment, tag: String, fragmentContainer: Int) {
        UIUtils.commitFragment(supportFragmentManager, fragment, tag, fragmentContainer)
    }

    fun changeSearchBarVisibility(visibility: Int) {
        search_bar.visibility = visibility
    }

    private fun openLogoutDialog() {
        AlertDialog.Builder(this)
                .setMessage(R.string.insure_logout)
                .setPositiveButton(R.string.logout) { _, _ ->
                    when {
                        presenter.getProvider() == FACEBOOK ->
                            LoginManager.getInstance().logOut()

                        presenter.getProvider() == GMAIL -> {
                            try {
                                val gso =
                                        GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                                                .requestEmail()
                                                .build()
                                val mGoogleApiClient = GoogleApiClient.Builder(this)
                                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                                        .build()
                                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback {
                                    Log.d(TAG, it.statusMessage)
                                }
                            } catch (illegalStateException: IllegalStateException) {
                                Log.d(TAG, illegalStateException.message)
                            }
                        }

                        presenter.getProvider() == INSTAGRAM -> {

                        }
                    }

                    presenter.clearUserData()
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                .setNegativeButton(R.string.cancel) { _, _ -> }
                .show()
    }

    override fun fillUserData(name: String, email: String, image: String) {
        user_name.text = name
        user_email.text = email
        if (image.isNotEmpty())
            Glide.with(this)
                    .load(BuildConfig.IMAGE + image)
                    .into(user_image)
    }

    override fun onLocationChanged(location: Location?) {
        if (location != null)
            presenter.saveLocation(
                    location.latitude.toFloat(),
                    location.longitude.toFloat()
            )

        if (location != null) {
            Log.d(TAG, "onLocationChanged -> latitude = ${location.latitude.toFloat()},longitude = ${location.longitude.toFloat()}")
            showError("onLocationChanged -> latitude = ${location.latitude.toFloat()},longitude = ${location.longitude.toFloat()}")
        } else
            Log.d(TAG, "onLocationChanged -> location = null")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (fragmentManager.backStackEntryCount > 0) // check if there is still fragments stacked in BackStack
            fragmentManager.popBackStackImmediate() // pop previous fragment
        else {
            if (main_fragment_container.visibility == View.VISIBLE) // check if main fragment container open
                hideMainFragmentContainer() // hide main fragment container
            else
                hideSubFragmentContainer() // hide sub fragment container
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {}

    override fun onProviderEnabled(provider: String?) {}

    override fun onProviderDisabled(provider: String?) {}

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PostArticleFragment.VIDEO_REQUEST_PERMESSIONS_CODE ->
                postArticleFragment.onRequestPermissionsResult(requestCode, permissions, grantResults)

            PostArticleFragment.IMAGES_REQUEST_PERMESSIONS_CODE ->
                postArticleFragment.onRequestPermissionsResult(requestCode, permissions, grantResults)

            PostStoryFragment.REQUEST_PERMESSIONS_CODE ->
                postStoryFragment.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d(TAG, "ImportFilesActivity -> onActivityResult -> requestCode = $requestCode")
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                PostArticleFragment.SELECT_VIDEO_REQUEST_CODE ->
                    postArticleFragment.onActivityResult(requestCode, resultCode, data)

                MedicalFileGroupContentFragment.UPLOAD_REQUEST_CODE ->
                    mainPagerAdapter.getItem(MainPagerAdapter.MEDICAL_FILES_FRAGMENT)
                            .onActivityResult(requestCode, resultCode, data)

                ProfileFragment.EDIT_PROFILE_REQUEST_CODE -> {
                    profileFragment.onActivityResult(requestCode, resultCode, data)
                    user_email.text = presenter.getUserEmail()
                    Glide.with(this)
                            .load(BuildConfig.IMAGE + presenter.getUserPicture())
                            .into(user_image)
                }
            }
    }

    companion object {
        val FEEDBACK_TAG = "FEEDBACK_TAG"
        val FROM_NOTIFICATION = "FROM_NOTIFICATION"
        val MAIN_FRAGMENT_CONTAINER = R.id.main_fragment_container
        val SUB_FRAGMENT_CONTAINER = R.id.sub_fragment_container
    }
}