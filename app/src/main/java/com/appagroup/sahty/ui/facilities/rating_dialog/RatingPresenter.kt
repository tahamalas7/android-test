package com.appagroup.sahty.ui.facilities.rating_dialog

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class RatingPresenter(view: RatingContract.View) : BasePresenter<RatingContract.View>(view), RatingContract.Presenter {

    private val TAG = "RatingPresenter"

    override fun submitRating(facilityId: Long, content: String, rate: Float) {
        Log.d(TAG, "facilityId = $facilityId")
        Log.d(TAG, "feedback = $content")
        Log.d(TAG, "rate = $rate")
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.submitRating(facilityId, content, rate), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {}

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "submitRating -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "submitRating -> onNext -> code = ${t.code()}")
                Log.d(TAG, "submitRating -> onNext -> body = ${t.body()}")
                view.changeProgressBarVisibility(View.GONE)
                view.rateSubmittedSuccessfully()
            }

            override fun onError(e: Throwable) {
                view.changeProgressBarVisibility(View.GONE)
                view.showError(R.string.something_wrong)
                Log.d(TAG, "submitRating -> onError -> error = ${e.message}")
            }
        })
    }
}