package com.appagroup.sahty.ui.facilities.hospitals.hospital_details

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseBottomSheetDialogFragment
import com.appagroup.sahty.entity.HospitalDetails
import com.appagroup.sahty.ui.facilities.rating_dialog.RatingDialog
import com.appagroup.sahty.utils.UIUtils
import kotlinx.android.synthetic.main.fragment_hospital_details.*

class HospitalDetailsFragment : BaseBottomSheetDialogFragment<HospitalDetailsContract.Presenter>(), HospitalDetailsContract.View {

    private lateinit var hospitalDetails: HospitalDetails
    private val specializationsAdapter = SpecializationsAdapter()
    private val openingHoursAdapter = OpeningHoursAdapter()
    private val RATE_DIALOG_REQUEST_CODE = 100

    override fun instantiatePresenter(): HospitalDetailsContract.Presenter = HospitalDetailsPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_hospital_details

    override fun initViews(view: View) {
        hospital_details_hours_recyclerView.layoutManager = LinearLayoutManager(activity)
        hospital_details_hours_recyclerView.adapter = openingHoursAdapter

        hospital_details_specialisations_recyclerView.layoutManager = LinearLayoutManager(activity)
        hospital_details_specialisations_recyclerView.adapter = specializationsAdapter

        Log.d("HospitalDetailPresenter", "arguments = $arguments")
        if (arguments != null)
            presenter.getHospitalDetails(arguments!!.getLong(HOSPITAL_ID))
        else
            showError(R.string.something_wrong)
    }

    override fun setListeners() {
        hospital_details_rate_bar.setOnClickListener {
            if (!hospitalDetails.beenRated) {
                val dialog = RatingDialog.getInstance(arguments!!.getLong(HOSPITAL_ID))
                dialog.setTargetFragment(this, RATE_DIALOG_REQUEST_CODE)
                dialog.show(activity!!.supportFragmentManager, TAG)
            } else
                showError(getString(R.string.err_alreday_hospital_rated))
        }

        hospital_details_location.setOnClickListener {
            UIUtils.openMap(
                    activity!!,
                    hospitalDetails.latitude.toString(),
                    hospitalDetails.longitude.toString(),
                    hospitalDetails.name)
        }
    }

    override fun setHospitalDetails(hospitalDetails: HospitalDetails) {
        this.hospitalDetails = hospitalDetails
        hospital_details_name.text = hospitalDetails.name
        hospital_details_location.text = hospitalDetails.location
        hospital_details_phone_number.text = hospitalDetails.contactInfo
        hospital_details_rate.text = if (hospitalDetails.rate != null)
            hospitalDetails.rate.toString()
        else
            0F.toString()

        if (hospitalDetails.specializations != null && hospitalDetails.specializations.size > 0) {
            specializationsAdapter.updateSpecializations(hospitalDetails.specializations)
        } else hospital_details_specialisations_lbl.visibility = View.GONE

        if (hospitalDetails.openingHours != null && hospitalDetails.openingHours.size > 0) {
            openingHoursAdapter.updateOpeningHours(hospitalDetails.openingHours)
        } else hospital_details_hours_lbl.visibility = View.GONE

        if (hospitalDetails.image != null)
            Glide.with(activity!!)
                    .load(BuildConfig.IMAGE + hospitalDetails.image)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?,
                                                  model: Any?,
                                                  target: Target<Drawable>?,
                                                  isFirstResource: Boolean): Boolean {
                            hospital_details_image_progressBar.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?,
                                                     model: Any?,
                                                     target: Target<Drawable>?,
                                                     dataSource: DataSource?,
                                                     isFirstResource: Boolean): Boolean {
                            hospital_details_image_progressBar.visibility = View.GONE
                            return false
                        }
                    })
                    .into(hospital_details_image)
        else
            hospital_details_image_progressBar.visibility = View.GONE
        setListeners()
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        hospital_details_progressBar.visibility = visibility
    }

    override fun changeContentVisibility(visibility: Int) {
        hospital_details_container.visibility = visibility
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RATE_DIALOG_REQUEST_CODE &&
                data != null &&
                data.getBooleanExtra(RatingDialog.RATED_SUCCESSFULLY, false)) {
            showAlertDialog(R.string.Review_submitted_successfully)
            hospitalDetails.beenRated =
                    data.getBooleanExtra(RatingDialog.RATED_SUCCESSFULLY, false)

        }
    }

    companion object {
        private val HOSPITAL_ID = "HOSPITAL_ID"
        val TAG = "HospitalDetailsFragment"

        fun newInstance(hospitalId: Long): HospitalDetailsFragment {
            val instance = HospitalDetailsFragment()
            val bundle = Bundle()
            bundle.putLong(HOSPITAL_ID, hospitalId)
            instance.arguments = bundle

            return instance
        }
    }
}