package com.appagroup.sahty.ui.profile

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.UserProfile

interface ProfileContract {

    interface View : IBaseView {

        fun setListeners()

        fun changeProgressBarVisibility(visibility: Int)

        fun updateProfile(userProfile: UserProfile)
    }

    interface Presenter : IBasePresenter<View> {

        fun getUserProfile()

        fun getUsername(): String

        fun getUserImage(): String

        fun getUserRole(): Int
    }
}