package com.appagroup.sahty.ui.login.select_role

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class SelectRolePresenter(view: SelectRoleContract.View) : BasePresenter<SelectRoleContract.View>(view), SelectRoleContract.Presenter {

    private val TAG = "SelectRolePresenter"

    override fun setUserRole(role: Int) {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.setRole(role), object : Observer<Response<BaseResponse<Any?>>> {
            override fun onComplete() {
                Log.d(TAG, "setUserRole -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "setUserRole -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<Any?>>) {
                Log.d(TAG, "setUserRole -> onNext -> code = ${t.code()}")
                Log.d(TAG, "setUserRole -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "setUserRole -> onNext -> status = ${t.body()!!.status}")
                    view.roleSubmittedSuccessfully()
                } else {
                    view.showError(R.string.err_set_role)
                    view.changeProgressBarVisibility(View.GONE)
                }
            }

            override fun onError(e: Throwable) {
                view.showError(R.string.err_set_role)
                view.changeProgressBarVisibility(View.GONE)
                Log.d(TAG, "setUserRole -> onError -> error = ${e.message}")
            }
        })
    }
}