package com.appagroup.sahty.ui.search

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Article
import com.appagroup.sahty.entity.Doctor
import com.appagroup.sahty.entity.Facility
import com.appagroup.sahty.entity.GeneralSearchResult

interface SearchContract {

    interface View : IBaseView {

        fun setListeners()

        fun setGeneralSearchResults(generalSearchResult: GeneralSearchResult)

        fun setFacilitiesSearchResults(facilities: MutableList<Facility>?)

        fun setArticlesSearchResults(articles: MutableList<Article>?)

        fun setDoctorsSearchResults(doctors: MutableList<Doctor>?)

        fun changeProgressBarVisibility(visibility: Int)

        fun setSearchProgressStatus(status: Boolean)

        fun clearFacilityItems()

        fun clearArticleItems()

        fun clearDoctorItems()
    }

    interface Presenter : IBasePresenter<View> {

        fun makeGeneralSearch(searchKey: String)

        fun searchForFacilities(searchKey: String, filter: Int)

        fun searchForArticles(searchKey: String, filter: Int)

        fun searchForDoctors(searchKey: String, filter: Int)

        fun resetPaginate()

        fun disposeSearch()
    }
}