package com.appagroup.sahty.ui.listener

import com.appagroup.sahty.entity.Accreditation

interface DialogDismissListener {

    fun accreditationCreated(accreditation: Accreditation)

    fun accreditationEdited(accreditation: Accreditation)

    fun accreditationDeleted(accreditationId: Long)
}