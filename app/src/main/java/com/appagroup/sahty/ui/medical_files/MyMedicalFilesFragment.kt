package com.appagroup.sahty.ui.medical_files

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.GridLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.entity.MedicalFileGroup
import com.appagroup.sahty.ui.ResumeFragment
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.ItemLongClickListener
import com.appagroup.sahty.ui.listener.OnClick
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.ui.medical_files.group_content.MedicalFileGroupContentFragment
import com.appagroup.sahty.ui.medical_files.group_content.import_files.ImportFilesActivity
import kotlinx.android.synthetic.main.fragment_my_medical_files.*

class MyMedicalFilesFragment : BaseFragment<MyMedicalFilesContract.Presenter>(), MyMedicalFilesContract.View, ResumeFragment {

    private lateinit var progressDialog: ProgressDialog
    private val groupsAdapter = MedicalFileGroupAdapter()
    private val CREATE_GROUP_REQUEST_CODE = 100
    private var fragmentLaunched = false
    private lateinit var selectedFileGroup: MedicalFileGroup
    private lateinit var groupContentFragment: MedicalFileGroupContentFragment

    override fun instantiatePresenter(): MyMedicalFilesContract.Presenter = MyMedicalFilesPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_my_medical_files

    override fun initViews(view: View) {
        medical_file_groups_recyclerView.layoutManager = GridLayoutManager(activity, 2)
        medical_file_groups_recyclerView.adapter = groupsAdapter
        medical_file_groups_recyclerView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                groupsAdapter.updateItemHeight(medical_file_groups_recyclerView.height / 2)
            }
        })

        setListeners()
    }

    override fun setListeners() {
        medical_file_groups_create_group_btn.setOnClickListener {
            openCreateMedicalFileGroupDialog()
        }

        groupsAdapter.setOnAddFileGroupClickListener(object : OnClick {
            override fun onClick() {
                openCreateMedicalFileGroupDialog()
            }
        })

        // open file group on group click
        groupsAdapter.setOnFileGroupClickListener(object : ItemClickListener<MedicalFileGroup> {
            override fun onClick(item: MedicalFileGroup) {
                selectedFileGroup = item
                val bundle = Bundle()
                groupContentFragment = MedicalFileGroupContentFragment.getInstance()
                bundle.putLong(MedicalFileGroupContentFragment.GROUP_ID, item.id)
                bundle.putString(MedicalFileGroupContentFragment.GROUP_TITLE, item.title)
                groupContentFragment.arguments = bundle
                (activity as MainActivity).showSubFragmentContainer()
                (activity as MainActivity).startFragment(
                        groupContentFragment,
                        MedicalFileGroupContentFragment.TAG,
                        MainActivity.SUB_FRAGMENT_CONTAINER)
            }
        })

        groupsAdapter.setOnFileGroupLongClickListener(object : ItemLongClickListener<Long> {
            override fun onLongClick(item: Long, view: View) {
                showDeletePopUpMenu(item, view)
            }
        })
    }

    private fun openCreateMedicalFileGroupDialog() {
        val dialog = CreateMedicalFileGroupDialog.getInstance()
        dialog.setTargetFragment(this, CREATE_GROUP_REQUEST_CODE)
        dialog.show(activity!!.supportFragmentManager, CreateMedicalFileGroupDialog.TAG)
    }

    private fun showDeletePopUpMenu(groupId: Long, view: View) {
        val popupMenu = PopupMenu(activity!!, view)
        popupMenu.inflate(R.menu.delete_menu)
        popupMenu.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem?): Boolean {
                AlertDialog.Builder(this@MyMedicalFilesFragment.context)
                        .setMessage(getString(R.string.warning_delete_medical_file_group))
                        .setPositiveButton(getString(R.string.delete)) { _, _ ->
                            if (groupId != -1L)
                                presenter.deleteMedicalFileGroup(groupId)
                            else
                                removeDeletedGroup(groupId)
                        }
                        .setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                            dialog.dismiss()
                        }
                        .show()
                popupMenu.dismiss()
                return true
            }
        })
        popupMenu.show()
    }

    override fun showDeleteProgressDialog() {
        progressDialog = ProgressDialog(activity)
        progressDialog.setMessage(getString(R.string.wait_until_delete_group))
        progressDialog.show()
    }

    override fun dismissDeleteProgressDialog() {
        progressDialog.dismiss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {

                // get the response of request create new group from MainActivity
                CREATE_GROUP_REQUEST_CODE -> {
                    if (data != null && data.getBooleanExtra(CreateMedicalFileGroupDialog.CREATED_SUCCESSFULLY, false)) {
                        groupsAdapter.addMedicalFileGroup(
                                data.getBundleExtra(CreateMedicalFileGroupDialog.MEDICAL_FILE_GROUP_BUNDLE)
                                        .getSerializable(CreateMedicalFileGroupDialog.MEDICAL_FILE_GROUP) as MedicalFileGroup
                        )
                        medical_file_groups_create_group_btn.visibility = View.GONE
                    }
                }

                MedicalFileGroupContentFragment.UPLOAD_REQUEST_CODE -> {

                    // passing activity result from MainActivity to MedicalFileGroupContentFragment
                    groupContentFragment.onActivityResult(requestCode, resultCode, data)

                    // set the selected group ID when it's newly created
                    if (::selectedFileGroup.isInitialized
                            && selectedFileGroup.id == -1L
                            && data != null
                            && data.getBooleanExtra(ImportFilesActivity.UPLOADED_SUCCESSFULLY, false)) {
                        selectedFileGroup.id = data.getLongExtra(ImportFilesActivity.GROUP_ID, -1L)
                    }
                }
            }
    }

    override fun updateMedicalFileGroups(groups: MutableList<MedicalFileGroup>) {
        if (groups.size > 0) {
            groupsAdapter.updateMedicalFileGroups(groups)
            medical_file_groups_create_group_btn.visibility = View.GONE
        } else
            medical_file_groups_create_group_btn.visibility = View.VISIBLE
    }

    override fun removeDeletedGroup(groupId: Long) {
        groupsAdapter.removeSelectedMedicalFileGroups(groupId)
        if (groupsAdapter.itemCount == 0)
            medical_file_groups_create_group_btn.visibility = View.VISIBLE
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        medical_file_groups_progressBar.visibility = visibility
    }

    override fun onResumeFragment() {
        if (!fragmentLaunched) {
            presenter.loadMedicalFileGroups()
            fragmentLaunched = true
        }
    }

    companion object {
        val TAG = "MyMedicalFilesFragment"
        fun getInstance(): MyMedicalFilesFragment =
                MyMedicalFilesFragment()
    }
}