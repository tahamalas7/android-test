package com.appagroup.sahty.ui.accreditation_dialog

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.Accreditation
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Specialty
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class AccreditationPresenter(view: AccreditationContract.View) : BasePresenter<AccreditationContract.View>(view), AccreditationContract.Presenter {

    private val TAG = "AccreditationPresenter"

    override fun getSpecialties(name: String) {
        view.changeSpecialtySearchProgressBarVisibility(View.VISIBLE)
        DisposableManager.disposeSearch()
        subscribe(dataManager.getSpecialties(name), object : Observer<Response<BaseResponse<MutableList<Specialty>>>> {
            override fun onComplete() {
                Log.d(TAG, "getSpecialties -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                DisposableManager.addSearchDisposable(d)
                Log.d(TAG, "getSpecialties -> onSubscribe")
            }

            override fun onNext(t: Response<BaseResponse<MutableList<Specialty>>>) {
                Log.d(TAG, "getSpecialties -> onNext -> code = ${t.code()}")
                Log.d(TAG, "getSpecialties -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.data != null) {
                    view.updateSpecialties(t.body()!!.data!!)
                    Log.d(TAG, "getSpecialties -> onNext -> status = ${t.body()!!.status}")
                    Log.d(TAG, "getSpecialties -> onNext -> data size = ${t.body()!!.data!!.size}")
                }

                view.changeSpecialtySearchProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                view.changeSpecialtySearchProgressBarVisibility(View.GONE)
                Log.d(TAG, "getSpecialties -> onError -> error = ${e.message}")
            }
        })
    }

    override fun deleteAccreditation(accreditationId: Long) {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.deleteAccreditation(accreditationId), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "deleteAccreditation -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "deleteAccreditation -> onSubscribe")
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "deleteAccreditation -> onNext -> code = ${t.code()}")
                Log.d(TAG, "deleteAccreditation -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "deleteAccreditation -> onNext -> status = ${t.body()!!.status}")
                    view.accreditationDeletedSuccessfully()
                } else
                    view.showError(R.string.something_wrong)

                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "deleteAccreditation -> onError -> error = ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
                view.showError(R.string.something_wrong)
            }
        })
    }

    override fun editAccreditation(accreditationId: Long, type: RequestBody, year: RequestBody,
                                   location: RequestBody, specialization: RequestBody) {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.editAccreditation(accreditationId, type, year, location, specialization), object : Observer<Response<BaseResponse<Accreditation>>> {
            override fun onComplete() {
                Log.d(TAG, "editAccreditation -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "editAccreditation -> onSubscribe")
            }

            override fun onNext(t: Response<BaseResponse<Accreditation>>) {
                Log.d(TAG, "editAccreditation -> onNext -> code = ${t.code()}")
                Log.d(TAG, "editAccreditation -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.data != null) {
                    Log.d(TAG, "editAccreditation -> onNext -> status = ${t.body()!!.status}")
                    Log.d(TAG, "editAccreditation -> onNext -> Accreditation = ${t.body()!!.data!!}")
                    view.accreditationEditedSuccessfully(t.body()!!.data!!)
                } else
                    view.showError(R.string.something_wrong)

                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "editAccreditation -> onError -> error = ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
                view.showError(R.string.something_wrong)
            }
        })
    }

    override fun editAccreditationWithImage(accreditationId: Long, type: RequestBody, year: RequestBody, location: RequestBody,
                                            specialization: RequestBody, image: MultipartBody.Part) {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.editAccreditationWithImage(accreditationId, type, year, location, specialization, image), object : Observer<Response<BaseResponse<Accreditation>>> {
            override fun onComplete() {
                Log.d(TAG, "editAccreditationWithImage -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "editAccreditationWithImage -> onSubscribe")
            }

            override fun onNext(t: Response<BaseResponse<Accreditation>>) {
                Log.d(TAG, "editAccreditationWithImage -> onNext -> code = ${t.code()}")
                Log.d(TAG, "editAccreditationWithImage -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.data != null) {
                    Log.d(TAG, "editAccreditationWithImage -> onNext -> status = ${t.body()!!.status}")
                    Log.d(TAG, "editAccreditationWithImage -> onNext -> Accreditation = ${t.body()!!.data!!}")
                    view.accreditationEditedSuccessfully(t.body()!!.data!!)
                } else
                    view.showError(R.string.something_wrong)

                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "editAccreditationWithImage -> onError -> error = ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
                view.showError(R.string.something_wrong)
            }
        })
    }

    override fun createAccreditation(type: RequestBody, year: RequestBody, location: RequestBody,
                                     specialization: RequestBody, image: MultipartBody.Part) {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.createAccreditation(type, year, location, specialization, image), object : Observer<Response<BaseResponse<Accreditation>>> {
            override fun onComplete() {
                Log.d(TAG, "createAccreditation -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "createAccreditation -> onSubscribe")
            }

            override fun onNext(t: Response<BaseResponse<Accreditation>>) {
                Log.d(TAG, "createAccreditation -> onNext -> code = ${t.code()}")
                Log.d(TAG, "createAccreditation -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.data != null) {
                    Log.d(TAG, "createAccreditation -> onNext -> status = ${t.body()!!.status}")
                    Log.d(TAG, "createAccreditation -> onNext -> Accreditation = ${t.body()!!.data!!}")
                    view.accreditationCreatedSuccessfully(t.body()!!.data!!)
                } else
                    view.showError(R.string.something_wrong)

                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "createAccreditation -> onError -> error = ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
                view.showError(R.string.something_wrong)
            }
        })
    }
}