package com.appagroup.sahty.ui

import android.util.Log
import com.appagroup.sahty.base.DataManagerProvider
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.utils.*
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class FollowService : DataManagerProvider(){

    private val TAG = "FollowService"

    fun followUser(userId : Long) {
        subscribe(dataManager.followUser(userId), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "followUser -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "followUser -> onSubscribe")
                DisposableManager.addFollowDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "followUser -> onNext -> code = ${t.code()}")
                Log.d(TAG, "followUser -> onNext -> status = ${t.body()!!.status}")
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "followUser -> onError -> error = ${e.message}")
            }
        })
    }

    fun unfollowUser(userId : Long) {
        subscribe(dataManager.unfollowUser(userId), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "unfollowUser -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "unfollowUser -> onSubscribe")
                DisposableManager.addFollowDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "unfollowUser -> onNext -> code = ${t.code()}")
                Log.d(TAG, "unfollowUser -> onNext -> status = ${t.body()!!.status}")
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "unfollowUser -> onError -> error = ${e.message}")
            }
        })
    }

    fun dispose() {
        DisposableManager.disposeFollow()
    }
}