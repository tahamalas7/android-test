package com.appagroup.sahty.ui.poll

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Poll
import com.appagroup.sahty.entity.PollOption
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_loader.view.*
import kotlinx.android.synthetic.main.item_poll.view.*

class PollsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    // define item types
    private val TYPE_POLL = 0
    private val TYPE_LOADER = 1
    private val TYPE_THANKS_CARD = 2

    private var editedPollId = 0L
    private var pollsEnded = false // used as a flag to know when polls are ended
    private lateinit var pollsLoader: ProgressBar
    private lateinit var pollOptionClickListener: ItemClickListener<PollOption>
    private lateinit var pollsBottomReachListener: BottomReachListener
    private val polls: MutableList<Poll> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_POLL -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_poll, parent, false)
                PollHolder(view)
            }
            TYPE_LOADER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_loader, parent, false)
                LoaderHolder(view)
            }
            TYPE_THANKS_CARD -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_poll_thanks, parent, false)
                ThanksCardHolder(view)
            }
            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemCount(): Int =
            if (polls.size > 0)
                polls.size + 1
            else polls.size

    override fun getItemViewType(position: Int): Int =
            if (position == itemCount - 1)
                if (pollsEnded) TYPE_THANKS_CARD
                else TYPE_LOADER
            else TYPE_POLL

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoaderHolder -> holder.changeLoaderVisibility()
            is PollHolder -> {
                holder.bind(polls[position])
            }
        }
    }

    fun updateVotedPoll(poll: Poll, optionId: Long) {
        // need to check if this poll has been voted before
        if (!poll.beenVoted) {
            poll.beenVoted = true
            poll.totalVotes++
        } else {
            for (i in 0 until poll.options.size) {
                if (poll.options[i].id == poll.selectedOptionId) {
                    poll.options[i].voteCount--
                    break
                }
            }
        }

        editedPollId = poll.id

        // increase vote count for the option
        poll.selectedOptionId = optionId
        for (i in 0 until poll.options.size) {
            if (poll.options[i].id == optionId) {
                poll.options[i].voteCount++
                break
            }
        }

        // find the position of the modified poll
        for (position in 0 until polls.size) {
            if (polls[position].id == poll.id) {
                notifyItemChanged(position)
                break
            }
        }
    }

    // cancel the existing vote on this Poll
    fun cancelVote(poll: Poll) {
        for (i in 0 until poll.options.size) {
            if (poll.options[i].id == poll.selectedOptionId) {
                poll.options[i].voteCount--
                poll.selectedOptionId = 0L
                poll.beenVoted = false
                poll.totalVotes--
                break
            }
        }

        for (position in 0 until polls.size) {
            if (polls[position].id == poll.id) {
                notifyItemChanged(position)
                break
            }
        }
    }

    fun updatePolls(polls: MutableList<Poll>) {
        this.polls.addAll(polls)
        notifyDataSetChanged()
    }

    fun clearItems() {
        this.polls.clear()
        pollsEnded = false
        notifyDataSetChanged()
    }

    // show/hide loader ProgressBar
    fun changeLoaderVisibility(visibility: Int) {
        if (::pollsLoader.isInitialized)
            pollsLoader.visibility = visibility
    }

    fun notifyPollsDone() {
        pollsEnded = true
        notifyItemChanged(itemCount - 1)
    }

    fun setOnPollOptionClickListener(pollOptionClickListener: ItemClickListener<PollOption>) {
        this.pollOptionClickListener = pollOptionClickListener
    }

    fun setOnPollsBottomReachListener(pollsBottomReachListener: BottomReachListener) {
        this.pollsBottomReachListener = pollsBottomReachListener
    }

    inner class ThanksCardHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class LoaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun changeLoaderVisibility() {
            pollsLoader = itemView.recyclerView_loader
        }
    }

    inner class PollHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(poll: Poll) {
            val adapter = OptionsAdapter(poll, poll.id == editedPollId)
            itemView.poll_question.text = poll.question

            itemView.poll_options_recyclerView.layoutManager = LinearLayoutManager(itemView.context)
            itemView.poll_options_recyclerView.adapter = adapter

            // set listener on options click
            adapter.setOnOptionClickListener(object : PollOptionClickListener {
                override fun onVote(pollOption: PollOption) {
                    pollOptionClickListener.onClick(pollOption)
                    updateVotedPoll(poll, pollOption.id)
                }

                override fun onCancelingVote(pollOption: PollOption) {
                    pollOptionClickListener.onClick(pollOption)
                    cancelVote(poll)
                }
            })

            // reset "editedPollId" variable after initialize
            if (poll.id == editedPollId)
                editedPollId = 0L
        }
    }
}