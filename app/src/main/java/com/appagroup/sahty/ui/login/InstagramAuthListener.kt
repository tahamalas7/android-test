package com.appagroup.sahty.ui.login

interface InstagramAuthListener {
    fun onAuthTokenReceived(authToken: String?)
}