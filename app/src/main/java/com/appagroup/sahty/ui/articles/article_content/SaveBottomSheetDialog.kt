package com.appagroup.sahty.ui.articles.article_content

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.appagroup.sahty.R
import com.appagroup.sahty.ui.listener.OnClick
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_save_bottom_sheet.view.*

class SaveBottomSheetDialog : BottomSheetDialogFragment() {

    private lateinit var saveBottomSheetClickListener: OnClick

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.dialog_save_bottom_sheet, container, false)
        val isSaved = arguments!!.getBoolean(IS_SAVED, false)
        val type = arguments!!.getString(TYPE, "")

        view.save_dialog_text.text =
                if (isSaved)
                    "${getString(R.string.unsave)} $type"
                else "${getString(R.string.save)} $type"

        view.save_dialog_icon.setImageResource(
                if (isSaved)
                    R.drawable.ic_save_on
                else R.drawable.ic_save_off
        )

        view.setOnClickListener {
            saveBottomSheetClickListener.onClick()
        }

        return view
    }

    fun setOnBottomSheetClickListener(saveBottomSheetClickListener: OnClick) {
        this.saveBottomSheetClickListener = saveBottomSheetClickListener
    }

    companion object {
        val IS_SAVED = "IS_SAVED"
        val TYPE = "TYPE"
        fun newInstance(isSaved: Boolean, type: String): SaveBottomSheetDialog {
            val instance = SaveBottomSheetDialog()
            val bundle = Bundle()
            bundle.putBoolean(IS_SAVED, isSaved)
            bundle.putString(TYPE, type)
            instance.arguments = bundle

            return instance
        }
    }
}