package com.appagroup.sahty.ui.messages

import android.app.Activity
import android.content.Intent
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.entity.Message
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.messages.create_message.CreateMessageActivity
import com.appagroup.sahty.ui.messages.message_content.MessageContentActivity
import kotlinx.android.synthetic.main.fragment_messages.*

class MessagesFragment : BaseFragment<MessagesContract.Presenter>(), MessagesContract.View {

    private lateinit var adapter: MessageAdapter
    private val MESSAGE_CONTENT_REQUEST_CODE = 23
    private val CREATE_MESSAGE_REQUEST_CODE = 24

    override fun instantiatePresenter(): MessagesContract.Presenter = MessagesPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_messages

    override fun initViews(view: View) {
        adapter = MessageAdapter(presenter.getUserId())
        messages_recyclerView.layoutManager = LinearLayoutManager(activity)
        messages_recyclerView.adapter = adapter

        presenter.loadMessages()
        setListeners()
    }

    override fun setListeners() {
        messages_back_arrow.setOnClickListener {
            activity!!.onBackPressed()
        }

        messages_create_message_fab.setOnClickListener {
            val intent = Intent(activity, CreateMessageActivity::class.java)
            startActivityForResult(intent, CREATE_MESSAGE_REQUEST_CODE)
        }

        adapter.setOnMessageClickListener(object : ItemClickListener<Message> {
            override fun onClick(item: Message) {
                val intent = Intent(activity, MessageContentActivity::class.java)
                intent.putExtra(MessageContentActivity.MESSAGE_ID, item.messageId)

                // check if message sender is the same user or not
                if (item.senderId == presenter.getUserId())
                    intent.putExtra(MessageContentActivity.RECEIVER_ID, item.receiverId)
                else intent.putExtra(MessageContentActivity.RECEIVER_ID, item.senderId)
                startActivityForResult(intent, MESSAGE_CONTENT_REQUEST_CODE)
            }
        })

        adapter.setOnMessagesBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                presenter.loadMessages()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null && resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                MESSAGE_CONTENT_REQUEST_CODE -> {
                    // check if there is any new new reply added
                    if (data.getStringExtra(MessageContentActivity.LAST_REPLY_CONTENT).isNotEmpty()) {
                        adapter.editMessage(
                                data.getLongExtra(MessageContentActivity.MESSAGE_ID, 0L),
                                data.getStringExtra(MessageContentActivity.LAST_REPLY_CONTENT),
                                data.getStringExtra(MessageContentActivity.LAST_REPLY_DATE)
                        )
                    }
                }

                CREATE_MESSAGE_REQUEST_CODE -> {
                    val messageId =
                            adapter.searchForMessage(
                                    data.getLongExtra(CreateMessageActivity.RECEIVER_ID,
                                    -1L))
                    if (messageId != -1L) {
                        adapter.editMessage(
                                messageId,
                                data.getStringExtra(CreateMessageActivity.MESSAGE_CONTENT),
                                getString(R.string.date_util_unit_just_now))

                    } else {
                        adapter.clearItems()
                        presenter.resetPaginate()
                        presenter.loadMessages()
                    }
                }
            }
        }
    }

    override fun changeLoaderVisibility(visibility: Int) {
        adapter.changeLoaderVisibility(visibility)
    }

    override fun hideMessagesProgressBar() {
        message_progressBar.visibility = View.GONE
    }

    override fun updateMessages(messages: MutableList<Message>) {
        adapter.updateMessages(messages)
    }

    companion object {
        val TAG = "MessagesFragment"
        fun getInstance(): MessagesFragment =
                MessagesFragment()
    }
}