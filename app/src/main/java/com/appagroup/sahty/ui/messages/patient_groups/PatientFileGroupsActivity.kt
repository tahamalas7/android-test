package com.appagroup.sahty.ui.messages.patient_groups

import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.GridLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.MedicalFileGroup
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.messages.patient_groups.group_content.GroupContentActivity
import kotlinx.android.synthetic.main.fragment_my_medical_files.*
import com.appagroup.sahty.ui.messages.patient_groups.PatientFileGroupsContract as Contract

class PatientFileGroupsActivity : BaseActivity<Contract.Presenter>(), Contract.View {

    private val adapter = FileGroupAdapter()
    private var patientId = 0L

    override fun instantiatePresenter(): Contract.Presenter = PatientFileGroupsPresenter(this)

    override fun getContentView(): Int = R.layout.fragment_my_medical_files

    override fun initViews() {
        medical_file_groups_hint.visibility = View.GONE
        medical_file_groups_lbl.text = getString(R.string.medical_files)
        medical_file_groups_patient_name.text = intent!!.getStringExtra(PATIENT_NAME)

        medical_file_groups_recyclerView.layoutManager = GridLayoutManager(this, 2)
        medical_file_groups_recyclerView.adapter = adapter
        medical_file_groups_recyclerView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                medical_file_groups_recyclerView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                adapter.updateItemHeight(medical_file_groups_recyclerView.height / 2)
            }
        })

        patientId = intent!!.getLongExtra(PATIENT_ID, 0L)
        presenter.getMedicalFileGroups(patientId)
        setListeners()

        Log.d(TAG, "PATIENT ID = $patientId")
        Log.d(TAG, "PATIENT NAME = ${intent!!.getStringExtra(PATIENT_NAME)}")
    }

    override fun setListeners() {
        adapter.setOnMedicalFileGroupClickListener(object : ItemClickListener<MedicalFileGroup> {
            override fun onClick(item: MedicalFileGroup) {
                Log.d(TAG, "ON CLICK, group id = ${item.id}")
                Log.d(TAG, "PATIENT ID = $patientId")
                Log.d(TAG, "GROUP ID = ${item.id}")
                Log.d(TAG, "GROUP NAME = ${item.title}")
                val intent = Intent(this@PatientFileGroupsActivity, GroupContentActivity::class.java)
                intent.putExtra(GroupContentActivity.PATIENT_ID, patientId)
                intent.putExtra(GroupContentActivity.GROUP_NAME, item.title)
                intent.putExtra(GroupContentActivity.GROUP_ID, item.id)
                startActivity(intent)
            }
        })
    }

    override fun updateGroups(groups: MutableList<MedicalFileGroup>) {
        adapter.updateGroups(groups)
        Log.d(TAG, "updateGroups, size = ${groups.size}")
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        medical_file_groups_progressBar.visibility = visibility
    }

    companion object {
        val TAG = "PatientGroupsActivity"
        val PATIENT_NAME = "PATIENT_NAME"
        val PATIENT_ID = "PATIENT_ID"
    }
}