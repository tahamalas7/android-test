package com.appagroup.sahty.ui.main

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView

interface MainContract {

    interface View : IBaseView {

        fun setListeners()

        fun fillUserData(name: String, email: String, image: String)
    }

    interface Presenter : IBasePresenter<View> {

        fun getProvider(): String

        fun getUserData()

        fun getUserPicture(): String

        fun getUserEmail(): String

        fun getUserRole(): Int

        fun getApplicationLanguage(): String

        fun setUserRole(role: Int)

        fun clearUserData()

        fun saveLocation(latitude: Float, longitude: Float)
    }
}