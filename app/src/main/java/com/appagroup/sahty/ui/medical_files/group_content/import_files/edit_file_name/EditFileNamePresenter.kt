package com.appagroup.sahty.ui.medical_files.group_content.import_files.edit_file_name

import com.appagroup.sahty.base.BasePresenter

class EditFileNamePresenter(view : EditFileNameContract.View) : BasePresenter<EditFileNameContract.View>(view), EditFileNameContract.Presenter {

}