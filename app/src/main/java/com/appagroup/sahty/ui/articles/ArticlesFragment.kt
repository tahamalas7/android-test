package com.appagroup.sahty.ui.articles

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Typeface
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.entity.Article
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.ui.FollowService
import com.appagroup.sahty.ui.articles.article_content.ArticleContentActivity
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.utils.ArticlesFilter
import kotlinx.android.synthetic.main.dialog_articles_filter.view.*
import kotlinx.android.synthetic.main.fragment_articles.*

class ArticlesFragment : BaseFragment<ArticlesContract.Presenter>(), ArticlesContract.View {

    private lateinit var articlesAdapter: ArticlesAdapter
    private var filter = ArticlesFilter.ALL
    private val followService = FollowService()
    private var categoryId = 0L

    override fun instantiatePresenter(): ArticlesContract.Presenter = ArticlesPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_articles

    override fun initViews(view: View) {
        val typeface = Typeface.createFromAsset(activity!!.assets, "GOTHIC.TTF")
        articlesAdapter = ArticlesAdapter(typeface, presenter.getUserId())
        articles_recyclerView.layoutManager = LinearLayoutManager(activity)
        articles_recyclerView.adapter = articlesAdapter
        articles_recyclerView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                if (articles_recyclerView != null) {
                    articles_recyclerView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    articlesAdapter.updateItemHeight(articles_recyclerView.height / 2)
                }
            }
        })

        val bundle = arguments

        // get category id from previous fragment
        if (bundle != null)
            categoryId = bundle.getLong("CATEGORY_ID", 0)

        presenter.loadArticles(categoryId, filter)
        setListeners()
    }

    override fun setListeners() {
        articles_filter_btn.setOnClickListener {
            openArticlesFilterDialog()
        }

        articles_back_arrow.setOnClickListener {
            activity!!.onBackPressed()
        }

        articlesAdapter.setOnBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                presenter.loadArticles(categoryId, filter)
            }
        })

        articlesAdapter.setOnFollowBtnClickListener(object : FollowButtonClickListener {
            override fun followUser(follow: Follow) {
                Log.d(TAG, "followUser -> user id = ${follow.id} ")
                followService.followUser(follow.id)
            }

            override fun unfollowUser(follow: Follow) {
                Log.d(TAG, "unfollowUser -> user id = ${follow.id} ")
                followService.unfollowUser(follow.id)
            }
        })

        articlesAdapter.setOnArticleClickListener(object : ItemClickListener<Article> {
            override fun onClick(item: Article) {
                val intent = Intent(activity, ArticleContentActivity::class.java)
                intent.putExtra(ArticleContentActivity.ARTICLE_ID, item.id)
                startActivity(intent)
            }
        })
    }

    override fun updateArticles(articles: MutableList<Article>) {
        articlesAdapter.updateArticles(articles)
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        articles_progressBar.visibility = visibility
    }

    override fun changeLoaderVisibility(visibility: Int) {
        articlesAdapter.changeLoaderVisibility(visibility)
    }

    private fun openArticlesFilterDialog() {
        val builder = AlertDialog.Builder(activity)
        val view = layoutInflater.inflate(R.layout.dialog_articles_filter, null)
        builder.setView(view)
        val dialog = builder.create()
        dialog.show()

        when (filter) {
            ArticlesFilter.ALL ->
                view.articles_filter_radioGroup.check(R.id.articles_filter_all_option)
            ArticlesFilter.Articles ->
                view.articles_filter_radioGroup.check(R.id.articles_filter_articles_option)
            ArticlesFilter.STORIES ->
                view.articles_filter_radioGroup.check(R.id.articles_filter_stories_option)
            ArticlesFilter.MOST_INTERACTIVE ->
                view.articles_filter_radioGroup.check(R.id.articles_filter_most_interactive_option)
            ArticlesFilter.FOLLOWING_USERS ->
                view.articles_filter_radioGroup.check(R.id.articles_filter_following_users_option)
        }

        // handle filter type after user choose a new filter
        view.articles_filter_apply_btn.setOnClickListener {
            when (view.articles_filter_radioGroup.checkedRadioButtonId) {
                R.id.articles_filter_all_option ->
                    filter = ArticlesFilter.ALL
                R.id.articles_filter_articles_option ->
                    filter = ArticlesFilter.Articles
                R.id.articles_filter_stories_option ->
                    filter = ArticlesFilter.STORIES
                R.id.articles_filter_most_interactive_option ->
                    filter = ArticlesFilter.MOST_INTERACTIVE
                R.id.articles_filter_following_users_option ->
                    filter = ArticlesFilter.FOLLOWING_USERS
            }
            changeProgressBarVisibility(View.VISIBLE) // showProgressDialog progressBar
            articlesAdapter.clearArticles() // clear items
            presenter.resetPaginate() // reset pagination page
            presenter.loadArticles(categoryId, filter) // load new items
            dialog.dismiss()
        }
    }

    override fun onPause() {
        super.onPause()
        followService.dispose()
    }

    companion object {
        val TAG = "ArticlesFragment"

        private var articlesFragment: ArticlesFragment? = null

        @JvmStatic
        fun getInstance(fragmentManager: FragmentManager): ArticlesFragment {
            return ArticlesFragment()
        }
    }
}