package com.appagroup.sahty.ui.medical_files.group_content

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.MedicalFile
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.ItemLongClickListener
import com.appagroup.sahty.ui.listener.OnClick
import kotlinx.android.synthetic.main.item_add_medical_file.view.*
import kotlinx.android.synthetic.main.item_medical_file.view.*

class MedicalFileAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_ADD_FILE = 0
    private val TYPE_MEDICAL_FILE = 1
    private var importMoreItemHeight: Int = 400
    private val files: MutableList<MedicalFile> = mutableListOf()
    private lateinit var addMedicalFileClickListener: OnClick
    private lateinit var medicalFileClickListener: ItemClickListener<MedicalFile>
    private lateinit var fileLongClickListener: ItemLongClickListener<Long>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ADD_FILE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_add_medical_file, parent, false)
                AddMedicalFileHolder(view)
            }
            TYPE_MEDICAL_FILE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_medical_file, parent, false)
                MedicalFileHolder(view)
            }
            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1)
            TYPE_ADD_FILE
        else TYPE_MEDICAL_FILE
    }

    override fun getItemCount(): Int =
            if (files.size == 0)
                0
            else
                files.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MedicalFileHolder -> {
                Log.d("onBindViewHolder", "is MedicalFileHolder")
                holder.bind(files[position])
                holder.itemView.setOnClickListener {
                    medicalFileClickListener.onClick(files[position])
                }
                holder.itemView.setOnLongClickListener {
                    fileLongClickListener.onLongClick(files[position].id, holder.itemView.medical_file_extension)
                    true
                }
            }

            is AddMedicalFileHolder -> {
                Log.d("onBindViewHolder", "is AddMedicalFileHolder")
                holder.setup()
                holder.itemView.setOnClickListener {
                    addMedicalFileClickListener.onClick()
                }
            }
        }
    }

    fun updateMedicalFileGroups(files: MutableList<MedicalFile>) {
        this.files.addAll(files)
        notifyDataSetChanged()
    }

    fun removeAllItems() {
        this.files.clear()
        notifyDataSetChanged()
    }

    fun removeSelectedMedicalFile(fileId: Long) {
        for (position in 0 until files.size) {
            if (files[position].id == fileId) {
                this.files.removeAt(position)
                notifyDataSetChanged()
                break
            }
        }
    }

    fun updateImportFileItemHeight(importMoreItemHeight: Int) {
        this.importMoreItemHeight = importMoreItemHeight
    }

    fun setOnAddFileGroupClickListener(addMedicalFileClickListener: OnClick) {
        this.addMedicalFileClickListener = addMedicalFileClickListener
    }

    fun setOnFileGroupClickListener(medicalFileClickListener: ItemClickListener<MedicalFile>) {
        this.medicalFileClickListener = medicalFileClickListener
    }

    fun setOnFileLongClickListener(fileLongClickListener: ItemLongClickListener<Long>) {
        this.fileLongClickListener = fileLongClickListener
    }

    inner class AddMedicalFileHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun setup() {
            if (itemCount > 1) {
                val params = this.itemView.layoutParams
                params.height = importMoreItemHeight
                this.itemView.layoutParams = params
            } else
                itemView.add_medical_files_text.text = itemView.context.getString(R.string.import_files)
        }
    }

    inner class MedicalFileHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(medicalFile: MedicalFile) {
            itemView.medical_file_name.text = medicalFile.name
//            itemView.medical_file_extension.text = medicalFile.name

            itemView.medical_file_icon.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    itemView.viewTreeObserver.removeOnGlobalLayoutListener(this)

                    // set file name width the same of file icon width
                    val nameParams = itemView.medical_file_name.layoutParams
                    nameParams.width = itemView.medical_file_icon.width
                    itemView.medical_file_name.layoutParams = nameParams
                    itemView.medical_file_name.setPadding(24, 0, 24, 0)

                    // set extension height and width the same of file icon
                    val extensionParams = itemView.medical_file_extension.layoutParams
                    extensionParams.height = itemView.medical_file_icon.height
                    extensionParams.width = itemView.medical_file_icon.width
                    itemView.medical_file_extension.layoutParams = extensionParams
                    itemView.medical_file_extension.setPadding(32, 32, 32, 32)

                    // set image height and width the same of file icon
                    val imageParams = itemView.medical_file_image.layoutParams
                    imageParams.height = itemView.medical_file_icon.height
                    imageParams.width = itemView.medical_file_icon.width
                    itemView.medical_file_image.layoutParams = imageParams
                    itemView.medical_file_image.setPadding(32, 32, 32, 32)
                }
            })

            Glide.with(itemView.context)
                    .load(BuildConfig.IMAGE + medicalFile.url)
                    .into(itemView.medical_file_image)
        }
    }
}