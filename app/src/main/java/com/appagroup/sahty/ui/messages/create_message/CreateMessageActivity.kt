package com.appagroup.sahty.ui.messages.create_message

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.view.View
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.Doctor
import com.appagroup.sahty.entity.SendReplyResponse
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.messages.select_doctor.SelectDoctorDialog
import kotlinx.android.synthetic.main.activity_create_message.*

class CreateMessageActivity : BaseActivity<CreateMessageContract.Presenter>(), CreateMessageContract.View {

    private lateinit var progressDialog: ProgressDialog
    private var doctorId = 0L

    override fun instantiatePresenter(): CreateMessageContract.Presenter = CreateMessagePresenter(this)

    override fun getContentView(): Int = R.layout.activity_create_message

    override fun initViews() {
        setListeners()
    }

    override fun setListeners() {
        create_message_back_arrow.setOnClickListener {
            onBackPressed()
        }

        create_message_send_btn.setOnClickListener {
            if (doctorId != 0L) {
                if (create_message_text.text.trim().isNotEmpty()) {
                    presenter.sendMessage(doctorId, create_message_text.text.toString().trim())
                }
            } else
                showError(getString(R.string.err_no_doctor_selected))
        }

        create_message_add_doctor.setOnClickListener {
            val dialog = SelectDoctorDialog.getInstance()
            dialog.show(supportFragmentManager, SelectDoctorDialog.TAG)

            dialog.setOnSelectDoctorListener(object : ItemClickListener<Doctor> {
                override fun onClick(item: Doctor) {
                    doctorId = item.id
                    create_message_doctor_info_bar.visibility = View.VISIBLE
                    create_message_add_doctor_bar.visibility = View.GONE
                    create_message_doctor_name.text = item.name
                    if (item.image != null)
                        Glide.with(this@CreateMessageActivity)
                                .load(BuildConfig.IMAGE + item.image)
                                .into(create_message_doctor_pic)
                }
            })
        }
    }

    override fun messageSentSuccessfully(message: SendReplyResponse) {
        AlertDialog.Builder(this)
                .setMessage(getString(R.string.message_sent_successfully))
                .setPositiveButton(R.string.ok) {_, _ ->
                    val intent = Intent()
                    intent.putExtra(RECEIVER_ID, message.receiverId)
                    intent.putExtra(MESSAGE_CONTENT, message.content)
                    setResult(Activity.RESULT_OK, intent)
                    onBackPressed()
                }
                .show()
    }

    override fun openSelectDoctorDialog() {
        val dialog = SelectDoctorDialog.getInstance()
        dialog.show(supportFragmentManager, SelectDoctorDialog.TAG)
        dialog.setOnSelectDoctorListener(object : ItemClickListener<Doctor> {
            override fun onClick(item: Doctor) {
                create_message_add_doctor_bar.visibility = View.GONE
                create_message_doctor_info_bar.visibility = View.VISIBLE
                create_message_doctor_name.text = item.name
                if (item.image != null)
                    Glide.with(this@CreateMessageActivity)
                            .load(BuildConfig.IMAGE + item.image)
                            .into(create_message_doctor_pic)
            }
        })
    }

    override fun showProgressDialog() {
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage(getString(R.string.please_wait))
        progressDialog.show()
    }

    override fun hideProgressDialog() {
        progressDialog.dismiss()
    }

    companion object {
        val TAG = "CreateMessageActivity"
        val RECEIVER_ID = "RECEIVER_ID"
        val MESSAGE_CONTENT = "MESSAGE_CONTENT"
    }
}