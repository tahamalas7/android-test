package com.appagroup.sahty.ui.saved.saved_stories

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.entity.Story
import com.appagroup.sahty.ui.FollowService
import com.appagroup.sahty.ui.articles.article_content.ArticleContentActivity
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.activity_saved_stories.*
import com.appagroup.sahty.ui.saved.saved_stories.SavedStoriesContract as Contract

class SavedStoriesActivity : BaseActivity<Contract.Presenter>(), Contract.View {

    private lateinit var adapter: StoriesAdapter
    private val followService = FollowService()
    private val STORY_CONTENT_REQUEST_CODE = 56

    override fun instantiatePresenter(): Contract.Presenter = SavedStoriesPresenter(this)

    override fun getContentView(): Int = R.layout.activity_saved_stories

    override fun initViews() {
        val typeface = Typeface.createFromAsset(assets, "GOTHIC.TTF")
        adapter = StoriesAdapter(typeface)

        saved_stories_recyclerView.layoutManager = LinearLayoutManager(this)
        saved_stories_recyclerView.adapter = adapter
        saved_stories_recyclerView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                saved_stories_recyclerView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                adapter.updateItemHeight(saved_stories_recyclerView.height/2)
            }
        })

        changeProgressBarVisibility(View.VISIBLE)
        presenter.getSavedStories()
        setListeners()
    }

    override fun setListeners() {
        saved_stories_back_arrow.setOnClickListener {
            onBackPressed()
        }

        adapter.setOnBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                presenter.getSavedStories()
            }
        })

        adapter.setOnFollowBtnClickListener(object : FollowButtonClickListener {
            override fun followUser(follow: Follow) {
                Log.d(TAG, "followUser -> user id = ${follow.id} ")
                followService.followUser(follow.id)
            }

            override fun unfollowUser(follow: Follow) {
                Log.d(TAG, "unfollowUser -> user id = ${follow.id} ")
                followService.unfollowUser(follow.id)
            }
        })

        adapter.setOnStoryClickListener(object : ItemClickListener<Story> {
            override fun onClick(item: Story) {
                val intent = Intent(this@SavedStoriesActivity, ArticleContentActivity::class.java)
                intent.putExtra(ArticleContentActivity.ARTICLE_ID, item.id)
                startActivityForResult(intent, STORY_CONTENT_REQUEST_CODE)
            }
        })
    }

    override fun updateStories(articles: MutableList<Story>) {
        adapter.updateStories(articles)
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        saved_stories_progressBar.visibility = visibility
    }

    override fun changeLoaderVisibility(visibility: Int) {
        adapter.changeLoaderVisibility(visibility)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == STORY_CONTENT_REQUEST_CODE && data != null) {
            if (intent.getBooleanExtra(ArticleContentActivity.IS_ARTICLE_UNSAVED, false))
                adapter.removeItem(intent.getLongExtra(ArticleContentActivity.ARTICLE_ID, 0L))
        }
    }

    override fun onPause() {
        super.onPause()
        followService.dispose()
    }

    companion object {
        val TAG = "SavedStoriesActivity"
    }
}