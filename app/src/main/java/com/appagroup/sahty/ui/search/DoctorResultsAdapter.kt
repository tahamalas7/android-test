package com.appagroup.sahty.ui.search

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Doctor
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_search_doctor.view.*

class DoctorResultsAdapter : RecyclerView.Adapter<DoctorResultsAdapter.DoctorResultsHolder>() {

    private val TAG = "DoctorResultsAdapter"
    private val doctors: MutableList<Doctor> = mutableListOf()
    private lateinit var doctorClickListener: ItemClickListener<Doctor>
    private lateinit var bottomReachListener: BottomReachListener

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): DoctorResultsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_doctor, parent, false)
        return DoctorResultsHolder(view)
    }

    override fun getItemCount(): Int = doctors.size

    override fun onBindViewHolder(holder: DoctorResultsHolder, position: Int) {
        holder.bind(doctors[position])
        holder.itemView.setOnClickListener {
            doctorClickListener.onClick(doctors[position])
        }

        if (position == itemCount - 3)
            bottomReachListener.onBottomReach()
    }

    fun updateDoctors(doctors: MutableList<Doctor>) {
        this.doctors.addAll(doctors)
        notifyDataSetChanged()
    }

    fun clearItems() {
        this.doctors.clear()
        notifyDataSetChanged()
    }

    fun setOnDoctorClickListener(doctorClickListener: ItemClickListener<Doctor>) {
        this.doctorClickListener = doctorClickListener
    }

    fun setOnBottomReachListener(bottomReachListener: BottomReachListener) {
        this.bottomReachListener = bottomReachListener
    }

    inner class DoctorResultsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(doctor: Doctor) {
            Log.d("uuuuuuuu", "DOCTOR ID = ${doctor.id}")
            Log.d("uuuuuuuu", "DOCTOR NAME = ${doctor.name}")
            Log.d("uuuuuuuu", "DOCTOR IMAGE = ${doctor.image}")
            itemView.search_doctor_name.text = doctor.name
            if (doctor.image != null && doctor.image.isNotEmpty())
                Glide.with(itemView.context)
                        .load(BuildConfig.IMAGE + doctor.image)
                        .into(itemView.search_doctor_image)
        }
    }
}