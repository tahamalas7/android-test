package com.appagroup.sahty.ui.medical_files.group_content.import_files

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.PopupMenu
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.MedicalFileGroup
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.ItemLongClickListener
import com.appagroup.sahty.ui.listener.OnClick
import com.appagroup.sahty.ui.medical_files.group_content.MedicalFileGroupContentFragment
import com.appagroup.sahty.ui.medical_files.group_content.import_files.edit_file_name.EditFileNameFragment
import com.appagroup.sahty.ui.medical_files.group_content.import_files.edit_file_name.OnFileRenamed
import com.appagroup.sahty.utils.FileHandler
import com.nbsp.materialfilepicker.MaterialFilePicker
import com.nbsp.materialfilepicker.ui.FilePickerActivity
import kotlinx.android.synthetic.main.activity_import_files.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class ImportFilesActivity : BaseActivity<ImportFilesContract.Presenter>(), ImportFilesContract.View {

    private val adapter = ImportedFilesAdapter()
    private var uploadedSuccessfully = false
    private lateinit var progressDialog: ProgressDialog

    override fun instantiatePresenter(): ImportFilesContract.Presenter = ImportFilesPresenter(this)

    override fun getContentView(): Int = R.layout.activity_import_files

    override fun initViews() {
        import_files_recyclerView.layoutManager = GridLayoutManager(this, 3)
        import_files_recyclerView.adapter = adapter
        setListeners()
    }

    override fun setListeners() {
        import_files_back_arrow.setOnClickListener {
            onBackPressed()
        }

        import_files_upload_btn.setOnClickListener {
            if (adapter.itemCount > 1)
                presenter.uploadFiles(
                        RequestBody.create(okhttp3.MediaType.parse("text/plain"), "${intent.getLongExtra(GROUP_ID, 0)}"),
                        RequestBody.create(okhttp3.MediaType.parse("text/plain"), intent.getStringExtra(GROUP_TITLE)),
                        convertFilesListMultiPartList(adapter.getImportedFiles())
                )
            else
                showError(getString(R.string.err_upload_files))
        }

        adapter.setOnImportMedicalFileClickListener(object : OnClick {
            override fun onClick() {
                ActivityCompat.requestPermissions(this@ImportFilesActivity, arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        REQUEST_PERMISSIONS_CODE)
            }
        })

        adapter.setOnFileClickListener(object : ItemClickListener<String> {
            override fun onClick(item: String) {
                val intent = FileHandler.getFileIntent(item)
                try {
                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.d(MedicalFileGroupContentFragment.TAG, "error open file = ${e.message}")
                }
            }
        })

        adapter.setOnFileLongClickListener(object : ItemLongClickListener<String> {
            override fun onLongClick(item: String, view: View) {
                openDeleteDialog(item, view)
            }
        })
    }

    private fun openDeleteDialog(file: String, view: View) {
        val popupMenu = PopupMenu(this, view)
        popupMenu.inflate(R.menu.delete_menu)
        popupMenu.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem?): Boolean {
                AlertDialog.Builder(this@ImportFilesActivity)
                        .setMessage(getString(R.string.warning_remove_file))
                        .setPositiveButton(getString(R.string.delete)) { _, _ ->
                            adapter.removeFile(file)
                        }
                        .setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                            dialog.dismiss()
                        }
                        .show()
                popupMenu.dismiss()
                return true
            }
        })
        popupMenu.show()
    }

    private fun convertFilesListMultiPartList(files: MutableList<String>): MutableList<MultipartBody.Part> {
        val multiPartList: MutableList<MultipartBody.Part> = mutableListOf()
        for (file in files) {
            val imgFile = File(file)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile)
            multiPartList.add(MultipartBody.Part.createFormData("url[]", imgFile.name, requestFile))
        }
        return multiPartList
    }

    private fun showEditFileNameDialog(filePath: String) {
        val dialog = EditFileNameFragment.newInstance(filePath)
        dialog.show(supportFragmentManager, EditFileNameFragment.TAG)
        dialog.setOnDismissListener(object : OnFileRenamed {
            override fun onRename(filePath: String) {
                adapter.addFile(filePath)
                import_files_upload_btn.setImageResource(R.drawable.ic_upload_on)
                Log.d(TAG, "Edited File Path = $filePath")
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "ImportFilesActivity -> onActivityResult -> requestCode = $requestCode")
        ImportFilesActivity
        if (requestCode == 34 && resultCode == Activity.RESULT_OK) {
            val filePath = data!!.getStringExtra(FilePickerActivity.RESULT_FILE_PATH)
            if (FileHandler.isFileValid(filePath))
                showEditFileNameDialog(filePath)
            else
                showMessageDialog(R.string.err_not_supported_file)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        Log.d(TAG, "ImportFilesActivity -> onRequestPermissionsResult -> requestCode = $requestCode")
        when (requestCode) {
            REQUEST_PERMISSIONS_CODE ->
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    MaterialFilePicker()
                            .withActivity(this@ImportFilesActivity)
                            .withRequestCode(34)
                            .withHiddenFiles(true) // Show hidden files and folders
                            .start()
        }
    }

    override fun showProgressDialog() {
        progressDialog = ProgressDialog(this)
        progressDialog.setCancelable(false)
        progressDialog.setMessage(getString(R.string.wait_upload_files))
        progressDialog.show()
    }

    override fun hideProgressDialog() {
        progressDialog.dismiss()
    }

    override fun uploadCompleted(fileGroup: MedicalFileGroup?) {
        uploadedSuccessfully = true
        val returnIntent = Intent()
        returnIntent.putExtra(UPLOADED_SUCCESSFULLY, uploadedSuccessfully)
        if (fileGroup != null)
            returnIntent.putExtra(GROUP_ID, fileGroup.id)
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

    override fun onPause() {
        super.onPause()
        if (!uploadedSuccessfully) {
            val returnIntent = Intent()
            returnIntent.putExtra(UPLOADED_SUCCESSFULLY, uploadedSuccessfully)
            setResult(Activity.RESULT_OK, returnIntent)
        }
    }

    companion object {
        val GROUP_ID = "GROUP_ID"
        val GROUP_TITLE = "GROUP_TITLE"
        val UPLOADED_SUCCESSFULLY = "UPLOADED_SUCCESSFULLY"
        val REQUEST_PERMISSIONS_CODE = 65
        val TAG = "ImportFilesActivity"
    }
}