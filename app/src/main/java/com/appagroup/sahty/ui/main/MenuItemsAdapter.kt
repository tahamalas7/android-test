package com.appagroup.sahty.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.MenuItem
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.utils.Menu
import com.appagroup.sahty.utils.UserRole
import kotlinx.android.synthetic.main.item_menu.view.*

class MenuItemsAdapter(private val userRole: Int) : RecyclerView.Adapter<MenuItemsAdapter.ViewHolder>() {

    private lateinit var menuItemClickListener: ItemClickListener<MenuItem>
    private val items: MutableList<MenuItem> = mutableListOf()

    init {
        getMenuItems()
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_menu, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener {
            menuItemClickListener.onClick(items[position])
        }
    }

    fun setMenuItemClickListener(menuItemClickListener: ItemClickListener<MenuItem>) {
        this.menuItemClickListener = menuItemClickListener
    }

    private fun getMenuItems() {
        items.add(MenuItem(Menu.MENU_HOME, R.string.menu_home, R.drawable.ic_menu_home))
        items.add(MenuItem(Menu.MENU_MESSAGES, R.string.menu_messages, R.drawable.ic_menu_messages))
        items.add(MenuItem(Menu.MENU_POLLS, R.string.menu_polls, R.drawable.ic_menu_polls))
        items.add(MenuItem(Menu.MENU_PROFILE, R.string.menu_profile, R.drawable.ic_menu_profile))
        items.add(MenuItem(Menu.MENU_SAVED, R.string.menu_saved, R.drawable.ic_menu_saved))
        items.add(MenuItem(Menu.MENU_FEEDBACK, R.string.menu_feedback, R.drawable.ic_menu_feedback))
        items.add(MenuItem(Menu.MENU_SETTINGS, R.string.menu_settings, R.drawable.ic_menu_settings))
        when (userRole) {
            UserRole.ROLE_READER ->
                items.add(MenuItem(Menu.MENU_POST_STORY, R.string.menu_post_story, R.drawable.ic_menu_post))

            UserRole.ROLE_CONFIRMED_DOCTOR ->
                items.add(MenuItem(Menu.MENU_POST_ARTICLE, R.string.menu_post_article, R.drawable.ic_menu_post))
        }
        items.add(MenuItem(Menu.MENU_LOGOUT, R.string.menu_logout, R.drawable.ic_menu_logout))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: MenuItem) {
            itemView.menu_item_title.text = itemView.context.getText(item.name)
            itemView.menu_item_icon.setImageResource(item.icon)
        }
    }
}