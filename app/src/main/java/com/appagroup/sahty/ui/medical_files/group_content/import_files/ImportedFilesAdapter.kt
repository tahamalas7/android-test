package com.appagroup.sahty.ui.medical_files.group_content.import_files

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.R
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.ItemLongClickListener
import com.appagroup.sahty.ui.listener.OnClick
import kotlinx.android.synthetic.main.item_imported_file.view.*

class ImportedFilesAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_IMPORT_FILE = 0
    private val TYPE_IMPORTED_FILE = 1
    private val files: MutableList<String> = mutableListOf()
    private lateinit var importMedicalFileClickListener: OnClick
    private lateinit var fileClickListener: ItemClickListener<String>
    private lateinit var fileLongClickListener: ItemLongClickListener<String>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_IMPORT_FILE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_import_new_file, parent, false)
                ImportFileHolder(view)
            }
            TYPE_IMPORTED_FILE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_imported_file, parent, false)
                ImportedFileHolder(view)
            }
            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1)
            TYPE_IMPORT_FILE
        else TYPE_IMPORTED_FILE
    }

    override fun getItemCount(): Int = files.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ImportedFileHolder -> {
                holder.bind(files[position])

                // set on file click listener
                holder.itemView.setOnClickListener {
                    fileClickListener.onClick(files[position])
                }

                // set on file long click listener
                holder.itemView.setOnLongClickListener {
                    fileLongClickListener.onLongClick(files[position], holder.itemView.imported_file_extension)
                    true
                }
            }

            is ImportFileHolder ->
                holder.itemView.setOnClickListener {
                    importMedicalFileClickListener.onClick()
                }
        }
    }

    fun getImportedFiles(): MutableList<String> =
            files

    fun addFile(file: String) {
        files.add(file)
        notifyDataSetChanged()
    }

    fun removeFile(file: String) {
        for (position in 0 until files.size)
            if (files[position] == file) {
                files.removeAt(position)
                notifyDataSetChanged()
                break
            }
    }

    fun setOnImportMedicalFileClickListener(importMedicalFileClickListener: OnClick) {
        this.importMedicalFileClickListener = importMedicalFileClickListener
    }

    fun setOnFileClickListener(fileClickListener: ItemClickListener<String>) {
        this.fileClickListener = fileClickListener
    }

    fun setOnFileLongClickListener(fileLongClickListener: ItemLongClickListener<String>) {
        this.fileLongClickListener = fileLongClickListener
    }

    private fun getFileName(filePath: String): String {
        return filePath.substring(filePath.lastIndexOf('/') + 1, filePath.lastIndexOf('.'))
    }

    inner class ImportFileHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class ImportedFileHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(file: String) {
            itemView.imported_file_name.text = getFileName(file)
//            itemView.imported_file_extension.text = getFileExtension(file)

            itemView.imported_file_icon.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    itemView.viewTreeObserver.removeOnGlobalLayoutListener(this)

                    // set file name width the same of file icon width
                    val nameParams = itemView.imported_file_name.layoutParams
                    nameParams.width = itemView.imported_file_icon.width
                    itemView.imported_file_name.layoutParams = nameParams
                    itemView.imported_file_name.setPadding(24, 0, 24, 0)

                    // set extension height and width the same of file icon
                    val extensionParams = itemView.imported_file_extension.layoutParams
                    extensionParams.height = itemView.imported_file_icon.height
                    extensionParams.width = itemView.imported_file_icon.width
                    itemView.imported_file_extension.layoutParams = extensionParams
                    itemView.imported_file_extension.setPadding(32, 32, 32, 32)

                    // set image height and width the same of file icon
                    val imageParams = itemView.imported_file_image.layoutParams
                    imageParams.height = itemView.imported_file_icon.height
                    imageParams.width = itemView.imported_file_icon.width
                    itemView.imported_file_image.layoutParams = imageParams
                    itemView.imported_file_image.setPadding(32, 32, 32, 32)
                }
            })

            Glide.with(itemView.context)
                    .load(file)
                    .into(itemView.imported_file_image)
        }
    }
}