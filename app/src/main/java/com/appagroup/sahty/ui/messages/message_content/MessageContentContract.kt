package com.appagroup.sahty.ui.messages.message_content

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.MessageReply
import com.appagroup.sahty.entity.Share

interface MessageContentContract {

    interface View : IBaseView {

        fun setListeners()

        fun changeLoaderVisibility(visibility: Int)

        fun hideRepliesProgressBar()

        fun changeShareButtonProgressBarVisibility(visibility: Int)

        fun updateReplies(replies: MutableList<MessageReply>)

        fun addReply(userId: Long, userName: String, userRole: Int, content: String)

        fun updateMedicalFileShares(list: MutableList<Share>?)
    }

    interface Presenter : IBasePresenter<View> {

        fun loadReplies(receiverId: Long)

        fun sendReply(receiverId: Long, content: String)

        fun getMedicalFileShares(receiverId: Long)

        fun updateShareMedicalFilesStatus(receiverId: Long, status: Int)

        fun getUserId(): Long

        fun getUserRole(): Int

        fun getUserName(): String
    }
}