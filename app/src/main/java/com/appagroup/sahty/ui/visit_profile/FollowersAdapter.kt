package com.appagroup.sahty.ui.visit_profile

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import kotlinx.android.synthetic.main.item_follow.view.*

class FollowersAdapter(private val typeface: Typeface, private val userId: Long)
    : RecyclerView.Adapter<FollowersAdapter.FollowersHolder>() {

    private lateinit var followButtonClickListener: FollowButtonClickListener
    private val followers: MutableList<Follow> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): FollowersAdapter.FollowersHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_follow, parent, false)
        return FollowersHolder(view)
    }

    override fun getItemCount(): Int = followers.size

    override fun onBindViewHolder(holder: FollowersAdapter.FollowersHolder, position: Int) {
        val follower = followers[position]
        holder.bind(follower)

        holder.itemView.follow_btn.setOnClickListener {
            if (follower.isFollowed) {
                followButtonClickListener.unfollowUser(follower)
                holder.setAsNotFollowed(follower)
            } else {
                followButtonClickListener.followUser(follower)
                holder.setAsFollowed(follower)
            }
        }
    }

    fun updateFollows(followers: MutableList<Follow>) {
        this.followers.addAll(followers)
        notifyDataSetChanged()
    }

    fun setOnFollowButtonClickListener(followButtonClickListener: FollowButtonClickListener) {
        this.followButtonClickListener = followButtonClickListener
    }

    inner class FollowersHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(follower: Follow) {
            itemView.follow_username.text = follower.name
            itemView.follow_username.typeface = typeface

            if (follower.image != null)
                Glide.with(itemView.context)
                        .load(BuildConfig.IMAGE + follower.image)
                        .into(itemView.follow_user_image)

            if (follower.isFollowed)
                setAsFollowed(follower)
            else setAsNotFollowed(follower)

            if (follower.id == userId)
                itemView.follow_btn.visibility = View.GONE
            else itemView.follow_btn.visibility = View.VISIBLE
        }

        fun setAsFollowed(follower: Follow) {
            itemView.follow_btn.setBackgroundResource(R.drawable.background_follow_button_on)
            itemView.follow_btn.text = itemView.context.getString(R.string.unfollow)
            follower.isFollowed = true
        }

        fun setAsNotFollowed(follower: Follow) {
            itemView.follow_btn.setBackgroundResource(R.drawable.background_follow_button_off)
            itemView.follow_btn.text = itemView.context.getString(R.string.follow)
            follower.isFollowed = false
        }
    }
}