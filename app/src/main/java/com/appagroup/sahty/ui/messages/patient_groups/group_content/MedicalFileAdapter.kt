package com.appagroup.sahty.ui.messages.patient_groups.group_content

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.MedicalFile
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_medical_file.view.*

class MedicalFileAdapter : RecyclerView.Adapter<MedicalFileAdapter.MedicalFileHolder>() {

    private lateinit var fileClickListener: ItemClickListener<MedicalFile>
    private val files: MutableList<MedicalFile> = mutableListOf()
    private var itemHeight = 0

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): MedicalFileHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_medical_file, parent, false)
        return MedicalFileHolder(view)
    }

    override fun getItemCount(): Int = files.size

    override fun onBindViewHolder(holder: MedicalFileHolder, position: Int) {
        val params = holder.itemView.layoutParams
        params.height = itemHeight
        holder.itemView.layoutParams = params

        holder.bind(files[position])
        holder.itemView.setOnClickListener {
            fileClickListener.onClick(files[position])
        }
    }

    fun updateFiles(files: MutableList<MedicalFile>) {
        this.files.addAll(files)
        notifyDataSetChanged()
    }

    fun updateItemHeight(itemHeight: Int) {
        this.itemHeight = itemHeight
    }

    fun setOnMedicalFileClickListener(fileClickListener: ItemClickListener<MedicalFile>) {
        this.fileClickListener = fileClickListener
    }

    inner class MedicalFileHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(file: MedicalFile) {
            itemView.medical_file_name.text = file.name
            itemView.medical_file_icon.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    itemView.viewTreeObserver.removeOnGlobalLayoutListener(this)

                    // set file name width the same of file icon width
                    val nameParams = itemView.medical_file_name.layoutParams
                    nameParams.width = itemView.medical_file_icon.width
                    itemView.medical_file_name.layoutParams = nameParams
                    itemView.medical_file_name.setPadding(24, 0, 24, 0)

                    // set extension height and width the same of file icon
                    val extensionParams = itemView.medical_file_extension.layoutParams
                    extensionParams.height = itemView.medical_file_icon.height
                    extensionParams.width = itemView.medical_file_icon.width
                    itemView.medical_file_extension.layoutParams = extensionParams
                    itemView.medical_file_extension.setPadding(32, 32, 32, 32)

                    // set image height and width the same of file icon
                    val imageParams = itemView.medical_file_image.layoutParams
                    imageParams.height = itemView.medical_file_icon.height
                    imageParams.width = itemView.medical_file_icon.width
                    itemView.medical_file_image.layoutParams = imageParams
                    itemView.medical_file_image.setPadding(32, 32, 32, 32)
                }
            })

            Glide.with(itemView.context)
                    .load(BuildConfig.IMAGE + file.url)
                    .into(itemView.medical_file_image)
        }
    }
}