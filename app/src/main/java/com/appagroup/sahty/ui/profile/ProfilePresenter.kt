package com.appagroup.sahty.ui.profile

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.UserProfile
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class ProfilePresenter(view: ProfileContract.View) : BasePresenter<ProfileContract.View>(view), ProfileContract.Presenter {

    private val TAG = "ProfilePresenter"

    override fun getUserProfile() {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.getUserProfile(), object : Observer<Response<BaseResponse<UserProfile>>> {
            override fun onComplete() {
                Log.d(TAG, "getUserProfile -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "getUserProfile -> onSubscribe")
                DisposableManager.addProfileDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<UserProfile>>) {
                Log.d(TAG, "getUserProfile -> onNext -> code = ${t.code()}")
                Log.d(TAG, "getUserProfile -> onNext -> body = ${t.body()}")

                view.changeProgressBarVisibility(View.GONE)

                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "getUserProfile -> onNext -> status = ${t.body()!!.status}")
                    view.updateProfile(t.body()!!.data!!)
                } else
                    view.showError(R.string.err_load_profile)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "getUserProfile -> onError -> error = ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
                view.showError(R.string.err_load_profile)
            }
        })
    }

    override fun getUsername(): String =
            dataManager.getUserName()

    override fun getUserImage(): String =
            dataManager.getUserImage()

    override fun getUserRole(): Int =
            dataManager.getUserRole()

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeProfile()
    }
}