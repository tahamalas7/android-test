package com.appagroup.sahty.ui.login.select_role

interface RoleSubmitListener {

    fun roleSubmittedSuccessfully(role: Int)

    fun dialogDismissed()
}