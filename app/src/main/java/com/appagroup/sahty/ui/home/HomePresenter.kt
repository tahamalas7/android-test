package com.appagroup.sahty.ui.home

import android.util.Log
import android.view.View
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Category
import com.appagroup.sahty.entity.Insights
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class HomePresenter(view: HomeContract.View) : BasePresenter<HomeContract.View>(view), HomeContract.Presenter {

    val TAG = "HomePresenter"

    override fun getCategories() {
        view.updateCategoriesProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.getCategories(), object : Observer<Response<BaseResponse<MutableList<Category>>>> {
            override fun onComplete() {
                Log.d(TAG, "getCategories -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                DisposableManager.addHomeDisposable(d)
                Log.d(TAG, "getCategories -> onSubscribe")
            }

            override fun onNext(t: Response<BaseResponse<MutableList<Category>>>) {
                Log.d(TAG, "getCategories -> onNext-> Response code = ${t.code()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    view.updateCategories(t.body()!!.data!!)
                    Log.d(TAG, "getCategories -> onNext-> categories count = ${t.body()!!.data!!.size}")

                }
                view.updateCategoriesProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                view.updateCategoriesProgressBarVisibility(View.GONE)
                Log.d(TAG, "getCategories -> onError-> message = ${e.message}")
            }
        })
    }

    override fun getInsights() {
        view.updateInsightsProgressBarVisibility(View.VISIBLE)
        subscribe(
                dataManager.getInsights(dataManager.getDeviceLatitude(), dataManager.getDeviceLongitude()),
                object : Observer<Response<BaseResponse<Insights>>> {
                    override fun onComplete() {
                        Log.d(TAG, "getInsights -> onComplete")
                    }

                    override fun onSubscribe(d: Disposable) {
                        Log.d(TAG, "getInsights -> onSubscribe")
                        DisposableManager.addHomeDisposable(d)
                    }

                    override fun onNext(t: Response<BaseResponse<Insights>>) {
                        Log.d(TAG, "getInsights -> onNext-> Response code = ${t.code()}")
                        Log.d(TAG, "getInsights -> onNext-> t.body() = ${t.body()}")

                        if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                            view.updateInsights(t.body()!!.data!!)
                            Log.d(TAG, "getInsights -> onNext-> insights = ${t.body()!!.data!!}")
                        }

                        view.updateInsightsProgressBarVisibility(View.GONE)
                    }

                    override fun onError(e: Throwable) {
                        Log.d(TAG, "getInsights -> onError-> message = ${e.message}")
                        view.updateInsightsProgressBarVisibility(View.GONE)
                    }
                })
    }

    override fun unsubscribe() {
        DisposableManager.disposeHome()
    }
}