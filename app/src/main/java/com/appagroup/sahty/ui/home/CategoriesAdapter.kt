package com.appagroup.sahty.ui.home

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Category
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_category.view.*

class CategoriesAdapter(val typeface: Typeface) : PagerAdapter() {

    private val categories: MutableList<Category> = mutableListOf()
    private lateinit var categoryClickListener: ItemClickListener<Category>

//    init {
//        categories.add(Category(0, "", ""))
//        categories.add(Category(0, "", ""))
//        categories.add(Category(0, "", ""))
//    }

    override fun getCount(): Int =
            categories.size

    override fun isViewFromObject(p0: View, p1: Any): Boolean =
            p0 == p1

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater = container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = layoutInflater.inflate(R.layout.item_category, container, false)
        val category = categories[position]

        itemView.category_name.text = category.name
        itemView.category_name.typeface = typeface

        if (category.image != null && category.image != "")
            Glide.with(container.context)
                    .load(BuildConfig.IMAGE + category.image)
                    .into(itemView.category_image)

        itemView.setOnClickListener {
            categoryClickListener.onClick(category)
        }

        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }

    fun updateCategories(categories: MutableList<Category>) {
        this.categories.clear()
        this.categories.addAll(categories)
        notifyDataSetChanged()
    }

    fun setOnCategoryClickListener(categoryClickListener: ItemClickListener<Category>) {
        this.categoryClickListener = categoryClickListener
    }

    fun getCategories(): MutableList<Category> = categories
}