package com.appagroup.sahty.ui.post_story

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface PostStoryContract {

    interface View : IBaseView {

        fun setListeners()

        fun checkFieldsValidate(): Boolean

        fun storyPostedSuccessfully()

        fun showProgressDialog()

        fun hideProgressDialog()
    }

    interface Presenter : IBasePresenter<View> {

        fun postStory(
                enTitle: RequestBody,
                enContent: RequestBody,
                arTitle: RequestBody,
                arContent: RequestBody,
                categoryId: RequestBody,
                image: MultipartBody.Part)

        fun getUserName(): String

        fun getUserImage(): String
    }
}