package com.appagroup.sahty.ui.articles

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Article

interface ArticlesContract {

    interface View : IBaseView {

        fun setListeners()

        fun updateArticles(articles: MutableList<Article>)

        fun changeProgressBarVisibility(visibility: Int)

        fun changeLoaderVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun loadArticles(categoryId: Long, filter: Int)

        fun getUserId(): Long

        fun resetPaginate()
    }
}