package com.appagroup.sahty.ui.facilities.pharmacies

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Pharmacy

interface PharmaciesContract {

    interface View : IBaseView {

        fun setListeners()

        fun updatePharmacies(pharmacies: MutableList<Pharmacy>)

        fun changeProgressBarVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun loadPharmacies()
    }
}