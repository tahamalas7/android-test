package com.appagroup.sahty.ui.medical_files.group_content

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.MedicalFile

interface MedicalFileGroupContentContract {

    interface View : IBaseView {

        fun setListeners()

        fun updateMedicalFiles(files: MutableList<MedicalFile>)

        fun changeProgressBarVisibility(visibility: Int)

        fun removeDeletedFile(fileId: Long)

        fun showDeleteProgressDialog()

        fun dismissDeleteProgressDialog()
    }

    interface Presenter : IBasePresenter<View> {

        fun getMedicalFiles(groupId: Long)

        fun deleteFile(fileId: Long)
    }
}