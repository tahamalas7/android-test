package com.appagroup.sahty.ui.saved.saved_videos

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Video

interface SavedVideosContract {

    interface View : IBaseView {

        fun setListeners()

        fun updateArticles(videos: MutableList<Video>)

        fun changeProgressBarVisibility(visibility: Int)

        fun changeLoaderVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun getSavedVideos()
    }
}