package com.appagroup.sahty.ui.saved.saved_stories

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Story

interface SavedStoriesContract {

    interface View : IBaseView {

        fun setListeners()

        fun updateStories(articles: MutableList<Story>)

        fun changeProgressBarVisibility(visibility: Int)

        fun changeLoaderVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun getSavedStories()
    }
}