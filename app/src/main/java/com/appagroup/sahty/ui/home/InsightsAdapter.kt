package com.appagroup.sahty.ui.home

import android.content.Context
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.*
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.facebook.common.internal.ImmutableList
import kotlinx.android.synthetic.main.item_insights_article.view.*
import kotlinx.android.synthetic.main.item_insights_video.view.*

class InsightsAdapter(private val typeFaceBold: Typeface, private val typeFaceRegular: Typeface) : PagerAdapter() {

    private val items: MutableList<Any> = mutableListOf()

    private lateinit var itemView: View

    private lateinit var followButtonClickListener: FollowButtonClickListener

    private lateinit var videoClickListener: ItemClickListener<Video>
    private lateinit var articleClickListener: ItemClickListener<Article>
    private lateinit var pharmacyClickListener: ItemClickListener<Pharmacy>
    private lateinit var hospitalClickListener: ItemClickListener<Hospital>


    override fun isViewFromObject(p0: View, p1: Any): Boolean =
            p0 == p1

    override fun getCount(): Int = items.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater = container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        when (items[position]) {
            is Video -> {
                itemView = layoutInflater.inflate(R.layout.item_insights_video, container, false)
                initLastVideo(container, items[position] as Video)
            }

            is Poll -> {
                itemView = layoutInflater.inflate(R.layout.item_insights_video, container, false)
                initLastVideo(container, items[position] as Video)
            }

            is Pharmacy -> {
                itemView = layoutInflater.inflate(R.layout.item_insights_medical_center, container, false)
                initPharmacy(container, items[position] as Pharmacy)
            }

            is Hospital -> {
                itemView = layoutInflater.inflate(R.layout.item_insights_medical_center, container, false)
                initHospital(container, items[position] as Hospital)
            }

            is Article -> {
                itemView = layoutInflater.inflate(R.layout.item_insights_article, container, false)
                initArticle(container, items[position] as Article)
            }
        }

        container.addView(itemView)
        return itemView
    }

    private fun initArticle(container: ViewGroup, article: Article) {
        itemView.insights_article_title.typeface = typeFaceBold
        itemView.insights_article_username.typeface = typeFaceRegular

        itemView.insights_article_title.text = article.title
        itemView.insights_article_username.text = "${container.context.getString(R.string.by)} ${article.writer.name}"
//        if (article.media != null)
//            Glide.with(itemView.context)
//                    .load()
        loadImage(article.media, itemView.insights_article_image, itemView.insights_article_image_progressBar)

        if (article.writer.isFollowed)
            itemView.insights_article_follow_btn.setImageResource(R.drawable.ic_user_followed)
        else
            itemView.insights_article_follow_btn.setImageResource(R.drawable.ic_follow_user)

        setOnFollowClickListener(itemView.insights_article_follow_btn, article.writer)

        itemView.setOnClickListener {
            articleClickListener.onClick(article)
        }
    }

    private fun initLastVideo(container: ViewGroup, video: Video) {
        itemView.insights_video_title.typeface = typeFaceBold
        itemView.insights_video_username.typeface = typeFaceRegular

        itemView.insights_video_title.text = video.title
        itemView.insights_video_username.text = "${container.context.getString(R.string.by)} ${video.writer.name}"
        if (video.thumbnail != null) {
            Glide.with(itemView).load(BuildConfig.IMAGE + video.thumbnail).into(itemView.insights_video_thumbnail)
            Log.d("sssssssss", "thumbnail = ${BuildConfig.IMAGE + video.thumbnail}")
        }

        if (video.writer.isFollowed)
            itemView.insights_video_follow_btn.setImageResource(R.drawable.ic_user_followed)
        else
            itemView.insights_video_follow_btn.setImageResource(R.drawable.ic_follow_user)

        setOnFollowClickListener(itemView.insights_video_follow_btn, video.writer)

        itemView.setOnClickListener {
            videoClickListener.onClick(video)
        }
    }

    private fun initPharmacy(container: ViewGroup, pharmacy: Pharmacy) {

        itemView.setOnClickListener {
            pharmacyClickListener.onClick(pharmacy)
        }
    }

    private fun initHospital(container: ViewGroup, hospital: Hospital) {

        itemView.setOnClickListener {
            hospitalClickListener.onClick(hospital)
        }
    }

    private fun loadImage(media: Media?, image: ImageView, progressBar: ProgressBar) {
        if (media != null)
            Log.d("aaaaaaa", BuildConfig.IMAGE + media.url)
        if (media != null)
            Glide.with(itemView)
                    .load(BuildConfig.IMAGE +
                            if (media.type.toInt() == 2)
                                media.thumbnail
                            else
                                media.url)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?,
                                                  model: Any?,
                                                  target: Target<Drawable>?,
                                                  isFirstResource: Boolean): Boolean {
                            progressBar.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?,
                                                     model: Any?,
                                                     target: Target<Drawable>?,
                                                     dataSource: DataSource?,
                                                     isFirstResource: Boolean): Boolean {
                            progressBar.visibility = View.GONE
                            return false
                        }
                    })
                    .into(image)
    }

    private fun setOnFollowClickListener(followBtn: ImageView, writer: Writer) {
        followBtn.setOnClickListener {
            if (writer.isFollowed) {
                updateFollowIcon(writer.id, false)
                followButtonClickListener.unfollowUser(Follow(
                        writer.id,
                        writer.name,
                        writer.image,
                        true))
                followBtn.setImageResource(R.drawable.ic_follow_user)
            } else {
                updateFollowIcon(writer.id, true)
                followButtonClickListener.followUser(Follow(
                        writer.id,
                        writer.name,
                        writer.image,
                        true))
                followBtn.setImageResource(R.drawable.ic_user_followed)
            }
        }
    }

    private fun updateFollowIcon(writerId: Long, followStatus: Boolean) {
        for (item in items) {
            when (item) {
                is Article -> if (item.writer.id == writerId) item.writer.isFollowed = followStatus
                is Video -> if (item.writer.id == writerId) item.writer.isFollowed = followStatus
            }
        }
        notifyDataSetChanged()
    }

    fun updateInsights(insights: Insights) {
        items.clear()
        if(insights.lastStory != null)
            items.add(insights.lastStory)
        if(insights.lastVideo != null)
            items.add(insights.lastVideo)
//        if(insights.interactivePoll != null)
//            items.add(insights.interactivePoll)
        if(insights.nearestHospital != null)
            items.add(insights.nearestHospital)
        if(insights.nearestPharmacy != null)
            items.add(insights.nearestPharmacy)
        items.addAll(insights.topArticles)
        notifyDataSetChanged()
        Log.d("updateInsights", "updateInsights -> item size = ${items.size}")
    }

    fun updateInsights(insights: ImmutableList<Any>) {
        items.clear()
        items.addAll(insights)
        notifyDataSetChanged()
        Log.d("updateInsights", "updateInsights -> item size = ${items.size}")
    }

    fun getInsights(): MutableList<Any> = items

    fun setVideoClickListener(videoClickListener: ItemClickListener<Video>) {
        this.videoClickListener = videoClickListener
    }

    fun setArticleClickListener(articleClickListener: ItemClickListener<Article>) {
        this.articleClickListener = articleClickListener
    }

    fun setPharmacyClickListener(pharmacyClickListener: ItemClickListener<Pharmacy>) {
        this.pharmacyClickListener = pharmacyClickListener
    }

    fun setHospitalClickListener(hospitalClickListener: ItemClickListener<Hospital>) {
        this.hospitalClickListener = hospitalClickListener
    }

    fun setFollowButtonClickListener(followButtonClickListener: FollowButtonClickListener) {
        this.followButtonClickListener = followButtonClickListener
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}