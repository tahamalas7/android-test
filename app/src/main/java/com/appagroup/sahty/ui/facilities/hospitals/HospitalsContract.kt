package com.appagroup.sahty.ui.facilities.hospitals

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Hospital

interface HospitalsContract {

    interface View : IBaseView {

        fun setListeners()

        fun updateHospitals(hospitals: MutableList<Hospital>)

        fun changeProgressBarVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun loadHospitals()
    }
}