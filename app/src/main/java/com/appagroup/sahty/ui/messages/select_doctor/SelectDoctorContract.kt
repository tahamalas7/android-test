package com.appagroup.sahty.ui.messages.select_doctor

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Doctor

interface SelectDoctorContract {

    interface View : IBaseView {

        fun setListeners()

        fun updateDoctors(doctors: MutableList<Doctor>)

        fun changeProgressBarVisibility(visibility: Int)

        fun changeLoaderVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun searchForDoctors(keyWord: String, filter: Int)

        fun resetPageCounter()

        fun getUserId(): Long
    }
}