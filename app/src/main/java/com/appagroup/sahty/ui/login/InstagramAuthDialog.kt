package com.appagroup.sahty.ui.login

import android.app.Activity
import android.app.Dialog
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.appagroup.sahty.R
import com.appagroup.sahty.ui.login.insta.BASE_URL
import com.appagroup.sahty.ui.login.insta.CLIENT_ID
import com.appagroup.sahty.ui.login.insta.REDIRECT_URL
import im.delight.android.webview.AdvancedWebView
import kotlinx.android.synthetic.main.aaaa_insta_dialog.*

class InstagramAuthDialog(val activity: Activity) : Dialog(activity), AdvancedWebView.Listener {

    private lateinit var instagramAuthListener: InstagramAuthListener
    var access_token = ""
    var authComplete = false
    private val TAG = "INSTAGRAM"
    private val url = BASE_URL + "oauth/authorize/?client_id=" + CLIENT_ID + "&redirect_uri=" + REDIRECT_URL + "&response_type=token" + "&display=touch&scope=public_content"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.d(TAG, "onCreate -> url = $url")
        setContentView(R.layout.aaaa_insta_dialog)
        initializeWebView()
    }

    private fun initializeWebView() {
        instagram_auth_webView.setListener(activity, this)
        instagram_auth_webView.settings.allowContentAccess = true
        instagram_auth_webView.settings.javaScriptEnabled = true
        instagram_auth_webView.settings.domStorageEnabled = true
        instagram_auth_webView.settings.useWideViewPort = true
        instagram_auth_webView.settings.loadWithOverviewMode = true
        instagram_auth_webView.loadUrl(url)
    }

    override fun onPageFinished(url: String?) {
        Log.d(TAG, "initializeWebView -> onPageFinished -> url = $url")
        // need to check here
        if (url != null && url.contains("#access_token") && !authComplete) {
            val uri = Uri.parse(url)
            access_token = uri.encodedFragment ?: ""
            Log.d(TAG, "initializeWebView -> onPageFinished -> access_token = $access_token")

            // get the whole token after '=' sign
            access_token = access_token.substring(access_token.lastIndexOf('=') + 1)
            Log.d(TAG, "initializeWebView -> onPageFinished -> access_token after substring = $access_token")

            authComplete = true
            instagramAuthListener.onAuthTokenReceived(access_token)
            dismiss()
        } else {
            Log.d(TAG, "initializeWebView -> onPageFinished -> getting error fetching access token!")
//            dismiss()
        }
    }

    fun setOnInstagramAuthTokenReceived(instagramAuthListener: InstagramAuthListener) {
        this.instagramAuthListener = instagramAuthListener
    }

    override fun onPageError(errorCode: Int, description: String?, failingUrl: String?) {
        Log.d(TAG, "onPageError -> code = $errorCode")
        Log.d(TAG, "onPageError -> description = $description")
        Log.d(TAG, "onPageError -> failingUrl = $failingUrl")
    }

    override fun onDownloadRequested(url: String?, suggestedFilename: String?, mimeType: String?, contentLength: Long, contentDisposition: String?, userAgent: String?) {
    }

    override fun onExternalPageRequest(url: String?) {
    }

    override fun onPageStarted(url: String?, favicon: Bitmap?) {
    }
}