package com.appagroup.sahty.ui.facilities.hospitals

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Hospital
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_hospital.view.*

class HospitalAdapter : RecyclerView.Adapter<HospitalAdapter.HospitalHolder>() {

    private var itemHeight: Int = 400
    private val hospitals: MutableList<Hospital> = mutableListOf()
    private lateinit var hospitalClickListener: ItemClickListener<Hospital>
    private lateinit var bottomReachListener: BottomReachListener

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): HospitalHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_hospital, parent, false)
        return HospitalHolder(view)
    }

    override fun getItemCount(): Int = hospitals.size

    override fun onBindViewHolder(holder: HospitalHolder, position: Int) {
        val params = holder.itemView.layoutParams
        params.height = itemHeight
        holder.itemView.layoutParams = params

        val hospital = hospitals[position]
        holder.bind(hospital)

        holder.itemView.setOnClickListener {
            hospitalClickListener.onClick(hospital)
        }

        if (position > itemCount - 2)
            bottomReachListener.onBottomReach()
    }

    fun updateHospitals(hospitals: MutableList<Hospital>) {
        this.hospitals.addAll(hospitals)
        notifyDataSetChanged()
    }

    fun updateItemHeight(itemHeight: Int) {
        this.itemHeight = itemHeight
    }

    fun setOnHospitalClickListener(hospitalClickListener: ItemClickListener<Hospital>) {
        this.hospitalClickListener = hospitalClickListener
    }

    fun setOnBottomReachListener(bottomReachListener: BottomReachListener) {
        this.bottomReachListener = bottomReachListener
    }

    inner class HospitalHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(hospital: Hospital) {
            itemView.hospital_name.text = hospital.name
            itemView.hospital_location.text = hospital.location
            itemView.hospital_rate.text = if (hospital.rate != null)
                hospital.rate.toString()
            else
                0F.toString()

            if (hospital.image != null)
                Glide.with(itemView)
                        .load(BuildConfig.IMAGE + hospital.image)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?,
                                                      model: Any?,
                                                      target: Target<Drawable>?,
                                                      isFirstResource: Boolean): Boolean {
                                itemView.hospital_image_progressBar.visibility = View.GONE
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?,
                                                         model: Any?,
                                                         target: Target<Drawable>?,
                                                         dataSource: DataSource?,
                                                         isFirstResource: Boolean): Boolean {
                                itemView.hospital_image_progressBar.visibility = View.GONE
                                return false
                            }
                        })
                        .into(itemView.hospital_image)
            else
                itemView.hospital_image_progressBar.visibility = View.GONE
        }
    }
}