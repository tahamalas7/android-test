package com.appagroup.sahty.ui.saved

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.appagroup.sahty.R
import com.appagroup.sahty.ui.saved.saved_articles.SavedArticlesActivity
import com.appagroup.sahty.ui.saved.saved_stories.SavedStoriesActivity
import com.appagroup.sahty.ui.saved.saved_videos.SavedVideosActivity
import kotlinx.android.synthetic.main.fragment_saved.*

class SavedFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_saved, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
    }

    private fun setListeners() {
        saved_back_arrow.setOnClickListener {
            activity!!.onBackPressed()
        }

        saved_articles_card.setOnClickListener {
            val intent = Intent(activity, SavedArticlesActivity::class.java)
            startActivity(intent)
        }

        saved_videos_card.setOnClickListener {
            val intent = Intent(activity, SavedVideosActivity::class.java)
            startActivity(intent)
        }

        saved_stories_card.setOnClickListener {
            val intent = Intent(activity, SavedStoriesActivity::class.java)
            startActivity(intent)
        }
    }

    companion object {
        val TAG = "SavedFragment"
        fun getInstance(): SavedFragment =
                SavedFragment()
    }
}