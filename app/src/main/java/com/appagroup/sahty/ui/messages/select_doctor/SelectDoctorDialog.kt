package com.appagroup.sahty.ui.messages.select_doctor

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseDialog
import com.appagroup.sahty.custom.CustomLinearLayoutManager
import com.appagroup.sahty.entity.Doctor
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.utils.DoctorSearch
import kotlinx.android.synthetic.main.dialog_select_doctor.*

class SelectDoctorDialog : BaseDialog<SelectDoctorContract.Presenter>(), SelectDoctorContract.View {

    private lateinit var selectDoctorListener: ItemClickListener<Doctor>
    private var specialtyFilterEnabled = true
    private var nameFilterEnabled = true
    private val adapter = DoctorAdapter()

    override fun instantiatePresenter(): SelectDoctorContract.Presenter = SelectDoctorPresenter(this)

    override fun getLayout(): Int = R.layout.dialog_select_doctor

    override fun initViews(view: View) {
        val params = select_doctor_recyclerView.layoutParams
        params.height = (0.3 * activity!!.windowManager.defaultDisplay.height).toInt()

//        select_doctor_recyclerView.layoutManager = LinearLayoutManager(activity)
        select_doctor_recyclerView.layoutManager = CustomLinearLayoutManager(activity!!)
        select_doctor_recyclerView.adapter = adapter
        select_doctor_recyclerView.layoutParams = params

        setListeners()
    }

    override fun setListeners() {
        select_doctor_name_search_filter.setOnClickListener {
            when {
                nameFilterEnabled -> {
                    // if doctors-filter enabled, just turn it off
                    select_doctor_name_search_filter.setBackgroundResource(R.drawable.back_button_disabled)
                    nameFilterEnabled = false
                }
                !nameFilterEnabled && specialtyFilterEnabled -> {
                    // if doctors-filter disabled and specialisations-filter enabled, switch between them
                    select_doctor_name_search_filter.setBackgroundResource(R.drawable.back_button_enabled)
                    select_doctor_specialty_search_filter.setBackgroundResource(R.drawable.back_button_disabled)
                    nameFilterEnabled = true
                    specialtyFilterEnabled = false
                }
                else -> {
                    // else turn doctors-filter enabled on
                    select_doctor_name_search_filter.setBackgroundResource(R.drawable.back_button_enabled)
                    nameFilterEnabled = true
                }
            }
            adapter.removeItems()
            presenter.resetPageCounter()
            getDoctors()
        }

        select_doctor_specialty_search_filter.setOnClickListener {
            when {
                specialtyFilterEnabled -> {
                    // if specialisations-filter enabled, just turn it off
                    select_doctor_specialty_search_filter.setBackgroundResource(R.drawable.back_button_disabled)
                    specialtyFilterEnabled = false
                }
                nameFilterEnabled -> {
                    // if specialisations-filter disabled and doctors-filter enabled, switch between them
                    select_doctor_name_search_filter.setBackgroundResource(R.drawable.back_button_disabled)
                    select_doctor_specialty_search_filter.setBackgroundResource(R.drawable.back_button_enabled)
                    nameFilterEnabled = false
                    specialtyFilterEnabled = true
                }
                else -> {
                    // else turn specialisations-filter enabled on
                    select_doctor_specialty_search_filter.setBackgroundResource(R.drawable.back_button_enabled)
                    specialtyFilterEnabled = true
                }
            }
            adapter.removeItems()
            presenter.resetPageCounter()
            getDoctors()
        }

        select_doctor_search_bar.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                select_doctor_search_filter_bar.visibility = View.VISIBLE
                select_doctor_search_hint.visibility = View.GONE
            }
        }

        select_doctor_search_bar.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                adapter.removeItems()
                presenter.resetPageCounter()
                if (s.toString().trim().isEmpty())
                    select_doctor_empty_search.visibility = View.VISIBLE
                else {

                    select_doctor_empty_search.visibility = View.GONE
                }
                getDoctors()
            }
        })

        adapter.setOnDoctorClickListener(object : ItemClickListener<Doctor> {
            override fun onClick(item: Doctor) {
                if (item.id != presenter.getUserId()) {
                    selectDoctorListener.onClick(item)
                    dialog.dismiss()
                }
            }
        })

        adapter.setOnLastDoctorReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                getDoctors()
            }
        })
    }

    private fun getDoctors() {
        val searchKey = select_doctor_search_bar.text.toString().trim()
        if (searchKey.isNotEmpty()) {
            presenter.searchForDoctors(
                    searchKey,
                    if (nameFilterEnabled && !specialtyFilterEnabled)
                        DoctorSearch.NAME
                    else if (!nameFilterEnabled && specialtyFilterEnabled)
                        DoctorSearch.SPECIALTY
                    else
                        DoctorSearch.ALL
            )
        }
    }

    override fun updateDoctors(doctors: MutableList<Doctor>) {
        adapter.updateDoctors(doctors)
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        select_doctor_progressBar.visibility = visibility
    }

    override fun changeLoaderVisibility(visibility: Int) {
        adapter.updateLoaderVisibility(visibility)
    }

    fun setOnSelectDoctorListener(selectDoctorListener: ItemClickListener<Doctor>) {
        this.selectDoctorListener = selectDoctorListener
    }

    companion object {
        val TAG = "SelectDoctorDialog"
        fun getInstance(): SelectDoctorDialog =
                SelectDoctorDialog()
    }
}