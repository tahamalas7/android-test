package com.appagroup.sahty.ui.poll

import android.animation.ObjectAnimator
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Poll
import com.appagroup.sahty.entity.PollOption
import kotlinx.android.synthetic.main.item_poll_option.view.*

class OptionsAdapter(private val poll: Poll, private val enableAnimation: Boolean)
    : RecyclerView.Adapter<OptionsAdapter.OptionsHolder>() {

    private lateinit var optionClickListener: PollOptionClickListener

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): OptionsAdapter.OptionsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_poll_option, parent, false)
        return OptionsHolder(view)
    }

    override fun getItemCount(): Int = poll.options.size

    override fun onBindViewHolder(holder: OptionsAdapter.OptionsHolder, position: Int) {
        holder.bind(poll.options[position])

        holder.itemView.setOnClickListener {
            // need to check if option has been selected
            if (poll.selectedOptionId != poll.options[position].id)
                optionClickListener.onVote(poll.options[position])
            else
                optionClickListener.onCancelingVote(poll.options[position])
        }
    }

    fun setOnOptionClickListener(optionClickListener: PollOptionClickListener) {
        this.optionClickListener = optionClickListener
    }

    inner class OptionsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(option: PollOption) {
            itemView.option_text.text = option.name
            itemView.option_vote_count.text = option.voteCount.toString()

            // decrease text size when 'voteCount' digits more than 3
            if (option.voteCount.toString().length > 3)
                itemView.option_vote_count.textSize = 16F

            // set votes percentage
            val percentage =
                    if (poll.totalVotes > 0)
                        ((option.voteCount.toFloat() / poll.totalVotes.toFloat()) * 100F).toInt()
                    else poll.totalVotes

            // when "enableAnimation" is true, Poll options will appear with animation
            if (enableAnimation) {
                val progressAnimator = ObjectAnimator.ofInt(
                        itemView.option_ProgressBar,
                        if (poll.beenVoted && poll.selectedOptionId == option.id)
                            "secondaryProgress"
                        else
                            "progress",
                        0,
                        percentage
                )
                progressAnimator.duration = 300
                progressAnimator.interpolator = LinearInterpolator()
                progressAnimator.start()
            } else {
                if (poll.beenVoted && poll.selectedOptionId == option.id)
                    itemView.option_ProgressBar.secondaryProgress = percentage
                else
                    itemView.option_ProgressBar.progress = percentage
            }

        }
    }
}
