package com.appagroup.sahty.ui.facilities.pharmacies

import android.view.View
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.entity.Pharmacy
import com.appagroup.sahty.ui.facilities.pharmacies.pharmacy_details.PharmacyDetailsFragment
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_facilities_pharmacies.*

class PharmaciesFragment : BaseFragment<PharmaciesContract.Presenter>(), PharmaciesContract.View {

    private val adapter = PharmacyAdapter()

    override fun instantiatePresenter(): PharmaciesContract.Presenter = PharmaciesPresenter(this)

    override fun getLayout(): Int = R.layout.item_facilities_pharmacies

    override fun initViews(view: View) {
        pharmacies_recyclerView.layoutManager = LinearLayoutManager(activity)
        pharmacies_recyclerView.adapter = adapter
        pharmacies_recyclerView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                adapter.updateItemHeight(pharmacies_recyclerView.height/2)
            }
        })

        presenter.loadPharmacies()
        setListeners()
    }

    override fun setListeners() {
        adapter.setOnPharmaciesClickListener(object : ItemClickListener<Pharmacy> {
            override fun onClick(item: Pharmacy) {
                val pharmacyDetailsFragment = PharmacyDetailsFragment.newInstance(item.id)
                pharmacyDetailsFragment.show(activity!!.supportFragmentManager, PharmacyDetailsFragment.TAG)
            }
        })

        adapter.setOnBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                presenter.loadPharmacies()
            }
        })
    }

    override fun updatePharmacies(pharmacies: MutableList<Pharmacy>) {
        adapter.updatePharmacies(pharmacies)
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        pharmacies_progressBar.visibility = visibility
    }

    companion object {
        val TAG = "PharmaciesFragment"
        fun getInstance(): PharmaciesFragment = PharmaciesFragment()
    }
}