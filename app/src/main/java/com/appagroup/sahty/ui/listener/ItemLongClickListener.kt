package com.appagroup.sahty.ui.listener

import android.view.View

interface ItemLongClickListener<T> {

    fun onLongClick(item: T, view: View)
}