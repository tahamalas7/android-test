package com.appagroup.sahty.ui.medical_files

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.MedicalFileGroup
import com.appagroup.sahty.utils.hideKeyboard
import kotlinx.android.synthetic.main.dialog_craete_medical_file_group.*

class CreateMedicalFileGroupDialog : DialogFragment() {

    private var medicalFileGroup: MedicalFileGroup? = null
    private var createdSuccessfully = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog_Alert)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_craete_medical_file_group, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setListeners()
    }

    private fun setListeners() {
        create_medical_file_group_btn.setOnClickListener {
            when {
                create_medical_file_group_name.text.toString().isEmpty() -> {
                    create_medical_file_group_error_message.text = getString(R.string.error_empty_field)
                    create_medical_file_group_error.visibility = View.VISIBLE
                }
                create_medical_file_group_name.text.toString().length > 80 -> {
                    create_medical_file_group_error_message.text = getString(R.string.error_long_medical_file_group_name)
                    create_medical_file_group_error.visibility = View.VISIBLE
                }
                else -> {
                    hideKeyboard()
                    Log.d(TAG, "group name = ${create_medical_file_group_name.text.trim()}")
                    this.medicalFileGroup = MedicalFileGroup(-1, create_medical_file_group_name.text.toString().trim())
                    createdSuccessfully = true
                    dialog.dismiss()
                }
            }
        }

        create_medical_file_group_name.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                if (create_medical_file_group_error.visibility == View.VISIBLE)
                    create_medical_file_group_error.visibility = View.GONE
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
        })
    }

    override fun onPause() {
        super.onPause()
        val intent = Intent()
        val bundle = Bundle()
        bundle.putSerializable(MEDICAL_FILE_GROUP, medicalFileGroup)
        intent.putExtra(CREATED_SUCCESSFULLY, createdSuccessfully)
        intent.putExtra(MEDICAL_FILE_GROUP_BUNDLE, bundle)
        targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, intent)
    }

    companion object {
        val MEDICAL_FILE_GROUP = "MEDICAL_FILE_GROUP"
        val MEDICAL_FILE_GROUP_BUNDLE = "MEDICAL_FILE_GROUP_BUNDLE"
        val CREATED_SUCCESSFULLY = "RATED_SUCCESSFULLY"
        val TAG = "RatingDialog"

        fun getInstance(): CreateMedicalFileGroupDialog =
                CreateMedicalFileGroupDialog()
    }

}