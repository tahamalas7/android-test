package com.appagroup.sahty.ui.medical_files.group_content

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.GridLayoutManager
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.entity.MedicalFile
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.ItemLongClickListener
import com.appagroup.sahty.ui.listener.OnClick
import com.appagroup.sahty.ui.medical_files.group_content.import_files.ImportFilesActivity
import com.appagroup.sahty.utils.FileHandler
import kotlinx.android.synthetic.main.fragment_medical_file_group_content.*
import com.appagroup.sahty.ui.medical_files.group_content.MedicalFileGroupContentContract as Contract


class MedicalFileGroupContentFragment : BaseFragment<Contract.Presenter>(), Contract.View {

    private lateinit var progressDialog: ProgressDialog
    private val fileAdapter = MedicalFileAdapter()
    private var groupId = -1L
    private var groupTitle = ""

    override fun instantiatePresenter(): Contract.Presenter = MedicalFileGroupContentPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_medical_file_group_content

    override fun initViews(view: View) {
        groupId = arguments!!.getLong(GROUP_ID)
        groupTitle = arguments!!.getString(GROUP_TITLE)!!
        Log.d(TAG, "Group id = $groupId")
        Log.d(TAG, "Group name = $groupTitle")
        group_content_title.text = groupTitle
        group_files_recyclerView.layoutManager = GridLayoutManager(activity, 3)
        group_files_recyclerView.adapter = fileAdapter
        group_files_recyclerView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                fileAdapter.updateImportFileItemHeight(group_files_recyclerView.height / 3)
            }
        })

        if (groupId == -1L)
            group_content_empty_message.visibility = View.VISIBLE
        else
            presenter.getMedicalFiles(groupId)
        setListeners()
    }

    override fun setListeners() {
        group_content_import_files.setOnClickListener {
            startImportFilesActivity()
        }

        group_content_back_arrow.setOnClickListener {
            activity!!.onBackPressed()
        }

        fileAdapter.setOnAddFileGroupClickListener(object : OnClick {
            override fun onClick() {
                startImportFilesActivity()
            }
        })

        fileAdapter.setOnFileGroupClickListener(object : ItemClickListener<MedicalFile> {
            @SuppressLint("SetJavaScriptEnabled")
            override fun onClick(item: MedicalFile) {
                Log.d("presenter", "click -> file URL = ${item.url}")
                val intent = FileHandler.getFileIntent(BuildConfig.IMAGE + item.url)
                try {
                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.d(TAG, "error open file = ${e.message}")
                }
            }
        })

        fileAdapter.setOnFileLongClickListener(object : ItemLongClickListener<Long> {
            override fun onLongClick(item: Long, view: View) {
                showDeletePopUpMenu(item, view)
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK)
            when (requestCode) {
                UPLOAD_REQUEST_CODE ->
                    if (data != null && data.getBooleanExtra(ImportFilesActivity.UPLOADED_SUCCESSFULLY, false)) {
                        val id = data.getLongExtra(ImportFilesActivity.GROUP_ID, -1L)
                        if (id != -1L)
                            groupId = id
                        fileAdapter.removeAllItems()
                        presenter.getMedicalFiles(groupId)
                    }
            }
    }

    private fun showDeletePopUpMenu(groupId: Long, view: View) {
        val popupMenu = PopupMenu(activity!!, view)
        popupMenu.inflate(R.menu.delete_menu)
        popupMenu.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem?): Boolean {
                AlertDialog.Builder(this@MedicalFileGroupContentFragment.context)
                        .setMessage(getString(R.string.warning_delete_file))
                        .setPositiveButton(getString(R.string.delete)) { _, _ ->
                            if (groupId != -1L)
                                presenter.deleteFile(groupId)
                            else
                                removeDeletedFile(groupId)
                        }
                        .setNegativeButton(getString(R.string.cancel)) { dialog, _ ->
                            dialog.dismiss()
                        }
                        .show()
                popupMenu.dismiss()
                return true
            }
        })
        popupMenu.show()
    }

    override fun removeDeletedFile(fileId: Long) {
        fileAdapter.removeSelectedMedicalFile(fileId)
        if (fileAdapter.itemCount == 0)
            group_content_empty_message.visibility = View.VISIBLE
    }

    override fun showDeleteProgressDialog() {
        progressDialog = ProgressDialog(activity)
        progressDialog.setMessage(getString(R.string.wait_until_delete_file))
        progressDialog.show()
    }

    override fun dismissDeleteProgressDialog() {
        progressDialog.dismiss()
    }

    override fun updateMedicalFiles(files: MutableList<MedicalFile>) {
        if (files.size > 0) {
            fileAdapter.updateMedicalFileGroups(files)
            group_content_empty_message.visibility = View.GONE
        } else
            group_content_empty_message.visibility = View.VISIBLE
    }

    private fun startImportFilesActivity() {
        val intent = Intent(activity, ImportFilesActivity::class.java)
        intent.putExtra(ImportFilesActivity.GROUP_ID, groupId)
        intent.putExtra(ImportFilesActivity.GROUP_TITLE, groupTitle)
        activity!!.startActivityForResult(intent, UPLOAD_REQUEST_CODE)
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        group_content_progressBar.visibility = visibility
    }

    companion object {
        val TAG = "GroupContentFragment"
        val GROUP_ID = "GROUP_ID"
        val GROUP_TITLE = "GROUP_TITLE"
        val UPLOAD_REQUEST_CODE = 104

        fun getInstance(): MedicalFileGroupContentFragment =
                MedicalFileGroupContentFragment()
    }
}