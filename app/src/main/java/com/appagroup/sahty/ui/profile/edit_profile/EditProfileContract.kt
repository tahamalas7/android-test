package com.appagroup.sahty.ui.profile.edit_profile

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import okhttp3.MultipartBody

interface EditProfileContract {

    interface View : IBaseView {

        fun setListeners()

        fun pictureChangedSuccessfully(image: String)

        fun passwordChangedSuccessfully()

        fun emailChangedSuccessfully()

        fun setInvalidPasswordError()

        fun setInvalidOldPasswordError()

        fun setInvalidNewEmaildError()

        fun showProgressDialog(messageId: Int)

        fun hideProgressDialog()
    }

    interface Presenter : IBasePresenter<View> {

        fun changePicture(image: MultipartBody.Part)

        fun changePassword(oldPassword: String, newPassword: String)

        fun changeEmail(password: String, newEmail: String)

        fun changeAvailability(availability: Int)

        fun dispose()

        fun getUserName(): String

        fun getUserImage(): String

        fun getUserRole(): Int

        fun getLoginProvider(): String

        fun getDoctorAvailability(): Int

        fun saveUserImage(image: String)
    }
}