package com.appagroup.sahty.ui.login.select_role

import android.os.AsyncTask
import android.util.Log
import android.widget.Toast
import com.facebook.FacebookSdk.getApplicationContext
import org.json.JSONException
import org.json.JSONObject

class RequestInstagramAPI : AsyncTask<Void, String, String>() {

    private val TAG = "RequestInstagramAPI"

    override fun doInBackground(vararg params: Void?): String? {

        return null
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        if (result != null) {
            try {
                val jsonObject = JSONObject(result)
                val jsonData = jsonObject.getJSONObject("data")
                if (jsonData.has("id")) {
                    Log.d(TAG, "USER ID   = ${jsonData.getString("id")}")
                    Log.d(TAG, "USER NAME = ${jsonData.getString("username")}")
                    Log.d(TAG, "USER PIC  = ${jsonData.getString("profile_picture")}")
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        } else {
            val toast = Toast.makeText(getApplicationContext(), "Login error!", Toast.LENGTH_LONG)
            toast.show()
        }
    }
}