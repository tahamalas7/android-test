package com.appagroup.sahty.ui.saved.saved_videos

import android.content.Intent
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.Video
import com.appagroup.sahty.ui.SaveService
import com.appagroup.sahty.ui.articles.article_content.SaveBottomSheetDialog
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.OnClick
import com.appagroup.sahty.ui.videos.VideoPlayerActivity
import com.appagroup.sahty.ui.videos.VideosAdapter
import com.appagroup.sahty.utils.SaveTypes
import kotlinx.android.synthetic.main.fragment_saved_videos.*
import com.appagroup.sahty.ui.saved.saved_videos.SavedVideosContract as Contract

class SavedVideosActivity : BaseActivity<Contract.Presenter>(), Contract.View {

    private val adapter = VideosAdapter()
    private val saveService = SaveService()

    override fun instantiatePresenter(): Contract.Presenter = SavedVideosPresenter(this)

    override fun getContentView(): Int = R.layout.fragment_saved_videos

    override fun initViews() {
        saved_videos_recyclerView.layoutManager = LinearLayoutManager(this)
        saved_videos_recyclerView.adapter = adapter

        presenter.getSavedVideos()
        setListeners()
    }

    override fun setListeners() {
        saved_videos_back_arrow.setOnClickListener { onBackPressed() }

        adapter.setOnPlayButtonClickListener(object : ItemClickListener<Video> {
            override fun onClick(item: Video) {
                val intent = Intent(this@SavedVideosActivity, VideoPlayerActivity::class.java)
                intent.putExtra(VideoPlayerActivity.VIDEO_URL, item.url)
                intent.putExtra(VideoPlayerActivity.VIDEO_SOURCE, VideoPlayerActivity.TYPE_EXTERNAL)
                startActivity(intent)
            }
        })

        adapter.setOnVideosBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                presenter.getSavedVideos()
            }
        })

        adapter.setOnOptionsButtonClickListener(object : ItemClickListener<Video> {
            override fun onClick(item: Video) {
                val dialog = SaveBottomSheetDialog.newInstance(item.isSaved, getString(R.string.video))
                dialog.show(supportFragmentManager, "SaveBottomSheetDialog")

                dialog.setOnBottomSheetClickListener(object : OnClick {
                    override fun onClick() {
                        saveService.unsave(item.id, SaveTypes.VIDEO)
                        adapter.removeItem(item.id)
                        dialog.dismiss()
                    }
                })
            }
        })
    }

    override fun updateArticles(videos: MutableList<Video>) {
        adapter.updateVideos(videos)
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        saved_videos_progressBar.visibility = visibility
    }

    override fun changeLoaderVisibility(visibility: Int) {
        adapter.changeLoaderVisibility(visibility)
    }

    companion object {
        val TAG = "SavedVideosActivity"
    }
}