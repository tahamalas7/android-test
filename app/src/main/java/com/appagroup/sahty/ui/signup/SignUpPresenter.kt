package com.appagroup.sahty.ui.signup

import android.util.Log
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Specialty
import com.appagroup.sahty.entity.User
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class SignUpPresenter(view: SignUpContract.View) : BasePresenter<SignUpContract.View>(view), SignUpContract.Presenter {

    val TAG = "SignUpPresenter"

    override fun commitSignUp(name: String, email: String, password: String, role: Int, fcm_token: String) {
        view.showProgressBar()
        subscribe(dataManager.register(name, email, password, role, fcm_token), object : Observer<Response<BaseResponse<User>>> {
            override fun onComplete() {
                Log.d(TAG, "commitSignUp -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                DisposableManager.add(d)
                Log.d(TAG, "commitSignUp -> onSubscribe")
                Log.d(TAG, "socialLogin -> onSubscribe -> name = $name")
                Log.d(TAG, "socialLogin -> onSubscribe -> fcm_token = $fcm_token")
                Log.d(TAG, "socialLogin -> onSubscribe -> password = $password")
                Log.d(TAG, "socialLogin -> onSubscribe -> email = $email")
                Log.d(TAG, "socialLogin -> onSubscribe -> role = $role")
            }

            override fun onNext(t: Response<BaseResponse<User>>) {
                Log.d(TAG, "commitSignUp -> onNext -> code = ${t.code()}")
                Log.d(TAG, "commitSignUp -> onNext -> body = ${t.body()}")
                when (t.code()) {
                    200 -> {
                        if (t.body() != null) {
                            saveUserData(t.body()!!.data!!)
                            view.signUpSuccessfully(t.body()!!.data!!.name)

                        } else
                            view.showMessageDialog(R.string.error_after_sign_up)
                    }

                    400 -> view.showError(R.string.validation_error)

                    401 -> view.showError(R.string.email_taken)
                    else -> view.showError(R.string.something_wrong)
                }

                view.hideProgressBar()
                view.changeSignUpProcessStatus(false)
            }

            override fun onError(e: Throwable) {
                view.showError(R.string.error_sign_up)
                view.hideProgressBar()
                view.changeSignUpProcessStatus(false)
                Log.d(TAG, "commitSignUp -> onError -> error = ${e.message}")
            }
        })
    }

    override fun commitSignUpWithAccreditation(name: String, email: String, password: String, role: Int, fcm_token: String, acc_type: String, acc_specialty: Long, acc_location: String, acc_year: String, acc_image: String) {
        view.showProgressBar()
        subscribe(dataManager.registerWithAccreditation(name, email, password, role, fcm_token, acc_type, acc_specialty, acc_location, acc_year, acc_image), object : Observer<Response<BaseResponse<User>>> {
            override fun onComplete() {
                Log.d(TAG, "commitSignUpWithAccreditation -> onComplete")
                Log.d(TAG, "commitSignUpWithAccreditation -> onSubscribe -> name = $name")
                Log.d(TAG, "commitSignUpWithAccreditation -> onSubscribe -> fcm_token = $fcm_token")
                Log.d(TAG, "commitSignUpWithAccreditation -> onSubscribe -> password = $password")
                Log.d(TAG, "commitSignUpWithAccreditation -> onSubscribe -> email = $email")
                Log.d(TAG, "commitSignUpWithAccreditation -> onSubscribe -> role = $role")
            }

            override fun onSubscribe(d: Disposable) {
                DisposableManager.add(d)
                Log.d(TAG, "commitSignUpWithAccreditation -> onSubscribe")
            }

            override fun onNext(t: Response<BaseResponse<User>>) {
                Log.d(TAG, "commitSignUpWithAccreditation -> onNext -> code = ${t.code()}")
                Log.d(TAG, "commitSignUpWithAccreditation -> onNext -> body = ${t.body()}")
                when (t.code()) {
                    200 -> {
                        if (t.body() != null) {
                            saveUserData(t.body()!!.data!!)
                            view.signUpSuccessfully(t.body()!!.data!!.name)
                            Log.d(TAG, "commitSignUpWithAccreditation -> onNext -> t.body() != null -> body = ${t.body()}")

                        } else {
                            view.showMessageDialog(R.string.error_after_sign_up)
                            Log.d(TAG, "commitSignUpWithAccreditation -> onNext -> t.body() == null -> code = ${t.code()}")
                        }
                    }

                    400 -> {
                        view.showError(R.string.validation_error)
                        Log.d(TAG, "commitSignUpWithAccreditation -> onNext -> validation_error -> code = ${t.code()}")
                    }


                    401 -> view.showError(R.string.email_taken)
                    else -> view.showError(R.string.something_wrong)
                }

                view.hideProgressBar()
                view.changeSignUpProcessStatus(false)
            }

            override fun onError(e: Throwable) {
                view.showError(R.string.error_sign_up)
                view.hideProgressBar()
                view.changeSignUpProcessStatus(false)
                Log.d(TAG, "commitSignUpWithAccreditation -> onError -> error = ${e.message}")
            }
        })
    }

    override fun getSpecialties(name: String) {
        view.showSpecialtySearchProgressBar()
        DisposableManager.disposeSearch()
        subscribe(dataManager.getSpecialties(name), object : Observer<Response<BaseResponse<MutableList<Specialty>>>> {
            override fun onComplete() {}

            override fun onSubscribe(d: Disposable) {
                DisposableManager.addSearchDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<MutableList<Specialty>>>) {
                if (t.body() != null && t.body()!!.status && t.body()!!.data != null) {
                    view.updateSpecialties(t.body()!!.data!!)
                    Log.d(TAG, "getSpecialties -> onNext -> data size = ${t.body()!!.data!!.size}")
                } else {
                    Log.d(TAG, "getSpecialties -> onNext -> code = ${t.code()}, body = ${t.body()}")
//                    if (t.body() != null)
//                        Log.d(TAG, "getSpecialties -> onNext -> body != null -> status = ${t.body()!!.status}, data = ${t.body()!!.data!!.size}")
                }

                view.hideSpecialtySearchProgressBar()
            }

            override fun onError(e: Throwable) {
                view.hideSpecialtySearchProgressBar()
                Log.d(TAG, "getSpecialties -> onError -> error = ${e.message}")
            }
        })
    }

    override fun saveUserData(user: User) {
        Log.d(TAG, "saveUserData -> \nid = ${user.id} \ntitle = ${user.name} \nemail = ${user.email} \nimage = ${user.image} \nphoneNumber = ${user.phoneNumber} \nrole = ${user.role} \naccess_token = ${user.access_token} \n")
        dataManager.setUserId(user.id)
        dataManager.setUserName(user.name)
        dataManager.setUserEmail(user.email)
        dataManager.setUserImage(user.image)
        dataManager.setUserRole(user.role.toInt())
        dataManager.setUserAccessToken("Bearer " + user.access_token)
        dataManager.setDoctorAvailability(user.availability)
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.dispose()
        DisposableManager.disposeSearch()
    }
}