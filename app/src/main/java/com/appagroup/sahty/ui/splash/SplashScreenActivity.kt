package com.appagroup.sahty.ui.splash

import android.app.Activity
import android.content.Context
import android.os.Bundle
import com.appagroup.sahty.R
import android.content.Intent
import android.os.Handler
import com.appagroup.sahty.ui.articles.article_content.ArticleContentActivity
import com.appagroup.sahty.ui.login.LoginActivity
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.ui.messages.message_content.MessageContentActivity
import com.appagroup.sahty.utils.NotificationsType
import com.appagroup.sahty.utils.User
import com.appagroup.sahty.utils.UserRole


class SplashScreenActivity : Activity() {

    private val SPLASH_DISPLAY_LENGTH = 1000L
    private val NOTIFICATION_REQUEST_CODE = 12

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val accessToken = getSharedPreferences("SahtySharedPreferences", Context.MODE_PRIVATE)
                .getString(User.USER_ACCESS_TOKEN, "") ?: ""

        val userRole = getSharedPreferences("SahtySharedPreferences", Context.MODE_PRIVATE)
                .getInt(User.USER_ROLE, 0)

        if (intent.extras != null && intent.hasExtra("type") && accessToken.isNotEmpty()) {
            val type = (intent?.extras?.get("type") as String).toInt()

            val intent: Intent?


            when (type) {
                NotificationsType.DOCTOR_CONFIRMED -> {
                    intent = Intent(this, MainActivity::class.java)
                    intent.putExtra(MainActivity.FROM_NOTIFICATION, true)
                }
                NotificationsType.NEW_MESSAGE -> {
                    intent = Intent(this, MessageContentActivity::class.java)
                    intent.putExtra(MessageContentActivity.RECEIVER_ID, (intent.extras?.get("data") as String).toLong())
                }
                NotificationsType.NEW_ARTICLE -> {
                    intent = Intent(this, ArticleContentActivity::class.java)
                    intent.putExtra(ArticleContentActivity.ARTICLE_ID, (intent.extras?.get("data") as String).toLong())
                }
                else -> intent = Intent(this, SplashScreenActivity::class.java)
            }

            startActivityForResult(intent, NOTIFICATION_REQUEST_CODE)
            return
        }
//        else if (accessToken.isEmpty()) {
//            startMainActivity()
//        }

        Handler().postDelayed({
            if (accessToken.isNotEmpty() && userRole != UserRole.ROLE_UNKNOWN_USER)
                startMainActivity()
            else startLoginActivity()
        }, SPLASH_DISPLAY_LENGTH)
    }

    private fun startLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun startMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == NOTIFICATION_REQUEST_CODE)
            startMainActivity()
    }
}