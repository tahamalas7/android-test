package com.appagroup.sahty.ui.articles.article_content

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.ArticleContent

interface ArticleContentContract {

    interface View : IBaseView {

        fun setListeners()

        fun changeProgressBarVisibility(visibility: Int)

        fun changeContentVisibility(visibility: Int)

        fun setArticleContent(articleContent: ArticleContent)
    }

    interface Presenter : IBasePresenter<View> {

        fun getArticleContent(articleId: Long)

        fun likeArticle(articleId: Long)

        fun unlikeArticle(articleId: Long)

        fun getUserId(): Long
    }
}