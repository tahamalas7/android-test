package com.appagroup.sahty.ui.messages

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Message

interface MessagesContract {

    interface View : IBaseView {

        fun setListeners()

        fun updateMessages(messages: MutableList<Message>)

        fun changeLoaderVisibility(visibility: Int)

        fun hideMessagesProgressBar()
    }

    interface Presenter : IBasePresenter<View> {

        fun loadMessages()

        fun resetPaginate()

        fun getUserId(): Long
    }
}