package com.appagroup.sahty.ui.messages.create_message

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.SendReplyResponse

interface CreateMessageContract {

    interface View : IBaseView {

        fun setListeners()

        fun messageSentSuccessfully(message: SendReplyResponse)

        fun openSelectDoctorDialog()

        fun showProgressDialog()

        fun hideProgressDialog()
    }

    interface Presenter : IBasePresenter<View> {

        fun sendMessage(receiverId: Long, content: String)
    }
}