package com.appagroup.sahty.ui.accreditation_dialog

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Accreditation
import com.appagroup.sahty.entity.Specialty
import okhttp3.MultipartBody
import okhttp3.RequestBody

interface AccreditationContract {

    interface View : IBaseView {

        fun setListeners()

        fun pickImage()

        fun updateSpecialties(specialties: MutableList<Specialty>)

        fun checkAccreditationData(): Boolean

        fun accreditationCreatedSuccessfully(accreditation: Accreditation)

        fun accreditationEditedSuccessfully(accreditation: Accreditation)

        fun accreditationDeletedSuccessfully()

        fun changeProgressBarVisibility(visibility: Int)

        fun changeSpecialtySearchProgressBarVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun getSpecialties(name: String)

        fun deleteAccreditation(accreditationId: Long)

        fun editAccreditation(
                accreditationId: Long,
                type: RequestBody,
                year: RequestBody,
                location: RequestBody,
                specialization: RequestBody)

        fun editAccreditationWithImage(
                accreditationId: Long,
                type: RequestBody,
                year: RequestBody,
                location: RequestBody,
                specialization: RequestBody,
                image: MultipartBody.Part)

        fun createAccreditation(
                type: RequestBody,
                year: RequestBody,
                location: RequestBody,
                specialization: RequestBody,
                image: MultipartBody.Part)
    }
}