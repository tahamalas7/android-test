package com.appagroup.sahty.ui.videos

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Video
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_loader.view.*
import kotlinx.android.synthetic.main.item_video.view.*

class VideosAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_LOADER = 0
    private val TYPE_VIDEO = 1
    private val videos: MutableList<Video> = mutableListOf()
    private lateinit var playButtonClickListener: ItemClickListener<Video>
    private lateinit var optionsButtonClickListener: ItemClickListener<Video>
    private lateinit var videosBottomReachListener: BottomReachListener
    private lateinit var videosLoader: ProgressBar

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_LOADER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_loader, parent, false)
                LoaderHolder(view)
            }
            TYPE_VIDEO -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_video, parent, false)
                VideoHolder(view)
            }
            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1)
            TYPE_LOADER
        else TYPE_VIDEO
    }

    override fun getItemCount(): Int = videos.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoaderHolder -> holder.changeLoaderVisibility()
            is VideoHolder -> {
                val video = videos[position]
                holder.bind(video)
                holder.itemView.video_play_btn.setOnClickListener {
                    playButtonClickListener.onClick(video)
                }

                holder.itemView.video_options_btn.setOnClickListener {
                    optionsButtonClickListener.onClick(video)
                }
            }
        }

        if (position == itemCount - 5)
            videosBottomReachListener.onBottomReach()
    }

    fun updateVideos(videos: MutableList<Video>) {
        this.videos.addAll(videos)
        notifyDataSetChanged()
    }

    fun changeLoaderVisibility(visibility: Int) {
        if (::videosLoader.isInitialized)
            videosLoader.visibility = visibility
    }

    fun removeItem(videoId: Long) {
        for (position in 0 until videos.size)
            if (videos[position].id == videoId) {
                videos.removeAt(position)
                notifyItemRemoved(position)
                break
            }
    }

    fun setOnPlayButtonClickListener(playButtonClickListener: ItemClickListener<Video>) {
        this.playButtonClickListener = playButtonClickListener
    }

    fun setOnOptionsButtonClickListener(optionsButtonClickListener: ItemClickListener<Video>) {
        this.optionsButtonClickListener = optionsButtonClickListener
    }

    fun setOnVideosBottomReachListener(videosBottomReachListener: BottomReachListener) {
        this.videosBottomReachListener = videosBottomReachListener
    }

    inner class LoaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun changeLoaderVisibility() {
            videosLoader = itemView.recyclerView_loader
        }
    }

    inner class VideoHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(video: Video) {
            itemView.video_title.text = video.title
            itemView.video_writer_name.text = "${itemView.context.getString(R.string.by)} ${video.writer.name}"

            Glide.with(itemView.context)
                    .load(BuildConfig.IMAGE + video.thumbnail)
                    .into(itemView.video_thumbnail)

            if (video.writer.image != null)
                Glide.with(itemView.context)
                        .load(BuildConfig.IMAGE + video.writer.image)
                        .into(itemView.video_writer_image)
        }
    }
}