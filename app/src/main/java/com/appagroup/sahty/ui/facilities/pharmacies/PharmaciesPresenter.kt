package com.appagroup.sahty.ui.facilities.pharmacies

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Paginate
import com.appagroup.sahty.entity.Pharmacy
import com.appagroup.sahty.utils.DisposableManager
import com.appagroup.sahty.utils.Facility
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class PharmaciesPresenter(view: PharmaciesContract.View) : BasePresenter<PharmaciesContract.View>(view), PharmaciesContract.Presenter {

    private val TAG = "PharmaciesPresenter"
    private var page = 1
    private var perPage = 10
    private var isThereMore = true

    override fun loadPharmacies() {
        if (isThereMore) {
            view.changeProgressBarVisibility(View.VISIBLE)
            subscribe(dataManager.getPharmacies(Facility.TYPE_PHARMACY, page, perPage), object : Observer<Response<BaseResponse<Paginate<MutableList<Pharmacy>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "loadPharmacies -> onComplete")
                }

                override fun onSubscribe(d: Disposable) {
                    Log.d(TAG, "loadPharmacies -> onSubscribe")
                    DisposableManager.addFacilitiesDisposable(d)
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Pharmacy>>>>) {
                    Log.d(TAG, "loadPharmacies -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "loadPharmacies -> onNext -> body = ${t.body()}")
                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        Log.d(TAG, "loadPharmacies -> onNext -> size = ${t.body()!!.data!!.data!!.size}")
                        page++
                        view.updatePharmacies(t.body()!!.data!!.data!!)
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            isThereMore = false
                    } else
                        view.showError(R.string.err_load_pharmacies)
                    view.changeProgressBarVisibility(View.GONE)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "loadPharmacies -> onError -> error = ${e.message}")
                    view.showError(R.string.err_load_pharmacies)
                    view.changeProgressBarVisibility(View.GONE)
                }
            })
        }
    }

    override fun unsubscribe() {
        DisposableManager.disposeFacilities()
    }
}