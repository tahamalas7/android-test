package com.appagroup.sahty.ui.poll

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Paginate
import com.appagroup.sahty.entity.Poll
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class PollsPresenter(view: PollContract.View) : BasePresenter<PollContract.View>(view), PollContract.Presenter {

    private val TAG = "PollsPresenter"
    private var pollsPage = 1
    private var loadMorePolls = true

    override fun loadPolls(filter: Int) {
        if (loadMorePolls) {
            if (pollsPage == 1) view.changeProgressBarVisibility(View.VISIBLE)
            else view.changeLoaderVisibility(View.VISIBLE)
            subscribe(dataManager.getPolls(filter, pollsPage), object : Observer<Response<BaseResponse<Paginate<MutableList<Poll>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "loadPolls -> onComplete")
                }

                override fun onSubscribe(d: Disposable) {
                    Log.d(TAG, "loadPolls -> onSubscribe")
                    DisposableManager.add(d)
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Poll>>>>) {
                    Log.d(TAG, "loadPolls -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "loadPolls -> onNext -> body = ${t.body()}")

                    view.changeProgressBarVisibility(View.GONE)
                    view.changeLoaderVisibility(View.GONE)

                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        Log.d(TAG, "loadPolls -> onNext -> polls count = ${t.body()!!.data!!.data!!.size}")
                        view.updatePolls(t.body()!!.data!!.data!!)
                        pollsPage++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page) {
                            view.notifyPollsDone()
                            loadMorePolls = false
                        }
                    } else
                        if (pollsPage == 1)
                            view.showError(R.string.err_failed_load_polls)
                        else view.showError(R.string.err_failed_load_more_polls)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "loadPolls -> onError -> error = ${e.message}")
                    view.changeProgressBarVisibility(View.GONE)
                    view.changeLoaderVisibility(View.GONE)

                    if (pollsPage == 1)
                        view.showError(R.string.err_failed_load_polls)
                    else view.showError(R.string.err_failed_load_more_polls)
                }
            })
        }
    }

    override fun submitVote(optionId: Long) {
        subscribe(dataManager.submitVote(optionId), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "submitVote -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "submitVote -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "submitVote -> onNext -> code = ${t.code()}")
                Log.d(TAG, "submitVote -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null) {
                    Log.d(TAG, "submitVote -> onNext -> status = ${t.body()!!.status}")
                    Log.d(TAG, "submitVote -> onNext -> message = ${t.body()!!.message}")
                }
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "submitVote -> onError -> error = ${e.message}")
            }
        })
    }

    override fun resetPaginate() {
        loadMorePolls = true
        pollsPage = 1
    }
}