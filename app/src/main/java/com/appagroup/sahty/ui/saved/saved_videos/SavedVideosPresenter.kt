package com.appagroup.sahty.ui.saved.saved_videos

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Paginate
import com.appagroup.sahty.entity.Video
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response
import com.appagroup.sahty.ui.saved.saved_videos.SavedVideosContract as Contract

class SavedVideosPresenter(view : Contract.View) : BasePresenter<Contract.View>(view), Contract.Presenter {

    private val TAG = "SavedVideosPresenter"
    private var videosPage = 1
    private var loadMoreVideos = true

    override fun getSavedVideos() {
        if (loadMoreVideos) {
            if (videosPage == 1) view.changeProgressBarVisibility(View.VISIBLE)
            else view.changeLoaderVisibility(View.VISIBLE)
            subscribe(dataManager.getSavedVideos(videosPage), object : Observer<Response<BaseResponse<Paginate<MutableList<Video>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "getSavedVideos -> onComplete ")
                }

                override fun onSubscribe(d: Disposable) {
                    DisposableManager.addVideosDisposable(d)
                    Log.d(TAG, "getSavedVideos -> onSubscribe ")
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Video>>>>) {
                    Log.d(TAG, "getSavedVideos -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "getSavedVideos -> onNext -> body = ${t.body()}")

                    view.changeLoaderVisibility(View.GONE)
                    view.changeProgressBarVisibility(View.GONE)

                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        Log.d(TAG, "getSavedVideos -> onNext -> Videos count = ${t.body()!!.data!!.data!!.size}")
                        view.updateArticles(t.body()!!.data!!.data!!)
                        videosPage++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMoreVideos = false
                    } else
                        if (videosPage == 1)
                            view.showError(R.string.err_failed_load_articles)
                        else view.showError(R.string.err_failed_load_more_articles)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "getSavedVideos -> onError -> error = ${e.message}")
                    view.changeLoaderVisibility(View.GONE)
                    view.changeProgressBarVisibility(View.GONE)

                    if (videosPage == 1)
                        view.showError(R.string.err_failed_load_articles)
                    else view.showError(R.string.err_failed_load_more_articles)
                }
            })
        }
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeVideos()
    }
}