package com.appagroup.sahty.ui.login.select_role

import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseDialog
import com.appagroup.sahty.utils.UserRole
import kotlinx.android.synthetic.main.dialog_select_role.*

class SelectRoleDialog : BaseDialog<SelectRoleContract.Presenter>(), SelectRoleContract.View {

    private val TAG = "SelectRoleDialog"
    private var userRole: Int = UserRole.ROLE_READER
    private var roleSubmittedSuccessfully = false
    private lateinit var roleSubmitListener: RoleSubmitListener

    override fun instantiatePresenter(): SelectRoleContract.Presenter = SelectRolePresenter(this)

    override fun getLayout(): Int = R.layout.dialog_select_role

    override fun initViews(view: View) {
        setListeners()
    }

    override fun setListeners() {
        select_role_reader_btn.setOnClickListener { selectReader() }

        select_role_doctor_btn.setOnClickListener { selectDoctor() }

        select_role_submit.setOnClickListener { presenter.setUserRole(userRole) }

        dialog.setOnDismissListener {
            if (!roleSubmittedSuccessfully)
                roleSubmitListener.dialogDismissed()
        }
    }

    private fun selectDoctor() {
        select_role_reader_btn.alpha = 0.4f
        select_role_doctor_btn.alpha = 1.0f
        userRole = UserRole.ROLE_DOCTOR_WITHOUT_CERTIFICATE
    }

    private fun selectReader() {
        select_role_doctor_btn.alpha = 0.4f
        select_role_reader_btn.alpha = 1.0f
        userRole = UserRole.ROLE_READER
    }

    override fun roleSubmittedSuccessfully() {
        roleSubmitListener.roleSubmittedSuccessfully(userRole)
        roleSubmittedSuccessfully = true
        dismiss()
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        select_role_progressBar.visibility = visibility
    }

    fun setOnRoleSubmitListener(roleSubmitListener: RoleSubmitListener) {
        this.roleSubmitListener = roleSubmitListener
    }

    companion object {
        val TAG = "SelectRoleDialog"
        fun getInstance(): SelectRoleDialog =
                SelectRoleDialog()
    }
}