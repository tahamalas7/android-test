package com.appagroup.sahty.ui.articles

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.Article
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Paginate
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class ArticlesPresenter(view: ArticlesContract.View)
    : BasePresenter<ArticlesContract.View>(view), ArticlesContract.Presenter {

    private val TAG = "ArticlesPresenter"
    private var articlesPage = 1
    private var loadMoreArticles = true

    override fun loadArticles(categoryId: Long, filter: Int) {
        if (loadMoreArticles) {
            if (articlesPage == 1) view.changeProgressBarVisibility(View.VISIBLE)
            else view.changeLoaderVisibility(View.VISIBLE)
            subscribe(dataManager.getArticlesAndStories(categoryId, filter, articlesPage), object : Observer<Response<BaseResponse<Paginate<MutableList<Article>>>>> {
                override fun onComplete() {}

                override fun onSubscribe(d: Disposable) {
                    DisposableManager.addArticlesDisposable(d)
                    Log.d(TAG, "loadArticles -> onSubscribe ")
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Article>>>>) {
                    Log.d(TAG, "loadArticles ->onNext -> code = ${t.code()}")
                    Log.d(TAG, "loadArticles ->onNext -> body = ${t.body()}")

                    view.changeLoaderVisibility(View.GONE)
                    view.changeProgressBarVisibility(View.GONE)

                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        Log.d(TAG, "loadArticles ->onNext -> Articles count = ${t.body()!!.data!!.data!!.size}")
                        view.updateArticles(t.body()!!.data!!.data!!)
                        articlesPage++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMoreArticles = false
                    } else
                        if (articlesPage == 1)
                            view.showError(R.string.err_failed_load_articles)
                        else view.showError(R.string.err_failed_load_more_articles)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "loadArticles ->onError -> error = ${e.message}")
                    view.changeLoaderVisibility(View.GONE)
                    view.changeProgressBarVisibility(View.GONE)

                    if (articlesPage == 1)
                        view.showError(R.string.err_failed_load_messages)
                    else view.showError(R.string.err_failed_load_more_messages)
                }
            })
        }
    }

    override fun getUserId(): Long =
            dataManager.getUserId()

    override fun resetPaginate() {
        articlesPage = 1
        loadMoreArticles = true
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeArticles()
    }
}