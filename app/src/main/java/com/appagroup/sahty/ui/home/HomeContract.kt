package com.appagroup.sahty.ui.home

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Category
import com.appagroup.sahty.entity.Insights

interface HomeContract {

    interface View : IBaseView {

        fun updateInsightsProgressBarVisibility(visibility: Int)

        fun updateCategoriesProgressBarVisibility(visibility: Int)

        fun updateCategories(categories: MutableList<Category>)

        fun updateInsights(insights: Insights)

        fun setListeners()
    }

    interface Presenter : IBasePresenter<View> {

        fun getCategories()

        fun getInsights()
    }
}