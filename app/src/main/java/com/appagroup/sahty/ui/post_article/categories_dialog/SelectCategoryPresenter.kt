package com.appagroup.sahty.ui.post_article.categories_dialog

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Category
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class SelectCategoryPresenter(view: SelectCategoryContract.View) : BasePresenter<SelectCategoryContract.View>(view), SelectCategoryContract.Presenter {

    private val TAG = "SelectCategoryPresenter"

    override fun loadCategories() {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.getCategories(), object : Observer<Response<BaseResponse<MutableList<Category>>>> {
            override fun onComplete() {
                Log.d(TAG, "loadCategories -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "loadCategories -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<MutableList<Category>>>) {
                Log.d(TAG, "loadCategories -> onNext -> code = ${t.code()}")
                Log.d(TAG, "loadCategories -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "loadCategories -> onNext -> status = ${t.body()!!.status}")
                    Log.d(TAG, "loadCategories -> onNext -> count = ${t.body()!!.data!!.size}")
                    view.updateCategories(t.body()!!.data!!)
                } else
                    view.showError(R.string.something_wrong)
                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "loadCategories -> onError -> error = ${e.message}")
                view.showError(R.string.something_wrong)
                view.changeProgressBarVisibility(View.GONE)
            }
        })
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.dispose()
    }
}
