package com.appagroup.sahty.ui.facilities.rating_dialog

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseDialog
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.utils.hideKeyboard
import kotlinx.android.synthetic.main.dialog_rate.*

class RatingDialog : BaseDialog<RatingContract.Presenter>(), RatingContract.View {

    private var rate = 1F
    private var ratedSuccessfully = false

    override fun instantiatePresenter(): RatingContract.Presenter = RatingPresenter(this)

    override fun getLayout(): Int = R.layout.dialog_rate

    override fun initViews(view: View) {
        if(tag == MainActivity.FEEDBACK_TAG) {
            rate_lbl.text = getString(R.string.how_can_we_improve)
            rate_hint.visibility = View.GONE
        } else {
            rate_lbl.text = getString(R.string.rate)
            rate_hint.text = getString(R.string.rate_hint)
            rate_feedback_content.hint = getString(R.string.note)
        }
        setListeners()
    }

    override fun setListeners() {
        rate_submit_btn.setOnClickListener {
            hideKeyboard()
            if (rate_feedback_content.text.toString() != "")
                presenter.submitRating(
                        arguments!!.getLong(FACILITY_ID),
                        rate_feedback_content.text.toString(),
                        rate)
            else
                rate_feedback_content.error = getString(R.string.error_empty_field)
        }

        rate_ratingBar.setOnRatingChangeListener { _, rating, _ ->
            rate = rating
        }
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        rate_progressBar.visibility = visibility
    }

    override fun rateSubmittedSuccessfully() {
        ratedSuccessfully = true
        dialog.dismiss()
    }

    override fun onPause() {
        super.onPause()
        if(targetFragment != null) {
            val intent = Intent()
            intent.putExtra(RATED_SUCCESSFULLY, ratedSuccessfully)
            targetFragment!!.onActivityResult(targetRequestCode, 200, intent)
        }
    }

    companion object {
        private val FACILITY_ID = "FACILITY_ID"
        val RATED_SUCCESSFULLY = "RATED_SUCCESSFULLY"
        val TAG = "RatingDialog"

        fun getInstance(facilityId: Long): RatingDialog {
            val instance = RatingDialog()
            val bundle = Bundle()
            bundle.putLong(FACILITY_ID, facilityId)
            instance.arguments = bundle
            return instance
        }
    }
}