package com.appagroup.sahty.ui.medical_files

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.MedicalFileGroup

interface MyMedicalFilesContract {

    interface View : IBaseView {

        fun updateMedicalFileGroups(groups: MutableList<MedicalFileGroup>)

        fun removeDeletedGroup(groupId: Long)

        fun changeProgressBarVisibility(visibility: Int)

        fun showDeleteProgressDialog()

        fun dismissDeleteProgressDialog()

        fun setListeners()
    }

    interface Presenter : IBasePresenter<View> {

        fun loadMedicalFileGroups()

        fun deleteMedicalFileGroup(groupId: Long)
    }
}