package com.appagroup.sahty.ui.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.PagerAdapter
import com.appagroup.sahty.R
import kotlinx.android.synthetic.main.item_followed_doctors.view.*

class FollowersPagerAdapter(
        private val followingAdapter: FollowingAdapter,
        private val followersAdapter: FollowersAdapter) : PagerAdapter() {

    override fun isViewFromObject(p0: View, p1: Any): Boolean =
            p0 == p1

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater = container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val itemView = layoutInflater.inflate(R.layout.item_followed_doctors, container, false)

        itemView.followers_recyclerView.layoutManager = LinearLayoutManager(container.context)

        if (position == 0)
            itemView.followers_recyclerView.adapter = followingAdapter
        else
            itemView.followers_recyclerView.adapter = followersAdapter

        container.addView(itemView)
        return itemView
    }

    override fun getCount(): Int = 2
}