package com.appagroup.sahty.ui.poll

import android.app.AlertDialog
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.entity.Poll
import com.appagroup.sahty.entity.PollOption
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.dialog_polls_filter.view.*
import com.appagroup.sahty.utils.PollsFilter as Filter
import kotlinx.android.synthetic.main.fragment_polls.*

class PollsFragment : BaseFragment<PollContract.Presenter>(), PollContract.View {

    private val adapter = PollsAdapter()
    private var filter = Filter.ALL

    override fun instantiatePresenter(): PollContract.Presenter = PollsPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_polls

    override fun initViews(view: View) {
        // initialize polls recyclerView
        polls_recyclerView.layoutManager = LinearLayoutManager(activity)
        polls_recyclerView.adapter = adapter

        presenter.loadPolls(filter)
        setListeners()
    }

    override fun setListeners() {
        polls_back_arrow.setOnClickListener { activity!!.onBackPressed() }

        polls_filter_btn.setOnClickListener { openSelectFilterDialog() }

        // set Listener for poll poll's options
        adapter.setOnPollOptionClickListener(object : ItemClickListener<PollOption> {
            override fun onClick(item: PollOption) {
                presenter.submitVote(item.id)
            }
        })

        // set listener when reach last poll card
        adapter.setOnPollsBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                presenter.loadPolls(filter)
            }
        })
    }

    override fun openSelectFilterDialog() {
        // initialize and show filter dialog
        val builder = AlertDialog.Builder(activity)
        val view = layoutInflater.inflate(R.layout.dialog_polls_filter, null)
        builder.setView(view)
        val dialog = builder.create()
        dialog.show()

        // set selected filter as checked
        when (filter) {
            Filter.ALL ->
                view.polls_filter_radioGroup.check(R.id.polls_filter_all_option)
            Filter.VOTED ->
                view.polls_filter_radioGroup.check(R.id.polls_filter_voted_option)
            Filter.NOT_VOTED ->
                view.polls_filter_radioGroup.check(R.id.polls_filter_not_voted_option)
            Filter.MOST_INTERACTIVE ->
                view.polls_filter_radioGroup.check(R.id.polls_filter_most_interactive_option)
        }

        // handle filter type after user choose a new filter
        view.polls_filter_apply_btn.setOnClickListener {
            when (view.polls_filter_radioGroup.checkedRadioButtonId) {
                R.id.polls_filter_all_option ->
                    filter = Filter.ALL
                R.id.polls_filter_voted_option ->
                    filter = Filter.VOTED
                R.id.polls_filter_not_voted_option ->
                    filter = Filter.NOT_VOTED
                R.id.polls_filter_most_interactive_option ->
                    filter = Filter.MOST_INTERACTIVE
            }
            changeProgressBarVisibility(View.VISIBLE) // showProgressDialog progressBar
            adapter.clearItems() // remove all items
            presenter.resetPaginate() // reset pagination page
            presenter.loadPolls(filter) // load new items
            dialog.dismiss()
        }
    }

    override fun updatePolls(polls: MutableList<Poll>) {
        adapter.updatePolls(polls)
    }

    override fun notifyPollsDone() {
        adapter.notifyPollsDone()
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        polls_progressBar.visibility = visibility
    }

    override fun changeLoaderVisibility(visibility: Int) {
        adapter.changeLoaderVisibility(visibility)
    }

    companion object {
        val TAG = "PollsFragment"
        fun getInstance(): PollsFragment =
                PollsFragment()
    }
}