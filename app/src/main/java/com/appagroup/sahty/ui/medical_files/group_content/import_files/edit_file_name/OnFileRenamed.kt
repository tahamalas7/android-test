package com.appagroup.sahty.ui.medical_files.group_content.import_files.edit_file_name

interface OnFileRenamed {

    fun onRename(filePath: String)
}