package com.appagroup.sahty.ui.messages.patient_groups.group_content

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.MedicalFile
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response
import com.appagroup.sahty.ui.messages.patient_groups.group_content.GroupContentContract as Contract

class GroupContentPresenter(view: Contract.View) : BasePresenter<Contract.View>(view), Contract.Presenter {

    private val TAG = "GroupContentPresenter"

    override fun getMedicalFiles(patientId: Long, groupId: Long) {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.getPatientMedicalFiles(patientId, groupId), object : Observer<Response<BaseResponse<MutableList<MedicalFile>>>> {
            override fun onComplete() {
                Log.d(TAG, "getMedicalFiles -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "getMedicalFiles -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<MutableList<MedicalFile>>>) {
                Log.d(TAG, "getMedicalFiles -> onNext -> Code = ${t.code()}")
                Log.d(TAG, "getMedicalFiles -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status)
                    view.updateFiles(t.body()!!.data!!)
                else
                    view.showError(R.string.err_failed_medical_files)

                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                view.changeProgressBarVisibility(View.GONE)
                Log.d(TAG, "getMedicalFiles -> onError -> Error = ${e.message}")
            }
        })
    }
}