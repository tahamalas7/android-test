package com.appagroup.sahty.ui.post_article.categories_dialog

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Category
import kotlinx.android.synthetic.main.item_select_category.view.*

class SelectCategoriesAdapter : RecyclerView.Adapter<SelectCategoriesAdapter.SelectCategoriesHolder>() {

    private val TAG = "SelectCategoriesAdapter"
    private val categories: MutableList<Category> = mutableListOf()
    private var selectedCategory: Category? = null
    private lateinit var lastCategorySelected: AppCompatCheckBox
    private lateinit var selectCategoryListener: SelectCategoryListener
    val NO_SELECT = null

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): SelectCategoriesAdapter.SelectCategoriesHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_select_category, parent, false)
        return SelectCategoriesHolder(view)
    }

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: SelectCategoriesAdapter.SelectCategoriesHolder, position: Int) {
        holder.bind(categories[position])
        holder.itemView.select_category_RadioButton.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                if (::lastCategorySelected.isInitialized)
                    lastCategorySelected.isChecked = false
                lastCategorySelected = holder.itemView.select_category_RadioButton
                selectedCategory = categories[position]
                selectCategoryListener.onSelect()
            } else {
                selectedCategory = NO_SELECT
                selectCategoryListener.onUnselect()
            }
        }
    }

    fun updateCategories(categories: MutableList<Category>) {
        this.categories.addAll(categories)
        notifyDataSetChanged()
    }

    fun setOnSelectCategoryListener(selectCategoryListener: SelectCategoryListener) {
        this.selectCategoryListener = selectCategoryListener
    }

    fun getSelectCategory(): Category? = selectedCategory

    inner class SelectCategoriesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(category: Category) {
            itemView.select_category_name.text = category.name
        }
    }
}