package com.appagroup.sahty.ui.medical_files.group_content.import_files

import com.appagroup.sahty.entity.MedicalFileGroup

interface FilesUploadedSuccessfullyListener {

    fun onFilesUploaded(fileGroup: MedicalFileGroup?)
}