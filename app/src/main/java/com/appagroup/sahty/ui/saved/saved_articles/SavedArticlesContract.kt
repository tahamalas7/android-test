package com.appagroup.sahty.ui.saved.saved_articles

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Article

interface SavedArticlesContract {

    interface View : IBaseView {

        fun setListeners()

        fun updateArticles(articles: MutableList<Article>)

        fun changeProgressBarVisibility(visibility: Int)

        fun changeLoaderVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun getSavedArticles()

        fun getUserId(): Long
    }
}