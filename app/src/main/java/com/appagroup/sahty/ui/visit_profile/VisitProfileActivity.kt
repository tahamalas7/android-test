package com.appagroup.sahty.ui.visit_profile

import android.content.Context
import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.Accreditation
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.entity.VisitProfile
import com.appagroup.sahty.ui.FollowService
import com.appagroup.sahty.ui.accreditation_dialog.AccreditationDialog
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.profile.AccreditationAdapter
import com.appagroup.sahty.utils.UserRole
import com.facebook.drawee.backends.pipeline.Fresco
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_visit_profile.*
import kotlinx.android.synthetic.main.item_followed_doctors.view.*


class VisitProfileActivity : BaseActivity<VisitProfileContract.Presenter>(), VisitProfileContract.View {

    private val TAG = "VisitProfileActivity"
    private var userId = 0L
    private var userImage: String? = ""
    private var isFollowed: Boolean? = null
    private val followService = FollowService()
    private lateinit var boldTypeface: Typeface
    private lateinit var lightTypeface: Typeface
    private lateinit var followingAdapter: FollowingAdapter
    private lateinit var followersAdapter: FollowersAdapter
    private lateinit var pagerAdapter: FollowersPagerAdapter
    private lateinit var accreditationAdapter: AccreditationAdapter

    override fun instantiatePresenter(): VisitProfileContract.Presenter = VisitProfilePresenter(this)

    override fun getContentView(): Int = R.layout.activity_visit_profile

    override fun initViews() {
        /** initialize Typefaces */
        lightTypeface = Typeface.createFromAsset(assets, "fonts/HelveticaNeueLight.ttf")
        boldTypeface = Typeface.createFromAsset(assets, "fonts/HelveticaNeueBold.ttf")

        followingAdapter = FollowingAdapter(lightTypeface, presenter.getUserId())
        followersAdapter = FollowersAdapter(lightTypeface, presenter.getUserId())

        /** initialize accreditation recyclerView */
        accreditationAdapter = AccreditationAdapter(lightTypeface)
        visit_profile_accreditations_recyclerView.layoutManager = LinearLayoutManager(this)
        visit_profile_accreditations_recyclerView.adapter = accreditationAdapter

        /** initialize viewPager */
        pagerAdapter = FollowersPagerAdapter()
        visit_profile_follow_viewPager.adapter = pagerAdapter
        visit_profile_follow_viewPager.currentItem = 0
        selectFollowingTab()

        userId = intent.getLongExtra(USER_ID, 0L)
        visit_profile_username.text = intent.getStringExtra(USER_NAME)

        presenter.loadProfile(userId)
        setListeners()
    }

    override fun setListeners() {
        visit_profile_back_arrow.setOnClickListener { onBackPressed() }

        /** handle follow click event **/
        visit_profile_follow_btn.setOnClickListener {
            Log.d("Follow", "follow user, user id = $isFollowed")
            if (isFollowed != null)
                isFollowed = if (isFollowed!!) {
                    followService.unfollowUser(userId)
                    visit_profile_follow_btn.setImageResource(R.drawable.ic_profile_follow)
                    false
                } else {
                    followService.followUser(userId)
                    visit_profile_follow_btn.setImageResource(R.drawable.ic_profile_unfollow)
                    true
                }
        }

        /** open visited user image **/
        visit_profile_user_image.setOnClickListener {
            if (userImage != null && userImage!!.isNotEmpty()) {
                Fresco.initialize(this)
                ImageViewer.Builder(this, arrayListOf(userImage))
                        .setStartPosition(0)
                        .setFormatter(ImageViewer.Formatter<String> { image -> BuildConfig.IMAGE + image })
                        .show()
            }
        }

        /** handle click event of followings tab  **/
        visit_profile_following_tab.setOnClickListener {
            if (visit_profile_follow_viewPager.currentItem == 1) {
                visit_profile_follow_viewPager.currentItem = 0
                selectFollowingTab()
            }
        }

        /** handle click event of followers tab  */
        visit_profile_followers_tab.setOnClickListener {
            if (visit_profile_follow_viewPager.currentItem == 0) {
                visit_profile_follow_viewPager.currentItem = 1
                selectFollowersTab()
            }
        }

        followingAdapter.setOnFollowButtonClickListener(object : FollowButtonClickListener {
            override fun followUser(follow: Follow) {
                followService.followUser(follow.id)
            }

            override fun unfollowUser(follow: Follow) {
                followService.unfollowUser(follow.id)
            }
        })

        followingAdapter.setOnUserClickListener(object : ItemClickListener<Follow> {
            override fun onClick(item: Follow) {
//                val intent = Intent(this@VisitProfileActivity, VisitProfileActivity::class.java)
//                intent.putExtra(VisitProfileActivity.USER_ID, item.id)
//                intent.putExtra(VisitProfileActivity.USER_NAME, item.name)
//                startActivityForResult(intent, VISIT_PROFILE_REQUEST_CODE)
            }
        })

        followersAdapter.setOnFollowButtonClickListener(object : FollowButtonClickListener {
            override fun followUser(follow: Follow) {
                followService.followUser(follow.id)
            }

            override fun unfollowUser(follow: Follow) {
                followService.unfollowUser(follow.id)
            }
        })

        accreditationAdapter.setOnAccreditationClickListener(object : ItemClickListener<Accreditation> {
            override fun onClick(item: Accreditation) {
                AccreditationDialog
                        .getInstance(AccreditationDialog.TYPE_VIEW, item)
                        .show(supportFragmentManager, TAG)
            }
        })
    }

    /** set information of visited profile */
    override fun setProfile(profile: VisitProfile) {
        userImage = profile.image
        isFollowed = profile.isFollowed

        if (profile.role != UserRole.ROLE_READER.toString() && profile.accreditations != null) {
            visit_profile_accreditations_recyclerView.visibility = View.VISIBLE
            visit_profile_accreditations_lbl.visibility = View.VISIBLE
            accreditationAdapter.updateAccreditations(profile.accreditations)
        }

        if (profile.isFollowed)
            visit_profile_follow_btn.setImageResource(R.drawable.ic_profile_unfollow)
        else visit_profile_follow_btn.setImageResource(R.drawable.ic_profile_follow)

        if (profile.following != null) {
            followingAdapter.updateFollows(profile.following)
            visit_profile_following_count.text = profile.following.size.toString()
        }

        if (profile.followers != null) {
            followersAdapter.updateFollows(profile.followers)
            visit_profile_followers_count.text = profile.followers.size.toString()
        }

        if (profile.specializations != null && profile.specializations.isNotEmpty())
            visit_profile_user_specialization.text = profile.specializations[0].name

        if (profile.image != null && profile.image.isNotEmpty())
            Glide.with(this)
                    .load(BuildConfig.IMAGE + profile.image)
                    .into(visit_profile_user_image)
    }

    private fun selectFollowingTab() {
        visit_profile_following_lbl.typeface = boldTypeface
        visit_profile_following_count.typeface = boldTypeface

        visit_profile_followers_lbl.typeface = lightTypeface
        visit_profile_followers_count.typeface = lightTypeface
    }

    private fun selectFollowersTab() {
        visit_profile_followers_lbl.typeface = boldTypeface
        visit_profile_followers_count.typeface = boldTypeface

        visit_profile_following_lbl.typeface = lightTypeface
        visit_profile_following_count.typeface = lightTypeface
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        visit_profile_follow_progressBar.visibility = visibility
    }

    inner class FollowersPagerAdapter : PagerAdapter() {

        override fun isViewFromObject(p0: View, p1: Any): Boolean =
                p0 == p1

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val layoutInflater = container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val itemView = layoutInflater.inflate(R.layout.item_followed_doctors, container, false)

            itemView.followers_recyclerView.layoutManager = LinearLayoutManager(container.context)

            if (position == 0)
                itemView.followers_recyclerView.adapter = followingAdapter
            else
                itemView.followers_recyclerView.adapter = followersAdapter

            container.addView(itemView)
            return itemView
        }

        override fun getCount(): Int = 2
    }

    companion object {
        val USER_ID = "USER_ID"
        val USER_NAME = "USER_NAME"
        val VISIT_PROFILE_REQUEST_CODE = "VISIT_PROFILE_REQUEST_CODE"
    }
}