package com.appagroup.sahty.ui.profile

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Accreditation
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_accreditation.view.*

class AccreditationAdapter(private val typeface: Typeface) : RecyclerView.Adapter<AccreditationAdapter.AccreditationHolder>() {

    private lateinit var accreditationClickListener: ItemClickListener<Accreditation>
    private val accreditations: MutableList<Accreditation> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): AccreditationHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_accreditation, parent, false)
        return AccreditationHolder(view)
    }

    override fun getItemCount(): Int = accreditations.size

    override fun onBindViewHolder(holder: AccreditationHolder, position: Int) {
        holder.bind(accreditations[position])
        holder.itemView.setOnClickListener {
            if (::accreditationClickListener.isInitialized)
                accreditationClickListener.onClick(accreditations[position])
        }
    }

    fun updateAccreditations(accreditation: MutableList<Accreditation>) {
        this.accreditations.addAll(accreditation)
        notifyDataSetChanged()
    }

    fun setOnAccreditationClickListener(accreditationClickListener: ItemClickListener<Accreditation>) {
        this.accreditationClickListener = accreditationClickListener
    }

    fun getAccreditations(): MutableList<Accreditation> =
            accreditations

    inner class AccreditationHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(accreditation: Accreditation) {
            itemView.accreditation_name.text = accreditation.type
            itemView.accreditation_year.text = accreditation.year

            itemView.accreditation_name.typeface = typeface
            itemView.accreditation_year.typeface = typeface
        }
    }
}