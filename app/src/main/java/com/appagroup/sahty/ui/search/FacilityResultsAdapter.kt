package com.appagroup.sahty.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Facility
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_search_facility.view.*

class FacilityResultsAdapter : RecyclerView.Adapter<FacilityResultsAdapter.FacilityResultsHolder>() {

    private val TAG = "FacilityResultsAdapter"
    private val facilities: MutableList<Facility> = mutableListOf()
    private lateinit var facilityClickListener: ItemClickListener<Facility>
    private lateinit var bottomReachListener: BottomReachListener

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): FacilityResultsAdapter.FacilityResultsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_facility, parent, false)
        return FacilityResultsHolder(view)
    }

    override fun getItemCount(): Int = facilities.size

    override fun onBindViewHolder(holder: FacilityResultsHolder, position: Int) {
        holder.bind(facilities[position])
        holder.itemView.setOnClickListener {
            facilityClickListener.onClick(facilities[position])
        }

        if (position == itemCount - 3)
            bottomReachListener.onBottomReach()
    }

    fun updateFacilities(facilities: MutableList<Facility>) {
        this.facilities.addAll(facilities)
        notifyDataSetChanged()
    }

    fun clearItems() {
        this.facilities.clear()
        notifyDataSetChanged()
    }

    fun setOnFacilityClickListener(facilityClickListener: ItemClickListener<Facility>) {
        this.facilityClickListener = facilityClickListener
    }

    fun setOnBottomReachListener(bottomReachListener: BottomReachListener) {
        this.bottomReachListener = bottomReachListener
    }

    inner class FacilityResultsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(facility: Facility) {
            itemView.search_facility_type.text = if (facility.type == com.appagroup.sahty.utils.Facility.TYPE_HOSPITAL)
                itemView.context.resources.getString(R.string.hospital) else itemView.context.resources.getString(R.string.pharmacy)
            itemView.search_facility_title.text = facility.name
            Glide.with(itemView.context)
                    .load(BuildConfig.IMAGE + facility.image)
                    .into(itemView.search_facility_image)
        }
    }
}