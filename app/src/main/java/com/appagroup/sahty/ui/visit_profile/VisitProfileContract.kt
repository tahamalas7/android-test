package com.appagroup.sahty.ui.visit_profile

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.VisitProfile

interface VisitProfileContract {

    interface View : IBaseView {

        fun setListeners()

        fun setProfile(profile: VisitProfile)

        fun changeProgressBarVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun loadProfile(userId: Long)

        fun getUserId(): Long
    }
}