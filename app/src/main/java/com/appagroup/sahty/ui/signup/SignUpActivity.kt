package com.appagroup.sahty.ui.signup

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.Accreditation
import com.appagroup.sahty.entity.Specialty
import com.appagroup.sahty.ui.accreditation_dialog.SpecialtiesAdapter
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.utils.UIUtils
import com.appagroup.sahty.utils.UserRole
import com.google.firebase.iid.FirebaseInstanceId
import gun0912.tedbottompicker.TedBottomPicker
import kotlinx.android.synthetic.main.activity_signup.*
import kotlinx.android.synthetic.main.dialog_accreditation.view.*
import java.util.regex.Pattern


class SignUpActivity : BaseActivity<SignUpContract.Presenter>(), SignUpContract.View {

    private val TAG = "SignUpActivity"
    private var userRole: Int = UserRole.ROLE_READER
    private val accreditation: Accreditation = Accreditation(specialityId = 0L)
    private var hasAnAccreditation = false
    private var signUpInProgress = false
    lateinit var accreditationView: View
    lateinit var specialtiesProgressBar: ProgressBar
    lateinit var specialtiesAdapter: SpecialtiesAdapter


    override fun instantiatePresenter(): SignUpContract.Presenter = SignUpPresenter(this)

    override fun getContentView(): Int = R.layout.activity_signup

    override fun initViews() {
        setListeners()
    }

    override fun setListeners() {
        signup_submit_btn.setOnClickListener {
            if (!signUpInProgress && checkData()) {
                changeSignUpProcessStatus(true)
                FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this@SignUpActivity) { instanceIdResult ->
                    submitSignUp(instanceIdResult.token)
                    Log.d(TAG, "FCM token = ${instanceIdResult.token}")
                }

                FirebaseInstanceId.getInstance().instanceId.addOnFailureListener(this@SignUpActivity) { instanceIdResult ->
                    submitSignUp("")
                    Log.d(TAG, "Failed to resolve FCM token")
                }
            }
        }

        signup_reader_btn.setOnClickListener {
            selectReader()
        }

        signup_doctor_btn.setOnClickListener {
            selectDoctor()
        }

        signup_back_btn.setOnClickListener {
            onBackPressed()
        }

        signup_accreditation_btn.setOnClickListener {
            showEnterAccreditationDialog()
        }
    }

    override fun showProgressBar() {
        signup_progress_bar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        signup_progress_bar.visibility = View.GONE
    }

    override fun signUpSuccessfully(name: String) {
        startActivity(Intent(this@SignUpActivity, MainActivity::class.java))
        showError("${getString(R.string.hello)} $name")
        finish()
    }

    override fun checkData(): Boolean {
        var isValid = true

        // check email format validation
        if (signup_email.text.toString().trim() == "") {
            signup_email.error = getString(R.string.error_empty_field)
            isValid = false

        } else {
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(signup_email.text.toString().trim())
            if (!matcher.matches()) {
                signup_email.error = getString(R.string.error_invalid_email)
                isValid = false
            }
        }

        // check title validation
        when {
            signup_name.text.toString().trim() == "" -> {
                signup_name.error = getString(R.string.error_empty_field)
                isValid = false
            }
            signup_name.text.toString().trim().length > 40 -> {
                signup_name.error = getString(R.string.error_long_name)
                isValid = false
            }
        }

        // check password validation
        when {
            signup_password.text.toString() == "" -> {
                signup_password.error = getString(R.string.error_empty_field)
                isValid = false
            }
            signup_password.text.toString().length < 6 -> {
                signup_password.error = getString(R.string.error_short_password)
                isValid = false
            }
        }

        // check password confirmation validation
        when {
            signup_password_confirm.text.toString() == "" -> {
                signup_password_confirm.error = getString(R.string.error_empty_field)
                isValid = false
            }
            signup_password_confirm.text.toString() != signup_password.text.toString() -> {
                signup_password_confirm.error = getString(R.string.error_password_does_not_match)
                isValid = false
            }
        }

        return isValid
    }

    override fun checkAccreditationData(view: View): Boolean {
        var isValid = true

        // check accreditation type if empty
        if (view.accreditation_type.text.isEmpty()) {
            view.accreditation_type.error = getString(R.string.error_empty_field)
            isValid = false
        }

        // check accreditation year if valid year
        if (view.accreditation_year.text.length != 4) {
            view.accreditation_year.error = getString(R.string.error_invalid_year)
            isValid = false
        }

        // check accreditation location if empty
        if (view.accreditation_location.text.isEmpty()) {
            view.accreditation_location.error = getString(R.string.error_empty_field)
            isValid = false
        }

        // check accreditation speciality if empty
        if (accreditation.speciality == "") {
            view.accreditation_specialty.error = getString(R.string.error_empty_field)
            isValid = false
        }

        // check accreditation image if empty
        if (accreditation.image == "") {
            view.accreditation_image_error_border.visibility = View.VISIBLE
            isValid = false
        } else
            view.accreditation_image_error_border.visibility = View.GONE

        return isValid
    }

    private fun addSpecialtyTextWatcher(specialtyEditText: EditText, recyclerView: RecyclerView) {
        specialtyEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                presenter.getSpecialties(s.toString())
                Log.d(TAG, "addSpecialtyTextWatcher -> onTextChanged -> text = ${s.toString()}")
                if (s!!.isEmpty())
                    recyclerView.visibility = View.GONE
                else
                    recyclerView.visibility = View.VISIBLE
            }
        })
    }

    private fun submitSignUp(fcm_token: String) {
        if (hasAnAccreditation)
            presenter.commitSignUpWithAccreditation(
                    signup_name.text.toString().trim(),
                    signup_email.text.toString().trim(),
                    signup_password.text.toString(),
                    userRole,
                    fcm_token,
                    accreditation.type!!,
                    accreditation.specialityId,
                    accreditation.location!!,
                    accreditation.year!!,
                    accreditation.image!!)
        else
            presenter.commitSignUp(
                    signup_name.text.toString().trim(),
                    signup_email.text.toString().trim(),
                    signup_password.text.toString(),
                    userRole,
                    fcm_token)
    }

    override fun updateSpecialties(specialties: MutableList<Specialty>) {
        specialtiesAdapter.updateSpecialties(specialties)
    }

    override fun showEnterAccreditationDialog() {
        // initialize accreditation dialog
        val builder = AlertDialog.Builder(this)
        accreditationView = layoutInflater.inflate(R.layout.dialog_accreditation, null)
        builder.setView(accreditationView)
        val dialog = builder.create()
        dialog.show()

        specialtiesProgressBar = accreditationView.findViewById(R.id.enter_accreditation_specialty_progressBar)

        addSpecialtyTextWatcher(accreditationView.accreditation_specialty, accreditationView.accreditation_specialties_recyclerView)

        accreditationView.accreditation_type.setText(accreditation.type)
        accreditationView.accreditation_specialty.setText(accreditation.speciality)
        accreditationView.accreditation_location.setText(accreditation.location)
        accreditationView.accreditation_year.setText(accreditation.year)
        if (accreditation.image != "")
            Glide.with(this).load(accreditation.image).into(accreditationView.accreditation_image)

        val typeface = Typeface.createFromAsset(this.assets, "fonts/GOTHIC.TTF")
        specialtiesAdapter = SpecialtiesAdapter(typeface)
        accreditationView.accreditation_specialties_recyclerView.layoutManager = LinearLayoutManager(this)
        accreditationView.accreditation_specialties_recyclerView.adapter = specialtiesAdapter

        accreditationView.submit_accreditation_button.setOnClickListener {
            if (checkAccreditationData(accreditationView)) {
                accreditation.type = accreditationView.accreditation_type.text.toString()
                accreditation.location = accreditationView.accreditation_location.text.toString()
                accreditation.year = accreditationView.accreditation_year.text.toString()
                hasAnAccreditation = true

                userRole = UserRole.ROLE_UNCONFIRMED_DOCTOR
                dialog.dismiss()

                Log.d(TAG, "accreditation is\ntype = ${accreditation.type}\nlocation = ${accreditation.location}\nyear = ${accreditation.year}\nspeciality = ${accreditation.speciality}\nimage = ${accreditation.image}\n")
            }
        }

        accreditationView.accreditation_image.setOnClickListener {
            accreditationView.accreditation_image_error_border.visibility = View.GONE

            // request READ_EXTERNAL_STORAGE and READ_EXTERNAL_STORAGE permissions
            ActivityCompat.requestPermissions(this@SignUpActivity, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    1)
        }

        specialtiesAdapter.setSpecialtyClickListener(object : ItemClickListener<Specialty> {
            override fun onClick(item: Specialty) {
                accreditationView.accreditation_specialty.setText(item.name)
                accreditation.speciality = item.name
                accreditation.specialityId = item.id
                accreditationView.accreditation_specialties_recyclerView.visibility = View.GONE
            }
        })

        accreditationView.accreditation_specialty.viewTreeObserver.addOnGlobalLayoutListener {
            val params = accreditationView.accreditation_specialties_recyclerView.layoutParams
            params.width = accreditationView.accreditation_specialty.width
            accreditationView.accreditation_specialties_recyclerView.layoutParams = params
        }
    }

    private fun selectDoctor() {
        signup_reader_btn.alpha = 0.4f
        signup_doctor_btn.alpha = 1.0f
        signup_accreditation_btn.visibility = View.VISIBLE

        userRole = if (hasAnAccreditation)
            UserRole.ROLE_UNCONFIRMED_DOCTOR
        else UserRole.ROLE_DOCTOR_WITHOUT_CERTIFICATE
    }

    private fun selectReader() {
        signup_doctor_btn.alpha = 0.4f
        signup_reader_btn.alpha = 1.0f
        signup_accreditation_btn.visibility = View.GONE
        userRole = UserRole.ROLE_READER
    }

    override fun pickImage() {
        try {
            val tedBottomPicker = TedBottomPicker.Builder(this)
                    .setOnImageSelectedListener { uri ->
                        var finalUri = uri.toString()
                        finalUri = finalUri.substring(7)
                        finalUri = finalUri.replace("%20".toRegex(), " ")

                        if (finalUri != "") {
                            finalUri = UIUtils.compressImage(this@SignUpActivity, finalUri)
                            Glide.with(this).load(finalUri).into(accreditationView.accreditation_image)
                            accreditation.image = finalUri
                        }
                        Log.d(TAG, "Final Uri = $finalUri")
                    }
                    .setTitle(getString(R.string.pick_an_image))
                    .setCameraTileBackgroundResId(R.color.ironGrayColor)
                    .setGalleryTileBackgroundResId(R.color.hintColor)
                    .setPeekHeight(1600)
                    .setCompleteButtonText(R.string.done)
                    .setEmptySelectionText(getString(R.string.no_select))
                    .create()

            tedBottomPicker.show(supportFragmentManager)
        } catch (e: NullPointerException) {
            Log.e(TAG, "NullPointerException: ${e.message}")
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == 1) {
            // call pickImage method if permission granted
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pickImage()
            }
        }
    }

    override fun showSpecialtySearchProgressBar() {
        accreditationView.enter_accreditation_specialty_progressBar.visibility = View.VISIBLE
    }

    override fun hideSpecialtySearchProgressBar() {
        accreditationView.enter_accreditation_specialty_progressBar.visibility = View.GONE
    }

    override fun changeSignUpProcessStatus(status: Boolean) {
        signUpInProgress = status
    }
}