package com.appagroup.sahty.ui.post_story

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.ui.post_article.categories_dialog.SelectCategoryDialog
import com.appagroup.sahty.utils.UIUtils
import com.appagroup.sahty.utils.toRequestBody
import com.facebook.drawee.backends.pipeline.Fresco
import com.stfalcon.frescoimageviewer.ImageViewer
import gun0912.tedbottompicker.TedBottomPicker
import kotlinx.android.synthetic.main.fragment_post_story.*
import kotlinx.android.synthetic.main.item_article_language.view.*
import kotlinx.android.synthetic.main.item_tab.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class PostStoryFragment : BaseFragment<PostStoryContract.Presenter>(), PostStoryContract.View {

    private lateinit var progressDialog: ProgressDialog
    private lateinit var enTitle: EditText
    private lateinit var enContent: EditText
    private lateinit var arTitle: EditText
    private lateinit var arContent: EditText
    private val adapter = LanguagesPagerAdapter()
    private var selectedCategoryId = -1L
    private var selectedImage = ""

    override fun instantiatePresenter(): PostStoryContract.Presenter = PostStoryPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_post_story

    override fun initViews(view: View) {
        // hide search bar
        (activity as MainActivity).changeSearchBarVisibility(View.GONE)

        progressDialog = ProgressDialog(activity)
        post_story_username.text = presenter.getUserName()
        if (presenter.getUserImage().isNotEmpty())
            Glide.with(activity!!)
                    .load(BuildConfig.IMAGE + presenter.getUserImage())
                    .into(post_story_user_image)

        // initialize languages ViewPager
        story_languages_viewPager.adapter = adapter
        post_story_language_tabs.setupWithViewPager(story_languages_viewPager)

        // initialize English tab
        val tabEnglish = LayoutInflater.from(activity).inflate(R.layout.item_tab, null)
        tabEnglish.tab_text.text = getString(R.string.english_version)
        post_story_language_tabs.getTabAt(0)!!.customView = tabEnglish

        // initialize Arabic tab
        val tabArabic = LayoutInflater.from(activity).inflate(R.layout.item_tab, null)
        tabArabic.tab_text.text = getString(R.string.arabic_version)
        post_story_language_tabs.getTabAt(1)!!.customView = tabArabic


        setListeners()
    }

    override fun setListeners() {
        post_story_back_arrow.setOnClickListener { activity!!.onBackPressed() }

        post_story_images_btn.setOnClickListener {
            // request READ_EXTERNAL_STORAGE and READ_EXTERNAL_STORAGE permissions
            ActivityCompat.requestPermissions(activity!!, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE),
                    REQUEST_PERMESSIONS_CODE)
        }

        post_story_import_image_button.setOnClickListener {
            // request READ_EXTERNAL_STORAGE and READ_EXTERNAL_STORAGE permissions
            ActivityCompat.requestPermissions(activity!!, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE),
                    REQUEST_PERMESSIONS_CODE)
        }

        post_story_select_category_btn.setOnClickListener {
            val dialog = SelectCategoryDialog.getInstance()
            dialog.setTargetFragment(this, SELECT_CATEGORY_REQUEST_CODE)
            dialog.show(activity!!.supportFragmentManager, TAG)
        }

        post_story_selected_image.setOnClickListener {
            Fresco.initialize(activity)
            ImageViewer.Builder(activity, arrayListOf(selectedImage))
                    .setStartPosition(0)
                    .setFormatter(ImageViewer.Formatter<String> { image -> "file://$image" })
                    .show()
        }

        post_story_submit.setOnClickListener {
            if (checkFieldsValidate()){
                val imgFile = File(selectedImage)
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile)
                presenter.postStory(
                        enTitle.text.toString().toRequestBody(),
                        enContent.text.toString().toRequestBody(),
                        arTitle.text.toString().toRequestBody(),
                        arContent.text.toString().toRequestBody(),
                        selectedCategoryId.toString().toRequestBody(),
                        MultipartBody.Part.createFormData("image", imgFile.name, requestFile)
                )
            }
        }
    }

    override fun checkFieldsValidate(): Boolean {
        var enValid = true
        var arValid = true

        // check for empty fields
        when {
            arTitle.text.trim().isEmpty() -> {
                arTitle.error = getString(R.string.error_empty_field)
                arValid = false
            }
            arTitle.text.trim().length > 40 -> {
                arTitle.error = getString(R.string.error_long_title)
                arValid = false
            }
            arTitle.text.trim().length < 10 -> {
                arTitle.error = getString(R.string.error_short_title)
                arValid = false
            }
        }

        if (arContent.text.trim().isEmpty()) {
            arContent.error = getString(R.string.error_empty_field)
            arValid = false
        } else if (arContent.text.trim().length < 50) {
            arContent.error = getString(R.string.error_short_content)
            arValid = false
        }

        when {
            enTitle.text.trim().isEmpty() -> {
                enTitle.error = getString(R.string.error_empty_field)
                enValid = false
            }
            enTitle.text.trim().length > 40 -> {
                enTitle.error = getString(R.string.error_long_title)
                enValid = false
            }
            enTitle.text.trim().length < 10 -> {
                enTitle.error = getString(R.string.error_short_title)
                enValid = false
            }
        }

        if (enContent.text.trim().isEmpty()) {
            enContent.error = getString(R.string.error_empty_field)
            enValid = false
        } else if (enContent.text.trim().length < 50) {
            enContent.error = getString(R.string.error_short_content)
            enValid = false
        }

        // slide ViewPager to the language with errors
        if (!enValid && story_languages_viewPager.currentItem == 1)
            story_languages_viewPager.currentItem = 0
        else if (!arValid && story_languages_viewPager.currentItem == 0)
            story_languages_viewPager.currentItem = 1

        return if ((enValid && arValid) && selectedCategoryId == -1L) {
            showMessageDialog(R.string.err_no_category_selected)
            false
        } else if (selectedImage.isEmpty()) {
            showMessageDialog(R.string.err_no_image_selected)
            false
        } else enValid && arValid
    }

    override fun storyPostedSuccessfully() {
        showMessageDialog(R.string.article_posted_successfully)
        activity!!.onBackPressed()
    }

    override fun showProgressDialog() {
        progressDialog.setMessage(getString(R.string.message_posting_article))
        progressDialog.show()
    }

    override fun hideProgressDialog() {
        progressDialog.dismiss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d(TAG, "onActivityResult -> data = $data")
        Log.d(TAG, "onActivityResult -> requestCode = $requestCode")
        if (data != null && requestCode == SELECT_CATEGORY_REQUEST_CODE) {
            if (data.getBooleanExtra(SelectCategoryDialog.SELECTED_SUCCESSFULLY, false)) {
                selectedCategoryId =
                        data.getLongExtra(SelectCategoryDialog.SELECTED_CATEGORY_ID, -1L)
                post_story_selected_category.text =
                        "#${data.getStringExtra(SelectCategoryDialog.SELECTED_CATEGORY_NAME)}"
                Log.d(TAG, "onActivityResult -> CATEGORY NAME = ${data.getStringExtra(SelectCategoryDialog.SELECTED_CATEGORY_NAME)}")
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                requestCode == REQUEST_PERMESSIONS_CODE)
            pickImage()
    }

    // open internal storage gallery
    private fun pickImage() {
        Log.d(TAG, "pickImage")
        val tedBottomPicker = TedBottomPicker.Builder(activity!!)
                .setOnImageSelectedListener { uri ->
                    selectedImage = uri.toString()
                    selectedImage = selectedImage.substring(7)
                    selectedImage = selectedImage.replace("%20", " ")
                    post_story_import_image_button.visibility = View.GONE

                    if (selectedImage != "") {
                        selectedImage = UIUtils.compressImage(activity!!, selectedImage)
                        Glide.with(this).load(selectedImage).into(post_story_selected_image)
                    }
                    Log.d(TAG, "Final Uri = $selectedImage")
                }
                .setTitle(getString(R.string.pick_an_image))
                .setCameraTileBackgroundResId(R.color.ironGrayColor)
                .setGalleryTileBackgroundResId(R.color.hintColor)
                .setPeekHeight(1600)
                .setCompleteButtonText(R.string.done)
                .setEmptySelectionText(getString(R.string.no_select))
                .create()

        tedBottomPicker.show(activity!!.supportFragmentManager)
    }

    inner class LanguagesPagerAdapter : PagerAdapter() {

        override fun isViewFromObject(p0: View, p1: Any): Boolean = p0 == p1

        override fun getCount(): Int = 2

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val layoutInflater = container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val itemView = layoutInflater.inflate(R.layout.item_article_language, container, false)

            when (position) {
                0 -> {
                    enTitle = itemView.article_language_title
                    enContent = itemView.article_language_content

                    enTitle.hint = getString(R.string.english_enter_title)
                    enContent.hint = getString(R.string.english_enter_content)
                }

                1 -> {
                    arTitle = itemView.article_language_title
                    arContent = itemView.article_language_content

                    arTitle.hint = getString(R.string.arabic_enter_title)
                    arContent.hint = getString(R.string.arabic_enter_content)
                }
            }

            container.addView(itemView)
            return itemView
        }
    }

    override fun onPause() {
        super.onPause()
        // show search bar
        (activity as MainActivity).changeSearchBarVisibility(View.VISIBLE)
    }

    companion object {
        val TAG = "PostStoryFragment"
        val REQUEST_PERMESSIONS_CODE = 92
        val SELECT_CATEGORY_REQUEST_CODE = 93

        fun getInstance(): PostStoryFragment =
                PostStoryFragment()
    }
}