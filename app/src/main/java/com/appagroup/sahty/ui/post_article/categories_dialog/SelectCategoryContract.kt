package com.appagroup.sahty.ui.post_article.categories_dialog

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.Category

interface SelectCategoryContract {

    interface View : IBaseView {

        fun setListeners()

        fun changeProgressBarVisibility(visibility: Int)

        fun updateCategories(categories: MutableList<Category>)
    }

    interface Presenter : IBasePresenter<View> {

        fun loadCategories()
    }
}