package com.appagroup.sahty.ui.profile

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import kotlinx.android.synthetic.main.item_follow.view.*

class FollowingAdapter(private val typeface: Typeface) : RecyclerView.Adapter<FollowingAdapter.FollowingHolder>() {

    private lateinit var followButtonClickListener: FollowButtonClickListener
    val followingList: MutableList<Follow> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): FollowingAdapter.FollowingHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_follow, parent, false)
        return FollowingHolder(view)
    }

    override fun getItemCount(): Int = followingList.size

    override fun onBindViewHolder(holder: FollowingAdapter.FollowingHolder, position: Int) {
        val following = followingList[position]
        holder.bind(following)
        holder.itemView.follow_btn.setOnClickListener {
            removeFollowingUser(following)
            notifyItemRemoved(position)
            followButtonClickListener.unfollowUser(following)
        }
    }

    fun updateFollows(followingList: MutableList<Follow>) {
        this.followingList.addAll(followingList)
        notifyDataSetChanged()
    }

    fun removeFollowingUser(followed: Follow) {
        for (position in 0 until followingList.size)
            if (followingList[position].id == followed.id) {
                followingList.removeAt(position)
                notifyDataSetChanged()
                break
            }
    }

    fun addFollowingUser(following: Follow) {
        followingList.add(following)
        notifyDataSetChanged()
    }

    fun setOnFollowButtonClickListener(followButtonClickListener: FollowButtonClickListener) {
        this.followButtonClickListener = followButtonClickListener
    }

    inner class FollowingHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(followed: Follow) {
            itemView.follow_username.text = followed.name
            itemView.follow_username.typeface = typeface

            if (followed.image != null)
                Glide.with(itemView.context)
                        .load(BuildConfig.IMAGE + followed.image)
                        .into(itemView.follow_user_image)

            itemView.follow_btn.setBackgroundResource(R.drawable.background_follow_button_on)
            itemView.follow_btn.text = itemView.context.getString(R.string.unfollow)
            followed.isFollowed = true
        }
    }
}