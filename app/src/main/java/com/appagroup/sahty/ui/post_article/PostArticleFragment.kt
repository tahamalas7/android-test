package com.appagroup.sahty.ui.post_article

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.ui.listener.OnClick
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.ui.post_article.categories_dialog.SelectCategoryDialog
import com.appagroup.sahty.ui.videos.VideoPlayerActivity
import com.appagroup.sahty.utils.CompressImageUtils
import com.appagroup.sahty.utils.JavaUIUtils
import com.appagroup.sahty.utils.toRequestBody
import com.facebook.drawee.backends.pipeline.Fresco
import com.stfalcon.frescoimageviewer.ImageViewer
import gun0912.tedbottompicker.TedBottomPicker
import kotlinx.android.synthetic.main.fragment_post_article.*
import kotlinx.android.synthetic.main.item_article_language.view.*
import kotlinx.android.synthetic.main.item_tab.view.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File


class PostArticleFragment : BaseFragment<PostArticleContract.Presenter>(), PostArticleContract.View {

    private lateinit var progressDialog: ProgressDialog
    private lateinit var enTitle: EditText
    private lateinit var enContent: EditText
    private lateinit var arTitle: EditText
    private lateinit var arContent: EditText
    private var compressImagesUtils = CompressImageUtils()
    private val adapter = LanguagesPagerAdapter()
    private val imagesAdapter = ImagesAdapter()
    private var articleWithImages = true
    private var selectedCategoryId = -1L
    private var videoUrl = ""

    override fun instantiatePresenter(): PostArticleContract.Presenter = PostArticlePresenter(this)

    override fun getLayout(): Int = R.layout.fragment_post_article

    override fun initViews(view: View) {
        // hide search bar
        (activity as MainActivity).changeSearchBarVisibility(View.GONE)

        progressDialog = ProgressDialog(activity)
        post_article_username.text = presenter.getUserName()
        if (presenter.getUserImage().isNotEmpty())
            Glide.with(activity!!)
                    .load(BuildConfig.IMAGE + presenter.getUserImage())
                    .into(post_article_user_image)

        // initialize languages ViewPager
        article_languages_viewPager.adapter = adapter
        post_article_language_tabs.setupWithViewPager(article_languages_viewPager)

        // initialize English tab
        val tabEnglish = LayoutInflater.from(activity).inflate(R.layout.item_tab, null)
        tabEnglish.tab_text.text = getString(R.string.english_version)
        post_article_language_tabs.getTabAt(0)!!.customView = tabEnglish

        // initialize Arabic tab
        val tabArabic = LayoutInflater.from(activity).inflate(R.layout.item_tab, null)
        tabArabic.tab_text.text = getString(R.string.arabic_version)
        post_article_language_tabs.getTabAt(1)!!.customView = tabArabic

        post_article_images_recyclerView.layoutManager = GridLayoutManager(activity, 3)
        post_article_images_recyclerView.adapter = imagesAdapter

        setListeners()
    }

    override fun setListeners() {
        post_article_back_arrow.setOnClickListener { activity!!.onBackPressed() }

        post_article_images_btn.setOnClickListener { selectImages() }

        post_article_video_btn.setOnClickListener { selectVideo() }

        post_article_select_category.setOnClickListener {
            val dialog = SelectCategoryDialog.getInstance()
            dialog.setTargetFragment(this, SELECT_CATEGORY_REQUEST_CODE)
            dialog.show(activity!!.supportFragmentManager, TAG)
        }

        post_article_selected_video_play.setOnClickListener {
            val intent = Intent(activity, VideoPlayerActivity::class.java)
            intent.putExtra(VideoPlayerActivity.VIDEO_URL, videoUrl)
            intent.putExtra(VideoPlayerActivity.VIDEO_SOURCE, VideoPlayerActivity.TYPE_INTERNAL)
            startActivity(intent)
        }

        post_article_selected_video_refresh_btn.setOnClickListener {
            // request READ_EXTERNAL_STORAGE and READ_EXTERNAL_STORAGE permissions
            ActivityCompat.requestPermissions(activity!!, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE),
                    VIDEO_REQUEST_PERMESSIONS_CODE)
        }

        // open gallery to choose single video
        post_article_add_video_button.setOnClickListener {
            // request READ_EXTERNAL_STORAGE and READ_EXTERNAL_STORAGE permissions
            ActivityCompat.requestPermissions(activity!!, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE),
                    VIDEO_REQUEST_PERMESSIONS_CODE)
        }

        // open gallery to choose multiple images
        imagesAdapter.setOnAddImageButtonClickListener(object : OnClick {
            override fun onClick() {
                // request READ_EXTERNAL_STORAGE and READ_EXTERNAL_STORAGE permissions
                ActivityCompat.requestPermissions(activity!!, arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE),
                        IMAGES_REQUEST_PERMESSIONS_CODE)
            }
        })

        imagesAdapter.setOnSelectedImageClickListener(object : SelectedImageClickListener {
            override fun onSelectedImageClick(position: Int, images: MutableList<String>) {
                Fresco.initialize(activity)
                ImageViewer.Builder(activity, images)
                        .setStartPosition(position)
                        .setFormatter(ImageViewer.Formatter<String> { image -> "file://$image" })
                        .show()
            }
        })

        post_article_submit.setOnClickListener {
            if (checkFieldsValidate()) {
                if (articleWithImages) {
                    presenter.postArticleWithImages(
                            enTitle.text.toString().toRequestBody(),
                            enContent.text.toString().toRequestBody(),
                            arTitle.text.toString().toRequestBody(),
                            arContent.text.toString().toRequestBody(),
                            selectedCategoryId.toString().toRequestBody(),
                            convertImagesListMultiPartList(imagesAdapter.getImages())
                    )
                } else {
                    val imgFile = File(videoUrl)
                    val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile)
                    presenter.postArticleWithVideo(
                            enTitle.text.toString().toRequestBody(),
                            enContent.text.toString().toRequestBody(),
                            arTitle.text.toString().toRequestBody(),
                            arContent.text.toString().toRequestBody(),
                            selectedCategoryId.toString().toRequestBody(),
                            MultipartBody.Part.createFormData("video", imgFile.name, requestFile)
                    )
                }
            }
        }
    }

    private fun convertImagesListMultiPartList(images: MutableList<String>): MutableList<MultipartBody.Part> {
        val multiPartList: MutableList<MultipartBody.Part> = mutableListOf()
        for (image in images) {
            var s = image + ""
            s = s.substring(7, s.length)
            s = s.replace("%20", " ")
            s = compressImagesUtils.compressImage(activity!!, s)

            val imgFile = File(s)
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile)
            multiPartList.add(MultipartBody.Part.createFormData("image[]", imgFile.name, requestFile))
        }
        return multiPartList
    }

    private fun selectImages() {
        post_article_images_btn.setImageResource(R.drawable.ic_camera_on)
        post_article_video_btn.setImageResource(R.drawable.ic_video_off)

        post_article_images_recyclerView.visibility = View.VISIBLE
        post_article_selected_video_bar.visibility = View.GONE
        articleWithImages = true
    }

    private fun selectVideo() {
        post_article_images_btn.setImageResource(R.drawable.ic_camera_off)
        post_article_video_btn.setImageResource(R.drawable.ic_video_on)

        post_article_images_recyclerView.visibility = View.GONE
        post_article_selected_video_bar.visibility = View.VISIBLE
        articleWithImages = false
    }

    override fun checkFieldsValidate(): Boolean {
        var enValid = true
        var arValid = true

        // check for empty fields
        when {
            arTitle.text.trim().isEmpty() -> {
                arTitle.error = getString(R.string.error_empty_field)
                arValid = false
            }
            arTitle.text.trim().length > 40 -> {
                arTitle.error = getString(R.string.error_long_title)
                arValid = false
            }
            arTitle.text.trim().length < 10 -> {
                arTitle.error = getString(R.string.error_short_title)
                arValid = false
            }
        }

        if (arContent.text.trim().isEmpty()) {
            arContent.error = getString(R.string.error_empty_field)
            arValid = false
        } else if (arContent.text.trim().length < 50) {
            arContent.error = getString(R.string.error_short_content)
            arValid = false
        }

        when {
            enTitle.text.trim().isEmpty() -> {
                enTitle.error = getString(R.string.error_empty_field)
                enValid = false
            }
            enTitle.text.trim().length > 40 -> {
                enTitle.error = getString(R.string.error_long_title)
                enValid = false
            }
            enTitle.text.trim().length < 10 -> {
                enTitle.error = getString(R.string.error_short_title)
                enValid = false
            }
        }

        if (enContent.text.trim().isEmpty()) {
            enContent.error = getString(R.string.error_empty_field)
            enValid = false
        } else if (enContent.text.trim().length < 50) {
            enContent.error = getString(R.string.error_short_content)
            enValid = false
        }

        // slide ViewPager to the language with errors
        if (!enValid && article_languages_viewPager.currentItem == 1)
            article_languages_viewPager.currentItem = 0
        else if (!arValid && article_languages_viewPager.currentItem == 0)
            article_languages_viewPager.currentItem = 1

        return if ((enValid && arValid) && selectedCategoryId == -1L) {
            showMessageDialog(R.string.err_no_category_selected)
            false
        } else if (articleWithImages && imagesAdapter.itemCount == 1) {
            showMessageDialog(R.string.err_no_images_selected)
            false
        } else if (!articleWithImages && videoUrl.isEmpty()) {
            showMessageDialog(R.string.err_no_video_selected)
            false
        } else enValid && arValid
    }

    override fun articlePostedSuccessfully() {
        showMessageDialog(R.string.article_posted_successfully)
        activity!!.onBackPressed()
    }

    override fun showProgressDialog() {
        progressDialog.setMessage(getString(R.string.message_posting_article))
        progressDialog.show()
    }

    override fun hideProgressDialog() {
        progressDialog.dismiss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d(TAG, "onActivityResult -> data = $data")
        Log.d(TAG, "onActivityResult -> requestCode = $requestCode")
        if (data != null) {
            when (requestCode) {
                SELECT_VIDEO_REQUEST_CODE -> {
                    videoUrl = JavaUIUtils.getFilePath(activity!!, Uri.parse(data.data!!.toString()))
                    Log.d(TAG, "EDITED videoUrl = $videoUrl")

                    post_article_add_video_button.visibility = View.GONE
                    post_article_selected_video_play.visibility = View.VISIBLE
                    post_article_selected_video_refresh_btn.visibility = View.VISIBLE

                    Glide.with(activity!!)
                            .load(videoUrl)
                            .into(post_article_selected_video_thumb)
                }

                SELECT_CATEGORY_REQUEST_CODE -> {

                    if (data.getBooleanExtra(SelectCategoryDialog.SELECTED_SUCCESSFULLY, false)) {
                        selectedCategoryId =
                                data.getLongExtra(SelectCategoryDialog.SELECTED_CATEGORY_ID, -1L)
                        post_article_selected_category.text =
                                "#${data.getStringExtra(SelectCategoryDialog.SELECTED_CATEGORY_NAME)}"
                        Log.d(TAG, "onActivityResult -> CATEGORY NAME = ${data.getStringExtra(SelectCategoryDialog.SELECTED_CATEGORY_NAME)}")
                    }
                }
            }

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            when (requestCode) {
                IMAGES_REQUEST_PERMESSIONS_CODE -> pickImages()
                VIDEO_REQUEST_PERMESSIONS_CODE -> {
                    Log.d(TAG, "TWO: onRequestPermissionsResult")
                    pickVideo()
                }
            }
        }
    }

    // open internal storage gallery
    private fun pickImages() {
        // setup TedBottomPicker view
        val bottomPicker = TedBottomPicker.Builder(activity!!)
                .setOnMultiImageSelectedListener { uriList ->
                    imagesAdapter.addImages(uriList)
                    Log.d(TAG, "image URL = ${uriList[0]}")
                }
                .setCameraTileBackgroundResId(R.color.textColor1)
                .setGalleryTileBackgroundResId(R.color.lightGrayColor)
                .setImageProvider { imageView, imageUri ->
                    imageView.scaleType = ImageView.ScaleType.CENTER_CROP
                    Glide.with(activity!!).load(imageUri).into(imageView)
                }
                .setPeekHeight(1600)
                .showTitle(false)
                .setCompleteButtonText(R.string.done)
                .setEmptySelectionText(R.string.no_select)
                .create()

        bottomPicker.show(activity!!.supportFragmentManager)
    }

    private fun pickVideo() {
        // setup TedBottomPicker view
        val intent = Intent()
        intent.type = "video/*"
        intent.action = Intent.ACTION_GET_CONTENT
        activity!!.startActivityForResult(intent, SELECT_VIDEO_REQUEST_CODE)
    }

    inner class LanguagesPagerAdapter : PagerAdapter() {

        override fun isViewFromObject(p0: View, p1: Any): Boolean = p0 == p1

        override fun getCount(): Int = 2

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val layoutInflater = container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val itemView = layoutInflater.inflate(R.layout.item_article_language, container, false)

            when (position) {
                0 -> {
                    enTitle = itemView.article_language_title
                    enContent = itemView.article_language_content

                    enTitle.hint = getString(R.string.english_enter_title)
                    enContent.hint = getString(R.string.english_enter_content)
                }

                1 -> {
                    arTitle = itemView.article_language_title
                    arContent = itemView.article_language_content

                    arTitle.hint = getString(R.string.arabic_enter_title)
                    arContent.hint = getString(R.string.arabic_enter_content)
                }
            }

            container.addView(itemView)
            return itemView
        }
    }

    override fun onPause() {
        super.onPause()
        // show search bar
        (activity as MainActivity).changeSearchBarVisibility(View.VISIBLE)
    }

    companion object {
        val TAG = "PostArticleFragment"
        val IMAGES_REQUEST_PERMESSIONS_CODE = 97
        val VIDEO_REQUEST_PERMESSIONS_CODE = 98
        val SELECT_CATEGORY_REQUEST_CODE = 95
        val SELECT_VIDEO_REQUEST_CODE = 96

        fun getInstance(): PostArticleFragment =
                PostArticleFragment()
    }
}