package com.appagroup.sahty.ui.facilities.rating_dialog

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView

interface RatingContract {

    interface View : IBaseView {

        fun setListeners()

        fun changeProgressBarVisibility(visibility: Int)

        fun rateSubmittedSuccessfully()
    }

    interface Presenter : IBasePresenter<View> {

        fun submitRating(facilityId: Long, content: String, rate: Float)
    }
}