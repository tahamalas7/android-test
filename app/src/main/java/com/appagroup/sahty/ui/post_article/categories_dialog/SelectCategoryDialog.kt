package com.appagroup.sahty.ui.post_article.categories_dialog

import android.content.Intent
import androidx.core.content.ContextCompat
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseDialog
import com.appagroup.sahty.entity.Category
import kotlinx.android.synthetic.main.dialog_select_category.*

class SelectCategoryDialog : BaseDialog<SelectCategoryContract.Presenter>(), SelectCategoryContract.View {

    private val adapter = SelectCategoriesAdapter()

    override fun instantiatePresenter(): SelectCategoryContract.Presenter = SelectCategoryPresenter(this)

    override fun getLayout(): Int = R.layout.dialog_select_category

    override fun initViews(view: View) {
        select_category_recyclerView.layoutManager = GridLayoutManager(activity!!, 3)
        select_category_recyclerView.adapter = adapter

        presenter.loadCategories()
        setListeners()
    }

    override fun setListeners() {
        submit_selected_category.setOnClickListener {
            if (adapter.getSelectCategory() != adapter.NO_SELECT)
                dismiss()
            else
                showError(getString(R.string.err_no_selected_category))
        }

        adapter.setOnSelectCategoryListener(object : SelectCategoryListener {
            override fun onSelect() {
                submit_selected_category.setTextColor(ContextCompat.getColor(activity!!, R.color.blackColor))
            }

            override fun onUnselect() {
                submit_selected_category.setTextColor(ContextCompat.getColor(activity!!, R.color.textColor5))
            }
        })
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        select_category_progressBar.visibility = visibility
    }

    override fun updateCategories(categories: MutableList<Category>) {
        adapter.updateCategories(categories)
    }

    override fun onPause() {
        super.onPause()

        if (targetFragment != null) {
            val intent = Intent()
            if (adapter.getSelectCategory() != adapter.NO_SELECT) {
                intent.putExtra(SELECTED_SUCCESSFULLY, true)
                intent.putExtra(SELECTED_CATEGORY_ID, adapter.getSelectCategory()!!.id)
                intent.putExtra(SELECTED_CATEGORY_NAME, adapter.getSelectCategory()!!.name)
            } else
                intent.putExtra(SELECTED_SUCCESSFULLY, false)
            targetFragment!!.onActivityResult(targetRequestCode, 200, intent)
        }
    }

    companion object {
        val SELECTED_SUCCESSFULLY = "RATED_SUCCESSFULLY"
        val SELECTED_CATEGORY_ID = "SELECTED_CATEGORY_ID"
        val SELECTED_CATEGORY_NAME = "SELECTED_CATEGORY_NAME"
        val TAG = "SelectCategoryDialog"

        fun getInstance(): SelectCategoryDialog =
                SelectCategoryDialog()
    }
}