package com.appagroup.sahty.ui.post_article.categories_dialog

interface SelectCategoryListener {

    fun onSelect()

    fun onUnselect()
}