package com.appagroup.sahty.ui.login

import android.content.Intent
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.ui.login.select_role.RoleSubmitListener
import com.appagroup.sahty.ui.login.select_role.SelectRoleDialog
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.ui.signup.SignUpActivity
import com.appagroup.sahty.utils.GMAIL
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*
import java.util.regex.Pattern


class LoginActivity : BaseActivity<LoginPresenter>(), LoginContract.View {

    val TAG = "LoginActivity"
    val GOOGLE_AUTH = 1243
    var loginInProgress = false
    private lateinit var callbackManager: CallbackManager

    override fun instantiatePresenter(): LoginPresenter = LoginPresenter(this)

    override fun initViews() {
        callbackManager = CallbackManager.Factory.create()
        login_button.setReadPermissions(Arrays.asList("email", "public_profile"))

        initializeInstagramAuth()
        initializeFacebookAuth()
        initializeGmailAuth()

        setListeners()
    }

    override fun getContentView(): Int = R.layout.activity_login

    override fun setListeners() {
        login_signup_btn.setOnClickListener {
            startActivity(Intent(this, SignUpActivity::class.java))
        }

        login_btn.setOnClickListener {
            if (!loginInProgress && checkData()) {
                changeLoginProcessStatus(true)
                FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this@LoginActivity) { instanceIdResult ->
                    submitLogin(instanceIdResult.token)
                    Log.d(TAG, "FCM token = ${instanceIdResult.token}")
                }

                FirebaseInstanceId.getInstance().instanceId.addOnFailureListener(this@LoginActivity) { instanceIdResult ->
                    submitLogin("")
                    Log.d(TAG, "Failed to resolve FCM token")
                }
            }
        }
    }

    private fun submitLogin(fcm_token: String) {
        presenter.login(
                login_email.text.toString(),
                login_password.text.toString(),
                fcm_token)
    }

    override fun checkData(): Boolean {
        var isValid = true

        // check email format validation
        if (login_email.text.toString().trim() == "") {
            login_email.error = getString(R.string.error_empty_field)
            isValid = false

        } else {
            val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
            val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
            val matcher = pattern.matcher(login_email.text.toString().trim())
            if (!matcher.matches()) {
                login_email.error = getString(R.string.error_invalid_email)
                isValid = false
            }
        }

        // check password validation
        when {
            login_password.text.toString() == "" -> {
                login_password.error = getString(R.string.error_empty_field)
                isValid = false
            }
            login_password.text.toString().length < 6 -> {
                login_password.error = getString(R.string.error_short_password)
                isValid = false
            }
        }

        return isValid
    }

    override fun showProgressBar() {
        login_progress_bar.visibility = View.VISIBLE
    }

    override fun hideProgressBar() {
        login_progress_bar.visibility = View.GONE
    }

    override fun changeLoginProcessStatus(status: Boolean) {
        loginInProgress = false
    }

    override fun loginSuccessfully(username: String) {
        startActivity(Intent(this@LoginActivity, MainActivity::class.java))
        showError("${getString(R.string.hello)} $username")
        finish()
    }

    override fun initializeFacebookAuth() {
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this@LoginActivity) { instanceIdResult ->
                    presenter.loginWithFacebook(instanceIdResult.token)
                    Log.d(TAG, "FCM token = ${instanceIdResult.token}")
                }

                FirebaseInstanceId.getInstance().instanceId.addOnFailureListener(this@LoginActivity) { instanceIdResult ->
                    presenter.loginWithFacebook("")
                    Log.d(TAG, "Failed to resolve FCM token")
                }
                Log.d(TAG, "initializeFacebookAuth -> FacebookCallback -> onSuccess")
            }

            override fun onCancel() {
                hideProgressBar()
                Log.d(TAG, "initializeFacebookAuth -> FacebookCallback -> onCancel")
            }

            override fun onError(error: FacebookException?) {
                hideProgressBar()
                Log.d(TAG, "initializeFacebookAuth -> FacebookCallback -> onError -> error = ${error!!.message}")
            }
        })

        login_facebook_btn.setOnClickListener {
            if (!loginInProgress) {
                changeLoginProcessStatus(true)
                showProgressBar()
                login_button.performClick()
            }
        }
    }

    override fun initializeGmailAuth() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        val googleSignInIntent = GoogleSignIn.getClient(this, gso)
        googleSignInIntent.silentSignIn()

        // Build a GoogleSignInClient with the options specified by gso.
        login_google_btn.setOnClickListener {
            if (!loginInProgress) {
                changeLoginProcessStatus(true)
                val signInIntent = googleSignInIntent.signInIntent
                startActivityForResult(signInIntent, GOOGLE_AUTH)
            }
        }
    }

    private fun handleGmailLoginResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            Log.d(TAG, "loginWithFacebook -> GET RESULT = ${completedTask.getResult(ApiException::class.java)}")
            val account = completedTask.getResult(ApiException::class.java)
            if (account != null && account.id != null && account.id!!.isNotEmpty()) {
                val name = account.displayName ?: "Undefined user"
                val email = account.email ?: "email"
                val image = account.photoUrl.toString()
                val token = account.idToken ?: "TOKEN"

                FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this@LoginActivity) { instanceIdResult ->
                    presenter.socialLogin(name, email, instanceIdResult.token, token, GMAIL)
                    Log.d(TAG, "FCM token = ${instanceIdResult.token}")
                }

                FirebaseInstanceId.getInstance().instanceId.addOnFailureListener(this@LoginActivity) {
                    presenter.socialLogin(name, email, "", token, GMAIL)
                    Log.d(TAG, "Failed to resolve FCM token")
                }
                Log.d(TAG, "loginWithFacebook -> newMeRequest -> email = $email, title = $name , image = $image , social_token = $token")

            }
        } catch (e: ApiException) {
            AlertDialog.Builder(this)
                    .setMessage(R.string.error_while_google_login)
                    .setNegativeButton(R.string.cancel) { _, _ -> }
                    .show()

            hideProgressBar()

            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "GoogleLoginResult: error = $e")
            Log.w(TAG, "GoogleLoginResult: failed code = " + e.statusCode)
        }
    }

    override fun initializeInstagramAuth() {
        login_instagram_btn.setOnClickListener {
            val dialog = InstagramAuthDialog(this)
            dialog.setCancelable(true)
            dialog.show()

            dialog.setOnInstagramAuthTokenReceived(object : InstagramAuthListener {
                override fun onAuthTokenReceived(authToken: String?) {
                    if (authToken != null) {
                        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this@LoginActivity) { instanceIdResult ->
                            presenter.requestInstagramAPI(authToken, instanceIdResult.token)
                            Log.d(TAG, "initializeInstagramAuth -> FCM token = ${instanceIdResult.token}")
                        }

                        FirebaseInstanceId.getInstance().instanceId.addOnFailureListener(this@LoginActivity) { instanceIdResult ->
                            presenter.requestInstagramAPI(authToken, "")
                            Log.d(TAG, "initializeInstagramAuth -> Failed to resolve FCM token")
                        }
//                        presenter.requestInstagramAPI(authToken)
                    }
                }
            })
        }
    }

    override fun openSelectRoleDialog(username: String) {
        val dialog = SelectRoleDialog.getInstance()
        dialog.show(supportFragmentManager, SelectRoleDialog.TAG)

        dialog.setOnRoleSubmitListener(object : RoleSubmitListener {
            override fun roleSubmittedSuccessfully(role: Int) {
                presenter.setUserRole(role)
                loginSuccessfully(username)
            }

            override fun dialogDismissed() {
                presenter.clearUserData()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        // FaceBook Auth
        callbackManager.onActivityResult(requestCode, resultCode, data)

        super.onActivityResult(requestCode, resultCode, data)

        // Google Auth
        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == GOOGLE_AUTH) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleGmailLoginResult(task)
        }
    }
}