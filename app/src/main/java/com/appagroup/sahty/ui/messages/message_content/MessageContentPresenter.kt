package com.appagroup.sahty.ui.messages.message_content

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.*
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response
import com.appagroup.sahty.ui.messages.message_content.MessageContentContract as Contract

class MessageContentPresenter(view: Contract.View) : BasePresenter<Contract.View>(view), Contract.Presenter {

    private val TAG = "MessageContentPresenter"
    private var repliesPage = 1
    private var loadMoreReplies = true

    override fun loadReplies(receiverId: Long) {
        Log.d(TAG, "loadReplies -> receiverId = $receiverId")
        if (loadMoreReplies) {
            if (repliesPage != 1) view.changeLoaderVisibility(View.VISIBLE)
            subscribe(dataManager.getMessageReplies(receiverId, repliesPage), object : Observer<Response<BaseResponse<Paginate<MutableList<MessageReply>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "loadReplies -> onComplete")
                }

                override fun onSubscribe(d: Disposable) {
                    Log.d(TAG, "loadReplies -> onSubscribe")
                    DisposableManager.add(d)
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<MessageReply>>>>) {
                    Log.d(TAG, "loadReplies -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "loadReplies -> onNext -> body = ${t.body()}")
                    Log.d(TAG, "loadReplies -> onNext -> raw = ${t.raw()}")

                    view.hideRepliesProgressBar()
                    view.changeLoaderVisibility(View.GONE)

                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        view.updateReplies(t.body()!!.data!!.data!!)
                        Log.d(TAG, "loadReplies -> onNext -> replies count = ${t.body()!!.data!!.data!!.size}")
                        repliesPage++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMoreReplies = false
                    } else
                        if (repliesPage == 1)
                            view.showError(R.string.err_failed_load_replies)
                        else view.showError(R.string.err_failed_load_more_replies)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "loadReplies -> onError -> error = ${e.message}")
                    view.hideRepliesProgressBar()
                    view.changeLoaderVisibility(View.GONE)

                    if (repliesPage == 1)
                        view.showError(R.string.err_failed_load_replies)
                    else view.showError(R.string.err_failed_load_more_replies)
                }
            })
        }
    }

    override fun sendReply(receiverId: Long, content: String) {
        subscribe(dataManager.sendReply(receiverId, content), object : Observer<Response<BaseResponse<SendReplyResponse>>> {
            override fun onComplete() {
                Log.d(TAG, "sendReply -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "sendReply -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<SendReplyResponse>>) {
                Log.d(TAG, "sendReply -> onNext -> code = ${t.code()}")
                Log.d(TAG, "sendReply -> onNext -> body = ${t.body()}")
                if (t.body() != null)
                    Log.d(TAG, "sendReply -> onNext -> status = ${t.body()!!.status}")
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "sendReply -> onError -> error = ${e.message}")
            }
        })
    }

    override fun getMedicalFileShares(receiverId: Long) {
        Log.d(TAG, "getMedicalFileShares -> receiverId = $receiverId")
        view.changeShareButtonProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.getIsMedicalFilesShared(receiverId), object : Observer<Response<BaseResponse<MutableList<Share>>>> {
            override fun onComplete() {
                Log.d(TAG, "getMedicalFileShares -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "getMedicalFileShares -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<MutableList<Share>>>) {
                Log.d(TAG, "getMedicalFileShares -> onNext -> code = ${t.code()}")
                Log.d(TAG, "getMedicalFileShares -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null) {
                    Log.d(TAG, "getMedicalFileShares -> onNext -> status = ${t.body()!!.status}")
                    Log.d(TAG, "getMedicalFileShares -> onNext -> data = ${t.body()!!.data}")
                    view.updateMedicalFileShares(t.body()!!.data)
                }
                view.changeShareButtonProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                view.changeShareButtonProgressBarVisibility(View.GONE)
                Log.d(TAG, "getMedicalFileShares -> onError -> error = ${e.message}")
            }
        })
    }

    override fun updateShareMedicalFilesStatus(receiverId: Long, status: Int) {
        subscribe(dataManager.updateShareMedicalFilesStatus(receiverId, status), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "updateShareMedicalFilesStatus -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "updateShareMedicalFilesStatus -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "updateShareMedicalFilesStatus -> onNext -> code = ${t.code()}")
                Log.d(TAG, "updateShareMedicalFilesStatus -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null) {
                    Log.d(TAG, "updateShareMedicalFilesStatus -> onNext -> status = ${t.body()!!.status}")
                    Log.d(TAG, "updateShareMedicalFilesStatus -> onNext -> message = ${t.body()!!.message}")
                }
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "updateShareMedicalFilesStatus -> onError -> error = ${e.message}")
            }
        })
    }

    override fun getUserId(): Long =
            dataManager.getUserId()

    override fun getUserRole(): Int =
            dataManager.getUserRole()

    override fun getUserName(): String =
            dataManager.getUserName()

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.dispose()
    }
}