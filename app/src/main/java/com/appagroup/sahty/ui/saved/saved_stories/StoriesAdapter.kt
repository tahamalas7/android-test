package com.appagroup.sahty.ui.saved.saved_stories

import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.entity.Story
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_loader.view.*
import kotlinx.android.synthetic.main.item_story.view.*

class StoriesAdapter(val typeface: Typeface) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var itemHeight: Int = 400
    private val TYPE_LOADER = 0
    private val TYPE_STORY = 1
    private var stories: MutableList<Story> = mutableListOf()
    private lateinit var bottomReachListener: BottomReachListener
    private lateinit var storyClickListener: ItemClickListener<Story>
    private lateinit var followButtonClickListener: FollowButtonClickListener
    private lateinit var storiesLoader: ProgressBar

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_LOADER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_loader, parent, false)
                LoaderHolder(view)
            }
            TYPE_STORY -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_story, parent, false)
                StoryHolder(view)
            }
            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1)
            TYPE_LOADER
        else TYPE_STORY
    }

    override fun getItemCount(): Int = stories.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoaderHolder -> holder.changeLoaderVisibility()
            is StoryHolder -> {
                val params = holder.itemView.layoutParams
                params.height = itemHeight
                holder.itemView.layoutParams = params

                val story = stories[position]
                holder.bind(story)

                holder.itemView.setOnClickListener {
                    storyClickListener.onClick(story)
                }

                holder.itemView.story_follow_btn.setOnClickListener {
                    if (story.writer.isFollowed) {
                        holder.itemView.story_follow_btn.setImageResource(R.drawable.ic_follow_user)
                        followButtonClickListener.unfollowUser(Follow(
                                story.writer.id,
                                story.writer.name,
                                story.writer.image,
                                true))
                        stories[position].writer.isFollowed = false
                    } else {
                        holder.itemView.story_follow_btn.setImageResource(R.drawable.ic_user_followed)
                        followButtonClickListener.followUser(Follow(
                                story.writer.id,
                                story.writer.name,
                                story.writer.image,
                                true))
                        stories[position].writer.isFollowed = true
                    }
                }
            }
        }

        if (position == itemCount - 5)
            bottomReachListener.onBottomReach()
    }

    fun updateStories(stories: MutableList<Story>) {
        this.stories.addAll(stories)
        notifyDataSetChanged()
    }

    fun removeItem(storyId: Long) {
        for (position in 0 until stories.size)
            if (stories[position].id == storyId) {
                stories.removeAt(position)
                notifyItemRemoved(position)
                break
            }
    }

    fun updateItemHeight(itemHeight: Int) {
        this.itemHeight = itemHeight
    }

    fun setOnBottomReachListener(bottomReachListener: BottomReachListener) {
        this.bottomReachListener = bottomReachListener
    }

    fun setOnStoryClickListener(storyClickListener: ItemClickListener<Story>) {
        this.storyClickListener = storyClickListener
    }

    fun setOnFollowBtnClickListener(followButtonClickListener: FollowButtonClickListener) {
        this.followButtonClickListener = followButtonClickListener
    }

    private fun getHighlight(string: String): String {
        if (string.length > 100) {
            for (i in 100 downTo 1) {
                if (string[i] == ' ') {
                    return string.substring(0, i).trim() + "..."
                }
            }
        }
        return string
    }

    fun changeLoaderVisibility(visibility: Int) {
        if (::storiesLoader.isInitialized)
            storiesLoader.visibility = visibility
    }

    inner class LoaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun changeLoaderVisibility() {
            storiesLoader = itemView.recyclerView_loader
        }
    }

    inner class StoryHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(story: Story) {
            itemView.story_title.typeface = typeface
            itemView.story_content.typeface = typeface
            itemView.story_continue_reading.typeface = typeface
            itemView.story_username.typeface = typeface

            itemView.story_title.text = story.title
            itemView.story_content.text = "${story.content}..."
            itemView.story_username.text = "${itemView.context.getString(R.string.by)} ${story.writer.name}"

            if (story.writer.isFollowed)
                itemView.story_follow_btn.setImageResource(R.drawable.ic_user_followed)
            else
                itemView.story_follow_btn.setImageResource(R.drawable.ic_follow_user)

            if (story.media != null)
                Glide.with(itemView)
                        .load(BuildConfig.IMAGE +
                                if (story.media.type.toInt() == 2)
                                    story.media.thumbnail
                                else
                                    story.media.url)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?,
                                                      model: Any?,
                                                      target: Target<Drawable>?,
                                                      isFirstResource: Boolean): Boolean {
                                itemView.story_image_progressBar.visibility = View.GONE
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?,
                                                         model: Any?,
                                                         target: Target<Drawable>?,
                                                         dataSource: DataSource?,
                                                         isFirstResource: Boolean): Boolean {
                                itemView.story_image_progressBar.visibility = View.GONE
                                return false
                            }
                        })
                        .into(itemView.story_image)
            else
                itemView.story_image_progressBar.visibility = View.GONE
        }
    }
}