package com.appagroup.sahty.ui.facilities.pharmacies.pharmacy_details

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.PharmacyDetails
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response
import com.appagroup.sahty.ui.facilities.pharmacies.pharmacy_details.PharmacyDetailsContract as Contract

class PharmacyDetailsPresenter(view: Contract.View) : BasePresenter<Contract.View>(view), Contract.Presenter {

    private val TAG = "PharmacyDetailPresenter"

    override fun getPharmacyDetails(pharmacyId: Long) {
        view.changeProgressBarVisibility(View.VISIBLE)
        view.changeContentVisibility(View.GONE)
        subscribe(dataManager.getPharmacyDetails(pharmacyId), object : Observer<Response<BaseResponse<PharmacyDetails>>> {
            override fun onComplete() {
                Log.d(TAG, "getPharmacyDetails -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                DisposableManager.add(d)
                Log.d(TAG, "getPharmacyDetails -> onSubscribe")
            }

            override fun onNext(t: Response<BaseResponse<PharmacyDetails>>) {
                Log.d(TAG, "getPharmacyDetails -> onNext -> code = ${t.code()}")
                Log.d(TAG, "getPharmacyDetails -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "getPharmacyDetails -> onNext -> data = ${t.body()!!.data}")
                    view.setPharmacyDetails(t.body()!!.data!!)
                    view.changeContentVisibility(View.VISIBLE)
                } else
                    view.showError(R.string.something_wrong)

                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "getPharmacyDetails -> onError -> error = ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
                view.showError(R.string.something_wrong)
            }
        })
    }

    override fun sendFeedback(hospitalId: Long, text: String, rate: Float) {

    }
}