package com.appagroup.sahty.ui.articles.article_content

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.ArticleContent
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class ArticleContentPresenter(view: ArticleContentContract.View)
    : BasePresenter<ArticleContentContract.View>(view), ArticleContentContract.Presenter {

    private val TAG = "ArticleContentPresenter"
    private val observer = object : Observer<Response<BaseResponse<Any>>> {
        override fun onComplete() {
            Log.d(TAG, "Save/Unsave -> onComplete")
        }

        override fun onSubscribe(d: Disposable) {
            Log.d(TAG, "Save/Unsave -> onSubscribe")
            DisposableManager.add(d)
        }

        override fun onNext(t: Response<BaseResponse<Any>>) {
            Log.d(TAG, "Save/Unsave -> onNext -> code = ${t.code()}")
            Log.d(TAG, "Save/Unsave -> onNext -> body = ${t.body()}")
        }

        override fun onError(e: Throwable) {
            Log.d(TAG, "Save/Unsave -> onError -> body = ${e.message}")
        }
    }

    override fun getArticleContent(articleId: Long) {
        view.changeProgressBarVisibility(View.VISIBLE)
        view.changeContentVisibility(View.GONE)
        subscribe(dataManager.getArticleContent(articleId), object : Observer<Response<BaseResponse<ArticleContent>>> {
            override fun onComplete() {
                Log.d(TAG, "getArticleContent -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "getArticleContent -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<ArticleContent>>) {
                Log.d(TAG, "getArticleContent -> onNext -> code = ${t.code()}")
                Log.d(TAG, "getArticleContent -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    view.setArticleContent(t.body()!!.data!!)
                    view.changeContentVisibility(View.VISIBLE)
                } else
                    view.showError(R.string.something_wrong)

                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "getArticleContent -> onError -> body = ${e.message}")
                view.showError(R.string.something_wrong)
                view.changeProgressBarVisibility(View.GONE)
            }
        })
    }

    override fun likeArticle(articleId: Long) {
        subscribe(dataManager.likeArticle(articleId), observer)
    }

    override fun unlikeArticle(articleId: Long) {
        subscribe(dataManager.unlikeArticle(articleId), observer)
    }

    override fun getUserId(): Long =
            dataManager.getUserId()

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.dispose()
    }
}