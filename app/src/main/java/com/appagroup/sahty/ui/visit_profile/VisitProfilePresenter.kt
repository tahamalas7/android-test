package com.appagroup.sahty.ui.visit_profile

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.VisitProfile
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response
import com.appagroup.sahty.ui.visit_profile.VisitProfileContract as Contract


class VisitProfilePresenter(view: Contract.View) : BasePresenter<Contract.View>(view), Contract.Presenter {

    private val TAG = "VisitProfilePresenter"

    override fun loadProfile(userId: Long) {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.visitProfile(userId), object : Observer<Response<BaseResponse<VisitProfile>>> {
            override fun onComplete() {
                Log.d(TAG, "loadProfile -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "loadProfile -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<VisitProfile>>) {
                Log.d(TAG, "loadProfile -> onNext -> code = ${t.code()}")
                Log.d(TAG, "loadProfile -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "loadProfile -> onNext -> status = ${t.body()!!.status}")
                    view.setProfile(t.body()!!.data!!)
                } else
                    view.showError(R.string.err_load_profile)
                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "loadProfile -> onNext -> onError = ${e.message}")
            }
        })
    }

    override fun getUserId(): Long =
            dataManager.getUserId()

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.dispose()
    }
}