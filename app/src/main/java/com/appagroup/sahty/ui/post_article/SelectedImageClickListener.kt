package com.appagroup.sahty.ui.post_article

interface SelectedImageClickListener {

    fun onSelectedImageClick(position: Int, images: MutableList<String>)
}