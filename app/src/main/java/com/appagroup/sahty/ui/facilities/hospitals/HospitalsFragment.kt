package com.appagroup.sahty.ui.facilities.hospitals

import android.view.View
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.entity.Hospital
import com.appagroup.sahty.ui.facilities.hospitals.hospital_details.HospitalDetailsFragment
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_facilities_hospitals.*

class HospitalsFragment : BaseFragment<HospitalsContract.Presenter>(), HospitalsContract.View{

    private val adapter = HospitalAdapter()

    override fun instantiatePresenter(): HospitalsContract.Presenter = HospitalsPresenter(this)

    override fun getLayout(): Int = R.layout.item_facilities_hospitals

    override fun initViews(view: View) {
        hospitals_recyclerView.layoutManager = LinearLayoutManager(activity)
        hospitals_recyclerView.adapter = adapter
        hospitals_recyclerView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                adapter.updateItemHeight(hospitals_recyclerView.height/2)
            }
        })

        presenter.loadHospitals()
        setListeners()
    }

    override fun setListeners() {
        adapter.setOnHospitalClickListener(object : ItemClickListener<Hospital> {
            override fun onClick(item: Hospital) {
                val hospitalDetailsFragment = HospitalDetailsFragment.newInstance(item.id)
                hospitalDetailsFragment.show(activity!!.supportFragmentManager, HospitalDetailsFragment.TAG)
            }
        })

        adapter.setOnBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                presenter.loadHospitals()
            }
        })
    }

    override fun updateHospitals(hospitals: MutableList<Hospital>) {
        adapter.updateHospitals(hospitals)
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        hospitals_progressBar.visibility = visibility
    }

    companion object {
        val TAG = "HospitalsFragment"

        fun getInstance(): HospitalsFragment = HospitalsFragment()
    }
}