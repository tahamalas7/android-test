package com.appagroup.sahty.ui.profile

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import kotlinx.android.synthetic.main.item_follow.view.*

class FollowersAdapter(private val typeface: Typeface) : RecyclerView.Adapter<FollowersAdapter.FollowersHolder>() {

    private lateinit var followButtonClickListener: FollowButtonClickListener
    private val followingList: MutableList<Follow> = mutableListOf()
    private val followersList: MutableList<Follow> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): FollowersHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_follow, parent, false)
        return FollowersHolder(view)
    }

    override fun getItemCount(): Int =
            followersList.size

    override fun onBindViewHolder(holder: FollowersHolder, position: Int) {
        holder.bind(followersList[position])
        val follower = followersList[position]

        holder.itemView.follow_btn.setOnClickListener {
            if (follower.isFollowed) {
                followButtonClickListener.unfollowUser(follower)
//                holder.setAsNotFollowed(follower)
                removeFollowingUser(follower.id)
            } else {
                followButtonClickListener.followUser(follower)
//                holder.setAsFollowed(follower)
                addFollowingUser(follower)
            }
        }
    }

    fun removeFollowingUser(followingUserId: Long) {
        for (position in 0 until followingList.size)
            if (followingList[position].id == followingUserId) {
                followingList.removeAt(position)
                break
            }

        for (position in 0 until followersList.size)
            if (followersList[position].id == followingUserId) {
                notifyItemChanged(position)
                followersList[position].isFollowed = false
                break
            }
    }

    fun addFollowingUser(Follower: Follow) {
        followingList.add(0, Follower)
        for (position in 0 until followersList.size)
            if (followersList[position].id == Follower.id) {
                notifyItemChanged(position)
                break
            }
    }

    fun updateFollows(followingList: MutableList<Follow>, followersList: MutableList<Follow>) {
        this.followingList.addAll(followingList)
        this.followersList.addAll(followersList)
        notifyDataSetChanged()
    }

    fun setOnFollowButtonClickListener(followButtonClickListener: FollowButtonClickListener) {
        this.followButtonClickListener = followButtonClickListener
    }

    inner class FollowersHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(follower: Follow) {
            itemView.follow_username.text = follower.name
            itemView.follow_username.typeface = typeface

            if (follower.image != null)
                Glide.with(itemView.context)
                        .load(BuildConfig.IMAGE + follower.image)
                        .into(itemView.follow_user_image)

            var followStatus = false
            // need to check if this follower is followed
            for (index in 0 until followingList.size)
                if (follower.id == followingList[index].id) {
                    setAsFollowed(follower)
                    followStatus = true
                    break
                }

            if (!followStatus)
                setAsNotFollowed(follower)
        }

        fun setAsFollowed(follower: Follow) {
            itemView.follow_btn.setBackgroundResource(R.drawable.background_follow_button_on)
            itemView.follow_btn.text = itemView.context.getString(R.string.unfollow)
            follower.isFollowed = true
        }

        fun setAsNotFollowed(follower: Follow) {
            itemView.follow_btn.setBackgroundResource(R.drawable.background_follow_button_off)
            itemView.follow_btn.text = itemView.context.getString(R.string.follow)
            follower.isFollowed = false
        }
    }
}