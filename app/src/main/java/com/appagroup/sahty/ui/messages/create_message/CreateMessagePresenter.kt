package com.appagroup.sahty.ui.messages.create_message

import android.util.Log
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.SendReplyResponse
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class CreateMessagePresenter(view: CreateMessageContract.View) : BasePresenter<CreateMessageContract.View>(view), CreateMessageContract.Presenter {

    private val TAG = "CreateMessagePresenter"

    override fun sendMessage(receiverId: Long, content: String) {
        Log.d(TAG, "sendMessage -> receiverId = $receiverId")
        Log.d(TAG, "sendMessage -> content = $content")
        view.showProgressDialog()
        subscribe(dataManager.sendReply(receiverId, content), object : Observer<Response<BaseResponse<SendReplyResponse>>> {
            override fun onComplete() {
                Log.d(TAG, "sendMessage -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "sendMessage -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<SendReplyResponse>>) {
                Log.d(TAG, "sendMessage -> onNext -> code = ${t.code()}")
                Log.d(TAG, "sendMessage -> onNext -> body = ${t.body()}")
//                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
//                    Log.d(TAG, "sendMessage -> onNext -> status = ${t.body()!!.status}")
//                    view.messageSentSuccessfully()
                if (t.code() == 200 && t.body() != null) {
                    Log.d(TAG, "sendMessage -> onNext -> status = ${t.body()!!.status}")
                    Log.d(TAG, "sendMessage -> onNext -> message = ${t.body()!!.message}")
                    if (t.body()!!.status)
                        view.messageSentSuccessfully(t.body()!!.data!!)
                } else
                    view.showError(R.string.something_wrong)
                view.hideProgressDialog()
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "sendMessage -> onError -> error = ${e.message}")
                view.hideProgressDialog()
            }
        })
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.dispose()
    }
}