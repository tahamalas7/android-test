package com.appagroup.sahty.ui.facilities.hospitals.hospital_details

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.HospitalDetails
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class HospitalDetailsPresenter(view: HospitalDetailsContract.View) : BasePresenter<HospitalDetailsContract.View>(view), HospitalDetailsContract.Presenter {

    private val TAG = "HospitalDetailPresenter"

    override fun getHospitalDetails(hospitalId: Long) {
        view.changeProgressBarVisibility(View.VISIBLE)
        view.changeContentVisibility(View.GONE)
        subscribe(dataManager.getHospitalDetails(hospitalId), object : Observer<Response<BaseResponse<HospitalDetails>>> {
            override fun onComplete() {}

            override fun onSubscribe(d: Disposable) {
                DisposableManager.add(d)
                Log.d(TAG, "getHospitalDetails -> onSubscribe")
            }

            override fun onNext(t: Response<BaseResponse<HospitalDetails>>) {
                Log.d(TAG, "getHospitalDetails -> onNext -> code = ${t.code()}")
                Log.d(TAG, "getHospitalDetails -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    view.setHospitalDetails(t.body()!!.data!!)
                    view.changeContentVisibility(View.VISIBLE)
                } else
                    view.showError(R.string.something_wrong)

                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "getHospitalDetails -> onError -> error = ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
                view.showError(R.string.something_wrong)
            }
        })
    }

    override fun sendFeedback(hospitalId: Long, text: String, rate: Float) {

    }
}