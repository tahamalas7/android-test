package com.appagroup.sahty.ui.listener

import com.appagroup.sahty.entity.Follow

interface FollowButtonClickListener {

    fun followUser(follow: Follow)

    fun unfollowUser(follow: Follow)
}