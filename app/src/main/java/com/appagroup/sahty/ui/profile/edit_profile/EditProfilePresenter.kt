package com.appagroup.sahty.ui.profile.edit_profile

import android.util.Log
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import okhttp3.MultipartBody
import retrofit2.Response

class EditProfilePresenter(view: EditProfileContract.View) : BasePresenter<EditProfileContract.View>(view), EditProfileContract.Presenter {

    private val TAG = "EditProfilePresenter"

    override fun changePicture(image: MultipartBody.Part) {
        view.showProgressDialog(R.string.message_change_picturs)
        subscribe(dataManager.changePicture(image), object : Observer<Response<BaseResponse<String>>> {
            override fun onComplete() {
                Log.d(TAG, "changePicture -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "changePicture -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<String>>) {
                Log.d(TAG, "changePicture -> onNext -> code = ${t.code()}")
                Log.d(TAG, "changePicture -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "changePicture -> onNext -> status = ${t.body()!!.status}")
                    view.pictureChangedSuccessfully(t.body()!!.data!!)
                } else
                    view.showError(R.string.err_change_picture)

                view.hideProgressDialog()
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "changePicture -> onError -> error = ${e.message}")
                view.showError(R.string.err_change_picture)
                view.hideProgressDialog()
            }
        })
    }

    override fun changePassword(oldPassword: String, newPassword: String) {
        view.showProgressDialog(R.string.message_change_password)
        subscribe(dataManager.changePassword(oldPassword, newPassword), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "changePassword -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "changePassword -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "changePassword -> onNext -> code = ${t.code()}")
                Log.d(TAG, "changePassword -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "changePassword -> onNext -> status = ${t.body()!!.status}")
                    view.passwordChangedSuccessfully()
                } else if (t.code() == 400)
                    view.setInvalidOldPasswordError()
                else
                    view.showError(R.string.err_change_password)

                view.hideProgressDialog()
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "changePassword -> onError -> error = ${e.message}")
                view.showError(R.string.err_change_password)
                view.hideProgressDialog()
            }
        })
    }

    override fun changeEmail(password: String, newEmail: String) {
        view.showProgressDialog(R.string.message_change_email)
        subscribe(dataManager.changeEmail(newEmail, password), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "changeEmail -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "changeEmail -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "changeEmail -> onNext -> code = ${t.code()}")
                Log.d(TAG, "changeEmail -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "changeEmail -> onNext -> status = ${t.body()!!.status}")
                    view.emailChangedSuccessfully()
                } else if (t.code() == 400)
                    view.setInvalidPasswordError()
                else if (t.code() == 401)
                    view.setInvalidNewEmaildError()
                else
                    view.showError(R.string.err_change_email)

                view.hideProgressDialog()
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "changeEmail -> onError -> error = ${e.message}")
                view.showError(R.string.err_change_email)
                view.hideProgressDialog()
            }
        })
    }

    override fun changeAvailability(availability: Int) {
        subscribe(dataManager.changeAvailability(), object : Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "changeAvailability -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "changeAvailability -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "changeAvailability -> onNext -> code = ${t.code()}")
                Log.d(TAG, "changeAvailability -> onNext -> body = ${t.body()}")

                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "changeAvailability -> onNext -> status = ${t.body()!!.status}")
                    dataManager.setDoctorAvailability(availability)
                }
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "changeAvailability -> onError -> error = ${e.message}")
            }
        })
    }

    override fun getUserName(): String =
            dataManager.getUserName()

    override fun getUserImage(): String =
            dataManager.getUserImage()

    override fun getUserRole(): Int =
            dataManager.getUserRole()

    override fun getDoctorAvailability(): Int =
            dataManager.getDoctorAvailability()

    override fun getLoginProvider(): String =
            dataManager.getLoginProvider()

    override fun dispose() =
            DisposableManager.dispose()

    override fun saveUserImage(image: String) {
        dataManager.setUserImage(image)
    }
}