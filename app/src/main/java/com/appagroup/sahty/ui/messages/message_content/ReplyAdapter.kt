package com.appagroup.sahty.ui.messages.message_content

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.MessageReply
import com.appagroup.sahty.ui.listener.BottomReachListener
import kotlinx.android.synthetic.main.item_loader.view.*
import kotlinx.android.synthetic.main.item_received_reply.view.*
import kotlinx.android.synthetic.main.item_sent_reply.view.*

class ReplyAdapter(val userId: Long) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = "ReplyAdapter"
    private val TYPE_LOADER = 0
    private val TYPE_SENT_REPLY = 1
    private val TYPE_RECEIVED_REPLY = 2
    private var loaderVisibility: Int = View.VISIBLE
    private val replies: MutableList<MessageReply> = mutableListOf()
    private lateinit var repliesBottomReachListener: BottomReachListener
    private lateinit var repliesLoader: ProgressBar

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return when(viewType) {
            TYPE_RECEIVED_REPLY -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_received_reply,parent,false)
                ReceivedReplyHolder(view)
            }
            TYPE_SENT_REPLY -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_sent_reply,parent,false)
                SentReplyHolder(view)
            }
            TYPE_LOADER -> {
                view = LayoutInflater.from(parent.context).inflate(R.layout.item_loader,parent,false)
                LoaderHolder(view)
            }
            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position == itemCount - 1 -> TYPE_LOADER
            replies[position].fromId == userId -> TYPE_SENT_REPLY
            else -> TYPE_RECEIVED_REPLY
        }
    }

    override fun getItemCount(): Int = replies.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoaderHolder -> holder.updateProgressBarVisibility()
            is SentReplyHolder -> holder.bind(replies[position])
            is ReceivedReplyHolder -> holder.bind(replies[position])
        }
        if (position == itemCount - 5)
            repliesBottomReachListener.onBottomReach()
    }

    fun updateReplies(replies: MutableList<MessageReply>) {
        this.replies.addAll(replies)
        notifyDataSetChanged()
    }

    fun updateReplies(reply: MessageReply) {
        this.replies.add(0, reply)
        notifyItemInserted(0) // we add at position 1 because 0 is booked up for loader
    }

    fun changeLoaderVisibility(visibility: Int) {
        if (::repliesLoader.isInitialized)
            repliesLoader.visibility = visibility
//        loaderVisibility = visibility
    }

    fun setOnRepliesBottomReachListener(repliesBottomReachListener: BottomReachListener) {
        this.repliesBottomReachListener = repliesBottomReachListener
    }

    inner class LoaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun updateProgressBarVisibility() {
            repliesLoader = itemView.recyclerView_loader
//            itemView.visibility = loaderVisibility
        }
    }

    inner class SentReplyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(reply: MessageReply) {
            itemView.sent_message_content.text = reply.content
            itemView.sent_message_date.text = reply.date
        }
    }

    inner class ReceivedReplyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(reply: MessageReply) {
            itemView.received_message_content.text = reply.content
            itemView.received_message_date.text = reply.date
        }
    }
}