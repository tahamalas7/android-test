package com.appagroup.sahty.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.appagroup.sahty.ui.facilities.FacilitiesFragment
import com.appagroup.sahty.ui.home.HomeFragment
import com.appagroup.sahty.ui.medical_files.MyMedicalFilesFragment
import com.appagroup.sahty.ui.videos.VideosFragment

class MainPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    private lateinit var myMedicalFilesFragment: MyMedicalFilesFragment
    private lateinit var facilitiesFragment: FacilitiesFragment
    private lateinit var videosFragment: VideosFragment
    private lateinit var homeFragment: HomeFragment


    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                if (!::facilitiesFragment.isInitialized)
                    facilitiesFragment = FacilitiesFragment.getInstance()
                facilitiesFragment
            }

            1 -> {
                if (!::videosFragment.isInitialized)
                    videosFragment = VideosFragment.getInstance()
                videosFragment
            }

            2 -> {
                if (!::homeFragment.isInitialized)
                    homeFragment = HomeFragment.getInstance()
                homeFragment
            }

            3 -> {
                if (!::myMedicalFilesFragment.isInitialized)
                    myMedicalFilesFragment = MyMedicalFilesFragment.getInstance()
                myMedicalFilesFragment
            }
            else -> throw RuntimeException("main viewPager is Out of range")
        }
    }

    override fun getCount(): Int = 4

    companion object {
        val FACILITIES_FRAGMENT = 0
        val VIDEOS_FRAGMENT = 1
        val HOME_FRAGMENT = 2
        val MEDICAL_FILES_FRAGMENT = 3
    }
}