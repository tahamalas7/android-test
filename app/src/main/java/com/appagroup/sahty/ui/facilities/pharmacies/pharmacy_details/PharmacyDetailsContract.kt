package com.appagroup.sahty.ui.facilities.pharmacies.pharmacy_details

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.PharmacyDetails

interface PharmacyDetailsContract {

    interface View : IBaseView {

        fun setListeners()

        fun changeProgressBarVisibility(visibility: Int)

        fun changeContentVisibility(visibility: Int)

        fun setPharmacyDetails(pharmacyDetails: PharmacyDetails)
    }

    interface Presenter : IBasePresenter<View> {

        fun getPharmacyDetails(pharmacyId: Long)

        fun sendFeedback(hospitalId: Long, text: String, rate: Float)
    }
}