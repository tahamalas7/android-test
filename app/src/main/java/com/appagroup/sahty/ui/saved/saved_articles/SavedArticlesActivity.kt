package com.appagroup.sahty.ui.saved.saved_articles

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.util.Log
import android.view.View
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.Article
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.ui.FollowService
import com.appagroup.sahty.ui.SaveService
import com.appagroup.sahty.ui.articles.ArticlesAdapter
import com.appagroup.sahty.ui.articles.article_content.ArticleContentActivity
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.activity_saved_articles.*
import com.appagroup.sahty.ui.saved.saved_articles.SavedArticlesContract as Contract

class SavedArticlesActivity : BaseActivity<Contract.Presenter>(), Contract.View {

    private lateinit var adapter: ArticlesAdapter
    private val followService = FollowService()
    private val saveService = SaveService()
    private val ARTICLE_CONTENT_REQUEST_CODE = 67

    override fun instantiatePresenter(): SavedArticlesPresenter = SavedArticlesPresenter(this)

    override fun getContentView(): Int = R.layout.activity_saved_articles

    override fun initViews() {
        val typeface = Typeface.createFromAsset(assets, "GOTHIC.TTF")
        adapter = ArticlesAdapter(typeface, presenter.getUserId())

        saved_articles_recyclerView.layoutManager = LinearLayoutManager(this)
        saved_articles_recyclerView.adapter = adapter
        saved_articles_recyclerView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                saved_articles_recyclerView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                adapter.updateItemHeight(saved_articles_recyclerView.height/2)
            }
        })

        changeProgressBarVisibility(View.VISIBLE)
        presenter.getSavedArticles()
        setListeners()
    }

    override fun setListeners() {
        saved_articles_back_arrow.setOnClickListener {
            onBackPressed()
        }

        adapter.setOnBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                presenter.getSavedArticles()
            }
        })

        adapter.setOnFollowBtnClickListener(object : FollowButtonClickListener {
            override fun followUser(follow: Follow) {
                Log.d(TAG, "followUser -> user id = ${follow.id} ")
                followService.followUser(follow.id)
            }

            override fun unfollowUser(follow: Follow) {
                Log.d(TAG, "unfollowUser -> user id = ${follow.id} ")
                followService.unfollowUser(follow.id)
            }
        })

        adapter.setOnArticleClickListener(object : ItemClickListener<Article> {
            override fun onClick(item: Article) {
                val intent = Intent(this@SavedArticlesActivity, ArticleContentActivity::class.java)
                intent.putExtra(ArticleContentActivity.ARTICLE_ID, item.id)
                startActivity(intent)
            }
        })
    }

    override fun updateArticles(articles: MutableList<Article>) {
        adapter.updateArticles(articles)
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        saved_articles_progressBar.visibility = visibility
    }

    override fun changeLoaderVisibility(visibility: Int) {
        adapter.changeLoaderVisibility(visibility)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == ARTICLE_CONTENT_REQUEST_CODE && data != null) {
            if (intent.getBooleanExtra(ArticleContentActivity.IS_ARTICLE_UNSAVED, false))
                adapter.removeItem(intent.getLongExtra(ArticleContentActivity.ARTICLE_ID, 0L))
        }
    }

    override fun onPause() {
        super.onPause()
        followService.dispose()
        saveService.dispose()
    }

    companion object {
        val TAG = "SavedArticlesActivity"
    }
}