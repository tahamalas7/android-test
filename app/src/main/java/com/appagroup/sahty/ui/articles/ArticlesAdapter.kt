package com.appagroup.sahty.ui.articles

import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Article
import com.appagroup.sahty.entity.Follow
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_article.view.*
import kotlinx.android.synthetic.main.item_loader.view.*

class ArticlesAdapter(private val typeface: Typeface, private val userId: Long) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var itemHeight: Int = 400
    private val TYPE_LOADER = 0
    private val TYPE_ARTICLE = 1
    private var articles: MutableList<Article> = mutableListOf()
    private lateinit var bottomReachListener: BottomReachListener
    private lateinit var articleClickListener: ItemClickListener<Article>
    private lateinit var followButtonClickListener: FollowButtonClickListener
    private lateinit var articlesLoader: ProgressBar

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_LOADER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_loader, parent, false)
                LoaderHolder(view)
            }
            TYPE_ARTICLE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_article, parent, false)
                ArticleHolder(view)
            }
            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1)
            TYPE_LOADER
        else TYPE_ARTICLE
    }

    override fun getItemCount(): Int = articles.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoaderHolder -> holder.changeLoaderVisibility()
            is ArticleHolder -> {
                val params = holder.itemView.layoutParams
                params.height = itemHeight
                holder.itemView.layoutParams = params

                val article = articles[position]
                holder.bind(article)

                holder.itemView.setOnClickListener {
                    articleClickListener.onClick(article)
                }

                holder.itemView.article_follow_btn.setOnClickListener {
                    if (article.writer.isFollowed) {
//                        holder.itemView.article_follow_btn.setImageResource(R.drawable.ic_follow_user)
                        unfollowUser(article.writer.id)
                        followButtonClickListener.unfollowUser(Follow(
                                article.writer.id,
                                article.writer.name,
                                article.writer.image,
                                false))
                        article.writer.isFollowed = false
                    } else {
//                        holder.itemView.article_follow_btn.setImageResource(R.drawable.ic_user_followed)
                        followUser(article.writer.id)
                        followButtonClickListener.followUser(Follow(
                                article.writer.id,
                                article.writer.name,
                                article.writer.image,
                                true))
                        article.writer.isFollowed = true
                    }
                }
            }
        }

        if (position == itemCount - 5)
            bottomReachListener.onBottomReach()
    }

    fun removeItem(articleId: Long) {
        for (position in 0 until articles.size)
            if (articles[position].id == articleId) {
                articles.removeAt(position)
                notifyItemRemoved(position)
                break
            }
    }

    fun updateArticles(articles: MutableList<Article>) {
        this.articles.addAll(articles)
        notifyDataSetChanged()
    }

    fun updateItemHeight(itemHeight: Int) {
        this.itemHeight = itemHeight
    }

    fun clearArticles() {
        this.articles.clear()
        notifyDataSetChanged()
    }

    fun setOnBottomReachListener(bottomReachListener: BottomReachListener) {
        this.bottomReachListener = bottomReachListener
    }

    fun setOnArticleClickListener(articleClickListener: ItemClickListener<Article>) {
        this.articleClickListener = articleClickListener
    }

    fun setOnFollowBtnClickListener(followButtonClickListener: FollowButtonClickListener) {
        this.followButtonClickListener = followButtonClickListener
    }

    private fun unfollowUser(userId: Long) {
        for (position in 0 until articles.size)
            if (articles[position].writer.id == userId)
                articles[position].writer.isFollowed = false

        notifyDataSetChanged()
    }

    private fun followUser(userId: Long) {
        for (position in 0 until articles.size)
            if (articles[position].writer.id == userId)
                articles[position].writer.isFollowed = true

        notifyDataSetChanged()
    }

    private fun getHighlight(string: String): String {
        if (string.length > 100) {
            for (i in 100 downTo 1) {
                if (string[i] == ' ') {
                    return string.substring(0, i).trim() + "..."
                }
            }
        }
        return string
    }

    fun changeLoaderVisibility(visibility: Int) {
        if (::articlesLoader.isInitialized)
            articlesLoader.visibility = visibility
    }

    inner class LoaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun changeLoaderVisibility() {
            articlesLoader = itemView.recyclerView_loader
        }
    }

    inner class ArticleHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(article: Article) {
            itemView.article_title.typeface = typeface
            itemView.article_content.typeface = typeface
            itemView.article_continue_reading.typeface = typeface
            itemView.article_username.typeface = typeface

            itemView.article_title.text = article.title
            itemView.article_content.text = "${getHighlight(article.content)}..."
            itemView.article_username.text = "${itemView.context.getString(R.string.by)} ${article.writer.name}"

            when {
                userId == article.writer.id ->
                    itemView.article_follow_btn.visibility = View.GONE
                article.writer.isFollowed ->
                    itemView.article_follow_btn.setImageResource(R.drawable.ic_user_followed)
                else ->
                    itemView.article_follow_btn.setImageResource(R.drawable.ic_follow_user)
            }

            if (article.media != null)
                Glide.with(itemView)
                        .load(BuildConfig.IMAGE +
                                if (article.media.type.toInt() == 2)
                                    article.media.thumbnail
                                else
                                    article.media.url)
                        .listener(object : RequestListener<Drawable> {
                            override fun onLoadFailed(e: GlideException?,
                                                      model: Any?,
                                                      target: Target<Drawable>?,
                                                      isFirstResource: Boolean): Boolean {
                                itemView.article_image_progressBar.visibility = View.GONE
                                return false
                            }

                            override fun onResourceReady(resource: Drawable?,
                                                         model: Any?,
                                                         target: Target<Drawable>?,
                                                         dataSource: DataSource?,
                                                         isFirstResource: Boolean): Boolean {
                                itemView.article_image_progressBar.visibility = View.GONE
                                return false
                            }
                        })
                        .into(itemView.article_image)
            else
                itemView.article_image_progressBar.visibility = View.GONE
        }
    }
}