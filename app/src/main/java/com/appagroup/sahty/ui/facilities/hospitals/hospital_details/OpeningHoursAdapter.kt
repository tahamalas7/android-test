package com.appagroup.sahty.ui.facilities.hospitals.hospital_details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.OpeningHours
import kotlinx.android.synthetic.main.item_text.view.*

class OpeningHoursAdapter : RecyclerView.Adapter<OpeningHoursAdapter.OpeningHoursHolder>() {

    private val openingHoursList: MutableList<OpeningHours> = mutableListOf()
    private val days: MutableMap<String, String> = mutableMapOf()

    init {
        days["1"] = "sat"
        days["2"] = "sun"
        days["3"] = "mon"
        days["4"] = "tue"
        days["5"] = "wed"
        days["6"] = "thu"
        days["7"] = "fri"
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): OpeningHoursHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_text, parent, false)
        return OpeningHoursHolder(view)
    }

    override fun getItemCount(): Int = openingHoursList.size

    override fun onBindViewHolder(p0: OpeningHoursHolder, p1: Int) {
        p0.bind(openingHoursList[p1])
    }

    fun updateOpeningHours(openingHoursList: MutableList<OpeningHours>) {
        this.openingHoursList.addAll(openingHoursList)
        notifyDataSetChanged()
    }

    inner class OpeningHoursHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(openingHours: OpeningHours) {
            itemView.recyclerView_item_text.text =
                    "${days[openingHours.day]} (${openingHours.start} - ${openingHours.end})"
        }
    }
}
