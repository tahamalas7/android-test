package com.appagroup.sahty.ui.facilities.hospitals

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Hospital
import com.appagroup.sahty.entity.Paginate
import com.appagroup.sahty.utils.DisposableManager
import com.appagroup.sahty.utils.Facility
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class HospitalsPresenter(view: HospitalsContract.View) : BasePresenter<HospitalsContract.View>(view), HospitalsContract.Presenter {

    private val TAG = "HospitalsPresenter"
    private var page = 1
    private var perPage = 10
    private var isThereMore = true

    override fun loadHospitals() {
        if (isThereMore) {
            view.changeProgressBarVisibility(View.VISIBLE)
            subscribe(dataManager.getHospitals(Facility.TYPE_HOSPITAL, page, perPage), object : Observer<Response<BaseResponse<Paginate<MutableList<Hospital>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "loadHospitals -> onComplete")
                }

                override fun onSubscribe(d: Disposable) {
                    Log.d(TAG, "loadHospitals -> onSubscribe")
                    DisposableManager.addFacilitiesDisposable(d)
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Hospital>>>>) {
                    Log.d(TAG, "loadHospitals -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "loadHospitals -> onNext -> body = ${t.body()}")
                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        Log.d(TAG, "loadHospitals -> onNext -> size = ${t.body()!!.data!!.data!!.size}")
                        page++
                        view.updateHospitals(t.body()!!.data!!.data!!)
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            isThereMore = false
                    } else
                        view.showError(R.string.err_load_hospitals)
                    view.changeProgressBarVisibility(View.GONE)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "loadHospitals -> onError -> error = ${e.message}")
                    view.showError(R.string.err_load_hospitals)
                    view.changeProgressBarVisibility(View.GONE)
                }
            })
        }
    }

    override fun unsubscribe() {
        DisposableManager.disposeFacilities()
    }
}