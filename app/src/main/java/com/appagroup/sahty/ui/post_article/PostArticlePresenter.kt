package com.appagroup.sahty.ui.post_article

import android.util.Log
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

class PostArticlePresenter(view: PostArticleContract.View) : BasePresenter<PostArticleContract.View>(view), PostArticleContract.Presenter {

    private val TAG = "PostArticlePresenter"

    val observer = object : Observer<Response<BaseResponse<Any>>> {
        override fun onComplete() {
            Log.d(TAG, "onComplete")
        }

        override fun onSubscribe(d: Disposable) {
            DisposableManager.addArticlesDisposable(d)
            Log.d(TAG, "onSubscribe")
        }

        override fun onNext(t: Response<BaseResponse<Any>>) {
            Log.d(TAG, "onNext -> code = ${t.code()}")
            Log.d(TAG, "onNext -> body = ${t.body()}")

            if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                Log.d(TAG, "onNext -> status = ${t.body()!!.status}")
                Log.d(TAG, "onNext -> message = ${t.body()!!.message}")
                view.articlePostedSuccessfully()
            } else
                view.showError(R.string.something_wrong)
            view.hideProgressDialog()
        }

        override fun onError(e: Throwable) {
            Log.d(TAG, "onError -> error = ${e.message}")
            view.showError(R.string.something_wrong)
            view.hideProgressDialog()
        }
    }

    override fun postArticleWithVideo(enTitle: RequestBody, enContent: RequestBody,
                                      arTitle: RequestBody, arContent: RequestBody,
                                      categoryId: RequestBody, video: MultipartBody.Part) {
        view.showProgressDialog()
        subscribe(dataManager.postArticleWithVideo(enTitle, enContent, arTitle, arContent, categoryId, video), observer)

    }

    override fun postArticleWithImages(enTitle: RequestBody, enContent: RequestBody,
                                       arTitle: RequestBody, arContent: RequestBody,
                                       categoryId: RequestBody, images: MutableList<MultipartBody.Part>) {
        view.showProgressDialog()
        subscribe(dataManager.postArticleWithImages(enTitle, enContent, arTitle, arContent, categoryId, images), observer)
    }

    override fun getUserName(): String =
            dataManager.getUserName()

    override fun getUserImage(): String =
            dataManager.getUserImage()

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeArticles()
    }
}
