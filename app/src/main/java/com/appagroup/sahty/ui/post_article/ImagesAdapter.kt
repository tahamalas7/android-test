package com.appagroup.sahty.ui.post_article

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.R
import com.appagroup.sahty.ui.listener.OnClick
import kotlinx.android.synthetic.main.item_selected_image.view.*
import java.util.*

class ImagesAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = "ImagesAdapter"
    private val TYPE_IMAGE = 1
    private val TYPE_ADD = 2
    private val images: MutableList<String> = mutableListOf()
    private lateinit var selectedImageClickListener: SelectedImageClickListener
    private lateinit var addImageClickListener: OnClick

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ADD -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_accreditation_add, parent, false)
                AddImageHolder(view)
            }

            TYPE_IMAGE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_selected_image, parent, false)
                ImagesHolder(view)
            }

            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int =
            if (position == 0)
                TYPE_ADD
            else TYPE_IMAGE

    override fun getItemCount(): Int = images.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is AddImageHolder -> {
                holder.itemView.setOnClickListener {
                    addImageClickListener.onClick()
                }
            }
            is ImagesHolder -> {
                val image = images[position - 1]
                holder.bind(image)
                holder.itemView.selected_image_remove_btn.setOnClickListener {
                    images.remove(image)
                    notifyDataSetChanged()
                }

                holder.itemView.selected_image.setOnClickListener {
                    selectedImageClickListener.onSelectedImageClick(position - 1, images)
                }
            }
        }

        holder.itemView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                val params = holder.itemView.layoutParams
                params.height = holder.itemView.width
                holder.itemView.layoutParams = params
                holder.itemView.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
    }

    fun addImages(images: ArrayList<Uri>) {
        for (uri in images)
            this.images.add(uri.toString())
        notifyDataSetChanged()
    }

    fun setOnAddImageButtonClickListener(addImageClickListener: OnClick) {
        this.addImageClickListener = addImageClickListener
    }

    fun setOnSelectedImageClickListener(selectedImageClickListener: SelectedImageClickListener) {
        this.selectedImageClickListener = selectedImageClickListener
    }

    fun getImages() = images

    inner class AddImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class ImagesHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(imageUrl: String) {
            Glide.with(itemView.context)
                    .load(imageUrl)
                    .into(itemView.selected_image)
        }
    }
}