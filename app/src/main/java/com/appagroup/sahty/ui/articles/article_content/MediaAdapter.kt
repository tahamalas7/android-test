package com.appagroup.sahty.ui.articles.article_content

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Media
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.utils.MediaType
import kotlinx.android.synthetic.main.item_pager_image.view.*
import kotlinx.android.synthetic.main.item_pager_video.view.*

class MediaAdapter : PagerAdapter() {

    private val TAG = "MediaAdapter"
    private val mediaItems: MutableList<Media> = mutableListOf()
    private lateinit var imageClickListener: ItemClickListener<Int>
    private lateinit var videoPlayerClickListener: ItemClickListener<String>

    override fun isViewFromObject(p0: View, p1: Any): Boolean =
            p0 == p1

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val layoutInflater = container.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val mediaItem = mediaItems[position]
        val itemView: View

        // initialize item as a video
        if (mediaItem.type == MediaType.TYPE_VIDEO) {
            itemView = layoutInflater.inflate(R.layout.item_pager_video, container, false)
            Glide.with(itemView)
                    .load(BuildConfig.IMAGE + mediaItem.thumbnail)
                    .into(itemView.video_pager_thumbnail)

            itemView.video_pager_play_btn.setOnClickListener {
                videoPlayerClickListener.onClick(mediaItem.url)
            }
        }

        // initialize item as an image
        else {
            itemView = layoutInflater.inflate(R.layout.item_pager_image, container, false)
            Glide.with(itemView)
                    .load(BuildConfig.IMAGE + mediaItem.url)

                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            itemView.image_pager_progressBar.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            itemView.image_pager_progressBar.visibility = View.GONE
                            return false
                        }
                    })
                    .into(itemView.image_pager_src)

            itemView.setOnClickListener { imageClickListener.onClick(position) }
        }

        container.addView(itemView)
        return itemView
    }

    override fun getCount(): Int = mediaItems.size

    fun updateMediaItems(mediaItems: MutableList<Media>) {
        this.mediaItems.addAll(mediaItems)
        notifyDataSetChanged()
    }

    fun setOnVideoPlayButtonClickListener(videoPlayerClickListener: ItemClickListener<String>) {
        this.videoPlayerClickListener = videoPlayerClickListener
    }

    fun setOnImageClickListener(imageClickListener: ItemClickListener<Int>) {
        this.imageClickListener = imageClickListener
    }

    fun getAllMediaItems(): MutableList<Media> =
            mediaItems

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }
}