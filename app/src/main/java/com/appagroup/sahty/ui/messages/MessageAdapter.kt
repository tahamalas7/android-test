package com.appagroup.sahty.ui.messages

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Message
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_loader.view.*
import kotlinx.android.synthetic.main.item_message.view.*

class MessageAdapter(private val userId: Long) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var messagesLoader: ProgressBar
    private lateinit var messageClickListener: ItemClickListener<Message>
    private lateinit var messagesBottomReachListener: BottomReachListener
    private val messages: MutableList<Message> = mutableListOf()
    private val TYPE_LOADER = 0
    private val TYPE_MESSAGE = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_LOADER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_loader, parent, false)
                LoaderHolder(view)
            }
            TYPE_MESSAGE -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_message, parent, false)
                MessageHolder(view)
            }
            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1)
            TYPE_LOADER
        else TYPE_MESSAGE
    }

    override fun getItemCount(): Int = messages.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoaderHolder -> holder.changeLoaderVisibility()
            is MessageHolder -> {
                holder.bind(messages[position])
                holder.itemView.setOnClickListener {
                    messageClickListener.onClick(messages[position])
                }
            }
        }

        if (position == itemCount - 5)
            messagesBottomReachListener.onBottomReach()
    }

    fun updateMessages(messages: MutableList<Message>) {
        this.messages.addAll(messages)
        notifyDataSetChanged()
    }

    fun searchForMessage(receiverId: Long): Long {
        for (position in 0 until messages.size)
            if (messages[position].receiverId == receiverId) {
                messageClickListener.onClick(messages[position])
                return messages[position].messageId
            }
        return -1
    }

    fun clearItems() {
        this.messages.clear()
    }

    fun editMessage(messageId: Long, content: String, date: String) {
        for (position in 0 until messages.size)
            if (messages[position].messageId == messageId) {
//                messages[position].content = content
//                messages[position].date = date
//                notifyItemChanged(position)

                val item = messages[position]
                item.content = content
                item.date = date
                messages.removeAt(position)
                messages.add(0, item)
                notifyDataSetChanged()
            }
    }

    fun setOnMessageClickListener(messageClickListener: ItemClickListener<Message>) {
        this.messageClickListener = messageClickListener
    }

    fun setOnMessagesBottomReachListener(messagesBottomReachListener: BottomReachListener) {
        this.messagesBottomReachListener = messagesBottomReachListener
    }

    fun changeLoaderVisibility(visibility: Int) {
        if (::messagesLoader.isInitialized)
            messagesLoader.visibility = visibility
    }

    inner class LoaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun changeLoaderVisibility() {
            messagesLoader = itemView.recyclerView_loader
        }
    }

    inner class MessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(message: Message) {
            itemView.item_message_reply.text = message.content
            itemView.item_message_date.text = message.date
            if (message.senderId == userId) {
                itemView.item_message_receiver_name.text = message.receiverName
                if (message.receiverPicture != null)
                    Glide.with(itemView.context)
                            .load(BuildConfig.IMAGE + message.receiverPicture)
                            .into(itemView.item_message_receiver_image)
            } else {
                itemView.item_message_receiver_name.text = message.senderName
                if (message.senderPicture != null)
                    Glide.with(itemView.context)
                            .load(BuildConfig.IMAGE + message.senderPicture)
                            .into(itemView.item_message_receiver_image)
            }
        }
    }
}