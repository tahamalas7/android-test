package com.appagroup.sahty.ui.facilities.pharmacies.pharmacy_details

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseBottomSheetDialogFragment
import com.appagroup.sahty.entity.PharmacyDetails
import com.appagroup.sahty.ui.facilities.hospitals.hospital_details.OpeningHoursAdapter
import com.appagroup.sahty.ui.facilities.rating_dialog.RatingDialog
import com.appagroup.sahty.utils.UIUtils
import kotlinx.android.synthetic.main.fragment_pharmacy_details.*

class PharmacyDetailsFragment : BaseBottomSheetDialogFragment<PharmacyDetailsContract.Presenter>(), PharmacyDetailsContract.View {

    private lateinit var pharmacyDetails: PharmacyDetails
    private val openingHoursAdapter = OpeningHoursAdapter()
    private val RATE_DIALOG_REQUEST_CODE = 100

    override fun instantiatePresenter(): PharmacyDetailsContract.Presenter = PharmacyDetailsPresenter(this)

    override fun getLayout(): Int = R.layout.fragment_pharmacy_details

    override fun initViews(view: View) {
        pharmacy_details_hours_recyclerView.layoutManager = LinearLayoutManager(activity)
        pharmacy_details_hours_recyclerView.adapter = openingHoursAdapter
        presenter.getPharmacyDetails(arguments!!.getLong(PHARMACY_ID))
    }

    override fun setListeners() {
        pharmacy_details_rate_bar.setOnClickListener {
            if (!pharmacyDetails.beenRated) {
                val dialog = RatingDialog.getInstance(arguments!!.getLong(PHARMACY_ID))
                dialog.setTargetFragment(this, RATE_DIALOG_REQUEST_CODE)
                dialog.show(activity!!.supportFragmentManager, TAG)
            } else
                showError(getString(R.string.err_alreday_pharmacy_rated))
        }

        pharmacy_details_location.setOnClickListener {
            UIUtils.openMap(
                    activity!!,
                    pharmacyDetails.latitude.toString(),
                    pharmacyDetails.longitude.toString(),
                    pharmacyDetails.name)
        }
    }

    override fun changeProgressBarVisibility(visibility: Int) {
        pharmacy_details_progressBar.visibility = visibility
    }

    override fun changeContentVisibility(visibility: Int) {
        pharmacy_details_container.visibility = visibility
    }

    override fun setPharmacyDetails(pharmacyDetails: PharmacyDetails) {
        this.pharmacyDetails = pharmacyDetails
        pharmacy_details_name.text = pharmacyDetails.name
        pharmacy_details_location.text = pharmacyDetails.location
        pharmacy_details_phone_number.text = pharmacyDetails.contactInfo
        pharmacy_details_rate.text = if (pharmacyDetails.rate != null)
            pharmacyDetails.rate.toString()
        else
            0F.toString()

        if (pharmacyDetails.openingHours != null) {
            openingHoursAdapter.updateOpeningHours(pharmacyDetails.openingHours)
            if(pharmacyDetails.openingHours.size == 0)
                pharmacy_details_hours_lbl.visibility = View.GONE
        }

        if (pharmacyDetails.image != null)
            Glide.with(activity!!)
                    .load(BuildConfig.IMAGE + pharmacyDetails.image)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?,
                                                  model: Any?,
                                                  target: Target<Drawable>?,
                                                  isFirstResource: Boolean): Boolean {
                            pharmacy_details_image_progressBar.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?,
                                                     model: Any?,
                                                     target: Target<Drawable>?,
                                                     dataSource: DataSource?,
                                                     isFirstResource: Boolean): Boolean {
                            pharmacy_details_image_progressBar.visibility = View.GONE
                            return false
                        }
                    })
                    .into(pharmacy_details_image)
        else
            pharmacy_details_image_progressBar.visibility = View.GONE

        setListeners()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RATE_DIALOG_REQUEST_CODE &&
                data != null &&
                data.getBooleanExtra(RatingDialog.RATED_SUCCESSFULLY, false)) {
            showAlertDialog(R.string.Review_submitted_successfully)
            pharmacyDetails.beenRated =
                    data.getBooleanExtra(RatingDialog.RATED_SUCCESSFULLY, false)
        }
    }

    companion object {
        private val PHARMACY_ID = "PHARMACY_ID"
        val TAG = "PharmacyDetailsFragment"

        fun newInstance(pharmacyId: Long): PharmacyDetailsFragment {
            val instance = PharmacyDetailsFragment()
            val bundle = Bundle()
            bundle.putLong(PHARMACY_ID, pharmacyId)
            instance.arguments = bundle

            return instance
        }
    }
}