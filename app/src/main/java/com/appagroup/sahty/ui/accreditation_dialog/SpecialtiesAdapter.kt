package com.appagroup.sahty.ui.accreditation_dialog

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Specialty
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_specialty.view.*

class SpecialtiesAdapter(val typeface: Typeface) : RecyclerView.Adapter<SpecialtiesAdapter.ViewHolder>() {

    private lateinit var specialtyClickListener: ItemClickListener<Specialty>
    private val specialties: MutableList<Specialty> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_specialty, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = specialties.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(specialties[position])
        holder.itemView.setOnClickListener {
            specialtyClickListener.onClick(specialties[position])
        }
    }

    fun setSpecialtyClickListener(specialtyClickListener: ItemClickListener<Specialty>) {
        this.specialtyClickListener = specialtyClickListener
    }

    fun updateSpecialties(specialties: MutableList<Specialty>) {
        this.specialties.clear()
        this.specialties.addAll(specialties)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(specialty: Specialty) {
            itemView.item_specialty_text.text = specialty.name
            itemView.item_specialty_text.typeface = typeface
        }
    }
}