package com.appagroup.sahty.ui.login.select_role

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView

interface SelectRoleContract {

    interface View : IBaseView {

        fun setListeners()

        fun roleSubmittedSuccessfully()

        fun changeProgressBarVisibility(visibility: Int)
    }

    interface Presenter : IBasePresenter<View> {

        fun setUserRole(role: Int)
    }
}