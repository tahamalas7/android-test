package com.appagroup.sahty.ui.medical_files.group_content

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.MedicalFile
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response
import com.appagroup.sahty.ui.medical_files.group_content.MedicalFileGroupContentContract as Contract

class MedicalFileGroupContentPresenter(view: Contract.View) : BasePresenter<Contract.View>(view), Contract.Presenter {

    private val TAG = "GroupContentPresenter"

    override fun getMedicalFiles(groupId: Long) {
        view.changeProgressBarVisibility(View.VISIBLE)
        subscribe(dataManager.getMedicalFiles(groupId), object : Observer<Response<BaseResponse<MutableList<MedicalFile>>>> {
            override fun onComplete() {
                Log.d(TAG, "getMedicalFiles -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "getMedicalFiles -> onSubscribe")
                DisposableManager.addMedicalFilesDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<MutableList<MedicalFile>>>) {
                Log.d(TAG, "getMedicalFiles -> onNext -> code = ${t.code()}")
                Log.d(TAG, "getMedicalFiles -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                    Log.d(TAG, "getMedicalFiles -> onNext -> data size = ${t.body()!!.data!!.size}")
                    view.updateMedicalFiles(t.body()!!.data!!)
                } else
                    view.showError(R.string.something_wrong)
                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "getMedicalFiles -> onError -> ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
            }
        })
    }

    override fun deleteFile(fileId: Long) {
        Log.d(TAG, "File ID = $fileId")
        view.showDeleteProgressDialog()
        subscribe(dataManager.deleteMedicalFile(fileId), object :Observer<Response<BaseResponse<Any>>> {
            override fun onComplete() {
                Log.d(TAG, "deleteFile -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "deleteFile -> onSubscribe")
                DisposableManager.addMedicalFilesDisposable(d)
            }

            override fun onNext(t: Response<BaseResponse<Any>>) {
                Log.d(TAG, "deleteFile -> onNext -> code = ${t.code()}")
                Log.d(TAG, "deleteFile -> onNext -> body = ${t.body()}")
                if (t.code() == 200 && t.body() != null && t.body()!!.status)
                    view.removeDeletedFile(fileId)
                else
                    view.showError(R.string.something_wrong)
                view.dismissDeleteProgressDialog()
                view.changeProgressBarVisibility(View.GONE)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "deleteFile -> onError -> ${e.message}")
                view.changeProgressBarVisibility(View.GONE)
                view.dismissDeleteProgressDialog()
            }
        })
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeMedicalFiles()
    }
}