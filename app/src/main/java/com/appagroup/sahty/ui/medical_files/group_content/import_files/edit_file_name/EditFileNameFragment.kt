package com.appagroup.sahty.ui.medical_files.group_content.import_files.edit_file_name

import android.os.Bundle
import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseBottomSheetDialogFragment
import kotlinx.android.synthetic.main.dialog_edit_file_name.*
import java.io.File

class EditFileNameFragment : BaseBottomSheetDialogFragment<EditFileNameContract.Presenter>(), EditFileNameContract.View {

    private var oldFilePath = ""
    private var newFilePath = ""
    private lateinit var onFileRenamed: OnFileRenamed

    override fun instantiatePresenter(): EditFileNameContract.Presenter = EditFileNamePresenter(this)

    override fun getLayout(): Int = R.layout.dialog_edit_file_name

    override fun initViews(view: View) {
        oldFilePath = arguments!!.getString(FILE_PATH)!!
        edit_file_name_text.setText(getFileName(oldFilePath))
        setListener()
    }

    private fun setListener() {
        edit_file_name_add_btn.setOnClickListener {
            if (edit_file_name_text.text.isEmpty())
                edit_file_name_text.error = getString(R.string.error_empty_field)
            else {
                newFilePath = replaceFileName(oldFilePath, getFileName(oldFilePath), edit_file_name_text.text.toString().trim())
                val from = File(oldFilePath)
                val to = File(newFilePath)
                val renamed  = from.renameTo(to)

                Log.d(TAG, "before = $oldFilePath")
                Log.d(TAG, "after  = $newFilePath")
                Log.d(TAG, "result = $renamed")

                onFileRenamed.onRename(newFilePath)
                dialog.dismiss()
            }
        }

        edit_file_name_cancel_btn.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun getFileName(filePath: String): String {
        return filePath.substring(filePath.lastIndexOf('/') + 1, filePath.lastIndexOf('.'))
    }

    private fun replaceFileName(filePath: String, oldName: String, newName: String): String {
        return filePath.replace(oldName, newName)
    }

    fun setOnDismissListener(onFileRenamed: OnFileRenamed) {
        this.onFileRenamed = onFileRenamed
    }

    companion object {
        val FILE_PATH = "FILE_PATH"
        val TAG = "EditFileNameFragment"
        fun newInstance(filePath: String): EditFileNameFragment {
            val instance = EditFileNameFragment()
            val bundle = Bundle()
            bundle.putString(FILE_PATH, filePath)
            instance.arguments = bundle
            return instance
        }
    }
}