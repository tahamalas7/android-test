package com.appagroup.sahty.ui.profile.edit_profile

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Environment
import android.transition.TransitionManager
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.Accreditation
import com.appagroup.sahty.ui.accreditation_dialog.AccreditationDialog
import com.appagroup.sahty.ui.listener.DialogDismissListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.OnClick
import com.appagroup.sahty.utils.*
import com.yalantis.ucrop.UCrop
import kotlinx.android.synthetic.main.activity_edit_profile.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class EditProfileActivity : BaseActivity<EditProfileContract.Presenter>(), EditProfileContract.View {

    private val TAG = "EditProfileActivity"
    private val EDIT_PICTURE_REQUEST_CODE = 75
    private lateinit var progressDialog: ProgressDialog
    private lateinit var accreditationDialog: AccreditationDialog
    private lateinit var accreditationsAdapter: EditAccreditationsAdapter

    override fun instantiatePresenter(): EditProfileContract.Presenter = EditProfilePresenter(this)

    override fun getContentView(): Int = R.layout.activity_edit_profile

    override fun initViews() {
        progressDialog = ProgressDialog(this)
        edit_profile_username.text = presenter.getUserName()
        if (presenter.getUserImage().isNotEmpty())
            Glide.with(this)
                    .load(BuildConfig.IMAGE + presenter.getUserImage())
                    .into(edit_profile_user_image)

        if (presenter.getUserRole() != UserRole.ROLE_READER) {
            edit_profile_availability_bar.visibility = View.VISIBLE
            edit_profile_accreditations_lbl.visibility = View.VISIBLE

            val typeface = Typeface.createFromAsset(assets, "fonts/HelveticaNeueBold.ttf")
            val accreditations = intent.getParcelableArrayListExtra<Accreditation>(ACCREDITATION_LIST)
            Log.d("EditProfileActivity", "ACCREDITATION COUNT = ${accreditations.size}")

            accreditationsAdapter = EditAccreditationsAdapter(typeface)
            accreditationsAdapter.setAccreditations(accreditations)
            for (acc in accreditations) {
                Log.d("ooooooooo", "iddddd = ${acc.specializationId}")
            }
            edit_profile_accreditations_recyclerView.adapter = accreditationsAdapter
            edit_profile_accreditations_recyclerView.layoutManager = GridLayoutManager(this@EditProfileActivity, 3)

            edit_profile_availability_checkBox.isChecked =
                    presenter.getDoctorAvailability() == DoctorAvailability.AVAILABLE_DOCTOR
        } else {
            edit_profile_availability_bar.visibility = View.GONE
            edit_profile_accreditations_lbl.visibility = View.GONE
        }

        when (presenter.getLoginProvider()) {
            INSTAGRAM -> {
                edit_profile_change_email_lbl.visibility = View.GONE
                edit_profile_change_email_bar.visibility = View.GONE

                edit_profile_change_password_lbl.visibility = View.GONE
                edit_profile_change_password_bar.visibility = View.GONE
            }

            FACEBOOK ->  {
                edit_profile_change_email_lbl.visibility = View.GONE
                edit_profile_change_email_bar.visibility = View.GONE

                edit_profile_change_password_lbl.visibility = View.GONE
                edit_profile_change_password_bar.visibility = View.GONE
            }

            GMAIL -> {
                edit_profile_change_email_lbl.visibility = View.GONE
                edit_profile_change_email_bar.visibility = View.GONE

                edit_profile_change_password_lbl.visibility = View.GONE
                edit_profile_change_password_bar.visibility = View.GONE
            }
        }

        setListeners()
    }

    override fun setListeners() {
        edit_profile_back_arrow.setOnClickListener { onBackPressed() }

        edit_profile_accreditations_lbl.setOnClickListener {
            // update the visibility of accreditations RecyclerView on label click
            // set animation on visibility change
            TransitionManager.beginDelayedTransition(edit_profile_accreditations_container)
            edit_profile_accreditations_recyclerView.visibility =
                    if (edit_profile_accreditations_recyclerView.visibility == View.VISIBLE)
                        View.GONE
                    else View.VISIBLE
        }

        edit_profile_change_password_lbl.setOnClickListener {
            // update the visibility of change password bar on label click
            // set animation on visibility change
            TransitionManager.beginDelayedTransition(edit_profile_accreditations_container)
            edit_profile_change_password_bar.visibility =
                    if (edit_profile_change_password_bar.visibility == View.VISIBLE)
                        View.GONE
                    else View.VISIBLE
        }

        edit_profile_change_email_lbl.setOnClickListener {
            // update the visibility of change email bar on label click
            // set animation on visibility change
            TransitionManager.beginDelayedTransition(edit_profile_accreditations_container)
            edit_profile_change_email_bar.visibility =
                    if (edit_profile_change_email_bar.visibility == View.VISIBLE)
                        View.GONE
                    else View.VISIBLE
        }

        edit_profile_availability_lbl.setOnClickListener {
            // update the visibility of change email bar on label click
            // set animation on visibility change
            TransitionManager.beginDelayedTransition(edit_profile_availability_bar)
            edit_profile_availability_hint.visibility =
                    if (edit_profile_availability_hint.visibility == View.VISIBLE)
                        View.GONE
                    else View.VISIBLE
        }

        edit_profile_change_user_image.setOnClickListener {
            // check for READ_EXTERNAL_STORAGE and WRITE_EXTERNAL_STORAGE permissions
            ActivityCompat.requestPermissions(this@EditProfileActivity, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    EDIT_PICTURE_REQUEST_CODE)

        }

        edit_profile_submit_password.setOnClickListener {
            if (checkChangePasswordFields()) {
                presenter.changePassword(
                        edit_profile_old_password_editText.text.toString().trim(),
                        edit_profile_new_password_editText.text.toString().trim()
                )
            }
        }

        edit_profile_submit_email.setOnClickListener {
            if (checkChangeEmailFields()) {
                presenter.changeEmail(
                        edit_profile_email_password_editText.text.toString().trim(),
                        edit_profile_new_email_editText.text.toString().trim()
                )
            }
        }

        edit_profile_availability_checkBox.setOnCheckedChangeListener { _, isChecked ->
            Log.d("EditProfileActivity", "AVAILABILITY CHECK = $isChecked")
            presenter.changeAvailability(
                    if (isChecked)
                        1
                    else 0
            )
        }

        progressDialog.setOnDismissListener {
            presenter.dispose()
        }

        if (::accreditationsAdapter.isInitialized) {
            accreditationsAdapter.setOnAddAccreditationClickListener(object : OnClick {
                override fun onClick() {
                    showAccreditationDialog(AccreditationDialog.TYPE_CREATE, null)
                }
            })

            accreditationsAdapter.setOnEditAccreditationClickListener(object : ItemClickListener<Accreditation> {
                override fun onClick(item: Accreditation) {
                    showAccreditationDialog(AccreditationDialog.TYPE_EDIT, item)
                }
            })
        }
    }

    private fun checkChangePasswordFields(): Boolean {
        var validaty = true
        if (edit_profile_old_password_editText.text.trim().isEmpty()) {
            edit_profile_old_password_editText.error = getString(R.string.error_empty_field)
            validaty = false
        }

        if (edit_profile_new_password_editText.text.trim().isEmpty()) {
            edit_profile_new_password_editText.error = getString(R.string.error_empty_field)
            validaty = false
        }

        if (edit_profile_confirm_password_editText.text.trim().isEmpty()) {
            edit_profile_confirm_password_editText.error = getString(R.string.error_empty_field)
            validaty = false
        }

        if (edit_profile_new_password_editText.text.trim().length < 6) {
            edit_profile_new_password_editText.error = getString(R.string.error_short_password)
            return false
        }

        if (edit_profile_new_password_editText.text.trim() != edit_profile_confirm_password_editText.text.trim()) {
            edit_profile_confirm_password_editText.error = getString(R.string.error_password_does_not_match)
            return false
        }

        return validaty
    }

    private fun checkChangeEmailFields(): Boolean {
        var validaty = true
        if (edit_profile_email_password_editText.text.trim().isEmpty()) {
            edit_profile_email_password_editText.error = getString(R.string.error_empty_field)
            validaty = false
        }

        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(edit_profile_new_email_editText.text.toString().trim())
        if (!matcher.matches()) {
            edit_profile_new_email_editText.error = getString(R.string.error_invalid_email)
            validaty = false
        }

        return validaty
    }

    private fun showAccreditationDialog(dialogType: Int, accreditation: Accreditation?) {
        accreditationDialog = AccreditationDialog.getInstance(dialogType, accreditation)
        accreditationDialog.show(supportFragmentManager, TAG)

        accreditationDialog.setOnDialogDismissed(object : DialogDismissListener {
            override fun accreditationCreated(accreditation: Accreditation) {
                accreditationsAdapter.addAccreditation(accreditation)
                Log.d("EditProfileActivity", "ACCREDITATION CREATED")
            }

            override fun accreditationEdited(accreditation: Accreditation) {
                accreditationsAdapter.editAccreditation(accreditation)
                Log.d("EditProfileActivity", "ACCREDITATION EDITED")
            }

            override fun accreditationDeleted(accreditationId: Long) {
                accreditationsAdapter.deleteAccreditation(accreditationId)
                Log.d("EditProfileActivity", "ACCREDITATION DELETED")
            }
        })
    }

    override fun pictureChangedSuccessfully(image: String) {
        presenter.saveUserImage(image)
        Glide.with(this)
                .load(BuildConfig.IMAGE + image)
                .into(edit_profile_user_image)

        setResult(Activity.RESULT_OK)
        showMessageDialog(R.string.message_picture_changed)
    }

    override fun passwordChangedSuccessfully() {
        edit_profile_old_password_editText.text.clear()
        edit_profile_new_password_editText.text.clear()
        edit_profile_confirm_password_editText.text.clear()
        showMessageDialog(R.string.message_password_changed)
    }

    override fun emailChangedSuccessfully() {
        edit_profile_email_password_editText.text.clear()
        edit_profile_new_email_editText.text.clear()
        showMessageDialog(R.string.message_email_changed)
        setResult(Activity.RESULT_OK)
    }

    override fun setInvalidPasswordError() {
        edit_profile_email_password_editText.error = getString(R.string.err_invalid_password)
    }

    override fun setInvalidOldPasswordError() {
        edit_profile_old_password_editText.error = getString(R.string.err_invalid_password)
    }

    override fun setInvalidNewEmaildError() {
        edit_profile_new_email_editText.error = getString(R.string.err_taken_email)
    }

    override fun showProgressDialog(messageId: Int) {
        progressDialog.setMessage(getString(messageId))
        progressDialog.show()
    }

    override fun hideProgressDialog() {
        progressDialog.dismiss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            // create random name for cropped image
            val str = "crop_" + SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(Date()) + ".jpg"
            val selectedUri = data!!.data

            if (selectedUri != null)
                UCrop.of(selectedUri, Uri.fromFile(File(Environment.getExternalStorageDirectory(), str)))
                        .withMaxResultSize(2000, 2000)
                        .start(this@EditProfileActivity)
            else
                handleCropResult(data)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == AccreditationDialog.REQUEST_PERMISSIONS_CODE) {
            accreditationDialog.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

        Log.d(TAG, "onRequestPermissionsResult -> requestCode = $requestCode")
        Log.d(TAG, "onRequestPermissionsResult -> grantResults = $grantResults")
        if (requestCode == EDIT_PICTURE_REQUEST_CODE) {
            // call pickImage method if permission granted
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT

                // open Ucrop interface
                startActivityForResult(Intent.createChooser(intent, getString(R.string.select_picture)), UCrop.REQUEST_CROP)
            }
        }
    }

    // handle picture after return from Ucrop interface
    private fun handleCropResult(result: Intent) {
        val finalResultUri = UCrop.getOutput(result)
        if (finalResultUri != null) {
            // parse image to MultipartBody.Part
            val s = finalResultUri.toString()
            val imgFile = File(s.substring(7, s.length))
            val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile)
            val imagePart = MultipartBody.Part.createFormData("image", imgFile.name, requestFile)

            presenter.changePicture(imagePart)

        } else
            Toast.makeText(this, R.string.cannot_retrieve_cropped_image, Toast.LENGTH_SHORT).show()
    }


    companion object {
        val ACCREDITATION_LIST = "ACCREDITATION_LIST"
    }
}