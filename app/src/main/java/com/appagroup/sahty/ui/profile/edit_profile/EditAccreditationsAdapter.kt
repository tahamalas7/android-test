package com.appagroup.sahty.ui.profile.edit_profile

import android.graphics.Typeface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Accreditation
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.listener.OnClick
import kotlinx.android.synthetic.main.item_accreditation_edit.view.*

class EditAccreditationsAdapter(private val typeface: Typeface) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var addAccreditationClickListener: OnClick
    private lateinit var editAccreditationClickListener: ItemClickListener<Accreditation>
    private val accreditations: MutableList<Accreditation> = mutableListOf()
    private val TAG = "EditAccreditationsAdapter"
    private val TYPE_EDIT = 1
    private val TYPE_ADD = 2

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_ADD -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_accreditation_add, parent, false)
                AddAccreditationHolder(view)
            }

            TYPE_EDIT -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.item_accreditation_edit, parent, false)
                EditAccreditationHolder(view)
            }

            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int =
            if (position == 0)
                TYPE_ADD
            else TYPE_EDIT

    override fun getItemCount(): Int =
            accreditations.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is AddAccreditationHolder -> {
                holder.itemView.setOnClickListener {
                    addAccreditationClickListener.onClick()
                }
            }

            is EditAccreditationHolder -> {
                val accreditation = accreditations[position - 1]
                holder.bind(accreditation)
                if (accreditations.size > 1 && !accreditation.isPrimary)
                    holder.itemView.setOnClickListener {
                        editAccreditationClickListener.onClick(accreditations[position - 1])
                    }
                else
                    holder.itemView.accreditation_edit_btn.visibility = View.GONE
            }
        }
    }

    fun setAccreditations(accreditations: MutableList<Accreditation>) {
        this.accreditations.addAll(accreditations)
        notifyDataSetChanged()
    }

    fun addAccreditation(accreditation: Accreditation) {
        this.accreditations.add(accreditation)
        notifyDataSetChanged()
    }

    fun deleteAccreditation(accreditationId: Long) {
        for (position in 0 until accreditations.size)
            if (accreditations[position].id == accreditationId) {
                accreditations.removeAt(position)
                notifyItemRemoved(position + 1)
                break
            }
    }

    fun editAccreditation(accreditation: Accreditation) {
        for (position in 0 until accreditations.size)
            if (accreditations[position].id == accreditation.id) {
                accreditations[position] = accreditation
                notifyItemChanged(position + 1)
                break
            }
    }

    fun setOnAddAccreditationClickListener(addAccreditationClickListener: OnClick) {
        this.addAccreditationClickListener = addAccreditationClickListener
    }

    fun setOnEditAccreditationClickListener(editAccreditationClickListener: ItemClickListener<Accreditation>) {
        this.editAccreditationClickListener = editAccreditationClickListener
    }

    inner class AddAccreditationHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class EditAccreditationHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(accreditation: Accreditation) {
            itemView.accreditation_edit_type.text = accreditation.type
            itemView.accreditation_edit_specialty.text = accreditation.speciality

            itemView.accreditation_edit_type.typeface = typeface
            itemView.accreditation_edit_specialty.typeface = typeface

            itemView.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    val params = itemView.layoutParams

                    Log.d("bbbbbbbb", "GGGG width = ${(itemView.width * 0.8).toInt()}")

                    params.height = (itemView.width * 0.8).toInt()
                    itemView.layoutParams = params
                    itemView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                }
            })
        }
    }
}