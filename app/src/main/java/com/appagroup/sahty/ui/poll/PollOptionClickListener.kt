package com.appagroup.sahty.ui.poll

import com.appagroup.sahty.entity.PollOption

interface PollOptionClickListener {

    fun onVote(pollOption: PollOption)

    fun onCancelingVote(pollOption: PollOption)
}