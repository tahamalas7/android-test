package com.appagroup.sahty.ui.facilities.hospitals.hospital_details

import com.appagroup.sahty.base.IBasePresenter
import com.appagroup.sahty.base.IBaseView
import com.appagroup.sahty.entity.HospitalDetails

interface HospitalDetailsContract {

    interface View : IBaseView {

        fun setListeners()

        fun changeProgressBarVisibility(visibility: Int)

        fun changeContentVisibility(visibility: Int)

        fun setHospitalDetails(hospitalDetails: HospitalDetails)
    }

    interface Presenter : IBasePresenter<View> {

        fun getHospitalDetails(hospitalId: Long)

        fun sendFeedback(hospitalId: Long, text: String, rate: Float)
    }
}