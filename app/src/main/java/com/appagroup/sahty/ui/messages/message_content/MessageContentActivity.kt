package com.appagroup.sahty.ui.messages.message_content

import android.app.Activity
import android.content.Intent
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseActivity
import com.appagroup.sahty.entity.MessageReply
import com.appagroup.sahty.entity.Share
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.messages.patient_groups.PatientFileGroupsActivity
import com.appagroup.sahty.utils.ShareStatus
import com.appagroup.sahty.utils.UIUtils
import kotlinx.android.synthetic.main.fragment_conversation.*
import com.appagroup.sahty.ui.messages.message_content.MessageContentContract as Contract
import com.appagroup.sahty.utils.UserRole as Role

class MessageContentActivity : BaseActivity<Contract.Presenter>(), Contract.View {

    private lateinit var adapter: ReplyAdapter
    private var shareStatus = false
    private var showStatus = false
    private var receiverName = ""
    private var messageId = 0L
    private var receiverId = 0L
    private var receiverRole = 0
    private var lastReplyContent = ""

    override fun instantiatePresenter(): Contract.Presenter = MessageContentPresenter(this)

    override fun getContentView(): Int = R.layout.fragment_conversation

    override fun initViews() {
        messageId = intent.getLongExtra(MESSAGE_ID, 0L)
        receiverId = intent.getLongExtra(RECEIVER_ID, 0L)
        conversation_receiver_name.text = receiverName

        adapter = ReplyAdapter(presenter.getUserId())
        val layoutManager = LinearLayoutManager(this)
        layoutManager.reverseLayout = true
        layoutManager.stackFromEnd = true
        conversation_replies_recyclerView.layoutManager = layoutManager
        conversation_replies_recyclerView.adapter = adapter

        presenter.loadReplies(receiverId)
        setListeners()
    }

    override fun setListeners() {
        conversation_back_arrow.setOnClickListener { onBackPressed() }

        conversation_send_reply_bar.setOnClickListener {
            if (conversation_send_reply_editText.text.trim().isNotEmpty()) {
                lastReplyContent = conversation_send_reply_editText.text.trim().toString()
                presenter.sendReply(receiverId, lastReplyContent)
                addReply(
                        presenter.getUserId(),
                        presenter.getUserName(),
                        presenter.getUserRole(),
                        lastReplyContent)
                conversation_send_reply_editText.text.clear()
                UIUtils.hideSoftKeyboard(this, conversation_send_reply_editText)
                conversation_replies_recyclerView.smoothScrollToPosition(0)
            }
        }

        conversation_share_btn.setOnClickListener {
            showChangeShareStatusDialog()
        }

        conversation_view_files_btn.setOnClickListener {
            if(showStatus) {
                Log.d("GroupContentActivity", "VIEW FILES -> PATIENT NAME = $receiverName")
                Log.d("GroupContentActivity", "VIEW FILES -> PATIENT ID = $receiverId")
                val intent = Intent(this@MessageContentActivity, PatientFileGroupsActivity::class.java)
                intent.putExtra(PatientFileGroupsActivity.PATIENT_NAME, receiverName)
                intent.putExtra(PatientFileGroupsActivity.PATIENT_ID, receiverId)
                startActivity(intent)
            }
        }

        adapter.setOnRepliesBottomReachListener(object : BottomReachListener {
            override fun onBottomReach() {
                presenter.loadReplies(receiverId)
            }
        })
    }

    override fun changeLoaderVisibility(visibility: Int) {
        adapter.changeLoaderVisibility(visibility)
    }

    override fun hideRepliesProgressBar() {
        conversation_progressBar.visibility = View.GONE
    }

    override fun changeShareButtonProgressBarVisibility(visibility: Int) {
        conversation_share_btn_progressBar.visibility = visibility
    }

    override fun updateReplies(replies: MutableList<MessageReply>) {
        adapter.updateReplies(replies)
        if (replies.size <= 0) return
        if (replies[0].fromId == receiverId) {
            receiverName = replies[0].fromName
            receiverRole = replies[0].fromRole
        } else {
            receiverName = replies[0].toName
            receiverRole = replies[0].toRole
        }
        presenter.getMedicalFileShares(receiverId)

        // set receiver name
        conversation_receiver_name.text = receiverName
    }

    override fun addReply(userId: Long, userName: String, userRole: Int, content: String) {
        adapter.updateReplies(MessageReply(
                0, content,
                userId, userName, userRole,
                receiverId, receiverName, receiverRole,
                getString(R.string.date_util_unit_just_now)
        ))
    }

    override fun updateMedicalFileShares(list: MutableList<Share>?) {
        if (list != null)
            for (share in list) {
                // check if user did share his medical files with his doctor
                if (share.fromId == presenter.getUserId() && share.status) {
                    shareStatus = true
                    conversation_share_btn.visibility = View.VISIBLE
                    conversation_share_btn.setImageResource(R.drawable.ic_share_on)
                }

                // check if receiver (patient) did share his medical file with user (doctor)
                if (share.toId == presenter.getUserId() && share.status) {
                    showStatus = true
                    conversation_view_files_btn.visibility = View.VISIBLE
                    conversation_view_files_btn.setImageResource(R.drawable.ic_visibility_on)
                }
            }

        // check if user is a doctor and receiver (patient) did share his medical files with user (doctor)
        if (!showStatus && presenter.getUserRole() == Role.ROLE_CONFIRMED_DOCTOR) {
            conversation_view_files_btn.visibility = View.VISIBLE
            conversation_view_files_btn.setImageResource(R.drawable.ic_visibility_off)
        }

        // check if receiver is a doctor and user (patient) did share his medical files with receiver (doctor)
        if (!shareStatus && receiverRole == Role.ROLE_CONFIRMED_DOCTOR) {
            conversation_share_btn.visibility = View.VISIBLE
            conversation_share_btn.setImageResource(R.drawable.ic_share_off)
        }
    }

    override fun onBackPressed() {
        val intent = Intent()
        intent.putExtra(MESSAGE_ID, messageId)
        intent.putExtra(LAST_REPLY_DATE, getString(R.string.date_util_unit_just_now))
        intent.putExtra(LAST_REPLY_CONTENT, lastReplyContent)
        setResult(Activity.RESULT_OK, intent)
        super.onBackPressed()
    }

    private fun showChangeShareStatusDialog() {
        // show dialog to ask user to submit (share) or (cancel sharing) his medical files
        AlertDialog.Builder(this)
                .setMessage(
                        if (shareStatus)
                            "${getString(R.string.warning_cancel_share_medical_files)} $receiverName"
                        else
                            "${getString(R.string.warning_share_medical_files)} $receiverName"
                )
                .setPositiveButton(R.string.ok) { _, _ ->
                    if (shareStatus) {
                        shareStatus = false
                        conversation_share_btn.setImageResource(R.drawable.ic_share_off)
                        presenter.updateShareMedicalFilesStatus(receiverId, ShareStatus.UNSHARE)
                    } else {
                        shareStatus = true
                        conversation_share_btn.setImageResource(R.drawable.ic_share_on)
                        presenter.updateShareMedicalFilesStatus(receiverId, ShareStatus.SHARE)
                    }
                }
                .setNegativeButton(R.string.cancel) { _, _ -> }
                .show()
    }

    companion object {
        val MESSAGE_ID = "MESSAGE_ID"
        val RECEIVER_ID = "RECEIVER_ID"
        val LAST_REPLY_DATE = "LAST_REPLY_DATE"
        val LAST_REPLY_CONTENT = "LAST_REPLY_CONTENT"
    }
}