package com.appagroup.sahty.ui.videos

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Paginate
import com.appagroup.sahty.entity.Video
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response

class VideosPresenter(view: VideosContract.View) : BasePresenter<VideosContract.View>(view), VideosContract.Presenter {

    private val TAG = "VideosPresenter"
    private var videosPage = 1
    private var loadMoreVideos = true

    override fun getVideos() {
        if (loadMoreVideos) {
            if (videosPage == 1) view.changeProgressBarVisibility(View.VISIBLE)
            else view.changeLoaderVisibility(View.VISIBLE)
            subscribe(dataManager.getVideos(videosPage), object : Observer<Response<BaseResponse<Paginate<MutableList<Video>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "getVideos -> onComplete ")
                }

                override fun onSubscribe(d: Disposable) {
                    DisposableManager.addVideosDisposable(d)
                    Log.d(TAG, "getVideos -> onSubscribe ")
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Video>>>>) {
                    Log.d(TAG, "getVideos -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "getVideos -> onNext -> body = ${t.body()}")

                    view.changeLoaderVisibility(View.GONE)
                    view.changeProgressBarVisibility(View.GONE)

                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        Log.d(TAG, "getVideos -> onNext -> Articles count = ${t.body()!!.data!!.data!!.size}")
                        view.updateVideos(t.body()!!.data!!.data!!)
                        videosPage++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMoreVideos = false
                    } else {
                        if (videosPage == 1)
                            view.showError(R.string.err_failed_load_videos)
                        else view.showError(R.string.err_failed_load_more_videos)
                    }
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "getVideos -> onError -> error message = ${e.message}")
                    Log.d(TAG, "getVideos -> onError -> error cause = ${e.cause}")
                    Log.d(TAG, "getVideos -> onError -> error stackTrace = ${e.stackTrace}")
                    view.changeLoaderVisibility(View.GONE)
                    view.changeProgressBarVisibility(View.GONE)

                    if (videosPage == 1)
                        view.showError(R.string.err_failed_load_videos)
                    else view.showError(R.string.err_failed_load_more_videos)
                }
            })
        }
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeVideos()
    }
}