package com.appagroup.sahty.ui.login.insta

import android.content.Context
import android.content.SharedPreferences
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.appagroup.sahty.R
import com.appagroup.sahty.ui.login.InstagramAuthDialog
import com.appagroup.sahty.ui.login.InstagramAuthListener
import kotlinx.android.synthetic.main.aaaa_insta_activity.*
import org.apache.http.HttpEntity
import org.apache.http.HttpResponse
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.http.util.EntityUtils
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException

class InstaActivity : AppCompatActivity() {

    private val TAG = "INSTAGRAM"
    lateinit var authDialog: InstagramAuthDialog
    var instaAccessToken = ""
    val INSTAGRAM_TOKEN = "INSTAGRAM_TOKEN"
    lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.aaaa_insta_activity)
        setListeners()

        prefs = getSharedPreferences("SahtySharedPreferences", Context.MODE_PRIVATE)
        instaAccessToken = prefs.getString(INSTAGRAM_TOKEN, "") ?: ""

        if (instaAccessToken.isNotEmpty()) {
            insta_login_btn.text = "LOGOUT"
            getUserInfoByAccessToken(instaAccessToken)
        } else
            insta_login_btn.text = "INSTAGRAM LOGIN"


    }

    private fun getUserInfoByAccessToken(instaAccessToken: String) {
        RequestInstagramAPI().execute()
    }

    inner class RequestInstagramAPI : AsyncTask<Void, String, String>() {
        override fun doInBackground(vararg params: Void?): String {
            val httpClient: HttpClient = DefaultHttpClient()
            val httpGet: HttpGet = HttpGet(GET_USER_INFO_URL + instaAccessToken)
            try {
                val response: HttpResponse = httpClient.execute(httpGet)
                val httpEntity: HttpEntity = response.entity
                val json: String = EntityUtils.toString(httpEntity)
                return json
            } catch (e: ClientProtocolException) {
                e.printStackTrace()
                Log.d(TAG, "doInBackground -> ClientProtocolException -> error = ${e.message}")
            } catch (e: IOException) {
                e.printStackTrace()
                Log.d(TAG, "doInBackground -> IOException -> error = ${e.message}")
            }

            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            if (result != null) {
                try {
                    val json: JSONObject = JSONObject(result)
                    Log.d(TAG, "onPostExecute -> try -> json = $json")
                    val jsonData: JSONObject = json.getJSONObject("data")
                    if (jsonData.has("id")) {
                        Log.d(TAG, "onPostExecute -> try -> ID EXIST")
                        insta_user_email.text = jsonData.getString("username")
                        insta_user_name.text = jsonData.getString("full_name")
                        Glide.with(this@InstaActivity)
                                .load(jsonData.getString("profile_picture").replace("\\", ""))
                                .into(insta_user_image)

                    } else {
                        Log.d(TAG, "onPostExecute -> try -> NO ID")
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Log.d(TAG, "onPostExecute -> JSONException -> error = ${e.message}")
                }
            } else
                Log.d(TAG, "onPostExecute -> result == null")
        }
    }

    fun setListeners() {
        insta_login_btn.setOnClickListener {
            if (instaAccessToken.isNotEmpty()) {
                prefs.edit().remove(INSTAGRAM_TOKEN).apply()
                insta_login_btn.text = "INSTAGRAM LOGIN"
                instaAccessToken = ""
            } else {
                authDialog = InstagramAuthDialog(this)
                authDialog.setCancelable(true)
                authDialog.show()

                authDialog.setOnInstagramAuthTokenReceived(object : InstagramAuthListener {
                    override fun onAuthTokenReceived(authToken: String?) {
                        Log.d(TAG, "InstaActivity -> onCodeReceived -> auth_token = $authToken")
                        if (authToken == null) {
                            return
                        }

                        prefs.edit().putString(INSTAGRAM_TOKEN, authToken).apply()
                        instaAccessToken = authToken
                        RequestInstagramAPI().execute()
                    }
                })
            }
        }
    }
}