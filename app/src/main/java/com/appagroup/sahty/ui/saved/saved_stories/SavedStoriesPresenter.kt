package com.appagroup.sahty.ui.saved.saved_stories

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Paginate
import com.appagroup.sahty.entity.Story
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response
import com.appagroup.sahty.ui.saved.saved_stories.SavedStoriesContract as Contract

class SavedStoriesPresenter(view : Contract.View) : BasePresenter<Contract.View>(view), Contract.Presenter {

    private val TAG = "SavedStoriesPresenter"
    private var storiesPage = 1
    private var loadMoreStories = true

    override fun getSavedStories() {
        if (loadMoreStories) {
            if (storiesPage == 1) view.changeProgressBarVisibility(View.VISIBLE)
            else view.changeLoaderVisibility(View.VISIBLE)
            subscribe(dataManager.getSavedStories(storiesPage), object : Observer<Response<BaseResponse<Paginate<MutableList<Story>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "getSavedStories -> onComplete ")
                }

                override fun onSubscribe(d: Disposable) {
                    DisposableManager.addArticlesDisposable(d)
                    Log.d(TAG, "getSavedStories -> onSubscribe ")
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Story>>>>) {
                    Log.d(TAG, "getSavedStories -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "getSavedStories -> onNext -> body = ${t.body()}")

                    view.changeLoaderVisibility(View.GONE)
                    view.changeProgressBarVisibility(View.GONE)

                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        Log.d(TAG, "getSavedStories -> onNext -> Stories count = ${t.body()!!.data!!.data!!.size}")
                        view.updateStories(t.body()!!.data!!.data!!)
                        storiesPage++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMoreStories = false
                    } else
                        if (storiesPage == 1)
                            view.showError(R.string.err_failed_load_stories)
                        else view.showError(R.string.err_failed_load_more_stories)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "getSavedStories -> onError -> error = ${e.message}")
                    view.changeLoaderVisibility(View.GONE)
                    view.changeProgressBarVisibility(View.GONE)

                    if (storiesPage == 1)
                        view.showError(R.string.err_failed_load_stories)
                    else view.showError(R.string.err_failed_load_more_stories)
                }
            })
        }
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeArticles()
    }
}