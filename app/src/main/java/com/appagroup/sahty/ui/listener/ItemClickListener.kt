package com.appagroup.sahty.ui.listener

interface ItemClickListener<T> {

    fun onClick(item: T)
}