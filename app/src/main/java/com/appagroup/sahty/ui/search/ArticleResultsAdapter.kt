package com.appagroup.sahty.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Article
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.utils.MediaType
import kotlinx.android.synthetic.main.item_search_article.view.*

class ArticleResultsAdapter : RecyclerView.Adapter<ArticleResultsAdapter.ArticleResultsHolder>() {

    private val TAG = "ArticleResultsAdapter"
    private val articles: MutableList<Article> = mutableListOf()
    private lateinit var articleClickListener: ItemClickListener<Article>
    private lateinit var bottomReachListener: BottomReachListener

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ArticleResultsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search_article, parent, false)
        return ArticleResultsHolder(view)
    }

    override fun getItemCount(): Int = articles.size

    override fun onBindViewHolder(holder: ArticleResultsHolder, position: Int) {
        holder.bind(articles[position])
        holder.itemView.setOnClickListener {
            articleClickListener.onClick(articles[position])
        }

        if (position == itemCount - 3)
            bottomReachListener.onBottomReach()
    }

    fun updateArticles(articles: MutableList<Article>) {
        this.articles.addAll(articles)
        notifyDataSetChanged()
    }

    fun clearItems() {
        this.articles.clear()
        notifyDataSetChanged()
    }

    fun setOnArticleClickListener(articleClickListener: ItemClickListener<Article>) {
        this.articleClickListener = articleClickListener
    }

    fun setOnBottomReachListener(bottomReachListener: BottomReachListener) {
        this.bottomReachListener = bottomReachListener
    }

    private fun getHighlight(string: String): String {
        if (string.length > 60) {
            for (i in 60 downTo 1) {
                if (string[i] == ' ') {
                    return string.substring(0, i).trim() + "..."
                }
            }
        }
        return string
    }

    inner class ArticleResultsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(article: Article) {
            itemView.search_article_title.text = article.title
            itemView.search_article_content.text = getHighlight(article.content)
            if (article.media != null)
                Glide.with(itemView.context)
                        .load(BuildConfig.IMAGE +
                                if (article.media.type == MediaType.TYPE_IMAGE)
                                    article.media.url
                                else article.media.thumbnail
                        )
                        .into(itemView.search_article_image)
        }
    }
}