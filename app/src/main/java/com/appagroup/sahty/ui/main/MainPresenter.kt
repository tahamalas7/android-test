package com.appagroup.sahty.ui.main

import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.utils.DisposableManager

class MainPresenter(view: MainContract.View) : BasePresenter<MainContract.View>(view), MainContract.Presenter {

    override fun clearUserData() = dataManager.clearUserData()

    override fun getProvider(): String = dataManager.getLoginProvider()

    override fun getUserData() {
        view.fillUserData(
                dataManager.getUserName(),
                dataManager.getUserEmail(),
                dataManager.getUserImage()
        )
    }

    override fun saveLocation(latitude: Float, longitude: Float) {
        dataManager.setDeviceLatitude(latitude)
        dataManager.setDeviceLongitude(longitude)
    }

    override fun getUserPicture(): String =
            dataManager.getUserImage()

    override fun getUserEmail(): String =
            dataManager.getUserEmail()

    override fun getUserRole(): Int =
            dataManager.getUserRole()

    override fun getApplicationLanguage(): String =
            dataManager.getApplicationLanguage()

    override fun setUserRole(role: Int) =
            dataManager.setUserRole(role)



    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.dispose()
    }
}