package com.appagroup.sahty.ui.facilities.hospitals.hospital_details

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Specialization
import kotlinx.android.synthetic.main.item_text.view.*

class SpecializationsAdapter : RecyclerView.Adapter<SpecializationsAdapter. SpecializationsHolder>() {

    private val specializationsList: MutableList<Specialization> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): SpecializationsHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_text, parent, false)
        return SpecializationsHolder(view)
    }

    override fun getItemCount(): Int = specializationsList.size

    override fun onBindViewHolder(holder: SpecializationsHolder, position: Int) {
        holder.bind(specializationsList[position])
    }

    fun updateSpecializations(specializations: MutableList<Specialization>) {
        this.specializationsList.addAll(specializations)
        notifyDataSetChanged()
    }

    inner class SpecializationsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(specialization: Specialization) {
            itemView.recyclerView_item_text.text = specialization.name
        }
    }
}