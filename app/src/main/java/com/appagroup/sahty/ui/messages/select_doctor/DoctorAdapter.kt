package com.appagroup.sahty.ui.messages.select_doctor

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.R
import com.appagroup.sahty.entity.Doctor
import com.appagroup.sahty.ui.listener.BottomReachListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import kotlinx.android.synthetic.main.item_doctor_search_result.view.*
import kotlinx.android.synthetic.main.item_loader.view.*

class DoctorAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private lateinit var loader: ProgressBar
    private lateinit var doctorClickListener: ItemClickListener<Doctor>
    private lateinit var lastDoctorReachListener: BottomReachListener
    private var doctors: MutableList<Doctor> = mutableListOf()
    private val TYPE_LOADER = 0
    private val TYPE_DOCTOR = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_DOCTOR -> {
                val view = LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.item_doctor_search_result, parent, false)
                DoctorHolder(view)
            }
            TYPE_LOADER -> {
                val view = LayoutInflater
                        .from(parent.context)
                        .inflate(R.layout.item_loader, parent, false)
                LoaderHolder(view)
            }
            else -> throw RuntimeException("there is no type that matches the type $viewType + make sure your using types correctly")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == itemCount - 1)
            TYPE_LOADER
        else TYPE_DOCTOR
    }

    override fun getItemCount(): Int = doctors.size + 1

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is LoaderHolder -> holder.updateProgressBarVisibility()
            is DoctorHolder -> {
                holder.bind(doctors[position])
                holder.itemView.setOnClickListener {
                    doctorClickListener.onClick(doctors[position])
                }
            }
        }

        if (position == itemCount - 2)
            lastDoctorReachListener.onBottomReach()
    }

    fun updateDoctors(doctors: MutableList<Doctor>) {
        this.doctors.addAll(doctors)
        notifyDataSetChanged()
    }

    fun updateLoaderVisibility(visibility: Int) {
        if (::loader.isInitialized)
            loader.visibility = visibility
    }

    fun removeItems() {
        Log.d("ssssssss", "removeItems")
        this.doctors.clear()
//        val list: MutableList<Doctor> = mutableListOf()
//        this.doctors = list
        notifyDataSetChanged()
    }

    fun setOnDoctorClickListener(doctorClickListener: ItemClickListener<Doctor>) {
        this.doctorClickListener = doctorClickListener
    }

    fun setOnLastDoctorReachListener(lastDoctorReachListener: BottomReachListener) {
        this.lastDoctorReachListener = lastDoctorReachListener
    }

    inner class LoaderHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun updateProgressBarVisibility() {
            loader = itemView.recyclerView_loader
        }
    }

    inner class DoctorHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(doctor: Doctor) {
            itemView.doctor_name.text = doctor.name
            itemView.doctor_specialty.text = doctor.specialization
            if (doctor.image != null)
                Glide.with(itemView.context)
                        .load(BuildConfig.IMAGE + doctor.image)
                        .into(itemView.doctor_image)
        }
    }
}