package com.appagroup.sahty.ui.home

import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BaseFragment
import com.appagroup.sahty.entity.*
import com.appagroup.sahty.ui.FollowService
import com.appagroup.sahty.ui.articles.ArticlesFragment
import com.appagroup.sahty.ui.articles.article_content.ArticleContentActivity
import com.appagroup.sahty.ui.listener.FollowButtonClickListener
import com.appagroup.sahty.ui.listener.ItemClickListener
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.ui.videos.VideoPlayerActivity
import com.appagroup.sahty.utils.UIUtils
import com.facebook.common.internal.ImmutableList
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment : BaseFragment<HomeContract.Presenter>(), HomeContract.View {

    private lateinit var categoriesAdapter: CategoriesAdapter
    private lateinit var insightAdapter: InsightsAdapter
    private val followService = FollowService()

    override fun instantiatePresenter(): HomeContract.Presenter = HomePresenter(this)

    override fun getLayout(): Int = R.layout.fragment_home

    override fun initViews(view: View) {
        val typeFaceBold = Typeface.createFromAsset(activity!!.assets, "fonts/GOTHICB.TTF")
        val typeFaceRegular = Typeface.createFromAsset(activity!!.assets, "fonts/GOTHIC.TTF")

        // initialize Categories ViewPager
        categoriesAdapter = CategoriesAdapter(typeFaceBold)
        home_categories_viewPager.currentItem = 1
        home_categories_viewPager.adapter = categoriesAdapter

        // initialize Insights ViewPager
        insightAdapter = InsightsAdapter(typeFaceBold, typeFaceRegular)
        home_slider.currentItem = 1
        home_slider.adapter = insightAdapter

        presenter.getInsights()
        presenter.getCategories()

        checkScreenSize()
        setListeners()
    }

    override fun setListeners() {
        categoriesAdapter.setOnCategoryClickListener(object : ItemClickListener<Category> {
            override fun onClick(item: Category) {
                val fragment = ArticlesFragment.getInstance((activity as MainActivity).supportFragmentManager)
                val bundle = Bundle()
                bundle.putLong("CATEGORY_ID", item.id)
                fragment.arguments = bundle
                (activity as MainActivity).showSubFragmentContainer()
                (activity as MainActivity).startFragment(
                        fragment,
                        ArticlesFragment.TAG,
                        MainActivity.SUB_FRAGMENT_CONTAINER)
            }
        })

        insightAdapter.setArticleClickListener(object : ItemClickListener<Article> {
            override fun onClick(item: Article) {
                val intent = Intent(activity, ArticleContentActivity::class.java)
                intent.putExtra("ARTICLE_ID", item.id)
                startActivity(intent)
            }
        })

        insightAdapter.setVideoClickListener(object : ItemClickListener<Video> {
            override fun onClick(item: Video) {
                val intent = Intent(this@HomeFragment.context, VideoPlayerActivity::class.java)
                intent.putExtra(VideoPlayerActivity.VIDEO_URL, item.url)
                intent.putExtra(VideoPlayerActivity.VIDEO_SOURCE, VideoPlayerActivity.TYPE_EXTERNAL)
                startActivity(intent)
            }
        })

        insightAdapter.setPharmacyClickListener(object : ItemClickListener<Pharmacy> {
            override fun onClick(item: Pharmacy) {

            }
        })

        insightAdapter.setHospitalClickListener(object : ItemClickListener<Hospital> {
            override fun onClick(item: Hospital) {

            }
        })

        insightAdapter.setFollowButtonClickListener(object : FollowButtonClickListener {
            override fun followUser(follow: Follow) {
                Log.d(TAG, "followUser -> user id = ${follow.id} ")
                followService.followUser(follow.id)
            }

            override fun unfollowUser(follow: Follow) {
                Log.d(TAG, "unfollowUser -> user id = ${follow.id} ")
                followService.unfollowUser(follow.id)
            }
        })
    }

    override fun updateCategories(categories: MutableList<Category>) {
        categoriesAdapter.updateCategories(categories)
    }

    override fun updateInsightsProgressBarVisibility(visibility: Int) {
        home_insights_progressBar.visibility = visibility
    }

    override fun updateCategoriesProgressBarVisibility(visibility: Int) {
        home_categories_progressBar.visibility = visibility
    }

    override fun updateInsights(insights: Insights) {
        insightAdapter.updateInsights(insights)
    }

    // get screen size to change viewPager padding
    private fun checkScreenSize() {
        val screenSize = UIUtils.getScreenSize(activity as Activity)

        home_categories_viewPager.clipToPadding = false
        when {
            screenSize < 240000.0 -> {
                home_categories_viewPager.setPadding(25, 4, 25, 0)
                home_categories_viewPager.pageMargin = 13
            }
            screenSize < 500000.0 -> {
                home_categories_viewPager.setPadding(64, 32, 64, 32)
                home_categories_viewPager.pageMargin = 32
            }
            screenSize > 500000.0 && screenSize < 4000000.0 -> {
                home_categories_viewPager.setPadding(80, 30, 80, 30)
                home_categories_viewPager.pageMargin = 40
            }
            else -> {
                home_categories_viewPager.setPadding(160, 80, 160, 80)
                home_categories_viewPager.pageMargin = 80
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        val categories: ImmutableList<Category> = ImmutableList.copyOf(categoriesAdapter.getCategories())
        val insights: ImmutableList<Any> = ImmutableList.copyOf(insightAdapter.getInsights())
        outState.putSerializable(CATEGORIES, categories)
        outState.putSerializable(INSIGHTS, insights)
//        outState.putSerializable(CATEGORIES_STATE, home_categories_viewPager.currentItem)
//        outState.putSerializable(INSIGHTS_STATE, home_slider.currentItem)
    }

    companion object {
        val TAG = "HomeFragment"
        val INSIGHTS = "Insights"
        val CATEGORIES = "Categories"
        val CATEGORIES_STATE = "CategoriesState"
        val INSIGHTS_STATE = "InsightsState"

        @JvmStatic
        fun getInstance(): HomeFragment =
                HomeFragment()
    }
}