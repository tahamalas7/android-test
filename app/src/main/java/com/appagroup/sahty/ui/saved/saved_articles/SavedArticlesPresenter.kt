package com.appagroup.sahty.ui.saved.saved_articles

import android.util.Log
import android.view.View
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.entity.Article
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.Paginate
import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import retrofit2.Response
import com.appagroup.sahty.ui.saved.saved_articles.SavedArticlesContract as Contract

class SavedArticlesPresenter(view : Contract.View) : BasePresenter<Contract.View>(view), Contract.Presenter {

    private val TAG = "SavedArticlesPresenter"
    private var articlesPage = 1
    private var loadMoreArticles = true

    override fun getSavedArticles() {
        if (loadMoreArticles) {
            if (articlesPage == 1) view.changeProgressBarVisibility(View.VISIBLE)
            else view.changeLoaderVisibility(View.VISIBLE)
            subscribe(dataManager.getSavedArticles(articlesPage), object : Observer<Response<BaseResponse<Paginate<MutableList<Article>>>>> {
                override fun onComplete() {
                    Log.d(TAG, "getSavedArticles -> onComplete ")
                }

                override fun onSubscribe(d: Disposable) {
                    DisposableManager.addArticlesDisposable(d)
                    Log.d(TAG, "getSavedArticles -> onSubscribe ")
                }

                override fun onNext(t: Response<BaseResponse<Paginate<MutableList<Article>>>>) {
                    Log.d(TAG, "getSavedArticles -> onNext -> code = ${t.code()}")
                    Log.d(TAG, "getSavedArticles -> onNext -> body = ${t.body()}")

                    view.changeLoaderVisibility(View.GONE)
                    view.changeProgressBarVisibility(View.GONE)

                    if (t.code() == 200 && t.body() != null && t.body()!!.status) {
                        Log.d(TAG, "getSavedArticles -> onNext -> Articles count = ${t.body()!!.data!!.data!!.size}")
                        view.updateArticles(t.body()!!.data!!.data!!)
                        articlesPage++
                        if (t.body()!!.data!!.current_page == t.body()!!.data!!.last_page)
                            loadMoreArticles = false
                    } else
                        if (articlesPage == 1)
                            view.showError(R.string.err_failed_load_articles)
                        else view.showError(R.string.err_failed_load_more_articles)
                }

                override fun onError(e: Throwable) {
                    Log.d(TAG, "getSavedArticles -> onError -> error = ${e.message}")
                    view.changeLoaderVisibility(View.GONE)
                    view.changeProgressBarVisibility(View.GONE)

                    if (articlesPage == 1)
                        view.showError(R.string.err_failed_load_articles)
                    else view.showError(R.string.err_failed_load_more_articles)
                }
            })
        }
    }

    override fun getUserId(): Long =
            dataManager.getUserId()

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.disposeArticles()
    }
}