package com.appagroup.sahty.ui.login

import android.os.Bundle
import android.util.Log
import com.appagroup.sahty.R
import com.appagroup.sahty.base.BasePresenter
import com.appagroup.sahty.data.http.ApiInterface
import com.appagroup.sahty.entity.BaseResponse
import com.appagroup.sahty.entity.InstagramResponse
import com.appagroup.sahty.entity.User
import com.appagroup.sahty.utils.*
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.google.gson.GsonBuilder
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class LoginPresenter(view: LoginContract.View) : BasePresenter<LoginContract.View>(view), LoginContract.Presenter {

    val TAG = "LoginPresenter"

    override fun login(email: String, password: String, fcm_token: String) {
        view.showProgressBar()
        subscribe(dataManager.login(email, password, fcm_token), object : Observer<Response<BaseResponse<User>>> {
            override fun onComplete() {}

            override fun onSubscribe(d: Disposable) {
                DisposableManager.add(d)
                Log.d(TAG, "login -> onSubscribe -> password = $password")
                Log.d(TAG, "login -> onSubscribe -> fcm_token = $fcm_token")
                Log.d(TAG, "login -> onSubscribe -> email = $email")
            }

            override fun onNext(t: Response<BaseResponse<User>>) {
                Log.d(TAG, "login -> onNext -> code = ${t.code()}")
                Log.d(TAG, "login -> onNext -> body = ${t.body()}")
                if (t.body() != null && t.body()!!.status) {
                    when (t.code()) {
                        200 -> {
                            if (t.body() != null && t.body()!!.data != null) {
                                saveUserData(t.body()!!.data!!)
                                view.loginSuccessfully(t.body()!!.data!!.name)
                                Log.d(TAG, "socialLogin -> onNext -> in}")
                            }
                        }

                        400 -> {
                            view.showError(view.getString(R.string.validation_error))
                        }
                    }
                } else
                    view.showError(view.getString(R.string.something_wrong))

                view.hideProgressBar()
                view.changeLoginProcessStatus(false)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "login -> onError -> error = ${e.message}")
                view.showError(view.getString(R.string.something_wrong))
                view.changeLoginProcessStatus(false)
                view.hideProgressBar()
            }
        })
    }

    override fun loginWithFacebook(fcm_token: String) {
        val request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken())
        { result, response ->
            Log.d(TAG, "loginWithFacebook -> newMeRequest -> response $response")

            try {
                val email = result.getString("email")
                val name = result.getString("name")
                val image = "http://graph.facebook.com/${result.getString("id")}/picture?type=large"
                val token = AccessToken.getCurrentAccessToken().token
                Log.d(TAG, "loginWithFacebook -> newMeRequest -> email = $email, title = $name , image = $image , social_token = $token")
                socialLogin(name, email, fcm_token, token, FACEBOOK)
            } catch (e: Exception) {
                Log.e(TAG, "${e.printStackTrace()}")
                view.showError(view.getString(R.string.something_wrong))
                view.changeLoginProcessStatus(false)
                view.hideProgressBar()
            }
        }

        val parameters = Bundle()
        parameters.putString("fields", "id,name,email,gender,birthday")
        request.parameters = parameters
        request.executeAsync()
        Log.d(TAG, "loginWithFacebook -> User signed up and logged in through Facebook!")
    }

    override fun socialLogin(name: String, email: String, fcm_token: String, social_token: String, provider: String) {
        Log.d(TAG, "socialLogin")
        view.showProgressBar()

        subscribe(dataManager.socialLogin(name, email, fcm_token, social_token, provider), object : Observer<Response<BaseResponse<User>>> {
            override fun onComplete() {
                Log.d(TAG, "socialLogin -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "socialLogin -> onSubscribe -> name = $name")
                Log.d(TAG, "socialLogin -> onSubscribe -> social_token = $social_token")
                Log.d(TAG, "socialLogin -> onSubscribe -> fcm_token = $fcm_token")
                Log.d(TAG, "socialLogin -> onSubscribe -> email = $email")
                Log.d(TAG, "socialLogin -> onSubscribe -> provider = $provider")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<BaseResponse<User>>) {
                Log.d(TAG, "socialLogin -> onNext -> code = ${t.code()}")
                Log.d(TAG, "socialLogin -> onNext -> body = ${t.body()}")
                if (t.body() != null && t.body()!!.status) {
                    when (t.code()) {
                        200 -> {
                            Log.d(TAG, "socialLogin -> onNext -> USER ROLE = ${t.body()!!.data!!.role}")
                            saveUserData(t.body()!!.data!!)
                            if (t.body()!!.data!!.role.toInt() == UserRole.ROLE_UNKNOWN_USER) {
                                view.openSelectRoleDialog(t.body()!!.data!!.name)
                                Log.d(TAG, "socialLogin -> onNext -> IFFFF")
                            } else {
                                view.loginSuccessfully(t.body()!!.data!!.name)
                                Log.d(TAG, "socialLogin -> onNext -> ELSEEE")
                            }
                        }

                        401 -> {
                            view.showError(view.getString(R.string.validation_error))
                        }
                    }
                } else
                    view.showError(view.getString(R.string.something_wrong))

                view.hideProgressBar()
                view.changeLoginProcessStatus(false)
            }

            override fun onError(e: Throwable) {
                view.showError(view.getString(R.string.something_wrong))
                view.changeLoginProcessStatus(false)
                view.hideProgressBar()
                Log.d(TAG, "socialLogin -> onError -> error = ${e.message}")
            }
        })
    }

    override fun saveUserData(user: User) {
        Log.d(TAG, "saveUserData ->\nid\t${user.id}\nname\t${user.name}\nemail\t${user.email}\nimage\t${user.image}\nphoneNumber\t${user.phoneNumber}\nrole\t${user.role}\naccess_token\t${user.access_token}\n")
        dataManager.setUserId(user.id)
        dataManager.setUserName(user.name)
        dataManager.setUserEmail(user.email)
        dataManager.setUserImage(user.image ?: "")
        dataManager.setUserRole(user.role.toInt())
        dataManager.setLoginProvider(user.provider ?: "")
        dataManager.setUserAccessToken("Bearer " + user.access_token)
        dataManager.setDoctorAvailability(user.availability)
    }

    override fun setUserRole(role: Int) =
        dataManager.setUserRole(role)

    override fun clearUserData() =
        dataManager.clearUserData()

    override fun requestInstagramAPI(authToken: String, fcm_token: String) {
        view.changeLoginProcessStatus(true)
        val gson = GsonBuilder().setLenient().create()

        val builder = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(OkHttpClient.Builder().connectTimeout(20, TimeUnit.SECONDS).build())
                .baseUrl(InstagramConstans.GET_USER_INFO_URL)
                .build()

        subscribe(builder.create(ApiInterface::class.java).requestInstagramApi(authToken)
                , object : Observer<Response<InstagramResponse>> {
            override fun onComplete() {
                Log.d(TAG, "requestInstagramAPI -> onComplete")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "requestInstagramAPI -> onSubscribe")
                DisposableManager.add(d)
            }

            override fun onNext(t: Response<InstagramResponse>) {
                Log.d(TAG, "requestInstagramAPI -> onNext -> code = ${t.code()}")
                Log.d(TAG, "requestInstagramAPI -> onNext -> body = ${t.body()}")

                if (t.body() != null) {
                    if (t.body()!!.instagramUserData != null) {
                        Log.d(TAG, "requestInstagramAPI -> onNext -> data = ${t.body()!!.instagramUserData}")
                        socialLogin(
                                t.body()!!.instagramUserData!!.full_name,
                                "${t.body()!!.instagramUserData!!.username}@instagram.com",
                                fcm_token,
                                authToken,
                                INSTAGRAM
                        )
                    } else
                        Log.d(TAG, "requestInstagramAPI -> onNext -> instagramUserData = null")
                }

                view.changeLoginProcessStatus(false)
            }

            override fun onError(e: Throwable) {
                Log.d(TAG, "requestInstagramAPI -> onError -> error = ${e.message}")
                view.changeLoginProcessStatus(false)
            }
        })
    }

    override fun unsubscribe() {
        super.unsubscribe()
        DisposableManager.dispose()
    }
}