package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class Facility (
        @SerializedName("id")
        val id: Long,
        @SerializedName("name")
        val name: String,
        @SerializedName("image")
        val image: String,
        @SerializedName("type")
        val type: Int,
        @SerializedName("avg_rate")
        val avg_rate: Float
)