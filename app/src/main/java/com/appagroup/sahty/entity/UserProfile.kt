package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class UserProfile (
        @SerializedName("userId")
        val id: Long,
//        @SerializedName("user_name")
//        val name: String,
//        @SerializedName("user_image")
//        val image: String,
        @SerializedName("user_certificate")
        val accreditations: MutableList<Accreditation>?,
        @SerializedName("follows_count")
        val followingCount: Int,
        @SerializedName("followsIdsName")
        val following: MutableList<Follow>,
        @SerializedName("followers_count")
        val followersCount: Int,
        @SerializedName("followersIdsName")
        val followers: MutableList<Follow>
)