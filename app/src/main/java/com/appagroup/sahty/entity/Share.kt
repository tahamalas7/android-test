package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class Share (
        @SerializedName("from")
        val fromId: Long,
        @SerializedName("to")
        val toId: Long,
        @SerializedName("share")
        val status: Boolean
)