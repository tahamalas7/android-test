package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

class OpeningHours (
        @SerializedName("id")
        val id: Long,
        @SerializedName("day")
        val day: String,
        @SerializedName("start_time")
        val start: String,
        @SerializedName("end_time")
        val end: String
)