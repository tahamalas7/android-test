package com.appagroup.sahty.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "categories")
class Category (
        @PrimaryKey
        @SerializedName("id")
        val id: Long,
        @SerializedName("title")
        val name: String,
        @SerializedName("image")
        val image: String?
) : Serializable