package com.appagroup.sahty.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "pharmacies")
class Pharmacy (
        @PrimaryKey
        @SerializedName("id")
        val id: Long,
        @SerializedName("name")
        val name: String,
        @SerializedName("location")
        val location: String,
        @SerializedName("image")
        val image: String?,
        @SerializedName("rate")
        val rate: Float? = 0F,
        var isInsight: Boolean = false
): Serializable