package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class PollOption (
        @SerializedName("id")
        val id: Long,
        @SerializedName("name")
        val name: String,
        @SerializedName("poll_id")
        val pollId: Long,
        @SerializedName("vote_count")
        var voteCount: Int
): Serializable