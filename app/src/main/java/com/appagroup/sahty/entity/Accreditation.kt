package com.appagroup.sahty.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Accreditation (
        @SerializedName("id")
        var id: Long = 0,
        @SerializedName("type")
        var type: String? = "",
        @SerializedName("specialization")
        var speciality: String? = "",
        @SerializedName("specialization_id")
        var specializationId: Long = 0,
        @SerializedName("year")
        var year: String? = "",
        @SerializedName("image")
        var image: String? = "",
        @SerializedName("location")
        var location: String? = "",
        @SerializedName("primary")
        var isPrimary: Boolean = false,
        var specialityId: Long
) : Serializable, Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readLong(),
                parcel.readString(),
                parcel.readString(),
                parcel.readLong(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readByte() != 0.toByte(),
                parcel.readLong())

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeLong(id)
                parcel.writeString(type)
                parcel.writeString(speciality)
                parcel.writeLong(specialityId)
                parcel.writeString(year)
                parcel.writeString(image)
                parcel.writeString(location)
                parcel.writeByte(if (isPrimary) 1 else 0)
                parcel.writeLong(specialityId)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<Accreditation> {
                override fun createFromParcel(parcel: Parcel): Accreditation {
                        return Accreditation(parcel)
                }

                override fun newArray(size: Int): Array<Accreditation?> {
                        return arrayOfNulls(size)
                }
        }
}