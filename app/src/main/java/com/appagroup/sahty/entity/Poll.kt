package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Poll (
        @SerializedName("id")
        val id: Long,
        @SerializedName("question")
        val question: String,
        @SerializedName("vote_count")
        var totalVotes: Int,
        @SerializedName("isVoted")
        var beenVoted: Boolean,
        @SerializedName("option_id")
        var selectedOptionId: Long? = 0,
        @SerializedName("options")
        val options: MutableList<PollOption>
): Serializable