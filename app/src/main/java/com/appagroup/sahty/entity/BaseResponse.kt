package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

class BaseResponse<T> (@SerializedName("status")
                       val status: Boolean = false,
                       @SerializedName("message")
                       val message: String,
                       @SerializedName("data")
                       val data: T?)
