package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

class Specialty (
        @SerializedName("id")
        val id: Long,
        @SerializedName("name")
        val name: String
)
