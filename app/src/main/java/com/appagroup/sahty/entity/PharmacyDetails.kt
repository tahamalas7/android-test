package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

class PharmacyDetails (
        @SerializedName("id")
        val id: Long,
        @SerializedName("name")
        val name: String,
        @SerializedName("location")
        val location: String,
        @SerializedName("contact_info")
        val contactInfo: String,
        @SerializedName("image")
        val image: String?,
        @SerializedName("avg_rate")
        val rate: Float? = 0F,
        @SerializedName("latitude")
        val latitude: Double,
        @SerializedName("longitude")
        val longitude : Double,
        @SerializedName("isRate")
        var beenRated: Boolean = false,
        @SerializedName("times")
        val openingHours: MutableList<OpeningHours>?
)