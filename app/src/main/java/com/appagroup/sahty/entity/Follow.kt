package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class Follow (
        @SerializedName("user_id")
        val id: Long,
        @SerializedName("name")
        val name: String,
        @SerializedName("image")
        val image: String?,
        @SerializedName("isFollowed")
        var isFollowed: Boolean
)