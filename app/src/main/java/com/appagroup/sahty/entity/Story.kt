package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class Story (
        @SerializedName("id")
        val id: Long,
        @SerializedName("title")
        val title: String,
        @SerializedName("content")
        val content: String,
        @SerializedName("category_id")
        val category_id: Long = 0,
        @SerializedName("media")
        val media: Media?,
        @SerializedName("writer")
        val writer: Writer
)