package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

class ArticleContent (
        @SerializedName("id")
        val id: Long,
        @SerializedName("title")
        val title: String,
        @SerializedName("content")
        val content: String,
        @SerializedName("category_id")
        val category_id: Long = 0,
        @SerializedName("likes")
        var likeCount: Int,
        @SerializedName("isLiked")
        val isLiked: Boolean,
        @SerializedName("isSaved")
        var isSaved: Boolean,
        @SerializedName("type") // article = 1, story = 2
        val type: Int,
        @SerializedName("created_at")
        val date: String,
        @SerializedName("image")
        val media: MutableList<Media>?,
        @SerializedName("writer")
        val writer: Writer
)