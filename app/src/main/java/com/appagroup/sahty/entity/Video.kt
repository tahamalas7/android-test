package com.appagroup.sahty.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "videos")
class Video(
        @PrimaryKey
        @SerializedName("id")
        val id: Long,
        @SerializedName("url")
        val url: String,
        @SerializedName("title")
        val title: String?,
        @SerializedName("type")
        val type: Int,
        @SerializedName("thumbs")
        val thumbnail: String?,
        @SerializedName("writer")
        val writer: Writer,
        @SerializedName("isSaved")
        var isSaved: Boolean,
        var isInsight: Boolean
): Serializable