package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

class SendReplyResponse (
        @SerializedName("id")
        val id: Long,
        @SerializedName("content")
        val content: String,
        @SerializedName("sender_id")
        val senderId: Long,
        @SerializedName("receiver_id")
        val receiverId: Long
)