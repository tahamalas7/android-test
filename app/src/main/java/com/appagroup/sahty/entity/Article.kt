package com.appagroup.sahty.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "articles")
class Article(
        @PrimaryKey
        @SerializedName("id")
        val id: Long,
        @SerializedName("title")
        val title: String,
        @SerializedName("content")
        val content: String,
        @SerializedName("category_id")
        val category_id: Long = 0,
        @SerializedName("media")
        val media: Media?,
        @SerializedName("writer")
        val writer: Writer,
        var isInsight: Boolean = false
) : Serializable
