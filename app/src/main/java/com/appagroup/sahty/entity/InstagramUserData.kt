package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class InstagramUserData (
        @SerializedName("id")
        val id: Long,
        @SerializedName("username")
        val username: String,
        @SerializedName("full_name")
        val full_name: String,
        @SerializedName("profile_picture")
        val profile_picture: String
)