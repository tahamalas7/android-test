package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

class Insights (
    @SerializedName("last story")
    val lastStory: Article?,
    @SerializedName("last video")
    val lastVideo: Video?,
    @SerializedName("article")
    val topArticles: MutableList<Article>,
    @SerializedName("poll interactive")
    val interactivePoll: Poll?,
    @SerializedName("nearest pharmacy")
    val nearestPharmacy: Pharmacy?,
    @SerializedName("nearest hospital")
    val nearestHospital: Hospital?
)