package com.appagroup.sahty.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "writers")
class Writer (
        @PrimaryKey
        @SerializedName("user_id")
        val id: Long,
        @SerializedName("user_name")
        val name: String,
        @SerializedName("user_image")
        val image: String?,
        @SerializedName("isFollowed")
        var isFollowed: Boolean
): Serializable