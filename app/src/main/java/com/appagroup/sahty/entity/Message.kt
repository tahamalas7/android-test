package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class Message (
        @SerializedName("id")
        val messageId: Long,
        @SerializedName("content")
        var content: String,
        @SerializedName("fromId")
        val senderId: Long,
        @SerializedName("from")
        val senderName: String,
        @SerializedName("fromPicture")
        val senderPicture: String?,
        @SerializedName("fromRole")
        val senderRole: Int,
        @SerializedName("toId")
        val receiverId: Long,
        @SerializedName("to")
        val receiverName: String,
        @SerializedName("toPicture")
        val receiverPicture: String?,
        @SerializedName("toRole")
        val receiverRole: Int,
        @SerializedName("date")
        var date: String
)