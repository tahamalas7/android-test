package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

class Feedback (
        @SerializedName("id")
        val id: Long,
        @SerializedName("content")
        val content: String,
        @SerializedName("rate")
        val rate: Float
)