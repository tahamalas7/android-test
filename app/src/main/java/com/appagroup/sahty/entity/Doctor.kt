package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName


class Doctor (
        @SerializedName("id")
        val id: Long,
        @SerializedName("name")
        val name: String,
        @SerializedName("image")
        val image: String?,
        @SerializedName("specialization")
        val specialization: String
)