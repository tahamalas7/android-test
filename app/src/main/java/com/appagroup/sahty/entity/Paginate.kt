package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

class Paginate<T> (
        @SerializedName("data")
        val data: T?,
        @SerializedName("current_page")
        val current_page: Int,
        @SerializedName("last_page")
        val last_page: Int
)