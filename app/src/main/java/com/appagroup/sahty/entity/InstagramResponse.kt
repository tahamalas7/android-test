package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class InstagramResponse (
        @SerializedName("data")
        val instagramUserData: InstagramUserData?
)