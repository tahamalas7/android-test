package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class GeneralSearchResult (
        @SerializedName("facilities")
        val facilities: MutableList<Facility>?,
        @SerializedName("articles")
        val articles: MutableList<Article>?,
        @SerializedName("doctors")
        val doctors: MutableList<Doctor>?
)