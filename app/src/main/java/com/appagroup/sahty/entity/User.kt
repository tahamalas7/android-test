package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class User(
        @SerializedName("id")
        val id: Long,
        @SerializedName("name")
        val name: String,
        @SerializedName("email")
        val email: String,
        @SerializedName("available")
        val availability: Int,
        @SerializedName("image")
        val image: String?,
        @SerializedName("phone_number")
        val phoneNumber: String?,
        @SerializedName("role")
        val role: String,
        @SerializedName("provider")
        val provider: String?,
        @SerializedName("token")
        val access_token: String
)