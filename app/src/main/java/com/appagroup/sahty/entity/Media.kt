package com.appagroup.sahty.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
class Media (
        @PrimaryKey
        @SerializedName("id")
        val id: Long,
        @SerializedName("url")
        val url: String,
        @SerializedName("type")
        val type: String,
        @SerializedName("thumbs")
        val thumbnail: String?
): Serializable