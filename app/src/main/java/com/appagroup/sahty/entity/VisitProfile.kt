package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class VisitProfile (
        @SerializedName("userId")
        val id: Long,
        @SerializedName("user_name")
        val name: String,
        @SerializedName("user_image")
        val image: String?,
        @SerializedName("user_role")
        val role: String,
        @SerializedName("isFollowing")
        val isFollowed: Boolean,
        @SerializedName("user_specialization")
        val specializations: MutableList<Specialty>?,
        @SerializedName("user_certificate")
        val accreditations: MutableList<Accreditation>?,
        @SerializedName("following")
        val following: MutableList<Follow>?,
        @SerializedName("followers")
        val followers: MutableList<Follow>?
)