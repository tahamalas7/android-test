package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MedicalFileGroup (
        @SerializedName("id")
        var id: Long,
        @SerializedName("name")
        val title: String,
        @SerializedName("files_count")
        val filesCount: Int = 0
) : Serializable