package com.appagroup.sahty.entity

class MenuItem (
        val id: Int,
        val name: Int,
        val icon: Int
)