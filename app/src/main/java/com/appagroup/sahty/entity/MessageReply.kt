package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class MessageReply (
        @SerializedName("id")
        val messageId: Long,
        @SerializedName("content")
        val content: String,
        @SerializedName("fromId")
        val fromId: Long,
        @SerializedName("from")
        val fromName: String,
        @SerializedName("fromRole")
        val fromRole: Int,
        @SerializedName("toId")
        val toId: Long,
        @SerializedName("to")
        val toName: String,
        @SerializedName("toRole")
        val toRole: Int,
        @SerializedName("date")
        val date: String
)