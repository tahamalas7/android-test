package com.appagroup.sahty.entity

import com.google.gson.annotations.SerializedName

data class Specialization (
        @SerializedName("id")
        val id: Long,
        @SerializedName("name")
        val name: String
)