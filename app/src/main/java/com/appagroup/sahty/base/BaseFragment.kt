package com.appagroup.sahty.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.appagroup.sahty.R
import com.appagroup.sahty.utils.UIUtils
import io.reactivex.disposables.Disposable

abstract class BaseFragment<P : IBasePresenter<IBaseView>> : IBaseView, Fragment() {

    private val TAG = "BaseFragment"

    protected lateinit var presenter: P

    lateinit var disposable: Disposable

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(getLayout(), container, false)
        presenter = instantiatePresenter()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewReady(view)
    }

    protected fun onViewReady(view: View) {
        if (UIUtils.checkInternetConnection(activity!!))
            initViews(view)
    }

    override fun retrieveContext() = activity!!

    override fun showMessageDialog(messageId: Int) {
        AlertDialog.Builder(activity!!)
                .setMessage(getString(messageId))
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
    }

    override fun showError(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun showError(messageId: Int) {
        Toast.makeText(activity, getString(messageId), Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()
    }

    protected abstract fun instantiatePresenter(): P

    protected abstract fun getLayout(): Int

    protected abstract fun initViews(view: View)

}