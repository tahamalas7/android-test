package com.appagroup.sahty.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import com.appagroup.sahty.R
import com.appagroup.sahty.utils.UIUtils

abstract class BaseDialog<P : IBasePresenter<IBaseView>> : IBaseView, DialogFragment() {

    private val TAG = "BaseDialog"

    protected lateinit var presenter: P

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.Theme_AppCompat_Light_Dialog_Alert)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(getLayout(), container, false)
        presenter = instantiatePresenter()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (UIUtils.checkInternetConnection(activity!!))
            initViews(view)
    }

    override fun retrieveContext(): Context = activity!!

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()
    }

    override fun showMessageDialog(messageId: Int) {
        AlertDialog.Builder(activity!!)
                .setMessage(getString(messageId))
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
    }

    override fun showError(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    protected abstract fun instantiatePresenter(): P

    protected abstract fun initViews(view: View)

    protected abstract fun getLayout(): Int
}