package com.appagroup.sahty.base

import android.content.Context
import androidx.annotation.StringRes

interface IBaseView {

    fun retrieveContext(): Context

    fun showMessageDialog(messageId: Int)

    fun showError(@StringRes messageId: Int) {
        retrieveContext().resources.getString(messageId)
    }

    fun showError(message: String) {}
}