package com.appagroup.sahty.base

import com.appagroup.sahty.utils.DisposableManager
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

open class BasePresenter<out V : IBaseView>(protected val view: V) : DataManagerProvider(), IBasePresenter<V> {

    private val TAG = "BasePresenter"

//    @Inject
//    protected lateinit var dataManager: DataManager
//
//    val injector: PresenterInjector = DaggerPresenterInjector
//            .builder()
//            .getContext(App.instance!!.applicationContext)
//            .applicationModule(ApplicationModule)
//            .build()
//
//    init {
//        inject()
//    }

    override fun <T> subscribe(observable: Observable<T>, observer: Observer<T>) {
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer)
    }

    override fun unsubscribe() {
        DisposableManager.dispose()
    }

    override fun retrieveView() = view

    /**
     * Injects the required dependencies
     */
//    private fun inject() {
//        when (this) {
//            is LoginPresenter -> injector.inject(this)
//
//            is SignUpPresenter -> injector.inject(this)
//
//            is MainPresenter -> injector.inject(this)
//
//            is HomePresenter -> injector.inject(this)
//
//            is ArticlesPresenter -> injector.inject(this)
//        }
//    }

}