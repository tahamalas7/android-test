package com.appagroup.sahty.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.annotation.CallSuper
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.appagroup.sahty.R
import com.appagroup.sahty.utils.UIUtils
import io.reactivex.disposables.Disposable

abstract class BaseActivity<P : IBasePresenter<IBaseView>> : IBaseView, AppCompatActivity() {

    private val TAG = "BaseActivity"

    protected lateinit var presenter: P

    lateinit var disposable: Disposable

    private var isBackFromBackground = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        if (this !is SplashActivity) {
//            setTheme(R.style.AppTheme)
        setContentView(getContentView())
//        }

        presenter = instantiatePresenter()

        onViewReady(savedInstanceState, intent)
    }

    @CallSuper
    protected open fun onViewReady(savedInstanceState: Bundle?, intent: Intent?) {
        //To be used by child activities
        if (UIUtils.checkInternetConnection(this))
            initViews()
    }

    override fun showError(messageId: Int) {
        Toast.makeText(this, getString(messageId), Toast.LENGTH_SHORT).show()
    }

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()

//        if(ApplicationLifecycleHandler.isApplicationVisible()) {
//            val serviceIntent = Intent(this, BackgroundSoundService::class.java)
//            serviceIntent.action = BackgroundSoundService.ACTION_PAUSE
//            startService(serviceIntent)
//        }
    }

    override fun onResume() {
        super.onResume()

//        if (ApplicationLifecycleHandler.isApplicationVisible() && isBackFromBackground) {
//            val serviceIntent = Intent(this, BackgroundSoundService::class.java)
//            serviceIntent.action = BackgroundSoundService.ACTION_RESUME
//            startService(serviceIntent)
//            isBackFromBackground = false
//        } else
//            isBackFromBackground = true
    }

    override fun showMessageDialog(messageId: Int) {
        AlertDialog.Builder(this)
                .setMessage(getString(messageId))
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
    }

    override fun showError(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun retrieveContext(): Context = this

    protected abstract fun instantiatePresenter(): P

    protected abstract fun initViews()

    protected abstract fun getContentView(): Int

}