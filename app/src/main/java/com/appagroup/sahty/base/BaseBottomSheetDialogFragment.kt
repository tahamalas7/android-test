package com.appagroup.sahty.base

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.appagroup.sahty.R
import com.appagroup.sahty.utils.UIUtils
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

abstract class BaseBottomSheetDialogFragment<P : IBasePresenter<IBaseView>> : BottomSheetDialogFragment(), IBaseView {

    private val TAG = "BaseBottomSheetDialogFragment"

    protected lateinit var presenter: P

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(getLayout(), container, false)
        presenter = instantiatePresenter()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onViewReady(savedInstanceState, view)
    }

    protected fun onViewReady(savedInstanceState: Bundle?, view: View) {
        if (UIUtils.checkInternetConnection(activity!!))
            initViews(view)
    }

    override fun showMessageDialog(messageId: Int) {
        AlertDialog.Builder(activity!!)
                .setMessage(getString(messageId))
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
    }

    override fun showError(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun retrieveContext(): Context = activity!!

    override fun onPause() {
        super.onPause()
        presenter.unsubscribe()
    }

    fun showAlertDialog(message: Int) {
        AlertDialog.Builder(activity!!)
                .setMessage(getString(message))
                .setPositiveButton(getString(R.string.ok)) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
    }

    protected abstract fun instantiatePresenter(): P

    protected abstract fun getLayout(): Int

    protected abstract fun initViews(view: View)
}