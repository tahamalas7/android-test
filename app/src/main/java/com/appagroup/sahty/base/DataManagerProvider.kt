package com.appagroup.sahty.base

import com.appagroup.sahty.app.App
import com.appagroup.sahty.data.DataManager
import com.appagroup.sahty.di.component.DaggerPresenterInjector
import com.appagroup.sahty.di.component.PresenterInjector
import com.appagroup.sahty.di.module.ApplicationModule
import com.appagroup.sahty.ui.FollowService
import com.appagroup.sahty.ui.SaveService
import com.appagroup.sahty.ui.accreditation_dialog.AccreditationPresenter
import com.appagroup.sahty.ui.articles.ArticlesPresenter
import com.appagroup.sahty.ui.articles.article_content.ArticleContentPresenter
import com.appagroup.sahty.ui.facilities.hospitals.HospitalsPresenter
import com.appagroup.sahty.ui.facilities.hospitals.hospital_details.HospitalDetailsPresenter
import com.appagroup.sahty.ui.facilities.pharmacies.PharmaciesPresenter
import com.appagroup.sahty.ui.facilities.pharmacies.pharmacy_details.PharmacyDetailsPresenter
import com.appagroup.sahty.ui.facilities.rating_dialog.RatingPresenter
import com.appagroup.sahty.ui.home.HomePresenter
import com.appagroup.sahty.ui.login.LoginPresenter
import com.appagroup.sahty.ui.login.select_role.SelectRolePresenter
import com.appagroup.sahty.ui.main.MainPresenter
import com.appagroup.sahty.ui.medical_files.MyMedicalFilesPresenter
import com.appagroup.sahty.ui.medical_files.group_content.MedicalFileGroupContentPresenter
import com.appagroup.sahty.ui.medical_files.group_content.import_files.ImportFilesPresenter
import com.appagroup.sahty.ui.messages.MessagesPresenter
import com.appagroup.sahty.ui.messages.create_message.CreateMessagePresenter
import com.appagroup.sahty.ui.messages.message_content.MessageContentPresenter
import com.appagroup.sahty.ui.messages.patient_groups.PatientFileGroupsPresenter
import com.appagroup.sahty.ui.messages.patient_groups.group_content.GroupContentPresenter
import com.appagroup.sahty.ui.messages.select_doctor.SelectDoctorPresenter
import com.appagroup.sahty.ui.poll.PollsPresenter
import com.appagroup.sahty.ui.post_article.PostArticlePresenter
import com.appagroup.sahty.ui.post_article.categories_dialog.SelectCategoryPresenter
import com.appagroup.sahty.ui.post_story.PostStoryPresenter
import com.appagroup.sahty.ui.profile.ProfilePresenter
import com.appagroup.sahty.ui.profile.edit_profile.EditProfilePresenter
import com.appagroup.sahty.ui.saved.saved_articles.SavedArticlesPresenter
import com.appagroup.sahty.ui.saved.saved_stories.SavedStoriesPresenter
import com.appagroup.sahty.ui.saved.saved_videos.SavedVideosPresenter
import com.appagroup.sahty.ui.search.SearchPresenter
import com.appagroup.sahty.ui.settings.SettingsPresenter
import com.appagroup.sahty.ui.signup.SignUpPresenter
import com.appagroup.sahty.ui.videos.VideosPresenter
import com.appagroup.sahty.ui.visit_profile.VisitProfilePresenter
import javax.inject.Inject

open class DataManagerProvider {

    @Inject
    protected lateinit var dataManager: DataManager

    private val injector: PresenterInjector = DaggerPresenterInjector
            .builder()
            .getContext(App.instance!!.applicationContext)
            .applicationModule(ApplicationModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects required dependencies
     */
    private fun inject() {
        when (this) {
            is LoginPresenter -> injector.inject(this)

            is SignUpPresenter -> injector.inject(this)

            is MainPresenter -> injector.inject(this)

            is HomePresenter -> injector.inject(this)

            is ArticlesPresenter -> injector.inject(this)

            is ArticleContentPresenter -> injector.inject(this)

            is FollowService -> injector.inject(this)

            is SaveService -> injector.inject(this)

            is VideosPresenter -> injector.inject(this)

            is PharmaciesPresenter -> injector.inject(this)

            is HospitalsPresenter -> injector.inject(this)

            is HospitalDetailsPresenter -> injector.inject(this)

            is PharmacyDetailsPresenter -> injector.inject(this)

            is RatingPresenter -> injector.inject(this)

            is MyMedicalFilesPresenter -> injector.inject(this)

            is MedicalFileGroupContentPresenter -> injector.inject(this)

            is ImportFilesPresenter -> injector.inject(this)

            is MessagesPresenter -> injector.inject(this)

            is MessageContentPresenter -> injector.inject(this)

            is SelectDoctorPresenter -> injector.inject(this)

            is CreateMessagePresenter -> injector.inject(this)

            is PatientFileGroupsPresenter -> injector.inject(this)

            is GroupContentPresenter -> injector.inject(this)

            is PollsPresenter -> injector.inject(this)

            is SavedArticlesPresenter -> injector.inject(this)

            is SavedVideosPresenter -> injector.inject(this)

            is SavedStoriesPresenter -> injector.inject(this)

            is ProfilePresenter -> injector.inject(this)

            is EditProfilePresenter -> injector.inject(this)

            is AccreditationPresenter -> injector.inject(this)

            is PostArticlePresenter -> injector.inject(this)

            is PostStoryPresenter -> injector.inject(this)

            is SelectCategoryPresenter -> injector.inject(this)

            is SettingsPresenter -> injector.inject(this)

            is SearchPresenter -> injector.inject(this)

            is VisitProfilePresenter -> injector.inject(this)

            is SelectRolePresenter -> injector.inject(this)
        }
    }
}