package com.appagroup.sahty.di.module

import android.content.Context
import androidx.room.Room
import com.appagroup.sahty.BuildConfig
import com.appagroup.sahty.data.DataManager
import com.appagroup.sahty.data.db.AppDao
import com.appagroup.sahty.data.db.AppDatabase
import com.appagroup.sahty.data.db.AppRepository
import com.appagroup.sahty.data.db.DbHelper
import com.appagroup.sahty.data.http.ApiInterface
import com.appagroup.sahty.data.http.HttpHelper
import com.appagroup.sahty.data.http.RetrofitHelper
import com.appagroup.sahty.data.prefs.PrefsHelper
import com.appagroup.sahty.data.prefs.SharedPreferences
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton


@Module
@Suppress("unused")
object ApplicationModule {

//    @Provides
//    @JvmStatic
//    internal fun provideContext(baseView: IBaseView): Context = baseView.retrieveContext()

    @Provides
    @Singleton
    fun shared(context: Context): android.content.SharedPreferences {
        return context.getSharedPreferences("SahtySharedPreferences", Context.MODE_PRIVATE)
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(shared: android.content.SharedPreferences): PrefsHelper =
            SharedPreferences(shared)

    @Provides
    @Singleton
    fun provideGsonConverterFactory(): GsonConverterFactory {
        val gson = GsonBuilder()
                .setLenient()
                .create()
        return GsonConverterFactory.create(gson)
    }

    @Provides
    @Singleton
    fun provideRxJava2CallAdapterFactory() =
            RxJava2CallAdapterFactory.create()

    @Provides
    @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        return OkHttpClient.Builder()
                .connectTimeout(20, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofitInstance(okHttpClient: OkHttpClient, gsonConverterFactory: GsonConverterFactory, rxJava2CallAdapterFactory: RxJava2CallAdapterFactory): Retrofit =
            Retrofit.Builder()
                    .addConverterFactory(gsonConverterFactory)
                    .addCallAdapterFactory(rxJava2CallAdapterFactory)
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.API_URL)
                    .build()

    @Provides
    @Singleton
    fun provideApiInterface(retrofit: Retrofit) =
            retrofit.create(ApiInterface::class.java)

    @Provides
    @Singleton
    fun provideHttpHelper(apiInterface: ApiInterface) : HttpHelper =
            RetrofitHelper(apiInterface)

    @Provides
    @Singleton
    @Named("dbName")
    fun provideDbName(): String {
        return "SahtyRoomDb"
    }

    @Provides
    @Singleton
    fun provideAppDao(appDatabase: AppDatabase): AppDao {
        return appDatabase.appDao()
    }

    @Provides
    @Singleton
    fun provideAppDatabase(context: Context, @Named("dbName") dbName: String): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, dbName)
                .fallbackToDestructiveMigration()
                .build()
    }

    @Provides
    @Singleton
    fun provideAppRepository(appDao: AppDao): DbHelper {
        return AppRepository(appDao)
    }

    @Provides
    @Singleton
    fun provideDataManager(dbHelper: DbHelper, httpHelper: HttpHelper, prefsHelper: PrefsHelper) =
            DataManager(dbHelper, httpHelper, prefsHelper)
}