package com.appagroup.sahty.di.component

import android.content.Context
import com.appagroup.sahty.ui.SaveService
import com.appagroup.sahty.di.module.ApplicationModule
import com.appagroup.sahty.ui.FollowService
import com.appagroup.sahty.ui.accreditation_dialog.AccreditationPresenter
import com.appagroup.sahty.ui.articles.ArticlesPresenter
import com.appagroup.sahty.ui.articles.article_content.ArticleContentPresenter
import com.appagroup.sahty.ui.facilities.hospitals.HospitalsPresenter
import com.appagroup.sahty.ui.facilities.hospitals.hospital_details.HospitalDetailsPresenter
import com.appagroup.sahty.ui.facilities.pharmacies.PharmaciesPresenter
import com.appagroup.sahty.ui.facilities.pharmacies.pharmacy_details.PharmacyDetailsPresenter
import com.appagroup.sahty.ui.facilities.rating_dialog.RatingPresenter
import com.appagroup.sahty.ui.home.HomePresenter
import com.appagroup.sahty.ui.login.LoginPresenter
import com.appagroup.sahty.ui.login.select_role.SelectRolePresenter
import com.appagroup.sahty.ui.main.MainPresenter
import com.appagroup.sahty.ui.medical_files.MyMedicalFilesPresenter
import com.appagroup.sahty.ui.medical_files.group_content.MedicalFileGroupContentPresenter
import com.appagroup.sahty.ui.medical_files.group_content.import_files.ImportFilesPresenter
import com.appagroup.sahty.ui.messages.MessagesPresenter
import com.appagroup.sahty.ui.messages.create_message.CreateMessagePresenter
import com.appagroup.sahty.ui.messages.message_content.MessageContentPresenter
import com.appagroup.sahty.ui.messages.patient_groups.PatientFileGroupsPresenter
import com.appagroup.sahty.ui.messages.patient_groups.group_content.GroupContentPresenter
import com.appagroup.sahty.ui.messages.select_doctor.SelectDoctorPresenter
import com.appagroup.sahty.ui.poll.PollsPresenter
import com.appagroup.sahty.ui.post_article.PostArticlePresenter
import com.appagroup.sahty.ui.post_article.categories_dialog.SelectCategoryPresenter
import com.appagroup.sahty.ui.post_story.PostStoryPresenter
import com.appagroup.sahty.ui.profile.ProfilePresenter
import com.appagroup.sahty.ui.profile.edit_profile.EditProfilePresenter
import com.appagroup.sahty.ui.saved.saved_articles.SavedArticlesPresenter
import com.appagroup.sahty.ui.saved.saved_stories.SavedStoriesPresenter
import com.appagroup.sahty.ui.saved.saved_videos.SavedVideosPresenter
import com.appagroup.sahty.ui.search.SearchPresenter
import com.appagroup.sahty.ui.settings.SettingsPresenter
import com.appagroup.sahty.ui.signup.SignUpPresenter
import com.appagroup.sahty.ui.videos.VideosPresenter
import com.appagroup.sahty.ui.visit_profile.VisitProfilePresenter
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class)])
interface PresenterInjector {

    fun inject(loginPresenter: LoginPresenter)

    fun inject(loginPresenter: SignUpPresenter)

    fun inject(mainPresenter: MainPresenter)

    fun inject(homePresenter: HomePresenter)

    fun inject(articlesPresenter: ArticlesPresenter)

    fun inject(articleContentPresenter: ArticleContentPresenter)

    fun inject(followService: FollowService)

    fun inject(saveService: SaveService)

    fun inject(videosPresenter: VideosPresenter)

    fun inject(pharmaciesPresenter: PharmaciesPresenter)

    fun inject(hospitalsPresenter: HospitalsPresenter)

    fun inject(hospitalDetailsPresenter: HospitalDetailsPresenter)

    fun inject(pharmacyDetailsPresenter: PharmacyDetailsPresenter)

    fun inject(ratingPresenter: RatingPresenter)

    fun inject(myMedicalFilesPresenter: MyMedicalFilesPresenter)

    fun inject(medicalFileGroupContentPresenter: MedicalFileGroupContentPresenter)

    fun inject(importFilesPresenter: ImportFilesPresenter)

    fun inject(messagesPresenter: MessagesPresenter)

    fun inject(messageContentPresenter: MessageContentPresenter)

    fun inject(selectDoctorPresenter: SelectDoctorPresenter)

    fun inject(createMessagePresenter: CreateMessagePresenter)

    fun inject(patientFileGroupsPresenter: PatientFileGroupsPresenter)

    fun inject(groupContentPresenter: GroupContentPresenter)

    fun inject(pollsPresenter: PollsPresenter)

    fun inject(savedArticlesPresenter: SavedArticlesPresenter)

    fun inject(savedVideosPresenter: SavedVideosPresenter)

    fun inject(savedStoriesPresenter: SavedStoriesPresenter)

    fun inject(profilePresenter: ProfilePresenter)

    fun inject(editProfilePresenter: EditProfilePresenter)

    fun inject(accreditationPresenter: AccreditationPresenter)

    fun inject(postArticlePresenter: PostArticlePresenter)

    fun inject(postStoryPresenter: PostStoryPresenter)

    fun inject(selectCategoryPresenter: SelectCategoryPresenter)

    fun inject(settingsPresenter: SettingsPresenter)

    fun inject(searchPresenter: SearchPresenter)

    fun inject(visitProfilePresenter: VisitProfilePresenter)

    fun inject(selectRolePresenter: SelectRolePresenter)

    @Component.Builder
    interface Builder {
        fun build(): PresenterInjector

        fun applicationModule(applicationModule: ApplicationModule): Builder

//        @BindsInstance
//        fun baseView(baseView: IBaseView): Builder
        @BindsInstance
        fun getContext(context: Context): Builder
    }
}