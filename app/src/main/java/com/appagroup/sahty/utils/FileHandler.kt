package com.appagroup.sahty.utils

import android.net.Uri
import android.util.Log
import android.content.Intent


object FileHandler {

    /** define supported extensions */
    private var pdfExtension = "pdf"
    private var textExtension = "txt"
    private var videoExtension = "mp4"
    private var imageExtensions: Array<String> = arrayOf("png", "jpg", "jpeg")
    private var excelExtensions: Array<String> = arrayOf("csv", "xls", "xlsm", "xlsx")
    private var wordExtensions: Array<String> = arrayOf("doc", "docx")

    /** check file extension and return the appropriate intent to open it */
    fun getFileIntent(fileUri: String): Intent {
        val ext = getFileExtension(fileUri)
//        val file = File(BuildConfig.IMAGE + fileUri)
        val uri = Uri.parse(fileUri)
        val intent = Intent()

        Log.d("presenter", "click -> FILE URL tooooo = $uri")

        intent.setDataAndType(uri, when {
            ext == pdfExtension -> "application/pdf"
            ext == textExtension -> "text/plain"
            ext == videoExtension -> "video/*"
            imageExtensions.contains(ext) -> "image/*"
            excelExtensions.contains(ext) -> "application/vnd.ms-excel"
            wordExtensions.contains(ext) -> "application/msword"
            else -> "*/*"
        })
        intent.action = Intent.ACTION_VIEW

        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        return intent
    }

    /** check whether the file extension is supported */
    fun isFileValid(fileUri: String): Boolean {
        val ext = getFileExtension(fileUri)
        return when {
            ext == pdfExtension -> true
            ext == textExtension -> true
            ext == videoExtension -> true
            imageExtensions.contains(ext) -> true
            excelExtensions.contains(ext) -> true
            wordExtensions.contains(ext) -> true
            else -> false
        }
    }

    /** used to return file extension */
    private fun getFileExtension(url: String): String {
        Log.d("FileHandler", "Extension = ${url.substring(url.lastIndexOf('.') + 1, url.length)}")
        return url.substring(url.lastIndexOf('.') + 1, url.length).toLowerCase()
    }
}