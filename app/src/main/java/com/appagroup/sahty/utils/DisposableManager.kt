package com.appagroup.sahty.utils

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

object DisposableManager {

    private var profileDisposable: CompositeDisposable? = null

    private var saveDisposable: CompositeDisposable? = null

    private var followDisposable: CompositeDisposable? = null

    private var articlesDisposable: CompositeDisposable? = null

    private var homeDisposable: CompositeDisposable? = null

    private var videosDisposable: CompositeDisposable? = null

    private var facilitiesDisposable: CompositeDisposable? = null

    private var searchDisposable: CompositeDisposable? = null

    private var medicalFilesDisposable: CompositeDisposable? = null

    private var compositeDisposable: CompositeDisposable? = null

    private var uploadDisposable: CompositeDisposable? = null


    fun addProfileDisposable(disposable: Disposable) {
        getProfileDisposable().add(disposable)
    }

    fun disposeProfile() {
        getProfileDisposable().dispose()
    }


    fun addSaveDisposable(disposable: Disposable) {
        getArticlesDisposable().add(disposable)
    }

    fun disposeSave() {
        getArticlesDisposable().dispose()
    }


    fun addFollowDisposable(disposable: Disposable) {
        getHomeDisposable().add(disposable)
    }

    fun disposeFollow() {
        getHomeDisposable().dispose()
    }


    fun addArticlesDisposable(disposable: Disposable) {
        getArticlesDisposable().add(disposable)
    }

    fun disposeArticles() {
        getArticlesDisposable().dispose()
    }


    fun addHomeDisposable(disposable: Disposable) {
        getHomeDisposable().add(disposable)
    }

    fun disposeHome() {
        getHomeDisposable().dispose()
    }


    fun addVideosDisposable(disposable: Disposable) {
        getVideosDisposable().add(disposable)
    }

    fun disposeVideos() {
        getVideosDisposable().dispose()
    }


    fun addFacilitiesDisposable(disposable: Disposable) {
        getFacilitiesDisposable().add(disposable)
    }

    fun disposeFacilities() {
        getFacilitiesDisposable().dispose()
    }


    fun addMedicalFilesDisposable(disposable: Disposable) {
        getMedicalFilesDisposable().add(disposable)
    }

    fun disposeMedicalFiles() {
        getMedicalFilesDisposable().dispose()
    }


    fun addSearchDisposable(disposable: Disposable) {
        getSearchDisposable().add(disposable)
    }

    fun disposeSearch() {
        getSearchDisposable().dispose()
    }


    fun add(disposable: Disposable) {
        getCompositeDisposable().add(disposable)
    }

    fun dispose() {
        getCompositeDisposable().dispose()
    }

    fun addUploadDisposable(disposable: Disposable) {
        getUploadDisposable().add(disposable)
    }

    fun disposeUpload() {
        getUploadDisposable().dispose()
    }



    private fun getProfileDisposable(): CompositeDisposable {
        if (profileDisposable == null || profileDisposable!!.isDisposed) {
            profileDisposable = CompositeDisposable()
        }
        return profileDisposable!!
    }

    private fun getSaveDisposable(): CompositeDisposable {
        if (saveDisposable == null || saveDisposable!!.isDisposed) {
            saveDisposable = CompositeDisposable()
        }
        return saveDisposable!!
    }

    private fun getFollowDisposable(): CompositeDisposable {
        if (followDisposable == null || followDisposable!!.isDisposed) {
            followDisposable = CompositeDisposable()
        }
        return followDisposable!!
    }

    private fun getArticlesDisposable(): CompositeDisposable {
        if (articlesDisposable == null || articlesDisposable!!.isDisposed) {
            articlesDisposable = CompositeDisposable()
        }
        return articlesDisposable!!
    }

    private fun getHomeDisposable(): CompositeDisposable {
        if (homeDisposable == null || homeDisposable!!.isDisposed) {
            homeDisposable = CompositeDisposable()
        }
        return homeDisposable!!
    }

    private fun getVideosDisposable(): CompositeDisposable {
        if (videosDisposable == null || videosDisposable!!.isDisposed) {
            videosDisposable = CompositeDisposable()
        }
        return videosDisposable!!
    }

    private fun getFacilitiesDisposable(): CompositeDisposable {
        if (facilitiesDisposable == null || facilitiesDisposable!!.isDisposed) {
            facilitiesDisposable = CompositeDisposable()
        }
        return facilitiesDisposable!!
    }

    private fun getMedicalFilesDisposable(): CompositeDisposable {
        if (medicalFilesDisposable == null || medicalFilesDisposable!!.isDisposed) {
            medicalFilesDisposable = CompositeDisposable()
        }
        return medicalFilesDisposable!!
    }

    private fun getSearchDisposable(): CompositeDisposable {
        if (searchDisposable == null || searchDisposable!!.isDisposed) {
            searchDisposable = CompositeDisposable()
        }
        return searchDisposable!!
    }

    private fun getCompositeDisposable(): CompositeDisposable {
        if (compositeDisposable == null || compositeDisposable!!.isDisposed) {
            compositeDisposable = CompositeDisposable()
        }
        return compositeDisposable!!
    }

    private fun getUploadDisposable(): CompositeDisposable {
        if (uploadDisposable == null || uploadDisposable!!.isDisposed) {
            uploadDisposable = CompositeDisposable()
        }
        return uploadDisposable!!
    }
}
