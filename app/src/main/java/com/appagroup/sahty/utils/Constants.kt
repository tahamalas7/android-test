package com.appagroup.sahty.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 *
 * created by taha.m on 26/11/2018
 *
 */

object User {
    val USER_ID = "USER_ID"
    val USER_NAME = "USER_NAME"
    val USER_EMAIL = "USER_EMAIL"
    val USER_IMAGE = "USER_IMAGE"
    val USER_ROLE = "USER_ROLE"
    val USER_PHONE_NUMBER = "USER_PHONE_NUMBER"
    val USER_ACCESS_TOKEN = "USER_ACCESS_TOKEN"
    val LOGIN_PROVIDER = "login_provider"
    val DOCTOR_AVAILABILITY = "USER_TOKEN"
}

object ApplicationsSettings {
    val MOBILE_DATA = "MOBILE_DATA"
    val NOTIFICATIONS_STATUS = "NOTIFICATIONS_STATUS"
    val APPLICATION_LANGUAGE = "application_language"
}

object UserRole {
    val ROLE_READER = 1
    val ROLE_UNCONFIRMED_DOCTOR = 2
    val ROLE_CONFIRMED_DOCTOR = 3
    val ROLE_DOCTOR_WITHOUT_CERTIFICATE = 4
    val ROLE_UNKNOWN_USER = 5
}

object DoctorSearch {
    val ALL = 0
    val NAME = 2
    val SPECIALTY = 1
}

object DoctorAvailability {
    val AVAILABLE_DOCTOR = 1
    val UNAVAILABLE_DOCTOR = 0
}

object ArticlesFilter {
    val ALL = 0
    val Articles = 1
    val STORIES = 2
    val MOST_INTERACTIVE = 3
    val FOLLOWING_USERS = 4
}

object PollsFilter {
    val ALL = 0
    val VOTED = 1
    val NOT_VOTED = 2
    val MOST_INTERACTIVE = 3
}

object Location {
    val LATITUDE = "latitude"
    val LONGITUDE = "longitude"
}

object Facility {
    val TYPE_HOSPITAL = 1
    val TYPE_PHARMACY = 2
}

object Follow {
    val FOLLOW = 1
    val UNFOLLOW = 2
}

object MediaType {
    val TYPE_IMAGE = "1"
    val TYPE_VIDEO = "2"
}

object ShareStatus {
    val UNSHARE = 0
    val SHARE = 1
}

object Menu {
    val MENU_HOME = 0
    val MENU_MESSAGES = 1
    val MENU_POLLS = 2
    val MENU_PROFILE = 3
    val MENU_SAVED = 4
    val MENU_FEEDBACK = 5
    val MENU_SETTINGS = 6
    val MENU_POST_ARTICLE = 7
    val MENU_POST_STORY = 8
    val MENU_LOGOUT = 9
}

object SavedTypes {
    val TYPE_VIDEOS = 1
    val TYPE_ARTICLES = 2
    val TYPE_STORIES = 3
}

object SaveTypes {
    val ARTICLE_OR_STORY = 1
    val VIDEO = 2
}

object Languages {
    val ENGLISH_LANGUAGE = "en"
    val ARABIC_LANGUAGE = "ar"
}

object GeneralSearch {
    val FACILITIES_FILTER = 1
    val ARTICLES_FILTER = 2
    val DOCTORS_FILTER = 3
    val NO_FILTER = 0
}

object NotificationsType {
    val DOCTOR_CONFIRMED = 1
    val NEW_MESSAGE = 2
    val NEW_ARTICLE = 3
}

object InstagramConstans {
    val BASE_URL = "https://api.instagram.com/"
    val REDIRECT_URL = "https://instagram.com/"

    val USER_INSTAGRAM_TOKEN = "USER_INSTAGRAM_TOKEN"
    val CLIENT_ID = "ba231a873ea145af9f97866de4718fe6"
    val CLIENT_SECRET = "d2dc4728401344bb8285885096eaa0c0"
    val GET_USER_INFO_URL = "https://api.instagram.com/v1/users/"
}

val INSTAGRAM = "instagram"
val FACEBOOK = "facebook"
val GMAIL = "gmail"


fun <T> subscribe(observable: Observable<T>, observer: Observer<T>) {
    observable.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(observer)
}

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(if (currentFocus == null) View(this) else currentFocus)
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}