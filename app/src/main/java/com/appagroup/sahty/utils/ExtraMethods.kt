package com.appagroup.sahty.utils

import okhttp3.RequestBody

fun String.toRequestBody(): RequestBody =
        RequestBody.create(okhttp3.MediaType.parse("text/plain"), this)

