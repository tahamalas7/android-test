package com.appagroup.sahty.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.appagroup.sahty.R
import com.appagroup.sahty.ui.articles.article_content.ArticleContentActivity
import com.appagroup.sahty.ui.main.MainActivity
import com.appagroup.sahty.ui.messages.message_content.MessageContentActivity
import com.appagroup.sahty.ui.splash.SplashScreenActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import java.lang.NullPointerException


/**
 * Created by taha.m & tayseer.d on 12/13/2018.
 */

class FirebaseMessageService : FirebaseMessagingService() {

    val TAG = "FirebaseMessageService"
    private val NOTIFICATION_ID = 810

    private val NOTIFICATION_CHANNEL_ID = "8010"
    private val NOTIFICATION_CHANNEL_NAME = "SAHTY"
    private val NOTIFICATION_CHANNEL_DESC = "Syrian Russian Business Council"

    override fun onNewToken(newToken: String?) {
        super.onNewToken(newToken)

        // Get updated InstanceID token.

        Log.d("FCM TOKEN GET", "Refreshed token: " + newToken!!)

        val intent = Intent("tokenReceiver")
        // You can also include some extra data.
        val broadcastManager = LocalBroadcastManager.getInstance(this)
        intent.putExtra("token", newToken)
        broadcastManager.sendBroadcast(intent)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d(TAG, "DATA = ${remoteMessage.data}")
        Log.d(TAG, "TITLE = ${remoteMessage.notification?.title}")
        Log.d(TAG, "BODY  = ${remoteMessage.notification?.body}")
        try {
            sendNotification(remoteMessage)
        } catch (npe: NullPointerException) {
            Log.d(TAG, npe.message.toString())
        }
    }

    private fun sendNotification(remoteMessage: RemoteMessage) {

        val type = remoteMessage.data["type"]?.toInt()

        val intent: Intent?

        when (type) {
            NotificationsType.DOCTOR_CONFIRMED -> {
                intent = Intent(this, MainActivity::class.java)
                intent.putExtra(MainActivity.FROM_NOTIFICATION, true)
            }
            NotificationsType.NEW_MESSAGE -> {
                intent = Intent(this, MessageContentActivity::class.java)
                intent.putExtra(MessageContentActivity.RECEIVER_ID, remoteMessage.data["data"]?.toLong())
            }
            NotificationsType.NEW_ARTICLE -> {
                intent = Intent(this, ArticleContentActivity::class.java)
                intent.putExtra(ArticleContentActivity.ARTICLE_ID, remoteMessage.data["data"]?.toLong())
            }
            else -> intent = Intent(this, SplashScreenActivity::class.java)
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notificationBuilder: NotificationCompat.Builder

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, NOTIFICATION_CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)

            // Configure the notification channel.
            with(notificationChannel) {
                description = NOTIFICATION_CHANNEL_DESC
                enableLights(true)
                lightColor = Color.BLUE
                enableVibration(true)
            }

            notificationManager.createNotificationChannel(notificationChannel)

            notificationBuilder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
        } else {
            notificationBuilder = NotificationCompat.Builder(this)
        }
        notificationBuilder.setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setSound(defaultSoundUri)
                .setContentTitle(remoteMessage.notification!!.title)
                .setContentText(remoteMessage.notification!!.body)
                .setContentIntent(pendingIntent)

        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build())
    }
}
