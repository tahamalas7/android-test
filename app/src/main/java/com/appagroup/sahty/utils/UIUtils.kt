package com.appagroup.sahty.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.media.ExifInterface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.appagroup.sahty.R
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.File

/**
 *
 * created by taha.m on 05/09/2018
 *
 */

object UIUtils {

    val WIDTH = 1
    val HEIGHT = 2
    val TEXT = 3

    fun checkInternetConnection(activity: Activity): Boolean {
        return if (isNetworkAvailable(activity))
            true
        else {
            showNoInternetConnection(activity)
            false
        }
    }

    fun isNetworkAvailable(activity: Activity): Boolean {
        val connectivityManager = activity.getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            return networkInfo?.isConnected ?: false
        } else false
    }

    fun showNoInternetConnection(activity: Activity) {
        AlertDialog.Builder(activity)
                .setMessage(R.string.no_internet_connection)
                .setNegativeButton(R.string.close) { dialog, _ ->
                    dialog.dismiss()
                    activity.finish()
                }
                .show()
    }

    private fun computeDiffFromNow(date1: Date): Map<TimeUnit, Long> {
        val diffInMillies = Date().time - date1.time
        val units = ArrayList(EnumSet.allOf(TimeUnit::class.java))
        units.reverse()
        val result = LinkedHashMap<TimeUnit, Long>()
        var milliesRest = diffInMillies
        for (unit in units) {
            val diff = unit.convert(milliesRest, TimeUnit.MILLISECONDS)
            val diffInMilliesForUnit = unit.toMillis(diff)
            milliesRest -= diffInMilliesForUnit
            result[unit] = diff
        }
        return result
    }

    fun getScreenSize(activity: Activity): Double {
        val display = activity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y
        return (width * height).toDouble()
    }

//    fun getScreenWidth(activity: Activity): Int {
//        val display = activity.windowManager.defaultDisplay
//        val size = Point()
//        display.getSize(size)
//        return size.x
//    }
//
//    fun getScreenHeight(activity: Activity): Int {
//        val display = activity.windowManager.defaultDisplay
//        val size = Point()
//        display.getSize(size)
//        return size.y
//    }

    fun transformSize(activity: Activity, sizeInDp: Int, type: Int): Double {
        val display = activity.windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y

        val displayMetrics = activity.resources.displayMetrics
        val dpHeight = displayMetrics.heightPixels / displayMetrics.density
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        val screenSizeInDp = dpHeight * dpWidth

        return when (type) {
            WIDTH -> (sizeInDp * width / dpWidth).toDouble()
            HEIGHT -> (sizeInDp * height / dpHeight).toDouble()
            else -> {
                val screenSize = getScreenSize(activity)
                screenSize * sizeInDp / screenSizeInDp
            }
        }
    }

    fun getScreenHeight(activity: Activity): Int {
        return activity.windowManager.defaultDisplay.height

    }

    fun getScreenWidth(activity: Activity): Int {
        return activity.windowManager.defaultDisplay.width
    }

    fun matchDimensions(view: View, type: Int) {
        if (type == 1) {

            val dimension = view.layoutParams.width
            val params = view.layoutParams
            params.width = dimension
            params.height = dimension
            view.layoutParams = params
        } else {

            val dimension = view.layoutParams.height
            val params = view.layoutParams
            params.width = dimension
            params.height = dimension
            view.layoutParams = params
        }
    }

    fun currentDate(): Date {
        val calendar = Calendar.getInstance()
        return calendar.time
    }

    fun getTimeAgo(dateStr: String, context: Context): String? {

        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        var date: Date? = null
        try {
            date = format.parse(dateStr)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        if (date == null) {
            return null
        }
        val time = date.time

        val curDate = currentDate()
        val now = curDate.time
        if (time > now || time <= 0) {
            return null
        }

        val dim = getTimeDistanceInMinutes(time)

        val timeAgo = when (dim) {
            0 -> context.resources.getString(R.string.date_util_term_less) + " " + context.resources.getString(R.string.date_util_term_a) + " " + context.resources.getString(R.string.date_util_unit_minute)
            1 -> return "1 " + context.resources.getString(R.string.date_util_unit_minute)
            in 2..44 -> dim.toString() + " " + context.resources.getString(R.string.date_util_unit_minutes)
            in 45..89 -> context.resources.getString(R.string.date_util_prefix_about) + " " + context.resources.getString(R.string.date_util_term_an) + " " + context.resources.getString(R.string.date_util_unit_hour)
            in 90..1439 -> context.resources.getString(R.string.date_util_prefix_about) + " " + Math.round((dim / 60).toFloat()) + " " + context.resources.getString(R.string.date_util_unit_hours)
            in 1440..2519 -> "1 " + context.resources.getString(R.string.date_util_unit_day)
            in 2520..43199 -> Math.round((dim / 1440).toFloat()).toString() + " " + context.resources.getString(R.string.date_util_unit_days)
            in 43200..86399 -> context.resources.getString(R.string.date_util_prefix_about) + " " + context.resources.getString(R.string.date_util_term_a) + " " + context.resources.getString(R.string.date_util_unit_month)
            in 86400..525599 -> Math.round((dim / 43200).toFloat()).toString() + " " + context.resources.getString(R.string.date_util_unit_months)
            in 525600..655199 -> context.resources.getString(R.string.date_util_prefix_about) + " " + context.resources.getString(R.string.date_util_term_a) + " " + context.resources.getString(R.string.date_util_unit_year)
            in 655200..914399 -> context.resources.getString(R.string.date_util_prefix_over) + " " + context.resources.getString(R.string.date_util_term_a) + " " + context.resources.getString(R.string.date_util_unit_year)
            in 914400..1051199 -> context.resources.getString(R.string.date_util_prefix_almost) + " 2 " + context.resources.getString(R.string.date_util_unit_years)
            else -> context.resources.getString(R.string.date_util_prefix_about) + " " + Math.round((dim / 525600).toFloat()) + " " + context.resources.getString(R.string.date_util_unit_years)
        }

//        val timeAgo = when (dim) {
//            0 -> return context.resources.getString(R.string.date_util_unit_just_now)
//            1 -> "1 " + context.resources.getString(R.string.date_util_unit_minute)
//            in 2..59 -> dim.toString() + " " + context.resources.getString(R.string.date_util_unit_minutes)
//            in 60..119 ->context.resources.getString(R.string.date_util_unit_hour)
//            in 120..1439 -> Math.round((dim / 60).toFloat()).toString() + " " + context.resources.getString(R.string.date_util_unit_hours)
//            in 1440..2879 -> "1 " + context.resources.getString(R.string.date_util_unit_day)
//            in 2520..43199 -> Math.round((dim / 1440).toFloat()).toString() + " " + context.resources.getString(R.string.date_util_unit_days)
//            in 43200..86399 -> context.resources.getString(R.string.date_util_unit_month)
//            in 86400..525599 -> Math.round((dim / 43200).toFloat()).toString() + " " + context.resources.getString(R.string.date_util_unit_months)
//            in 525600..914399 -> context.resources.getString(R.string.date_util_unit_year)
//            else -> Math.round((dim / 525600).toFloat()).toString() + " " + context.resources.getString(R.string.date_util_unit_years)
//        }
        return timeAgo + " " + context.resources.getString(R.string.date_util_suffix)

        return ""
    }

    private fun getTimeDistanceInMinutes(time: Long): Int {
        val timeDistance = currentDate().time - time
        return Math.round((Math.abs(timeDistance) / 1000 / 60).toFloat())
    }

    fun hideSoftKeyboard(activity: Activity, view: View) {
        val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun getRealPathFromURI(context: Context, contentURI: String): String {
        val contentUri = Uri.parse(contentURI)
        val cursor = context.contentResolver.query(contentUri, null, null, null, null)
        return if (cursor == null) {
            contentUri.getPath()
        } else {
            cursor.moveToFirst()
            val index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            cursor.getString(index)
        }
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }
        val totalPixels = (width * height).toFloat()
        val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }

        return inSampleSize
    }

    fun compressImage(context: Context, imageUri: String): String {

        val filePath = getRealPathFromURI(context, imageUri)
        var scaledBitmap: Bitmap? = null

        val options = BitmapFactory.Options()

        //      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
        //      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true
        var bmp = BitmapFactory.decodeFile(filePath, options)

        var actualHeight = options.outHeight
        var actualWidth = options.outWidth

        //      max Height and width values of the compressed image is taken as 816x612

        val maxHeight = 816.0f
        val maxWidth = 612.0f
        var imgRatio = (actualWidth / actualHeight).toFloat()
        val maxRatio = maxWidth / maxHeight

        //      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            when {
                imgRatio < maxRatio -> {
                    imgRatio = maxHeight / actualHeight
                    actualWidth = (imgRatio * actualWidth).toInt()
                    actualHeight = maxHeight.toInt()
                }
                imgRatio > maxRatio -> {
                    imgRatio = maxWidth / actualWidth
                    actualHeight = (imgRatio * actualHeight).toInt()
                    actualWidth = maxWidth.toInt()
                }
                else -> {
                    actualHeight = maxHeight.toInt()
                    actualWidth = maxWidth.toInt()

                }
            }
        }

        //      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight)

        //      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false

        //      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true
        options.inInputShareable = true
        options.inTempStorage = ByteArray(16 * 1024)

        try {
            //          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()

        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }

        val ratioX = actualWidth / options.outWidth.toFloat()
        val ratioY = actualHeight / options.outHeight.toFloat()
        val middleX = actualWidth / 2.0f
        val middleY = actualHeight / 2.0f

        val scaleMatrix = Matrix()
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)

        val canvas = Canvas(scaledBitmap)
        canvas.matrix = scaleMatrix
        canvas.drawBitmap(bmp, middleX - bmp.width / 2, middleY - bmp.height / 2, Paint(Paint.FILTER_BITMAP_FLAG))

        //      check the rotation of the image and display it properly
        val exif: ExifInterface
        try {
            exif = ExifInterface(filePath)

            val orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0)
            Log.d("EXIF", "Exif: $orientation")
            val matrix = Matrix()
            when (orientation) {
                6 -> {
                    matrix.postRotate(90F)
                    Log.d("EXIF", "Exif: $orientation")
                }
                3 -> {
                    matrix.postRotate(180F)
                    Log.d("EXIF", "Exif: $orientation")
                }
                8 -> {
                    matrix.postRotate(270F)
                    Log.d("EXIF", "Exif: $orientation")
                }
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap!!.width, scaledBitmap.height, matrix,
                    true)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        var out: FileOutputStream?
        val filename = getFilename()
        try {
            out = FileOutputStream(filename)

            //          write the compressed bitmap at the destination specified by filename.
            scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 80, out)

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        return filename

    }

    private fun getFilename(): String {
        val file = File(Environment.getExternalStorageDirectory().path, "Sahty/Images")
        if (!file.exists()) {
            file.mkdirs()
        }

        return file.absolutePath + "/" + System.currentTimeMillis() + ".jpg"
    }

    fun commitFragment(fragmentManager: FragmentManager, fragment: Fragment, tag: String, fragmentContainer: Int) {
        val fragmentTransaction = fragmentManager.beginTransaction()
//        fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left, R.anim.slide_in_left, R.anim.slide_out_right)
        fragmentTransaction.addToBackStack("SahtyBackStack")
        fragmentTransaction.replace(fragmentContainer, fragment, tag).commit()
    }

    fun openMap(context: Context, lat: String, lon: String, label: String) {
        val uri = "geo:0,0?q=$lat,$lon($label)"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        context.startActivity(intent)
    }
}
