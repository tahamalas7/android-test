package com.appagroup.sahty.data

import android.util.Log
import com.appagroup.sahty.data.db.DbHelper
import com.appagroup.sahty.data.http.HttpHelper
import com.appagroup.sahty.data.prefs.PrefsHelper
import com.appagroup.sahty.entity.*
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataManager @Inject constructor(
        private val dbHelper: DbHelper,
        private val httpHelper: HttpHelper,
        private val prefsHelper: PrefsHelper
) {

    /*** HttpHelper ***/

    fun register(name: String, email: String, password: String, role: Int, fcm_token: String): Observable<Response<BaseResponse<User>>> =
            httpHelper.register(name, email, password, role, fcm_token)

    fun registerWithAccreditation(name: String, email: String, password: String, role: Int, fcm_token: String, type: String, specialty: Long, location: String, year: String, image: String): Observable<Response<BaseResponse<User>>> {
        val imgFile = File(image)
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), imgFile)
        return httpHelper.registerWithAccreditation(
                RequestBody.create(okhttp3.MediaType.parse("text/plain"), name),
                RequestBody.create(okhttp3.MediaType.parse("text/plain"), email),
                RequestBody.create(okhttp3.MediaType.parse("text/plain"), password),
                RequestBody.create(okhttp3.MediaType.parse("text/plain"), "$role"),
                RequestBody.create(okhttp3.MediaType.parse("text/plain"), fcm_token),
                RequestBody.create(okhttp3.MediaType.parse("text/plain"), type),
                RequestBody.create(okhttp3.MediaType.parse("text/plain"), "$specialty"),
                RequestBody.create(okhttp3.MediaType.parse("text/plain"), location),
                RequestBody.create(okhttp3.MediaType.parse("text/plain"), year),
                MultipartBody.Part.createFormData("image", imgFile.name, requestFile))
    }

    fun login(email: String, password: String, fcm_token: String): Observable<Response<BaseResponse<User>>> =
            httpHelper.login(email, password, fcm_token)

    fun socialLogin(name: String, email: String, fcm_token: String, social_token: String, provider: String): Observable<Response<BaseResponse<User>>> =
            httpHelper.socialLogin(name, email, fcm_token, social_token, provider)

    fun setRole(role: Int): Observable<Response<BaseResponse<Any?>>> =
            httpHelper.setUserRole(getUserAccessToken(), role)

    fun getSpecialties(name: String): Observable<Response<BaseResponse<MutableList<Specialty>>>> =
            httpHelper.getSpecialties(name, getApplicationLanguage())

    fun getCategories(): Observable<Response<BaseResponse<MutableList<Category>>>> =
            httpHelper.getCategories(getUserAccessToken(), getApplicationLanguage())

    fun getInsights(longitude: Float, latitude: Float): Observable<Response<BaseResponse<Insights>>> {
        Log.d("getInsights", "language = ${getApplicationLanguage()}")
        Log.d("getInsights", "access token = ${getUserAccessToken()}")
        return httpHelper.getInsights(getUserAccessToken(), getApplicationLanguage()
//                , longitude, latitude
        )
    }


    fun getArticlesAndStories(categoryId: Long, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>> =
            httpHelper.getArticlesAndStories(getUserAccessToken(), getApplicationLanguage(), categoryId, filter, page)

    fun getArticleContent(articleId: Long): Observable<Response<BaseResponse<ArticleContent>>> =
            httpHelper.getArticleContent(getUserAccessToken(), articleId)

    fun followUser(userId: Long): Observable<Response<BaseResponse<Any>>> =
            httpHelper.followUser(getUserAccessToken(), userId)

    fun unfollowUser(userId: Long): Observable<Response<BaseResponse<Any>>> =
            httpHelper.unfollowUser(getUserAccessToken(), userId)

    fun save(articleId: Long, type: Int): Observable<Response<BaseResponse<Any>>> =
            httpHelper.save(getUserAccessToken(), articleId, type)

    fun unsave(articleId: Long, type: Int): Observable<Response<BaseResponse<Any>>> =
            httpHelper.unsave(getUserAccessToken(), articleId, type)

    fun likeArticle(articleId: Long): Observable<Response<BaseResponse<Any>>> =
            httpHelper.likeArticle(getUserAccessToken(), articleId)

    fun unlikeArticle(articleId: Long): Observable<Response<BaseResponse<Any>>> =
            httpHelper.unlikeArticle(getUserAccessToken(), articleId)

    fun getVideos(page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Video>>>>> =
            httpHelper.getVideos(getUserAccessToken(), getApplicationLanguage(), page)

    fun getPharmacies(facilityType: Int, page: Int, per_page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Pharmacy>>>>> =
            httpHelper.getPharmacies(getUserAccessToken(), getApplicationLanguage(), facilityType, page, per_page)

    fun getHospitals(facilityType: Int, page: Int, per_page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Hospital>>>>> =
            httpHelper.getHospitals(getUserAccessToken(), getApplicationLanguage(), facilityType, page, per_page)

    fun getPharmacyDetails(pharmacyId: Long): Observable<Response<BaseResponse<PharmacyDetails>>> =
            httpHelper.getPharmacyDetails(getUserAccessToken(), getApplicationLanguage(), pharmacyId)

    fun getHospitalDetails(hospitalId: Long): Observable<Response<BaseResponse<HospitalDetails>>> =
            httpHelper.getHospitalDetails(getUserAccessToken(), getApplicationLanguage(), hospitalId)

    fun submitRating(facilityId: Long, content: String, rate: Float): Observable<Response<BaseResponse<Any>>> =
            httpHelper.submitRating(getUserAccessToken(), facilityId, content, rate)

    fun getMedicalFileGroups(): Observable<Response<BaseResponse<MutableList<MedicalFileGroup>>>> =
            httpHelper.getMedicalFileGroups(getUserAccessToken())

    fun getMedicalFiles(groupId: Long): Observable<Response<BaseResponse<MutableList<MedicalFile>>>> =
            httpHelper.getMedicalFiles(getUserAccessToken(), groupId)

    fun deleteMedicalFileGroup(groupId: Long): Observable<Response<BaseResponse<Any>>> =
            httpHelper.deleteMedicalFileGroup(getUserAccessToken(), groupId)

    fun deleteMedicalFile(fileId: Long): Observable<Response<BaseResponse<Any>>> =
            httpHelper.deleteMedicalFile(getUserAccessToken(), fileId)

    fun uploadFiles(groupId: RequestBody, groupName: RequestBody, files: MutableList<MultipartBody.Part>): Observable<Response<BaseResponse<MedicalFileGroup?>>> =
            httpHelper.uploadFiles(getUserAccessToken(), groupId, groupName, files)

    fun getMessages(page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Message>>>>> =
            httpHelper.getMessages(getUserAccessToken(), getApplicationLanguage(), page)

    fun getMessageReplies(receiverId: Long, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<MessageReply>>>>> =
            httpHelper.getMessageReplies(getUserAccessToken(), getApplicationLanguage(), receiverId, page)

    fun getIsMedicalFilesShared(receiverId: Long): Observable<Response<BaseResponse<MutableList<Share>>>> =
            httpHelper.getShareMedicalFilesStatus(getUserAccessToken(), receiverId)

    fun sendReply(receiverId: Long, content: String): Observable<Response<BaseResponse<SendReplyResponse>>> =
            httpHelper.sendReply(getUserAccessToken(), receiverId, content)

    fun searchForDoctors(keyWord: String, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Doctor>>>>> =
            httpHelper.searchForDoctors(getUserAccessToken(), getApplicationLanguage(), keyWord, filter, page)

    fun updateShareMedicalFilesStatus(receiverId: Long, shareStatus: Int): Observable<Response<BaseResponse<Any>>> =
            httpHelper.updateShareMedicalFilesStatus(getUserAccessToken(), receiverId, shareStatus)

    fun getPatientMedicalFileGroups(patientId: Long): Observable<Response<BaseResponse<MutableList<MedicalFileGroup>>>> =
            httpHelper.getPatientMedicalFileGroups(getUserAccessToken(), patientId)

    fun getPatientMedicalFiles(patientId: Long, groupId: Long): Observable<Response<BaseResponse<MutableList<MedicalFile>>>> =
            httpHelper.getPatientMedicalFiles(getUserAccessToken(), patientId, groupId)

    fun getPolls(filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Poll>>>>> =
            httpHelper.getPolls(getUserAccessToken(), getApplicationLanguage(), filter, page)

    fun submitVote(optionId: Long): Observable<Response<BaseResponse<Any>>> =
            httpHelper.submitVote(getUserAccessToken(), optionId)

    fun getSavedVideos(page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Video>>>>> =
            httpHelper.getSavedVideos(getUserAccessToken(), getApplicationLanguage(), page)

    fun getSavedArticles(page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>> =
            httpHelper.getSavedArticles(getUserAccessToken(), getApplicationLanguage(), page)

    fun getSavedStories(page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Story>>>>> =
            httpHelper.getSavedStories(getUserAccessToken(), getApplicationLanguage(), page)

    fun getUserProfile(): Observable<Response<BaseResponse<UserProfile>>> =
            httpHelper.getUserProfile(getUserAccessToken(), getApplicationLanguage())


    fun changePassword(oldPassword: String, newPassword: String): Observable<Response<BaseResponse<Any>>> =
            httpHelper.changePassword(getUserAccessToken(), oldPassword, newPassword)

    fun changePicture(image: MultipartBody.Part): Observable<Response<BaseResponse<String>>> =
            httpHelper.changePicture(getUserAccessToken(), image)

    fun changeEmail(newEmail: String, password: String): Observable<Response<BaseResponse<Any>>> =
            httpHelper.changeEmail(getUserAccessToken(), newEmail, password)

    fun changeAvailability(): Observable<Response<BaseResponse<Any>>> =
            httpHelper.changeAvailability(getUserAccessToken())

    fun createAccreditation(name: RequestBody, year: RequestBody, location: RequestBody, specialization: RequestBody, image: MultipartBody.Part): Observable<Response<BaseResponse<Accreditation>>> =
            httpHelper.createAccreditation(getUserAccessToken(), name, year, location, specialization, image)

    fun editAccreditation(accreditationId: Long, type: RequestBody, year: RequestBody, location: RequestBody, specialization: RequestBody): Observable<Response<BaseResponse<Accreditation>>> =
            httpHelper.editAccreditation(getUserAccessToken(), accreditationId, type, year, location, specialization)

    fun editAccreditationWithImage(accreditationId: Long, type: RequestBody, year: RequestBody, location: RequestBody, specialization: RequestBody, image: MultipartBody.Part): Observable<Response<BaseResponse<Accreditation>>> =
            httpHelper.editAccreditationWithImage(getUserAccessToken(), accreditationId, type, year, location, specialization, image)

    fun deleteAccreditation(accreditationId: Long): Observable<Response<BaseResponse<Any>>> =
            httpHelper.deleteAccreditation(getUserAccessToken(), accreditationId)

    fun postArticleWithVideo(enTitle: RequestBody, enContent: RequestBody, arTitle: RequestBody, arContent: RequestBody, categoryId: RequestBody, video: MultipartBody.Part): Observable<Response<BaseResponse<Any>>> =
            httpHelper.postArticleWithVideo(getUserAccessToken(), enTitle, enContent, arTitle, arContent, categoryId, video)

    fun postArticleWithImages(enTitle: RequestBody, enContent: RequestBody, arTitle: RequestBody, arContent: RequestBody, categoryId: RequestBody, images: MutableList<MultipartBody.Part>): Observable<Response<BaseResponse<Any>>> =
            httpHelper.postArticleWithImages(getUserAccessToken(), enTitle, enContent, arTitle, arContent, categoryId, images)

    fun postStory(enTitle: RequestBody, enContent: RequestBody, arTitle: RequestBody, arContent: RequestBody, categoryId: RequestBody, image: MultipartBody.Part): Observable<Response<BaseResponse<Any>>> =
            httpHelper.postStory(getUserAccessToken(), enTitle, enContent, arTitle, arContent, categoryId, image)

    fun makeGeneralSearch(searchKey: String): Observable<Response<BaseResponse<GeneralSearchResult>>> =
            httpHelper.makeGeneralSearch(getUserAccessToken(), getApplicationLanguage(), searchKey)

    fun makeSearchForFacilities(searchKey: String, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Facility>>>>> =
            httpHelper.makeSearchForFacilities(getUserAccessToken(), getApplicationLanguage(), searchKey, filter, page)

    fun makeSearchForArticles(searchKey: String, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>> =
            httpHelper.makeSearchForArticles(getUserAccessToken(), getApplicationLanguage(), searchKey, filter, page)

    fun makeSearchForDoctors(searchKey: String, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Doctor>>>>> =
            httpHelper.makeSearchForDoctors(getUserAccessToken(), getApplicationLanguage(), searchKey, filter, page)

    fun visitProfile(userId: Long): Observable<Response<BaseResponse<VisitProfile>>> =
            httpHelper.visitProfile(getUserAccessToken(), getApplicationLanguage(), userId)


    /****** DataBase ******/

    /**** Insights ****/

    fun getInsightsArticlesFromDatabase() = dbHelper.getInsightsArticles()

    fun getInsightsVideoFromDatabase() = dbHelper.getInsightsVideo()

    fun getInsightsHospitalFromDatabase() = dbHelper.getInsightsHospital()

    fun getInsightsPharmacyFromDatabase() = dbHelper.getInsightsPharmacy()


    /**** Articles ****/

    fun deleteArticles(category_id: Long) = dbHelper.deleteArticles(category_id)

    fun updateArticles(articles: MutableList<Article>) = dbHelper.updateArticles(articles)

    fun getArticlesFromDatabase(category_id: Long) = dbHelper.getArticles(category_id)


    /**** Videos ****/

    fun deleteVideos() = dbHelper.deleteVideos()

    fun updateVideos(videos: MutableList<Video>) = dbHelper.updateVideos(videos)

    fun getVideosFromDatabase() = dbHelper.getVideos()


    /**** Hospitals ****/

    fun deleteHospitals() = dbHelper.deleteHospitals()

    fun updateHospitals(hospitals: MutableList<Hospital>) = dbHelper.updateHospitals(hospitals)

    fun getHospitalsFromDatabase() = dbHelper.getHospitals()


    /**** Pharmacies ****/

    fun deletePharmacies() = dbHelper.deletePharmacies()

    fun updatePharmacies(pharmacies: MutableList<Pharmacy>) = dbHelper.updatePharmacies(pharmacies)

    fun getPharmaciesFromDatabase() = dbHelper.getPharmacies()


    /**** Categories ****/

    fun deleteCategories() = dbHelper.deleteCategories()

    fun updateCategories(categories: MutableList<Category>) = dbHelper.updateCategories(categories)

    fun getCategoriesFromDatabase() = dbHelper.getCategories()


    /**** Shared Preferences ****/

    fun setUserId(id: Long) = prefsHelper.setUserId(id)

    fun setUserName(name: String) = prefsHelper.setUserName(name)

    fun setUserEmail(email: String) = prefsHelper.setUserEmail(email)

    fun setUserPhoneNumber(phoneNumber: Int) = prefsHelper.setUserPhoneNumber(phoneNumber)

    fun setUserImage(image: String?) = prefsHelper.setUserImage(image)

    fun setUserRole(role: Int) = prefsHelper.setUserRole(role)

    fun setUserAccessToken(token: String) = prefsHelper.setUserAccessToken(token)

    fun setUserInstagramToken(token: String) = prefsHelper.setUserInstagramToken(token)

    fun setLoginProvider(provider: String) = prefsHelper.setLoginProvider(provider)

    fun setDeviceLatitude(latitude: Float) = prefsHelper.setDeviceLatitude(latitude)

    fun setDeviceLongitude(longitude: Float) = prefsHelper.setDeviceLongitude(longitude)

    fun setDoctorAvailability(availability: Int) = prefsHelper.setDoctorAvailability(availability)

    fun setMobileDataStatus(status: Boolean) = prefsHelper.setMobileDataStatus(status)

    fun setApplicationLanguage(language: String) = prefsHelper.setApplicationLanguage(language)

    fun setNotificationsStatus(status: Boolean) = prefsHelper.setNotificationsStatus(status)


    fun getUserId(): Long = prefsHelper.getUserId()

    fun getUserName(): String = prefsHelper.getUserName()

    fun getUserEmail(): String = prefsHelper.getUserEmail()

    fun getUserPhoneNumber(): Int = prefsHelper.getUserPhoneNumber()

    fun getUserImage(): String = prefsHelper.getUserImage()

    fun getUserRole(): Int = prefsHelper.getUserRole()

    fun getUserAccessToken(): String = prefsHelper.getUserAccessToken()

    fun getUserInstagramToken(): String = prefsHelper.getUserInstagramToken()

    fun getLoginProvider(): String = prefsHelper.getLoginProvider()

    fun getApplicationLanguage(): String = prefsHelper.getApplicationLanguage()

    fun getDeviceLatitude(): Float = prefsHelper.getDeviceLatitude()

    fun getDeviceLongitude(): Float = prefsHelper.getDeviceLongitude()

    fun getDoctorAvailability(): Int = prefsHelper.getDoctorAvailability()

    fun getMobileDataStatus(): Boolean = prefsHelper.getMobileDataStatus()

    fun getNotificationsStatus(): Boolean = prefsHelper.getNotificationsStatus()

    fun clearUserData() = prefsHelper.clearUserData()

}