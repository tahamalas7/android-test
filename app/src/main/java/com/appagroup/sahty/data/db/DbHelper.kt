package com.appagroup.sahty.data.db

import com.appagroup.sahty.entity.*
import io.reactivex.Observable

interface DbHelper {

    /**** Insights ****/

    fun getInsightsArticles(): Observable<MutableList<Article>>

    fun getInsightsVideo(): Observable<Video>

    fun getInsightsHospital(): Observable<Pharmacy>

    fun getInsightsPharmacy(): Observable<Hospital>


    /**** Articles ****/

    fun deleteArticles(category_id: Long): Observable<Any>

    fun updateArticles(articles: MutableList<Article>): Observable<Any>

    fun getArticles(category_id: Long): Observable<MutableList<Article>>


    /**** Videos ****/

    fun deleteVideos(): Observable<Any>

    fun updateVideos(videos: MutableList<Video>): Observable<Any>

    fun getVideos(): Observable<MutableList<Video>>


    /**** Hospitals ****/

    fun deleteHospitals(): Observable<Any>

    fun updateHospitals(hospitals: MutableList<Hospital>): Observable<Any>

    fun getHospitals(): Observable<MutableList<Hospital>>


    /**** Pharmacies ****/

    fun deletePharmacies(): Observable<Any>

    fun updatePharmacies(pharmacies: MutableList<Pharmacy>): Observable<Any>

    fun getPharmacies(): Observable<MutableList<Pharmacy>>


    /**** Categories ****/

    fun deleteCategories(): Observable<Any>

    fun updateCategories(categories: MutableList<Category>): Observable<Any>

    fun getCategories(): Observable<MutableList<Category>>

}