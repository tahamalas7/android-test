package com.appagroup.sahty.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.appagroup.sahty.entity.*
import io.reactivex.Flowable

@Dao
interface AppDao {

    /**** Insights ****/

    @Query("SELECT * FROM articles WHERE isInsight = 'true'")
    fun getInsightsArticles(): Flowable<MutableList<Article>>

    @Query("SELECT * FROM videos WHERE isInsight = 'true'")
    fun getInsightsVideo(): Flowable<Video>

    @Query("SELECT * FROM pharmacies WHERE isInsight = 'true'")
    fun getInsightsHospital(): Flowable<Pharmacy>

    @Query("SELECT * FROM hospitals WHERE isInsight = 'true'")
    fun getInsightsPharmacy(): Flowable<Hospital>


    /**** Articles ****/

    @Query("DELETE FROM articles WHERE category_id = :category_id")
    fun deleteArticles(category_id: Long)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateArticles(articles: MutableList<Article>)

    @Query("SELECT * FROM articles WHERE category_id = :category_id ORDER BY id DESC")
    fun getArticles(category_id: Long): Flowable<MutableList<Article>>


    /**** Videos ****/

    @Query("DELETE FROM videos")
    fun deleteVideos()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateVideos(videos: MutableList<Video>)

    @Query("SELECT * FROM videos ORDER BY id DESC")
    fun getVideos(): Flowable<MutableList<Video>>


    /**** Hospitals ****/

    @Query("DELETE FROM hospitals")
    fun deleteHospitals()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateHospitals(hospitals: MutableList<Hospital>)

    @Query("SELECT * FROM hospitals ORDER BY id DESC")
    fun getHospitals(): Flowable<MutableList<Hospital>>


    /**** Pharmacies ****/

    @Query("DELETE FROM pharmacies")
    fun deletePharmacies()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updatePharmacies(pharmacies: MutableList<Pharmacy>)

    @Query("SELECT * FROM pharmacies ORDER BY id DESC")
    fun getPharmacies(): Flowable<MutableList<Pharmacy>>


    /**** Categories ****/

    @Query("DELETE FROM categories")
    fun deleteCategories()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateCategories(categories: MutableList<Category>)

    @Query("SELECT * FROM categories ORDER BY id DESC")
    fun getCategories(): Flowable<MutableList<Category>>

}