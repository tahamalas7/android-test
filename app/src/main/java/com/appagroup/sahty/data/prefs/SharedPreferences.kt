package com.appagroup.sahty.data.prefs

import android.content.SharedPreferences
import com.appagroup.sahty.utils.User
import com.appagroup.sahty.utils.Location
import com.appagroup.sahty.utils.ApplicationsSettings
import com.appagroup.sahty.utils.InstagramConstans
import javax.inject.Inject

class SharedPreferences @Inject constructor(private val sharedPreferences: SharedPreferences) : PrefsHelper {

    override fun setUserId(id: Long) =
            sharedPreferences.edit().putLong(User.USER_ID, id).apply()

    override fun setUserName(name: String) =
            sharedPreferences.edit().putString(User.USER_NAME, name).apply()

    override fun setUserEmail(email: String) =
            sharedPreferences.edit().putString(User.USER_EMAIL, email).apply()

    override fun setUserPhoneNumber(phoneNumber: Int) =
            sharedPreferences.edit().putInt(User.USER_PHONE_NUMBER, phoneNumber).apply()

    override fun setUserImage(image: String?) =
            sharedPreferences.edit().putString(User.USER_IMAGE, image).apply()

    override fun setUserRole(role: Int) =
            sharedPreferences.edit().putInt(User.USER_ROLE, role).apply()

    override fun setUserAccessToken(token: String) =
            sharedPreferences.edit().putString(User.USER_ACCESS_TOKEN, token).apply()

    override fun setUserInstagramToken(token: String) =
            sharedPreferences.edit().putString(InstagramConstans.USER_INSTAGRAM_TOKEN, token).apply()

    override fun setLoginProvider(provider: String) =
            sharedPreferences.edit().putString(User.LOGIN_PROVIDER, provider).apply()

    override fun setDeviceLatitude(latitude: Float) =
            sharedPreferences.edit().putFloat(Location.LATITUDE, latitude).apply()

    override fun setDeviceLongitude(longitude: Float) =
            sharedPreferences.edit().putFloat(Location.LONGITUDE, longitude).apply()

    override fun setDoctorAvailability(availability: Int) =
            sharedPreferences.edit().putInt(User.DOCTOR_AVAILABILITY, availability).apply()

    override fun setMobileDataStatus(status: Boolean) =
            sharedPreferences.edit().putBoolean(ApplicationsSettings.MOBILE_DATA, status).apply()

    override fun setApplicationLanguage(language: String) =
            sharedPreferences.edit().putString(ApplicationsSettings.APPLICATION_LANGUAGE, language).apply()

    override fun setNotificationsStatus(status: Boolean) =
            sharedPreferences.edit().putBoolean(ApplicationsSettings.NOTIFICATIONS_STATUS, status).apply()


    override fun getUserId(): Long =
            sharedPreferences.getLong(User.USER_ID, 0)

    override fun getUserName(): String =
            sharedPreferences.getString(User.USER_NAME, "") ?: ""

    override fun getUserEmail(): String =
            sharedPreferences.getString(User.USER_EMAIL, "") ?: ""

    override fun getUserPhoneNumber(): Int =
            sharedPreferences.getInt(User.USER_PHONE_NUMBER, 0)

    override fun getUserImage(): String =
            sharedPreferences.getString(User.USER_IMAGE, "") ?: ""

    override fun getUserRole(): Int =
            sharedPreferences.getInt(User.USER_ROLE, 0)

    override fun getUserAccessToken(): String =
            sharedPreferences.getString(User.USER_ACCESS_TOKEN, "") ?: ""

    override fun getUserInstagramToken(): String =
            sharedPreferences.getString(InstagramConstans.USER_INSTAGRAM_TOKEN, "") ?: ""

    override fun getLoginProvider(): String =
            sharedPreferences.getString(User.LOGIN_PROVIDER, "") ?: ""

    override fun getDeviceLatitude(): Float =
            sharedPreferences.getFloat(Location.LATITUDE, 0F)

    override fun getDeviceLongitude(): Float =
            sharedPreferences.getFloat(Location.LONGITUDE, 0F)

    override fun getDoctorAvailability(): Int =
            sharedPreferences.getInt(User.DOCTOR_AVAILABILITY, 0)

    override fun getMobileDataStatus(): Boolean =
            sharedPreferences.getBoolean(ApplicationsSettings.MOBILE_DATA, false)

    override fun getNotificationsStatus(): Boolean =
            sharedPreferences.getBoolean(ApplicationsSettings.NOTIFICATIONS_STATUS, true)

    override fun getApplicationLanguage(): String =
            sharedPreferences.getString(ApplicationsSettings.APPLICATION_LANGUAGE, "en") ?: "en"


    override fun clearUserData() {
        sharedPreferences.edit().remove(User.USER_ID).apply()
        sharedPreferences.edit().remove(User.USER_NAME).apply()
        sharedPreferences.edit().remove(User.USER_ROLE).apply()
        sharedPreferences.edit().remove(User.USER_EMAIL).apply()
        sharedPreferences.edit().remove(User.USER_IMAGE).apply()
        sharedPreferences.edit().remove(User.USER_ACCESS_TOKEN).apply()
        sharedPreferences.edit().remove(User.USER_PHONE_NUMBER).apply()
        sharedPreferences.edit().remove(User.DOCTOR_AVAILABILITY).apply()
        sharedPreferences.edit().remove(User.LOGIN_PROVIDER).apply()
        sharedPreferences.edit().remove(InstagramConstans.USER_INSTAGRAM_TOKEN).apply()
        sharedPreferences.edit().remove(ApplicationsSettings.MOBILE_DATA).apply()
        sharedPreferences.edit().remove(ApplicationsSettings.APPLICATION_LANGUAGE).apply()
        sharedPreferences.edit().remove(ApplicationsSettings.NOTIFICATIONS_STATUS).apply()
    }
}