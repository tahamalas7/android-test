package com.appagroup.sahty.data.prefs

interface PrefsHelper {

    fun setUserId(id: Long)

    fun setUserName(name: String)

    fun setUserEmail(email: String)

    fun setUserPhoneNumber(phoneNumber: Int)

    fun setUserImage(image: String?)

    fun setUserRole(role: Int)

    fun setUserAccessToken(token: String)

    fun setUserInstagramToken(token: String)

    fun setLoginProvider(provider: String)

    fun setApplicationLanguage(language: String)

    fun setDeviceLatitude(latitude: Float)

    fun setDeviceLongitude(longitude: Float)

    fun setDoctorAvailability(availability: Int)

    fun setMobileDataStatus(status: Boolean)

    fun setNotificationsStatus(status: Boolean)




    fun getUserId(): Long

    fun getUserName(): String

    fun getUserEmail(): String

    fun getUserPhoneNumber(): Int

    fun getUserImage(): String

    fun getUserRole(): Int

    fun getUserAccessToken(): String

    fun getUserInstagramToken(): String

    fun getLoginProvider(): String

    fun getApplicationLanguage(): String

    fun getDeviceLatitude(): Float

    fun getDeviceLongitude(): Float

    fun getDoctorAvailability(): Int

    fun getMobileDataStatus(): Boolean

    fun getNotificationsStatus(): Boolean


    fun clearUserData()
}