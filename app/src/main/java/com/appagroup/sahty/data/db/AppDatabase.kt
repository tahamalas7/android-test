package com.appagroup.sahty.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.appagroup.sahty.entity.*

@Database(entities = [Article::class, Media::class, Category::class, Writer::class, Video::class, Pharmacy::class, Hospital::class], version = 5, exportSchema = false)

@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun appDao(): AppDao

}