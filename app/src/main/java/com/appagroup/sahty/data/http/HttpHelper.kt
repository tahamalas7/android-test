package com.appagroup.sahty.data.http

import com.appagroup.sahty.entity.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response

interface HttpHelper {

    fun register(
            name: String,
            email: String,
            password: String,
            role: Int,
            fcm_token: String
    ): Observable<Response<BaseResponse<User>>>

    fun registerWithAccreditation(
            name: RequestBody,
            email: RequestBody,
            password: RequestBody,
            role: RequestBody,
            fcm_token: RequestBody,
            type: RequestBody,
            specialty: RequestBody,
            location: RequestBody,
            year: RequestBody,
            image: MultipartBody.Part
    ): Observable<Response<BaseResponse<User>>>

    fun login(
            email: String,
            password: String,
            fcm_token: String
    ): Observable<Response<BaseResponse<User>>>

    fun socialLogin(
            name: String,
            email: String,
            fcm_token: String,
            social_token: String,
            provider: String
    ): Observable<Response<BaseResponse<User>>>

    fun setUserRole(
            access_token: String,
            role: Int
    ): Observable<Response<BaseResponse<Any?>>>

    fun getSpecialties(
            name: String,
            language: String
    ): Observable<Response<BaseResponse<MutableList<Specialty>>>>

    fun getCategories(
            accessToken: String,
            language: String
    ): Observable<Response<BaseResponse<MutableList<Category>>>>

    fun getInsights(
            access_token: String,
            language: String
//            longitude: Float,
//            latitude: Float
    ): Observable<Response<BaseResponse<Insights>>>

    fun getArticlesAndStories(
            access_token: String,
            language: String,
            categoryId: Long,
            filter: Int,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>>

    fun getArticleContent(
            access_token: String,
            articleId: Long
    ): Observable<Response<BaseResponse<ArticleContent>>>


    // setAsFollowed & setAsNotFollowed article
    fun followUser(
            access_token: String,
            userId: Long
    ): Observable<Response<BaseResponse<Any>>>

    fun unfollowUser(
            access_token: String,
            userId: Long
    ): Observable<Response<BaseResponse<Any>>>


    // save & unsave article
    fun save(
            access_token: String,
            articleId: Long,
            type: Int
    ): Observable<Response<BaseResponse<Any>>>

    fun unsave(
            access_token: String,
            articleId: Long,
            type: Int
    ): Observable<Response<BaseResponse<Any>>>


    // like & unlike article
    fun likeArticle(
            access_token: String,
            articleId: Long
    ): Observable<Response<BaseResponse<Any>>>

    fun unlikeArticle(
            access_token: String,
            articleId: Long
    ): Observable<Response<BaseResponse<Any>>>

    fun getVideos(
            access_token: String,
            language: String,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Video>>>>>

    fun getPharmacies(
            access_token: String,
            language: String,
            facilityType: Int,
            page: Int,
            per_page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Pharmacy>>>>>

    fun getHospitals(
            access_token: String,
            language: String,
            facilityType: Int,
            page: Int,
            per_page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Hospital>>>>>

    fun getPharmacyDetails(
            access_token: String,
            language: String,
            pharmacyId: Long
    ): Observable<Response<BaseResponse<PharmacyDetails>>>

    fun getHospitalDetails(
            access_token: String,
            language: String,
            hospitalId: Long
    ): Observable<Response<BaseResponse<HospitalDetails>>>

    fun submitRating(
            access_token: String,
            facilityId: Long,
            content: String,
            rate: Float
    ): Observable<Response<BaseResponse<Any>>>

    fun getMedicalFileGroups(
            access_token: String
    ): Observable<Response<BaseResponse<MutableList<MedicalFileGroup>>>>

    fun getMedicalFiles(
            access_token: String,
            groupId: Long
    ): Observable<Response<BaseResponse<MutableList<MedicalFile>>>>

    fun deleteMedicalFileGroup(
            access_token: String,
            groupId: Long
    ): Observable<Response<BaseResponse<Any>>>

    fun deleteMedicalFile(
            access_token: String,
            fileId: Long
    ): Observable<Response<BaseResponse<Any>>>

    fun uploadFiles(
            access_token: String,
            groupId: RequestBody,
            groupName: RequestBody,
            files: MutableList<MultipartBody.Part>
    ): Observable<Response<BaseResponse<MedicalFileGroup?>>>

    fun getMessages(
            access_token: String,
            language: String,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Message>>>>>

    fun getMessageReplies(
            access_token: String,
            language: String,
            receiverId: Long,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<MessageReply>>>>>

    fun getShareMedicalFilesStatus(
            access_token: String,
            receiverId: Long
    ): Observable<Response<BaseResponse<MutableList<Share>>>>

    fun sendReply(
            access_token: String,
            receiverId: Long,
            content: String
    ): Observable<Response<BaseResponse<SendReplyResponse>>>

    fun searchForDoctors(
            access_token: String,
            language: String,
            keyWord: String,
            filter: Int,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Doctor>>>>>

    fun updateShareMedicalFilesStatus(
            access_token: String,
            receiverId: Long,
            shareStatus: Int
    ): Observable<Response<BaseResponse<Any>>>

    fun getPatientMedicalFileGroups(
            access_token: String,
            patientId: Long
    ): Observable<Response<BaseResponse<MutableList<MedicalFileGroup>>>>

    fun getPatientMedicalFiles(
            access_token: String,
            patientId: Long,
            groupId: Long
    ): Observable<Response<BaseResponse<MutableList<MedicalFile>>>>

    fun getPolls(
            access_token: String,
            language: String,
            filter: Int,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Poll>>>>>

    fun submitVote(
            access_token: String,
            optionId: Long
    ): Observable<Response<BaseResponse<Any>>>

    fun getSavedVideos(
            access_token: String,
            language: String,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Video>>>>>

    fun getSavedArticles(
            access_token: String,
            language: String,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>>

    fun getSavedStories(
            access_token: String,
            language: String,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Story>>>>>

    fun getUserProfile(
            access_token: String,
            language: String
    ): Observable<Response<BaseResponse<UserProfile>>>


    fun changePassword(
            access_token: String,
            oldPassword: String,
            newPassword: String
    ): Observable<Response<BaseResponse<Any>>>

    fun changePicture(
            access_token: String,
            image: MultipartBody.Part
    ): Observable<Response<BaseResponse<String>>>

    fun changeEmail(
            access_token: String,
            newEmail: String,
            password: String
    ): Observable<Response<BaseResponse<Any>>>

    fun changeAvailability(
            access_token: String
    ): Observable<Response<BaseResponse<Any>>>

    fun createAccreditation(
            access_token: String,
            name: RequestBody,
            year: RequestBody,
            location: RequestBody,
            specialization: RequestBody,
            image: MultipartBody.Part
    ): Observable<Response<BaseResponse<Accreditation>>>

    fun editAccreditation(
            access_token: String,
            accreditationId: Long,
            type: RequestBody,
            year: RequestBody,
            location: RequestBody,
            specialization: RequestBody
    ): Observable<Response<BaseResponse<Accreditation>>>

    fun editAccreditationWithImage(
            access_token: String,
            accreditationId: Long,
            type: RequestBody,
            year: RequestBody,
            location: RequestBody,
            specialization: RequestBody,
            image: MultipartBody.Part
    ): Observable<Response<BaseResponse<Accreditation>>>

    fun deleteAccreditation(
            access_token: String,
            accreditationId: Long
    ): Observable<Response<BaseResponse<Any>>>

    fun postArticleWithVideo(
            access_token: String,
            enTitle: RequestBody,
            enContent: RequestBody,
            arTitle: RequestBody,
            arContent: RequestBody,
            categoryId: RequestBody,
            video: MultipartBody.Part
    ): Observable<Response<BaseResponse<Any>>>

    fun postArticleWithImages(
            access_token: String,
            enTitle: RequestBody,
            enContent: RequestBody,
            arTitle: RequestBody,
            arContent: RequestBody,
            categoryId: RequestBody,
            images: MutableList<MultipartBody.Part>
    ): Observable<Response<BaseResponse<Any>>>

    fun postStory(
            access_token: String,
            enTitle: RequestBody,
            enContent: RequestBody,
            arTitle: RequestBody,
            arContent: RequestBody,
            categoryId: RequestBody,
            image: MultipartBody.Part
    ): Observable<Response<BaseResponse<Any>>>

    fun makeGeneralSearch(
            access_token: String,
            language: String,
            searchKey: String
    ): Observable<Response<BaseResponse<GeneralSearchResult>>>

    fun makeSearchForFacilities(
            access_token: String,
            language: String,
            searchKey: String,
            filter: Int,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Facility>>>>>

    fun makeSearchForArticles(
            access_token: String,
            language: String,
            searchKey: String,
            filter: Int,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>>

    fun makeSearchForDoctors(
            access_token: String,
            language: String,
            searchKey: String,
            filter: Int,
            page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Doctor>>>>>

    fun visitProfile(
            access_token: String,
            language: String,
            userId: Long
    ): Observable<Response<BaseResponse<VisitProfile>>>
}