package com.appagroup.sahty.data.http

import com.appagroup.sahty.entity.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST("user/register")
    @Headers("Accept: application/json")
    fun register(
            @Field("name") name: String,
            @Field("email") email: String,
            @Field("password") password: String,
            @Field("role") role: String,
            @Field("fcm_token") fcm_token: String
    ): Observable<Response<BaseResponse<User>>>

    @Multipart
    @POST("user/register")
    @Headers("Accept: application/json")
    fun registerWithAccreditation(
            @Part("name") name: RequestBody,
            @Part("email") email: RequestBody,
            @Part("password") password: RequestBody,
            @Part("role") role: RequestBody,
            @Part("fcm_token") fcm_token: RequestBody,
            @Part("type") type: RequestBody,
            @Part("specialization") specialty: RequestBody,
            @Part("location") location: RequestBody,
            @Part("year") year: RequestBody,
            @Part image: MultipartBody.Part
    ): Observable<Response<BaseResponse<User>>>

    @FormUrlEncoded
    @POST("user/login")
    @Headers("Accept: application/json")
    fun login(
            @Field("email") email: String,
            @Field("password") password: String,
            @Field("fcm_token") fcm_token: String
    ): Observable<Response<BaseResponse<User>>>

    @FormUrlEncoded
    @POST("user/socialLogin")
    @Headers("Accept: application/json")
    fun socialLogin(
            @Field("name") name: String,
            @Field("email") email: String,
            @Field("fcm_token") fcm_token: String,
            @Field("social_token") social_token: String,
            @Field("provider") provider: String
    ): Observable<Response<BaseResponse<User>>>

    @FormUrlEncoded
    @POST("user/editRole")
    @Headers("Accept: application/json")
    fun setUserRole(
            @Header("Authorization") access_token: String,
            @Field("role") role: Int
    ): Observable<Response<BaseResponse<Any?>>>

    @FormUrlEncoded
    @POST("specialization/searchSpec")
    @Headers("Accept: application/json")
    fun getSpecialties(
            @Field("name") name: String,
            @Header("Accept-Language") language: String
    ): Observable<Response<BaseResponse<MutableList<Specialty>>>>

    @GET("category")
    @Headers("Accept: application/json")
    fun getCategories(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String
    ): Observable<Response<BaseResponse<MutableList<Category>>>>

    //    @FormUrlEncoded
    @POST("user/getInsights")
    @Headers("Accept: application/json")
    fun getInsights(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String
//            ,
//            @Field("longitude")
//            longitude: Float,
//            @Field("latitude")
//            latitude: Float
    ): Observable<Response<BaseResponse<Insights>>>

    @FormUrlEncoded
    @POST("article/getArticleStory")
    @Headers("Accept: application/json")
    fun getArticlesAndStories(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("category") categoryId: Long,
            @Field("filter") filter: Int,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>>

    @GET("article/{article_id}")
    @Headers("Accept: application/json")
    fun getArticleContent(
            @Header("Authorization") access_token: String,
            @Path("article_id") articleId: Long
    ): Observable<Response<BaseResponse<ArticleContent>>>

    @POST("user/follow/{user_id}")
    @Headers("Accept: application/json")
    fun followUser(
            @Header("Authorization") access_token: String,
            @Path("user_id") userId: Long
    ): Observable<Response<BaseResponse<Any>>>

    @POST("user/unfollow/{user_id}")
    @Headers("Accept: application/json")
    fun unfollowUser(
            @Header("Authorization") access_token: String,
            @Path("user_id") userId: Long
    ): Observable<Response<BaseResponse<Any>>>

    @POST("article/save/{type}/{article_id}")
    @Headers("Accept: application/json")
    fun save(
            @Header("Authorization") access_token: String,
            @Path("article_id") articleId: Long,
            @Path("type") type: Int
    ): Observable<Response<BaseResponse<Any>>>

    @DELETE("article/unsave/{type}/{article_id}")
    @Headers("Accept: application/json")
    fun unsave(
            @Header("Authorization") access_token: String,
            @Path("article_id") articleId: Long,
            @Path("type") type: Int
    ): Observable<Response<BaseResponse<Any>>>

    @POST("article/like/{article_id}")
    @Headers("Accept: application/json")
    fun likeArticle(
            @Header("Authorization") access_token: String,
            @Path("article_id") articleId: Long
    ): Observable<Response<BaseResponse<Any>>>

    @DELETE("article/unlike/{article_id}")
    @Headers("Accept: application/json")
    fun unlikeArticle(
            @Header("Authorization") access_token: String,
            @Path("article_id") articleId: Long
    ): Observable<Response<BaseResponse<Any>>>

    @GET("media/getVideos")
    @Headers("Accept: application/json")
    fun getVideos(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Video>>>>>

    @FormUrlEncoded
    @POST("facility")
    @Headers("Accept: application/json")
    fun getPharmacies(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("type") facilityType: Int,
            @Query("page") page: Int,
            @Query("per_page") per_page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Pharmacy>>>>>

    @FormUrlEncoded
    @POST("facility")
    @Headers("Accept: application/json")
    fun getHospitals(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("type") facilityType: Int,
            @Query("page") page: Int,
            @Query("per_page") per_page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Hospital>>>>>

    @GET("facility/{pharmacy_id}")
    @Headers("Accept: application/json")
    fun getPharmacyDetails(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Path("pharmacy_id") pharmacyId: Long
    ): Observable<Response<BaseResponse<PharmacyDetails>>>

    @GET("facility/{hospital_id}")
    @Headers("Accept: application/json")
    fun getHospitalDetails(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Path("hospital_id") hospitalId: Long
    ): Observable<Response<BaseResponse<HospitalDetails>>>

    @FormUrlEncoded
    @POST("feedback")
    @Headers("Accept: application/json")
    fun submitRating(
            @Header("Authorization") access_token: String,
            @Field("facility_id") facilityId: Long,
            @Field("content") content: String,
            @Field("rate") rate: Float
    ): Observable<Response<BaseResponse<Any>>>

    @GET("medical")
    @Headers("Accept: application/json")
    fun getMedicalFileGroups(
            @Header("Authorization") access_token: String
    ): Observable<Response<BaseResponse<MutableList<MedicalFileGroup>>>>

    @GET("medical/{group_id}")
    @Headers("Accept: application/json")
    fun getMedicalFiles(
            @Header("Authorization") access_token: String,
            @Path("group_id") groupId: Long
    ): Observable<Response<BaseResponse<MutableList<MedicalFile>>>>

    @DELETE("medical/group/{group_id}")
    @Headers("Accept: application/json")
    fun deleteMedicalFileGroup(
            @Header("Authorization") access_token: String,
            @Path("group_id") groupId: Long
    ): Observable<Response<BaseResponse<Any>>>

    @DELETE("medical/file/{file_id}")
    @Headers("Accept: application/json")
    fun deleteMedicalFile(
            @Header("Authorization") access_token: String,
            @Path("file_id") fileId: Long
    ): Observable<Response<BaseResponse<Any>>>

    @Multipart
    @POST("medical")
    @Headers("Accept: application/json")
    fun uploadFiles(
            @Header("Authorization") access_token: String,
            @Part("id") groupId: RequestBody,
            @Part("name") groupName: RequestBody,
            @Part files: MutableList<MultipartBody.Part>
    ): Observable<Response<BaseResponse<MedicalFileGroup?>>>

    @GET("message")
    @Headers("Accept: application/json")
    fun getMessages(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Message>>>>>

    @FormUrlEncoded
    @POST("message/content")
    @Headers("Accept: application/json")
    fun getMessageReplies(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("receiver_id") receiverId: Long,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<MessageReply>>>>>

    @FormUrlEncoded
    @POST("message/by")
    @Headers("Accept: application/json")
    fun getShareMedicalFilesStatus(
            @Header("Authorization") access_token: String,
            @Field("receiver_id") receiverId: Long
    ): Observable<Response<BaseResponse<MutableList<Share>>>>

    @FormUrlEncoded
    @POST("message")
    @Headers("Accept: application/json")
    fun sendReply(
            @Header("Authorization") access_token: String,
            @Field("to") receiverId: Long,
            @Field("content") content: String
    ): Observable<Response<BaseResponse<SendReplyResponse>>>

    @FormUrlEncoded
    @POST("doctor/search")
    @Headers("Accept: application/json")
    fun searchForDoctors(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("name") keyWord: String,
            @Field("filter") filter: Int,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Doctor>>>>>

    @FormUrlEncoded
    @POST("message/share")
    @Headers("Accept: application/json")
    fun updateShareMedicalFilesStatus(
            @Header("Authorization") access_token: String,
            @Field("doctor_id") receiverId: Long,
            @Field("status") shareStatus: Int
    ): Observable<Response<BaseResponse<Any>>>

    @FormUrlEncoded
    @POST("message/patientGroup")
    @Headers("Accept: application/json")
    fun getPatientMedicalFileGroups(
            @Header("Authorization") access_token: String,
            @Field("patient_id") patientId: Long
    ): Observable<Response<BaseResponse<MutableList<MedicalFileGroup>>>>

    @FormUrlEncoded
    @POST("message/patientFiles/{group_id}")
    @Headers("Accept: application/json")
    fun getPatientMedicalFiles(
            @Header("Authorization") access_token: String,
            @Field("patient_id") patientId: Long,
            @Path("group_id") groupId: Long
    ): Observable<Response<BaseResponse<MutableList<MedicalFile>>>>

    @FormUrlEncoded
    @POST("poll")
    @Headers("Accept: application/json")
    fun getPolls(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("filter") filter: Int,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Poll>>>>>

    @POST("poll/vote/{option_id}")
    @Headers("Accept: application/json")
    fun submitVote(
            @Header("Authorization") access_token: String,
            @Path("option_id") optionId: Long
    ): Observable<Response<BaseResponse<Any>>>

    @FormUrlEncoded
    @POST("user/getSavedItem")
    @Headers("Accept: application/json")
    fun getSavedVideos(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("type") type: Int,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Video>>>>>

    @FormUrlEncoded
    @POST("user/getSavedItem")
    @Headers("Accept: application/json")
    fun getSavedArticles(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("type") type: Int,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>>

    @FormUrlEncoded
    @POST("user/getSavedItem")
    @Headers("Accept: application/json")
    fun getSavedStories(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("type") type: Int,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Story>>>>>

    @GET("user/getProfileUser")
    @Headers("Accept: application/json")
    fun getUserProfile(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String
    ): Observable<Response<BaseResponse<UserProfile>>>

    @FormUrlEncoded
    @POST("user/password/change")
    @Headers("Accept: application/json")
    fun changePassword(
            @Header("Authorization") access_token: String,
            @Field("old_password") oldPassword: String,
            @Field("new_password") newPassword: String
    ): Observable<Response<BaseResponse<Any>>>

    @Multipart
    @POST("user/picture/change")
    @Headers("Accept: application/json")
    fun changePicture(
            @Header("Authorization") access_token: String,
            @Part image: MultipartBody.Part
    ): Observable<Response<BaseResponse<String>>>

    @FormUrlEncoded
    @POST("user/email/change")
    @Headers("Accept: application/json")
    fun changeEmail(
            @Header("Authorization") access_token: String,
            @Field("email") newEmail: String,
            @Field("password") password: String
    ): Observable<Response<BaseResponse<Any>>>

    @POST("user/available")
    @Headers("Accept: application/json")
    fun changeAvailability(
            @Header("Authorization") access_token: String
    ): Observable<Response<BaseResponse<Any>>>

    @Multipart
    @POST("user/storeCertificate")
    @Headers("Accept: application/json")
    fun createAccreditation(
            @Header("Authorization") access_token: String,
            @Part("type") name: RequestBody,
            @Part("year") year: RequestBody,
            @Part("location") location: RequestBody,
            @Part("specialization") specialization: RequestBody,
            @Part image: MultipartBody.Part
    ): Observable<Response<BaseResponse<Accreditation>>>

    @Multipart
    @POST("user/updateCertificate/{accreditation_id}")
    @Headers("Accept: application/json")
    fun editAccreditation(
            @Header("Authorization") access_token: String,
            @Path("accreditation_id") accreditationId: Long,
            @Part("type") type: RequestBody,
            @Part("year") year: RequestBody,
            @Part("location") location: RequestBody,
            @Part("specialization") specialization: RequestBody
    ): Observable<Response<BaseResponse<Accreditation>>>

    @Multipart
    @POST("user/updateCertificate/{accreditation_id}")
    @Headers("Accept: application/json")
    fun editAccreditationWithImage(
            @Header("Authorization") access_token: String,
            @Path("accreditation_id") accreditationId: Long,
            @Part("type") type: RequestBody,
            @Part("year") year: RequestBody,
            @Part("location") location: RequestBody,
            @Part("specialization") specialization: RequestBody,
            @Part image: MultipartBody.Part
    ): Observable<Response<BaseResponse<Accreditation>>>

    @DELETE("user/deleteCertificate/{accreditation_id}")
    @Headers("Accept: application/json")
    fun deleteAccreditation(
            @Header("Authorization") access_token: String,
            @Path("accreditation_id") accreditationId: Long
    ): Observable<Response<BaseResponse<Any>>>

    @Multipart
    @POST("article")
    @Headers("Accept: application/json")
    fun postArticleWithVideo(
            @Header("Authorization") access_token: String,
            @Part("title_en") enTitle: RequestBody,
            @Part("content_en") enContent: RequestBody,
            @Part("title_ar") arTitle: RequestBody,
            @Part("content_ar") arContent: RequestBody,
            @Part("category") categoryId: RequestBody,
            @Part video: MultipartBody.Part
    ): Observable<Response<BaseResponse<Any>>>

    @Multipart
    @POST("article")
    @Headers("Accept: application/json")
    fun postArticleWithImages(
            @Header("Authorization") access_token: String,
            @Part("title_en") enTitle: RequestBody,
            @Part("content_en") enContent: RequestBody,
            @Part("title_ar") arTitle: RequestBody,
            @Part("content_ar") arContent: RequestBody,
            @Part("category") categoryId: RequestBody,
            @Part images: MutableList<MultipartBody.Part>
    ): Observable<Response<BaseResponse<Any>>>

    @Multipart
    @POST("story/store")
    @Headers("Accept: application/json")
    fun postStory(
            @Header("Authorization") access_token: String,
            @Part("title_en") enTitle: RequestBody,
            @Part("content_en") enContent: RequestBody,
            @Part("title_ar") arTitle: RequestBody,
            @Part("content_ar") arContent: RequestBody,
            @Part("category") categoryId: RequestBody,
            @Part image: MultipartBody.Part
    ): Observable<Response<BaseResponse<Any>>>

    @FormUrlEncoded
    @POST("user/search")
    @Headers("Accept: application/json")
    fun makeGeneralSearch(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("name") searchKey: String
    ): Observable<Response<BaseResponse<GeneralSearchResult>>>

    @FormUrlEncoded
    @POST("user/search")
    @Headers("Accept: application/json")
    fun makeSearchForFacilities(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("name") searchKey: String,
            @Field("filter") filter: Int,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Facility>>>>>

    @FormUrlEncoded
    @POST("user/search")
    @Headers("Accept: application/json")
    fun makeSearchForArticles(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("name") searchKey: String,
            @Field("filter") filter: Int,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>>

    @FormUrlEncoded
    @POST("user/search")
    @Headers("Accept: application/json")
    fun makeSearchForDoctors(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Field("name") searchKey: String,
            @Field("filter") filter: Int,
            @Query("page") page: Int
    ): Observable<Response<BaseResponse<Paginate<MutableList<Doctor>>>>>

    @POST("user/visitProfile/{user_id}")
    @Headers("Accept: application/json")
    fun visitProfile(
            @Header("Authorization") access_token: String,
            @Header("Accept-Language") language: String,
            @Path("user_id") userId: Long
    ): Observable<Response<BaseResponse<VisitProfile>>>

    @GET("self/")
    fun requestInstagramApi (
        @Query("access_token") authToken: String
    ): Observable<Response<InstagramResponse>>
}