package com.appagroup.sahty.data.http

import com.appagroup.sahty.utils.SavedTypes
import com.appagroup.sahty.entity.*
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import javax.inject.Inject

class RetrofitHelper @Inject constructor(private val apiInterface: ApiInterface) : HttpHelper {

    val TAG = "RetrofitHelper"


    override fun register(name: String, email: String, password: String, role: Int, fcm_token: String) =
            apiInterface.register(name, email, password, role.toString(), fcm_token)

    override fun registerWithAccreditation(name: RequestBody, email: RequestBody, password: RequestBody, role: RequestBody, fcm_token: RequestBody, type: RequestBody, specialty: RequestBody, location: RequestBody, year: RequestBody, image: MultipartBody.Part) =
            apiInterface.registerWithAccreditation(name, email, password, role, fcm_token, type, specialty, location, year, image)

    override fun login(email: String, password: String, fcm_token: String) =
            apiInterface.login(email, password, fcm_token)

    override fun socialLogin(name: String, email: String, fcm_token: String, social_token: String, provider: String) =
            apiInterface.socialLogin(name, email, fcm_token, social_token, provider)

    override fun setUserRole(access_token: String, role: Int): Observable<Response<BaseResponse<Any?>>> =
            apiInterface.setUserRole(access_token, role)

    override fun getSpecialties(name: String, language: String): Observable<Response<BaseResponse<MutableList<Specialty>>>> =
            apiInterface.getSpecialties(name, language)

    override fun getCategories(accessToken: String, language: String): Observable<Response<BaseResponse<MutableList<Category>>>> =
            apiInterface.getCategories(accessToken, language)

    override fun getInsights(access_token: String, language: String
//                             , longitude: Float, latitude: Float
    ): Observable<Response<BaseResponse<Insights>>> =
            apiInterface.getInsights(access_token, language
//                    , longitude, latitude
            )

    override fun getArticlesAndStories(access_token: String, language: String, categoryId: Long, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>> =
            apiInterface.getArticlesAndStories(access_token, language, categoryId, filter, page)

    override fun getArticleContent(access_token: String, articleId: Long): Observable<Response<BaseResponse<ArticleContent>>> =
            apiInterface.getArticleContent(access_token, articleId)

    override fun followUser(access_token: String, userId: Long): Observable<Response<BaseResponse<Any>>> =
            apiInterface.followUser(access_token, userId)

    override fun unfollowUser(access_token: String, userId: Long): Observable<Response<BaseResponse<Any>>> =
            apiInterface.unfollowUser(access_token, userId)

    override fun save(access_token: String, articleId: Long, type: Int): Observable<Response<BaseResponse<Any>>> =
            apiInterface.save(access_token, articleId, type)

    override fun unsave(access_token: String, articleId: Long, type: Int): Observable<Response<BaseResponse<Any>>> =
            apiInterface.unsave(access_token, articleId, type)

    override fun likeArticle(access_token: String, articleId: Long): Observable<Response<BaseResponse<Any>>> =
            apiInterface.likeArticle(access_token, articleId)

    override fun unlikeArticle(access_token: String, articleId: Long): Observable<Response<BaseResponse<Any>>> =
            apiInterface.unlikeArticle(access_token, articleId)

    override fun getVideos(access_token: String, language: String, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Video>>>>> =
            apiInterface.getVideos(access_token, language, page)

    override fun getPharmacies(access_token: String, language: String, facilityType: Int, page: Int, per_page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Pharmacy>>>>> =
            apiInterface.getPharmacies(access_token, language, facilityType, page, per_page)

    override fun getHospitals(access_token: String, language: String, facilityType: Int, page: Int, per_page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Hospital>>>>> =
            apiInterface.getHospitals(access_token, language, facilityType, page, per_page)

    override fun getPharmacyDetails(access_token: String, language: String, pharmacyId: Long): Observable<Response<BaseResponse<PharmacyDetails>>> =
            apiInterface.getPharmacyDetails(access_token, language, pharmacyId)

    override fun getHospitalDetails(access_token: String, language: String, hospitalId: Long): Observable<Response<BaseResponse<HospitalDetails>>> =
            apiInterface.getHospitalDetails(access_token, language, hospitalId)

    override fun submitRating(access_token: String, facilityId: Long, content: String, rate: Float): Observable<Response<BaseResponse<Any>>> =
            apiInterface.submitRating(access_token, facilityId, content, rate)

    override fun getMedicalFileGroups(access_token: String): Observable<Response<BaseResponse<MutableList<MedicalFileGroup>>>> =
            apiInterface.getMedicalFileGroups(access_token)

    override fun getMedicalFiles(access_token: String, groupId: Long): Observable<Response<BaseResponse<MutableList<MedicalFile>>>> =
            apiInterface.getMedicalFiles(access_token, groupId)

    override fun deleteMedicalFileGroup(access_token: String, groupId: Long): Observable<Response<BaseResponse<Any>>> =
            apiInterface.deleteMedicalFileGroup(access_token, groupId)

    override fun deleteMedicalFile(access_token: String, fileId: Long): Observable<Response<BaseResponse<Any>>> =
            apiInterface.deleteMedicalFile(access_token, fileId)

    override fun uploadFiles(access_token: String, groupId: RequestBody, groupName: RequestBody, files: MutableList<MultipartBody.Part>): Observable<Response<BaseResponse<MedicalFileGroup?>>> =
            apiInterface.uploadFiles(access_token, groupId, groupName, files)

    override fun getMessages(access_token: String, language: String, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Message>>>>> =
            apiInterface.getMessages(access_token, language, page)

    override fun getMessageReplies(access_token: String, language: String, receiverId: Long, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<MessageReply>>>>> =
            apiInterface.getMessageReplies(access_token, language, receiverId, page)

    override fun getShareMedicalFilesStatus(access_token: String, receiverId: Long): Observable<Response<BaseResponse<MutableList<Share>>>> =
            apiInterface.getShareMedicalFilesStatus(access_token, receiverId)

    override fun sendReply(access_token: String, receiverId: Long, content: String): Observable<Response<BaseResponse<SendReplyResponse>>> =
            apiInterface.sendReply(access_token, receiverId, content)

    override fun searchForDoctors(access_token: String, language: String, keyWord: String, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Doctor>>>>> =
            apiInterface.searchForDoctors(access_token, language, keyWord, filter, page)

    override fun updateShareMedicalFilesStatus(access_token: String, receiverId: Long, shareStatus: Int): Observable<Response<BaseResponse<Any>>> =
            apiInterface.updateShareMedicalFilesStatus(access_token, receiverId, shareStatus)

    override fun getPatientMedicalFileGroups(access_token: String, patientId: Long): Observable<Response<BaseResponse<MutableList<MedicalFileGroup>>>> =
            apiInterface.getPatientMedicalFileGroups(access_token, patientId)

    override fun getPatientMedicalFiles(access_token: String, patientId: Long, groupId: Long): Observable<Response<BaseResponse<MutableList<MedicalFile>>>> =
            apiInterface.getPatientMedicalFiles(access_token, patientId, groupId)

    override fun getPolls(access_token: String, language: String, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Poll>>>>> =
            apiInterface.getPolls(access_token, language, filter, page)

    override fun submitVote(access_token: String, optionId: Long): Observable<Response<BaseResponse<Any>>> =
            apiInterface.submitVote(access_token, optionId)

    override fun getSavedVideos(access_token: String, language: String, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Video>>>>> =
            apiInterface.getSavedVideos(access_token, language, SavedTypes.TYPE_VIDEOS, page)

    override fun getSavedArticles(access_token: String, language: String, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>> =
            apiInterface.getSavedArticles(access_token, language, SavedTypes.TYPE_ARTICLES, page)

    override fun getSavedStories(access_token: String, language: String, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Story>>>>> =
            apiInterface.getSavedStories(access_token, language, SavedTypes.TYPE_STORIES, page)

    override fun getUserProfile(access_token: String, language: String): Observable<Response<BaseResponse<UserProfile>>> =
            apiInterface.getUserProfile(access_token, language)


    override fun changePassword(access_token: String, oldPassword: String, newPassword: String): Observable<Response<BaseResponse<Any>>> =
            apiInterface.changePassword(access_token, oldPassword, newPassword)

    override fun changePicture(access_token: String, image: MultipartBody.Part): Observable<Response<BaseResponse<String>>> =
            apiInterface.changePicture(access_token, image)

    override fun changeEmail(access_token: String, newEmail: String, password: String): Observable<Response<BaseResponse<Any>>> =
            apiInterface.changeEmail(access_token, newEmail, password)

    override fun changeAvailability(access_token: String): Observable<Response<BaseResponse<Any>>> =
            apiInterface.changeAvailability(access_token)

    override fun createAccreditation(access_token: String, name: RequestBody, year: RequestBody, location: RequestBody, specialization: RequestBody, image: MultipartBody.Part): Observable<Response<BaseResponse<Accreditation>>> =
            apiInterface.createAccreditation(access_token, name, year, location, specialization, image)

    override fun editAccreditation(access_token: String, accreditationId: Long, type: RequestBody, year: RequestBody, location: RequestBody, specialization: RequestBody): Observable<Response<BaseResponse<Accreditation>>> =
            apiInterface.editAccreditation(access_token, accreditationId, type, year, location, specialization)

    override fun editAccreditationWithImage(access_token: String, accreditationId: Long, type: RequestBody, year: RequestBody, location: RequestBody, specialization: RequestBody, image: MultipartBody.Part): Observable<Response<BaseResponse<Accreditation>>> =
            apiInterface.editAccreditationWithImage(access_token, accreditationId, type, year, location, specialization, image)

    override fun deleteAccreditation(access_token: String, accreditationId: Long): Observable<Response<BaseResponse<Any>>> =
            apiInterface.deleteAccreditation(access_token, accreditationId)

    override fun postArticleWithVideo(access_token: String, enTitle: RequestBody, enContent: RequestBody, arTitle: RequestBody, arContent: RequestBody, categoryId: RequestBody, video: MultipartBody.Part): Observable<Response<BaseResponse<Any>>> =
            apiInterface.postArticleWithVideo(access_token, enTitle, enContent, arTitle, arContent, categoryId, video)

    override fun postArticleWithImages(access_token: String, enTitle: RequestBody, enContent: RequestBody, arTitle: RequestBody, arContent: RequestBody, categoryId: RequestBody, images: MutableList<MultipartBody.Part>): Observable<Response<BaseResponse<Any>>> =
            apiInterface.postArticleWithImages(access_token, enTitle, enContent, arTitle, arContent, categoryId, images)

    override fun postStory(access_token: String, enTitle: RequestBody, enContent: RequestBody, arTitle: RequestBody, arContent: RequestBody, categoryId: RequestBody, image: MultipartBody.Part): Observable<Response<BaseResponse<Any>>> =
            apiInterface.postStory(access_token, enTitle, enContent, arTitle, arContent, categoryId, image)

    override fun makeGeneralSearch(access_token: String, language: String, searchKey: String): Observable<Response<BaseResponse<GeneralSearchResult>>> =
            apiInterface.makeGeneralSearch(access_token, language, searchKey)

    override fun makeSearchForFacilities(access_token: String, language: String, searchKey: String, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Facility>>>>> =
            apiInterface.makeSearchForFacilities(access_token, language, searchKey, filter, page)

    override fun makeSearchForArticles(access_token: String, language: String, searchKey: String, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Article>>>>> =
            apiInterface.makeSearchForArticles(access_token, language, searchKey, filter, page)

    override fun makeSearchForDoctors(access_token: String, language: String, searchKey: String, filter: Int, page: Int): Observable<Response<BaseResponse<Paginate<MutableList<Doctor>>>>> =
            apiInterface.makeSearchForDoctors(access_token, language, searchKey, filter, page)

    override fun visitProfile(access_token: String, language: String, userId: Long): Observable<Response<BaseResponse<VisitProfile>>> =
            apiInterface.visitProfile(access_token, language, userId)
}