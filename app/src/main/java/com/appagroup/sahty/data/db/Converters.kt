package com.appagroup.sahty.data.db

import androidx.room.TypeConverter
import com.appagroup.sahty.entity.Media
import com.appagroup.sahty.entity.Writer
import com.facebook.common.internal.ImmutableList
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class Converters {

    @TypeConverter
    fun fromMediaToString(value: Media?): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun fromStringToMedia(value: String): Media? {
        val media = object : TypeToken<Media?>() {}.type
        return Gson().fromJson(value, media)
    }


    @TypeConverter
    fun fromWriterToString(value: Writer): String {
        return Gson().toJson(value)
    }

    @TypeConverter
    fun fromStringToWriter(value: String): Writer {
        val writer = object : TypeToken<Writer>() {}.type
        return Gson().fromJson(value, writer)
    }


    @TypeConverter
    fun fromStringToList(value: String): List<String>? {
        val listType = object : TypeToken<ArrayList<String>>() {}.type
        return Gson().fromJson<ArrayList<String>>(value, listType)
    }

    @TypeConverter
    fun fromArrayListToString(list: List<String>): String {
        val gson = Gson()
        return gson.toJson(list)
    }


    @TypeConverter
    fun fromStringToImmutableList(value: String): ImmutableList<String>? {
        val listType = object : TypeToken<ImmutableList<String>>() {}.type
        return Gson().fromJson<ImmutableList<String>>(value, listType)
    }

    @TypeConverter
    fun fromImmutableListToString(list: ImmutableList<String>): String {
        val gson = Gson()
        return gson.toJson(list)
    }


    @TypeConverter
    fun fromStringToImmutableListOfMedia(value: String): ImmutableList<Media>? {
        val listType = object : TypeToken<ImmutableList<Media>>() {}.type
        return Gson().fromJson<ImmutableList<Media>>(value, listType)
    }

    @TypeConverter
    fun fromImmutableListOfMediaToString(value: ImmutableList<Media>): String {
        return Gson().toJson(value)
    }


    @TypeConverter
    fun fromStringToListOfMedia(value: String): MutableList<Media> {
        val listType = object : TypeToken<MutableList<Media>>() {}.type
        return Gson().fromJson<MutableList<Media>>(value, listType)
    }

    @TypeConverter
    fun fromListOfMediaToString(value: MutableList<Media>): String {
        return Gson().toJson(value)
    }
}