package com.appagroup.sahty.data.db

import com.appagroup.sahty.entity.*
import io.reactivex.Completable
import io.reactivex.Observable
import javax.inject.Inject

class AppRepository @Inject constructor(private val appDao: AppDao) : DbHelper {

    /**** Insights ****/

    override fun getInsightsArticles(): Observable<MutableList<Article>> =
            appDao.getInsightsArticles().toObservable()

    override fun getInsightsVideo(): Observable<Video> =
            appDao.getInsightsVideo().toObservable()

    override fun getInsightsHospital(): Observable<Pharmacy> =
            appDao.getInsightsHospital().toObservable()

    override fun getInsightsPharmacy(): Observable<Hospital> =
            appDao.getInsightsPharmacy().toObservable()


    /**** Articles ****/

    override fun deleteArticles(category_id: Long): Observable<Any> =
            Completable.fromAction { appDao.deleteArticles(category_id) }.toObservable<Any>()!!

    override fun updateArticles(articles: MutableList<Article>): Observable<Any> =
            Completable.fromAction { appDao.updateArticles(articles) }.toObservable<Any>()!!

    override fun getArticles(category_id: Long): Observable<MutableList<Article>> =
            appDao.getArticles(category_id).toObservable()


    /**** Videos ****/

    override fun deleteVideos(): Observable<Any> =
            Completable.fromAction { appDao.deleteVideos() }.toObservable<Any>()!!

    override fun updateVideos(videos: MutableList<Video>): Observable<Any> =
            Completable.fromAction { appDao.updateVideos(videos) }.toObservable<Any>()!!

    override fun getVideos(): Observable<MutableList<Video>> =
            appDao.getVideos().toObservable()


    /**** Hospitals ****/

    override fun deleteHospitals(): Observable<Any> =
            Completable.fromAction { appDao.deleteHospitals() }.toObservable<Any>()!!

    override fun updateHospitals(hospitals: MutableList<Hospital>): Observable<Any> =
            Completable.fromAction { appDao.updateHospitals(hospitals) }.toObservable<Any>()!!

    override fun getHospitals(): Observable<MutableList<Hospital>> =
            appDao.getHospitals().toObservable()


    /**** Pharmacies ****/

    override fun deletePharmacies(): Observable<Any> =
            Completable.fromAction { appDao.deletePharmacies() }.toObservable<Any>()!!

    override fun updatePharmacies(pharmacies: MutableList<Pharmacy>): Observable<Any> =
            Completable.fromAction { appDao.updatePharmacies(pharmacies) }.toObservable<Any>()!!

    override fun getPharmacies(): Observable<MutableList<Pharmacy>> =
            appDao.getPharmacies().toObservable()


    /**** Categories ****/

    override fun deleteCategories(): Observable<Any> =
            Completable.fromAction { appDao.deleteCategories() }.toObservable<Any>()!!

    override fun updateCategories(categories: MutableList<Category>): Observable<Any> =
            Completable.fromAction { appDao.updateCategories(categories) }.toObservable<Any>()!!

    override fun getCategories(): Observable<MutableList<Category>> =
            appDao.getCategories().toObservable()
}